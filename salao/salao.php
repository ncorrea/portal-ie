<?php require_once("func.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Salão do Estudante</title>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<link href="errors.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function dataFormat(){
  if(document.getElementById("dateofbirth").value.length == 2)
    document.getElementById("dateofbirth").value += "/";
  if(document.getElementById("dateofbirth").value.length == 5)
    document.getElementById("dateofbirth").value += "/";
}

$().ready(function() {
	// validate signup form on keyup and submit
	$("#signupForm").validate({
		rules: {
			nome: { required: true, minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			ddd: {  required: true, minlength: 2
			},
			telefone: {  required: true, minlength: 8
			},
			comments{  required: true, minlength: 4
			}
		},
		messages: {
			nome: "Por favor, insira seu nome",
			email: "Por favor, insira um e-mail válido",
			ddd: "Por favor, insira o DDD",
			telefone: "Por favor, insira seu telefone",
			comments: "Por favor, insira seu comentário"
		}
	});
});
</script>

</head>

<body>
<form action="index.php" name="formulario" id="signupForm" method="post" enctype="multipart/form-data">
<?php
if (isset($_POST['enviar']) && $_POST['enviar'] == 'send') {
	$agencia = $_POST['agencia'];
	$estado = $stateFranchise[$agencia];
	
	$data['nome'] = $_POST["nome"];
	$data['sobrenome'] = '';
	$data['data_nascimento'] = $_POST["dateofbirth"];
	$data['email'] = $_POST["email"];
	$data['ddd'] = $_POST["ddd"];
	$data['telefone'] = $_POST["telefone"];
	$data['ddd2'] =  $_POST["ddd2"];
	$data['telefone2'] =  $_POST["telefone2"];
	$data['cidade'] = $cidadeEstado[$estado];
	$data['estado'] = $estado;		
	$data['agencia'] = $franquia[$agencia];
	$data['interesse'] =  '12'; //Ação Externa
	$data['produto'] = $_POST['product'];
	$data['travel_quantity'] = '0';
	$data['sexo'] = $_POST["sex"];
	$data['comentario'] = $_POST["comments"];
	$data['emailfranquia'] = $_POST["agencia"];
	$data['pais'] = $_POST["pais"];
	$data['paises'] = $_POST["paises"];

	 if(empty($data['nome'])) {
		 $retorno = '<span>Informe seu nome</span>';
	 }elseif (empty($data['email'])) {
		 $retorno = '<span>Informe seu e-mail</span>';
	 }elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
		 $retorno = '<span>Informe um e-mail válido</span>';
	 }elseif (empty($data['ddd'])) {
		 $retorno = '<span>Digite o ddd</span>';
	 }elseif (empty($data['telefone'])) {
		 $retorno = '<span>Digite o telefone</span>';
	 }elseif (empty($data['comentario'])) {
		 $retorno = '<span>Digite o comentário</span>';
     }elseif (empty($data['produto'])) {
		 $retorno = '<span>Selecione pelo menos um produto de interesse</span>';
	 }if (empty($retorno)) {
	 		$conteudo = "<br>Países de interesse: ";
			If (count($data['pais']) > 0){
				for ($i=0; $i < count($data['pais']); $i++){
					$prod = Trim($data['pais'][$i]);
					$conteudo.= $prod;
					if ($i != count($data['pais']) - 1){
						$conteudo.= ", " ;
					} 
				} 
				if ($data['paises'] != ""){
						$conteudo.= "<br>Outro: " . $data['paises'];
					}
			}
			$data['comentario'].= $conteudo;
			
			
	 		
	 		//Faz o envio do e-mail e insere no webservice
			enviaMail($data);
			$saida = WebServiceSpecta($data);
			
			//Faz o unset para limpar o formulário
			
			unset($data);
			echo "<span class=\"yes\">Formulário enviado com sucesso!</span>";
	 } else {
		 echo "<span class=\"no\">$retorno</span>";
	 }
}
?>
<p>
    <label for="nome">Nome Completo</label>
  	<input type="text" name="nome" id="nome" value="<?php echo $data['nome']; ?>">
</p>
<p>
    <label for="email">E-mail</label>
    <input type="email" name="email" id="email" value="<?php echo $data['email']; ?>">
  </p>
  <p>
    <label for="dateofbirth">Data de nascimento</label>
    <input name="dateofbirth" type="text" id="dateofbirth" onkeypress="dataFormat();" size="20">
  <p>
    <label for="telefone">Telefone Celular</label>
    <input name="ddd" type="text" id="ddd" size="5" maxlength="2" value="<?php echo $data['ddd']; ?>">
    <input name="telefone" type="text" id="telefone" maxlength="11" value="<?php echo $data['telefone']; ?>">
  </p>
  <p>
    <label for="telefone">Outro Telefone</label>
    <input name="ddd2" type="text" id="ddd2" size="5" maxlength="2" value="<?php echo $data['ddd2']; ?>">
    <input name="telefone2" type="text" id="telefone2" maxlength="11" value="<?php echo $data['telefone2']; ?>">
  </p>
    <p>Sexo:
  		<input type="radio" name="sex" value="1">Masculino
		<input type="radio" name="sex" value="2">Feminino
	</p>

 <label>Agência</label>
  
 <select id="agencia" name="agencia"><option value="" selected="selected">- Selecione -</option>
 
 <option value="bauru@ieintercambio.com.br">IE Bauru</option>
 
 <option value="campinas@ieintercambio.com.br">IE Campinas</option>
 
 <option value="higienopolis@ieintercambio.com.br">IE Higienópolis</option>
 
 <option value="moema@ieintercambio.com.br">IE Moema</option>
 
 <option value="morumbi@ieintercambio.com.br">IE Morumbi</option>
 
 <option value="paraiso@ieintercambio.com.br">IE Paraíso</option>
 
 <option value="piracicaba@ieintercambio.com.br">IE Piracicaba</option>
 
 <option value="santana@ieintercambio.com.br">IE Santana</option>
 
 <option value="santoamaro@ieintercambio.com.br">IE Santo Amaro</option>
 
 <option value="sbc@ieintercambio.com.br">IE São Bernardo do Campo</option>
 
 <option value="saocarlos@ieintercambio.com.br">IE São Carlos</option>
 
 <option value="sjc@ieintercambio.com.br">IE São José dos Campos</option>
 
 <option value="sorocaba@ieintercambio.com.br">IE Sorocaba</option>
 
 <option value="taubate@ieintercambio.com.br">IE Taubaté</option>
 
 </select>
 
  <p>Produtos de Interesse:
    <label>
      <input type="checkbox" name="product[]" value="2" id="Produtos">
    Estudo no Exterior</label>
    <label>
      <input type="checkbox" name="product[]" value="5" id="Produtos">
      High School</label>
      <label>
      <input type="checkbox" name="product[]" value="1" id="Produtos">
      Au Pair</label>
    <label>
      <input type="checkbox" name="product[]" value="4" id="Produtos">
      Trainee</label>
    <label>
      <input type="checkbox" name="product[]" value="3" id="Produtos">
      Work Experience</label>
    <label>
      <input type="checkbox" name="product[]" value="14" id="Produtos">
      Intercâmbio de férias Teen</label>
    <label>
      <input type="checkbox" name="product[]" value="6" id="Produtos">
      Trip Experience</label>
    <label>
      <input type="checkbox" name="product[]" value="11" id="Produtos">
      Bus Pass</label>
    <label>
      <input type="checkbox" name="product[]" value="15" id="Produtos">
      Trabalho Voluntário</label>
  </p>
  
 <p>Países de Preferência:
    <label>
      <input type="checkbox" name="pais[]" value="África do Sul" id="pais">
   África do Sul</label>
   <label>
      <input type="checkbox" name="pais[]" value="Canadá" id="pais">
  Canadá</label>
   <label>
      <input type="checkbox" name="pais[]" value="Estados Unidos" id="pais">
   Estados Unidos</label>
   <label>
      <input type="checkbox" name="pais[]" value="Inglaterra" id="pais">
   Inglaterra</label>
   <label>
      <input type="checkbox" name="pais[]" value="Irlanda" id="pais">
  Irlanda</label>
     <label>
      <input type="checkbox" name="pais[]" value="Nova Zelândia" id="pais">
  Nova Zelândia</label>
     <label>
      <input type="checkbox" name="pais[]" value="Austrália" id="pais">
  Austrália</label>
     <label>
      <input type="checkbox" name="pais[]" value="Malta" id="pais">
  Malta</label>
  <label>
      Outro: <input type="text" name="paises" id="paises" value="<?php echo $data['paises']; ?>"></label>
  </p>

  
  <p>
    <label for="comments">Comentários<br>
      Descreva aqui as preferências de duração do intercâmbio e acomodação<br>
    </label>
    <textarea name="comments" id="comments" cols="45" rows="5"><?php echo $data['comentario']; ?></textarea>
  </p>
  <p>
  <input type="hidden" name="enviar" value="send" />
    <input type="submit" name="Enviar" value="Enviar" />
  </p>
</form>
</body>
</html>
