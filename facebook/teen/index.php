<?php require_once("func.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="overflow: hidden">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Intercâmbio de Férias Teen</title>

<style>
#tudo{width:802px; text-align:center;}

#fundo{width:802px;position:absolute;z-index:1;}

#topo{position:absolute;z-index:1000;}

#video{position:absolute;margin:315px 0 0 0;z-index:1000;}

#conheca{position:absolute;margin:265px 0 0 10px;z-index:1000;}

#fotos{position:absolute;margin:322px 0 0 480px;z-index:1000}

#escolha{position:absolute;margin:660px 0 0 10px;z-index:1000;}

#form{position:absolute; margin:300px 0 0 480px; z-index:1000; text-align:left;}

#fundoform{position:absolute; margin:277px 0 0 465px; z-index:999;}

#selo{position:absolute; margin:800px 0 0 280px; z-index:1001;}


#titulopct01{position:absolute;margin:890px 0 0 10px;z-index:1001;}

#titulopct02{position:absolute;margin:890px 0 0 420px;z-index:1001;}

#titulopct03{position:absolute;margin:1200px 0 0 10px;z-index:1001;}

#titulopct04{position:absolute;margin:1200px 0 0 420px;z-index:1001;}


#textopct01{position:absolute;margin:915px 0 0 10px;width:370px;text-align:left;z-index:1001;}

#textopct02{position:absolute;margin:915px 0 0 420px;width:370px;text-align:left;z-index:1001;}

#textopct03{position:absolute;margin:1225px 0 0 10px;width:370px;text-align:left;z-index:1001;}

#textopct04{position:absolute;margin:1225px 0 0 420px;width:370px;text-align:left;z-index:1001;}


#investimento01{position:absolute; margin:970px 0 0 10px;z-index:1001;}

#investimento02{position:absolute; margin:970px 0 0 420px;z-index:1001;}

#investimento03{position:absolute; margin:1280px 0 0 10px;z-index:1001;}

#investimento04{position:absolute; margin:1280px 0 0 420px;z-index:1001;}


#data01{position:absolute; margin:950px 0 0 10px; z-index:1001; text-align:left;}

#data02{position:absolute; margin:950px 0 0 420px; z-index:1001; text-align:left;}

#data03{position:absolute; margin:1260px 0 0 10px; z-index:1001; text-align:left;}

#data04{position:absolute; margin:1260px 0 0 420px; z-index:1001; text-align:left;}


#pct01{position:absolute;margin:730px 0 0 0;z-index:1000;}

#pct02{position:absolute;margin:730px 0 0 400px;z-index:1000;}

#pct03{position:absolute;margin:1040px 0 0 0;z-index:1000;}

#pct04{position:absolute;margin:1040px 0 0 400px;z-index:1000;}

#asterisco{position:absolute; margin:1395px 0 0 10px; width:790px; text-align:left; z-index:1000;}


@font-face {font-family: "androgyne";src: url("http://www.ieintercambio.com.br/facebook/teen/androgyne.otf");}

@font-face {font-family: "museo500";src: url("http://www.ieintercambio.com.br/facebook/teen/museo500.otf");}

@font-face {font-family: "museo300";src: url("http://www.ieintercambio.com.br/facebook/teen/museo300.otf");}

@font-face {font-family: "museo900";src: url("http://www.ieintercambio.com.br/facebook/teen/museo900.otf");}

.titulo{font-family:androgyne;font-size:34px;color:#d8127a;}

.titulo2{font-family:museo500;font-size:22px;color:#d8127a;}

.texto{font-family:museo300;font-size:14px;}

.textoasterisco{font-family:museo300;font-size:12px;}

.textoform{font-family:museo500;font-size:14px; color:#FFF;}

.avista{font-family:museo300;font-size:14px;color:#51b848;}

.parcela{font-family:museo900;font-size:24px;color:#51b848;}

.data{font-family:museo300;font-size:14px;color:#d8127a;}

</style>

<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<link href="errors.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

$().ready(function() {
	// validate signup form on keyup and submit
	$("#signupForm").validate({
		rules: {
			nome: { required: true, minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			comments:{  required: true, minlength: 4
			}
		},
		messages: {
			nome: "Por favor, insira seu nome",
			email: "Por favor, insira um e-mail válido",
			ddd: "Por favor, insira o DDD",
			telefone: "Por favor, insira seu telefone",
			comments: "Por favor, insira seu comentário"
		}
	});
});
</script>

</head>

<body style="overflow: hidden">

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3881937-1']);
  _gaq.push(['_setDomainName', 'ieintercambio.com.br']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<div id="tudo">

<div id="topo"><img src="http://www.ieintercambio.com.br/facebook/teen/topo.png" /></div>

<div id="conheca" class="titulo">Conheça o Programa</div>

<div id="video"><iframe width="466" height="335" src="http://www.youtube.com/embed/Hel8e6X20-w" frameborder="0" allowfullscreen></iframe></div>


<div id="form">
<br />
<form action="http://www.ieintercambio.com.br/facebook/teen/index.php" name="formulario" id="signupForm" method="post" enctype="multipart/form-data">
<?php
if (isset($_POST['enviar']) && $_POST['enviar'] == 'send') {
	$agencia = $_POST['agencia'];
	$estado = $stateFranchise[$agencia];
	
	$data['nome'] = $_POST["nome"];
	$data['sobrenome'] = '';
	$data['data_nascimento'] = '';
	$data['email'] = $_POST["email"];
	$data['ddd'] = $_POST["ddd"];
	$data['telefone'] = $_POST["telefone"];
	$data['ddd2'] = '';
	$data['telefone2'] = '';		
	$data['cidade'] = $cidadeEstado[$estado];
	$data['estado'] = $estado;		
	$data['agencia'] = $franquia[$agencia];
	$data['interesse'] =  '6'; //Facebook
	$data['produto'] = array('2');
	$data['travel_quantity'] = '0';
	$data['sexo'] = '1';
	$data['comentario'] = $_POST["comments"];
	$data['emailfranquia'] = $_POST["agencia"];

	 if(empty($data['nome'])) {
		 $retorno = '<span style="color:#f00000"><br>Informe seu nome</span>';
	 }elseif (empty($data['email'])) {
		 $retorno = '<span style="color:#f00000"><br>Informe seu e-mail</span>';
	 }elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
		 $retorno = '<span style="color:#f00000"><br>Informe um e-mail válido</span>';
	 }elseif (empty($data['ddd'])) {
		 $retorno = '<span style="color:#f00000"><br>Digite o ddd</span>';
	 }elseif (empty($data['telefone'])) {
		 $retorno = '<span style="color:#f00000"><br>Digite o telefone</span>';
	 }elseif (empty($data['comentario'])) {
		 $retorno = '<span style="color:#f00000"><br>Digite o comentário</span>';
	 }if (empty($retorno)) {
	 		//Faz o envio do e-mail e insere no webservice
			$data['comentario'] ="Produto de interesse: Intercambio de ferias Teen<br/> " . $_POST["comments"];
			enviaMail($data);
			$saida = WebServiceSpecta($data);
			//Faz o unset para limpar o formulário
			unset($data);
			//echo $saida;
			echo "<span class=\"textoform\">Formulário enviado com sucesso!</span>";
	 } else {
		 echo "<span class=\"textoform\">$retorno</span>";
	 }
}
?>
<p>
    <label for="nome"><span class="textoform">Nome:</span></label>
  	<input type="text" name="nome" id="nome" size="35"  value="<?php echo $data['nome']; ?>">
</p>
<p>
    <label for="email"><span class="textoform">E-mail:</span></label>
    <input type="email" name="email" id="email" size="35" value="<?php echo $data['email']; ?>">
</p>
<p>
    <label for="telefone"><span class="textoform">Telefone:</span></label>
    <input name="ddd" type="text" id="ddd" size="5" maxlength="2" value="<?php echo $data['ddd']; ?>">
    <input name="telefone" type="text" id="telefone" maxlength="11" value="<?php echo $data['telefone']; ?>">
</p>
  
<p>
  <label><span class="textoform">Agência de preferência:</span><br /></label>  
 <select id="agencia" name="agencia"><option value="" selected="selected">- Selecione -</option>
 <option value="aracaju@ieintercambio.com.br">IE SE Aracaju</option>
<option value="barra@ieintercambio.com.br">IE RJ Barra</option>
<option value="bauru@ieintercambio.com.br">IE SP Bauru</option>
<option value="bh@ieintercambio.com.br">IE MG Belo Horizonte</option>
<option value="brasilia@ieintercambio.com.br">IE DF Brasília</option>
<option value="cabofrio@ieintercambio.com.br">IE RJ Cabo Frio</option>
<option value="campinas@ieintercambio.com.br">IE SP Campinas</option>
<option value="campogrande@ieintercambio.com.br">IE MS Campo Grande</option>
<option value="campos@ieintercambio.com.br">IE RJ Campos</option>
<option value="caxiasdosul@ieintercambio.com.br">IE RS Caxias do Sul</option>
<option value="florianopolis@ieintercambio.com.br">IE SC Florianópolis</option>
<option value="fortaleza@ieintercambio.com.br">IE CE Fortaleza</option>
<option value="goiania@ieintercambio.com.br">IE GO Goiânia</option>
<option value="higienopolis@ieintercambio.com.br">IE SP Higienópolis</option>
<option value="ipanema@ieintercambio.com.br">IE RJ Ipanema</option>
<option value="joaopessoa@ieintercambio.com.br">IE PB João Pessoa</option>
<option value="juizdefora@ieintercambio.com.br">IE MG Juiz de Fora</option>
<option value="macae@ieintercambio.com.br">IE RJ Macaé</option>
<option value="manaus@ieintercambio.com.br">IE AM Manaus</option>
<option value="morumbi@ieintercambio.com.br">IE SP Morumbi</option>
<option value="natal@ieintercambio.com.br">IE Natal</option>
<option value="niteroi@ieintercambio.com.br">IE RJ Niterói</option>
<option value="paraiso@ieintercambio.com.br">IE SP Paraíso</option>
<option value="petropolis@ieintercambio.com.br">IE RJ Petrópolis</option>
<option value="piracicaba@ieintercambio.com.br">IE SP Piracicaba</option>
<option value="portoalegre@ieintercambio.com.br">IE RS Porto Alegre</option>
<option value="recife@ieintercambio.com.br">IE PE Recife</option>
<option value="rjcentro@ieintercambio.com.br">IE RJ Centro</option>
<option value="salvador@ieintercambio.com.br">IE BA Salvador</option>
<option value="santana@ieintercambio.com.br">IE SP Santana</option>
<option value="saocarlos@ieintercambio.com.br">IE SP São Carlos</option>
<option value="saoluis@ieintercambio.com.br">IE MA São Luís</option>
<option value="sbc@ieintercambio.com.br">IE SP São Bernardo do Campo</option>
<option value="sjc@ieintercambio.com.br">IE SP São José dos Campos</option>
<option value="sorocaba@ieintercambio.com.br">IE SP Sorocaba</option>
<option value="taubate@ieintercambio.com.br">IE SP Taubaté</option>
<option value="uberlandia@ieintercambio.com.br">IE MG Uberlândia</option>
<option value="varginha@ieintercambio.com.br">IE MG Varginha</option>
<option value="vix@ieintercambio.com.br">IE ES Vitória</option>
 </select>
</p>
<p>
    <label for="comments"><span class="textoform">Comentários<br></span>
    </label>
    <textarea name="comments" id="comments" cols="35" rows="3"><?php echo $data['comentario']; ?></textarea>
</p>
<p>
  <input type="hidden" name="enviar" value="send" />
    <input type="submit" name="Enviar" value="Enviar" />
</p>
</form>
</div>

<div id="fundoform"><img src="http://www.ieintercambio.com.br/facebook/teen/form.png" /></div>


<div id="escolha" class="titulo">Escolha seu intercâmbio</div>

<div id="selo"><img src="http://www.ieintercambio.com.br/facebook/teen/selo.png" /></div>
<div id="titulopct01"><span class="titulo2">Discover California</span></div>
<div id="textopct01"><span class="texto">Aulas de inglês e no tempo livre praia, atividades e passeios por lugares famosos como Hollywood, Beverly Hills e Disney!</span></div>
<div id="investimento01"><span class="avista"><br />A partir de 10x R$ 846,06*</span></div>
<div id="data01"><span class="data"><br />5 jan até 25 jan de 2014</span></div>


<div id="titulopct02"><span class="titulo2">Summer Adventures - Brisbane</span></div>
<div id="textopct02"><span class="texto">Aprendizado, passeios, trocas culturais e muita aventura! Atividades como escalada, caiaque, visita ao Santuário dos Koalas e muito mais!</span></div>
<div id="investimento02"><span class="avista"><br />A partir de 10x R$ 480,34*</span></div>
<div id="data02"><span class="data"><br />Entre 6 jan até 22 fev de 2014</span></div>


<div id="titulopct03"><span class="titulo2">Winter Adventures Vancouver</span></div>
<div id="textopct03"><span class="texto">Estudantes poderão fazer ski, snowboard, participar de festivais de inverno, tour e muito mais em uma das cidades mais seguras do mundo.</span></div>
<div id="investimento03"><span class="avista"><br />A partir de 10x R$ 575,75*</span></div>
<div id="data03"><span class="data"><br />Entre 30 dez a 14 fev de 2014</span></div>


<div id="titulopct04"><span class="titulo2">Winter Adventures Bournemouth</span></div>
<div id="textopct04"><span class="texto">Aulas de inglês animadas e divertidas, além de excursões e atividades de lazer, de cultura e esportes.</span></div>
<div id="investimento04"><span class="avista"><br />A partir de 10x R$ 586,18*</span></div>
<div id="data04"><span class="data"><br />06 de Janeiro até 01 de Fevereiro</span></div>


<div id="pct01"><img src="http://www.ieintercambio.com.br/facebook/teen/pct01.png" usemap="#Map" border="0">
  <map name="Map" id="Map">
    <area title="" class='iframe' shape="rect" coords="294,262,393,293" href="http://www.ieintercambio.com.br/facebook/teen/form.php" />
  </map>
</div>

<div id="pct02"><img src="http://www.ieintercambio.com.br/facebook/teen/pct02.png" usemap="#Map2" border="0">
  <map name="Map2" id="Map2">
    <area title="" class='iframe' shape="rect" coords="306,264,401,292" href="http://www.ieintercambio.com.br/facebook/teen/form.php" />
  </map>
</div>

<div id="pct03"><img src="http://www.ieintercambio.com.br/facebook/teen/pct03.png" usemap="#Map3" border="0">
  <map name="Map3" id="Map3">
    <area title="" class='iframe' shape="rect" coords="294,263,393,291" href="http://www.ieintercambio.com.br/facebook/teen/form.php" />
  </map>
</div>

<div id="pct04"><img src="http://www.ieintercambio.com.br/facebook/teen/pct04.png" usemap="#Map4" border="0">
  <map name="Map4" id="Map4">
    <area title="" class='iframe' shape="rect" coords="305,269,399,298" href="http://www.ieintercambio.com.br/facebook/teen/form.php" />
  </map>
</div>1

<div id="asterisco">
<span class="textoasterisco">
Valores em reais calculados sobre o câmbio turismo do dia 26/07/13, quando USD = 2,39, CAD = 2,35, EUR = 3,19, AUS = 2,23, GBP = 3,71. Haverá conversão no ato da compra. Serviço de aconselhamento no valor de USD 195 não incluido e deve ser pago à vista. Sujeito à variação cambial sem aviso prévio.
</span>
</div>

<div id="fundo" > <img src="http://www.ieintercambio.com.br/facebook/teen/fundo.jpg" /> </div>

</div>


<div id="fb-root" style="width:1px;height:1px;position:absolute;"></div> 
<script type="text/javascript"> 
  window.fbAsyncInit = function() {
    FB.init({appId: 'yourappidhere', status: true, cookie: true, xfbml: true});
    FB.Canvas.setSize(true,100);
  };
  (function() {
    var e = document.createElement('script'); e.async = true;
    e.src = document.location.protocol +
      '//connect.facebook.net/en_US/all.js';
    document.getElementById('fb-root').appendChild(e);
  }());
</script>


</body>
</html>
