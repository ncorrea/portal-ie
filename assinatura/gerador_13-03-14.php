<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Gerador de Assinatura IE</title>
<style type="text/css">
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
}
.titulo {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #09C;
}
.texto {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0;
}
.conteudo {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #C30;
}
</style>
</head>

<body>
<h1 class="titulo"><strong>GERADOR DE ASSINATURA IE</strong></h1>
<h6 class="texto">Após gerar a assinatura, selecionar tudo, copiar e colar na área de assinatura conforme manual: <a href="https://sites.google.com/a/ieintercambio.com.br/ferramentas-online/manuais/adequar-para-assinatura-padrao-ie">https://sites.google.com/a/ieintercambio.com.br/ferramentas-online/manuais/adequar-para-assinatura-padrao-ie</a></h6>
<form action="assinatura.php" method="post">
  <p class="conteudo">Nome do funcionário: 
<input name="nome" type="text" size="40" maxlength="100" />&nbsp;</p>
<p class="conteudo">Função: 
    <select name="funcao" id="funcao">
      <option value="">Escolha sua função...</option>
      <option value="Consultor">Consultor</option>
      <option value="Consultora">Consultora</option>
      <option value="Gerente">Gerente</option>
      <option value="Franqueado">Franqueado</option>
    </select>
</p>
<p class="conteudo">Localidade:

    <select name="agencia" title="Agencia preferencial de atendimento">
      <option value="">Escolha o local...</option>																				
      <option value="Manaus">IE AM - Manaus</option>
      <option value="Maceió">IE AL - Maceió</option>
      <option value="Salvador">IE BA - Salvador</option>
      <option value="Fortaleza">IE CE - Fortaleza</option>
      <option value="Brasília">IE DF - Brasília</option>
      <option value="Vitória">IE ES - Vitória</option>
      <option value="Linhares">IE ES - Linhares</option>
      <option value="Goiânia">IE GO - Goiânia</option>
      <option value="São Luís">IE MA - São Luís</option>
      <option value="Belo Horizonte">IE MG - Belo Horizonte</option>
      <option value="Itaúna">IE MG - Itaúna</option>
      <option value="Juiz de Fora">IE MG - Juiz de Fora</option>
      <option value="Uberlândia">IE MG - Uberlândia</option>
      <option value="Vargina">IE MG - Varginha</option>
      <option value="Campo Grande">IE MS - Campo Grande</option>
      <option value="Bélem">IE PA - Belém</option>
      <option value="João Pessoa">IE PB - João Pessoa</option>
      <option value="Recife">IE PE - Recife</option>
      <option value="Curitiba">IE PR - Curitiba</option>
      <option value="Barra">IE RJ - Barra</option>
      <option value="Cabo Frio">IE RJ - Cabo Frio</option>
      <option value="Campos">IE RJ - Campos</option>
      <option value="RJ Centro">IE RJ - Centro</option>
      <option value="Ipanema">IE RJ - Ipanema</option>
      <option value="Macaé">IE RJ - Macaé</option>
      <option value="Niterói">IE RJ - Niterói</option>
      <option value="Petrópolis">IE RJ - Petropolis</option>
      <option value="Natal">IE RN - Natal</option>
      <option value="Caxias do Sul">IE RS - Caxias do Sul</option>
      <option value="Passo Fundo">IE RS - Passo Fundo</option>
      <option value="Porto Alegre">IE RS - Porto Alegre</option>
      <option value="Florianópolis">IE SC - Florianópolis</option>
      <option value="Aracaju">IE SE - Aracaju</option>
      <option value="Bauru">IE SP - Bauru</option>
      <option value="Brooklin">IE SP - Brooklin</option>
      <option value="Campinas">IE SP - Campinas</option>
      <option value="Higienópolis">IE SP - Higienópolis</option>
      <option value="Jardins">IE SP - Jardins</option>
      <option value="Moema">IE SP - Moema</option>   
      <option value="Morumbi">IE SP - Morumbi</option>
      <option value="Paraíso">IE SP - Paraíso</option>
      <option value="Piracicaba">IE SP - Piracicaba</option>
      <option value="Santana">IE SP - Santana</option>
      <option value="Santo Amaro">IE SP - Santo Amaro</option>
      <option value="São Bernardo do Campo">IE SP - São Bernardo do Campo</option>
      <option value="São Carlos">IE SP - São Carlos</option>
      <option value="São José dos Campos">IE SP - São José dos Campos</option>
      <option value="Sorocaba">IE SP - Sorocaba</option>
      <option value="Taubaté">IE SP - Taubaté</option>
       </select>
  <br />						
</p>
<p class="conteudo">Telefone da agência:
  <input name="ddd" type="text" id="ddd" size="4" maxlength="2" />
  <input name="telefone" type="text" id="telefone" size="14" maxlength="10" />
  ex: 3038-3900</p>
  <input type="submit" name="Gerar" id="Gerar" value="Gerar" />
</form>
</body>
</html>