<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Gerador de Assinatura IE</title>
<style type="text/css">
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
}
.titulo {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #09C;
}
.texto {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #0;
}
.conteudo {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #C30;
}
</style>
</head>

<body>
<h1 class="titulo"><strong>GERADOR DE ASSINATURA IE</strong></h1>
<h6 class="texto">Após gerar a assinatura, selecionar tudo, copiar e colar na área de assinatura conforme manual: <a href="https://sites.google.com/a/ieintercambio.com.br/ferramentas-online/manuais/adequar-para-assinatura-padrao-ie">https://sites.google.com/a/ieintercambio.com.br/ferramentas-online/manuais/adequar-para-assinatura-padrao-ie</a></h6>
<form action="assinatura.php" method="post">
  <p class="conteudo">Nome do funcionário: 
<input name="nome" type="text" size="40" maxlength="100" />&nbsp;</p>
<p class="conteudo">Função: 
    <input name="funcao" type="text" id="funcao" size="40" maxlength="100" />
</p>
<p class="conteudo">Localidade:

    <select name="agencia" title="Agencia">
      <option value="Matriz">IE Matriz</option>
      <option value="Head Office">IE Head Office</option>
    </select>
  <br />						
</p>
<p class="conteudo">Telefone:
  <input name="ddd" type="text" id="ddd" size="4" maxlength="2" />
  <input name="telefone" type="text" id="telefone" size="14" maxlength="10" />
  ex: 3038-3900</p>
  <input type="submit" name="Gerar" id="Gerar" value="Gerar" />
</form>
</body>
</html>