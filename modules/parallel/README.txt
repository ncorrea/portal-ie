// $Id: README.txt,v 1.1.2.2 2011/02/07 11:21:03 wimleers Exp $

Please visit the Parallel project page for all details:
http://drupal.org/project/parallel

The Paralle module is now obsolete. Please upgrade to the CDN module. An
upgrade path is provided. The upgrade path is documented in the CDN module's
UPGRADE.txt file.
