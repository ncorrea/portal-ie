<?php
/**
 * @file
 * Install, update and uninstall functions for the closure_compiler module.
 */

/**
 * Update to version 7.x-2.x. This modifies the variables used, as the admin
 * form has changed.
 */
function closure_compiler_update_7200() {
  module_load_include('inc', 'closure_compiler', 'closure_compiler.defines');

  variable_del('closure_compiler_service');

  variable_set('google_closure_compiler_process_method', variable_get('closure_compiler_process_method', CLOSURE_COMPILER_SEND_CONTENTS));
  variable_del('closure_compiler_process_method');

  variable_set('google_closure_compiler_compilation_level', variable_get('closure_compiler_compilation_level', CLOSURE_COMPILER_SEND_CONTENTS));
  variable_del('closure_compiler_compilation_level');
}

/**
 * Implements hook_requirements().
 */
function closure_compiler_requirements($phase) {
  $requirements = array();
  $t = get_t();

  // Check the status of the closure_compiler.
  if ($phase == 'runtime') {
    module_load_include('inc', 'closure_compiler', 'closure_compiler.functions');

    $errors = array();

    // JS aggregation status.
    if (!variable_get('preprocess_js', 0)) {
      $errors[] = $t('JavaScript aggregation must be enabled for the Closure Compiler module to operate');
    }

    // Sub-modules
    $enabled_services = variable_get('closure_compiler_service', array());
    foreach ($enabled_services as $module) {
      if (module_load_include('inc', $module, $module . '.status')) {
        $function = '_' . $module . '_requirements_status';
        if (function_exists($function)) {
          $errors += call_user_func($function);
        }
      }
    }

    // Number of (un-)optimized JS files found.
    $js_files = closure_compiler_get_js_files();
    $status = $t('Found @optimized optimized JS files and @unoptimized unoptimized JS files', array('@optimized' => count($js_files['optimized']), '@unoptimized' => count($js_files['unoptimized'])));
    if (!empty($errors)) {
      $errors[] = $status;
    }

    $requirements['closure_compiler'] = array(
      'title' => $t('Closure Compiler'),
      'value' => !empty($errors) ? theme('item_list', array('items' => $errors)) : $status,
      'severity' => !empty($errors) ? REQUIREMENT_ERROR : REQUIREMENT_OK,
      'description' => $t('A more in-depth status report is available at !link', array('!link' => l('/admin/reports/closure_compiler', 'admin/reports/closure_compiler'))),
    );
  }

  return $requirements;
}
