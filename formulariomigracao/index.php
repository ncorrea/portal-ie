<?php require_once("func.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fomulário de Migração</title>
</head>
<style>

#centro{width:700px;background-color:#FFF;height:800px;padding:20px;text-align:left;}
#background{background-color:#E9E9E9;}
.titulo{font-family:Arial, Helvetica, sans-serif; font-size:36px;}
.texto{font-family:Arial, Helvetica, sans-serif; font-size:14px;}

</style>

<body id="background">
<center>

<div id="centro">

<span class="titulo">Formulário de Migração</span>
<br /><br />

<script type="text/javascript" src="js/form-validation.js"></script>
<form action="http://www.ieintercambio.com.br/formulariomigracao/index.php" name="formulario" id="contactForm" method="post" enctype="multipart/form-data">
	<?php
        if (isset($_POST['enviar']) && $_POST['enviar'] == 'send') {
            $agencia = $_POST['agencia'];
            $estado = $stateFranchise[$agencia];
            
            $data['nome'] = $_POST["nome"];
			$data['nome2'] = $_POST["nome2"];
            $data['sobrenome'] = '';
            $data['data_nascimento'] = '';
            $data['email'] = $_POST["email"];
            $data['ddd'] = $_POST["ddd"];
            $data['telefone'] = $_POST["telefone"];
            $data['ddd2'] = '';
            $data['telefone2'] = '';		
            $data['cidade'] = $cidadeEstado[$estado];
            $data['estado'] = $estado;
            $data['agencia'] = $franquia[$agencia];
			$data['agencia2'] = $franquia2[$agencia2];
            $data['interesse'] =  '6'; //Facebook
            $data['produto'] = array('2');
            $data['travel_quantity'] = '0';
            $data['sexo'] = '1';
            $data['comentario'] = $_POST["comments"];
            $data['emailfranquia'] = $_POST["agencia"];
			$data['emailfranquia2'] = $_POST["agencia2"];
        
             if(empty($data['nome'])) {
                 $retorno = '<span style="color:#f00000"><br>Informe seu nome</span>';
             }elseif (empty($data['email'])) {
                 $retorno = '<span style="color:#f00000"><br>Informe e-mail cadastrado</span>';
             }elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                 $retorno = '<span style="color:#f00000"><br>Informe um e-mail válido</span>';
             }elseif (empty($data['ddd'])) {
                 $retorno = '<span style="color:#f00000"><br>Digite o ddd</span>';
             }elseif (empty($data['telefone'])) {
                 $retorno = '<span style="color:#f00000"><br>Digite o telefone</span>';
             }elseif (empty($data['comentario'])) {
                 $retorno = '<span style="color:#f00000"><br>Digite o comentário</span>';
			 }elseif (empty($data['agencia'])) {
                 $retorno = '<span style="color:#f00000"><br>Selecione a agência</span>';
			 }elseif (empty($data['nome2'])) {
                 $retorno = '<span style="color:#f00000"><br>Digite seu nome</span>';
			 }if (empty($retorno)) {
                    //Faz o envio do e-mail
                    $data['comentario'] = $_POST["comments"];
                    enviaMail($data);
                    //Faz o unset para limpar o formulário
                    unset($data);
                    echo "<span class=\"textoform\">Formulário enviado com sucesso!<br><br></span>";
             } else {
                 echo "<span class=\"textoform\">$retorno</span>";
             }
        }
        ?>
        
         <div>
            <span class="texto">Nome do prospect: </span><br />
            <input name="nome" id="nome" type="text" class="form-poshytip" title="Entre nome no prospect" style="width:300px;"/>
        </div>
        <br /><br />
        <div>
            <span class="texto">E-mail do prospect (cadastrado no SAIE): </span><br />
            <input name="email" id="email" type="text" class="form-poshytip" title="Entre com seu e-mail" style="width:300px;"/>
        </div>
        <br /><br />
        <div>
            <span class="texto">Telefone do prospect: </span><br />
            <input name="ddd" id="ddd" type="text" class="form-poshytip" title="Entre com seu DDD" style="width:35px;" />
            <input name="telefone" id="telefone" type="text" class="form-poshytip" title="Entre com seu telefone" style="width:200px;" />
            
        </div>
        <br /><br />
        <div>
            <span class="texto">Agência do último atendimento: </span><br />
            <select id="agencia" name="agencia" class="form-poshytip">
            <option value="" selected="selected">- Selecione -</option>
            <option value="maceio@ieintercambio.com.br">IE AL Maceió</option>
            <option value="manaus@ieintercambio.com.br">IE AM Manaus</option>
            <option value="salvador@ieintercambio.com.br">IE BA Salvador</option>
            <option value="fortaleza@ieintercambio.com.br">IE CE Fortaleza</option>
            <option value="brasilia@ieintercambio.com.br">IE DF Brasília</option>
            <option value="linhares@ieintercambio.com.br">IE ES Linhares</option>
            <option value="vix@ieintercambio.com.br">IE ES Vitória</option>
            <option value="saoluis@ieintercambio.com.br">IE MA São Luís</option>
            <option value="bh@ieintercambio.com.br">IE MG Belo Horizonte</option>
            <option value="itauna@ieintercambio.com.br">IE MG Itaúna</option>
            <option value="juizdefora@ieintercambio.com.br">IE MG Juiz de Fora</option>
            <option value="varginha@ieintercambio.com.br">IE MG Varginha</option>
            <option value="campogrande@ieintercambio.com.br">IE MS Campo Grande</option>
            <option value="recife@ieintercambio.com.br">IE PE Recife</option>
            <option value="barra@ieintercambio.com.br">IE RJ Barra</option>
            <option value="cabofrio@ieintercambio.com.br">IE RJ Cabo Frio</option>
            <option value="campos@ieintercambio.com.br">IE RJ Campos</option>
            <option value="centro@ieintercambio.com.br">IE RJ Centro</option>
            <option value="ipanema@ieintercambio.com.br">IE RJ Ipanema</option>
            <option value="macae@ieintercambio.com.br">IE RJ Macaé</option>
            <option value="niteroi@ieintercambio.com.br">IE RJ Niterói</option>
            <option value="petropolis@ieintercambio.com.br">IE RJ Petrópolis</option>
            <option value="natal@ieintercambio.com.br">IE Natal</option>
            <option value="caxiasdosul@ieintercambio.com.br">IE RS Caxias do Sul</option>
            <option value="portoalegre@ieintercambio.com.br">IE RS Porto Alegre</option>
            <option value="florianopolis@ieintercambio.com.br">IE SC Florianópolis</option>
            <option value="bauru@ieintercambio.com.br">IE SP Bauru</option>
            <option value="campinas@ieintercambio.com.br">IE SP Campinas</option>
            <option value="higienopolis@ieintercambio.com.br">IE SP Higienópolis</option>
            <option value="moema@ieintercambio.com.br">IE SP Moema</option>
            <option value="morumbi@ieintercambio.com.br">IE SP Morumbi</option>
            <option value="paraiso@ieintercambio.com.br">IE SP Paraíso</option>
            <option value="paulista@ieintercambio.com.br">IE SP Paulista</option>
            <option value="piracicaba@ieintercambio.com.br">IE SP Piracicaba</option>
            <option value="santana@ieintercambio.com.br">IE SP Santana</option>
            <option value="santoamaro@ieintercambio.com.br">IE SP Santo Amaro</option>
            <option value="sbc@ieintercambio.com.br">IE SP São Bernardo do Campo</option>
            <option value="saocarlos@ieintercambio.com.br">IE SP São Carlos</option>
            <option value="sjc@ieintercambio.com.br">IE SP São José dos Campos</option>
            <option value="sorocaba@ieintercambio.com.br">IE SP Sorocaba</option>
            <option value="taubate@ieintercambio.com.br">IE SP Taubaté</option>
            </select>
        </div>

        <br /><br />
        <div>
            <span class="texto">Motivo da migração: </span><br />
            <textarea  name="comments"  id="comments" rows="10" cols="50" class="form-poshytip" title="Entre com sua mensagem"></textarea>
        </div>
        <br /><br />
        <div>
            <span class="texto">Seu nome: </span><br />
            <input name="nome2" id="nome2" type="text" class="form-poshytip" title="Entre com seu nome" style="width:300px;"/>
        </div>
        <br /><br />
        <div>
            <span class="texto">Selecione a sua agência: </span><br />
            <select id="agencia2" name="agencia2" class="form-poshytip">
            <option value="" selected="selected">- Selecione -</option>
            <option value="maceio@ieintercambio.com.br">IE AL Maceió</option>
            <option value="manaus@ieintercambio.com.br">IE AM Manaus</option>
            <option value="salvador@ieintercambio.com.br">IE BA Salvador</option>
            <option value="fortaleza@ieintercambio.com.br">IE CE Fortaleza</option>
            <option value="brasilia@ieintercambio.com.br">IE DF Brasília</option>
            <option value="linhares@ieintercambio.com.br">IE ES Linhares</option>
            <option value="vix@ieintercambio.com.br">IE ES Vitória</option>
            <option value="saoluis@ieintercambio.com.br">IE MA São Luís</option>
            <option value="bh@ieintercambio.com.br">IE MG Belo Horizonte</option>
            <option value="itauna@ieintercambio.com.br">IE MG Itaúna</option>
            <option value="juizdefora@ieintercambio.com.br">IE MG Juiz de Fora</option>
            <option value="varginha@ieintercambio.com.br">IE MG Varginha</option>
            <option value="campogrande@ieintercambio.com.br">IE MS Campo Grande</option>
            <option value="recife@ieintercambio.com.br">IE PE Recife</option>
            <option value="barra@ieintercambio.com.br">IE RJ Barra</option>
            <option value="cabofrio@ieintercambio.com.br">IE RJ Cabo Frio</option>
            <option value="campos@ieintercambio.com.br">IE RJ Campos</option>
            <option value="centro@ieintercambio.com.br">IE RJ Centro</option>
            <option value="ipanema@ieintercambio.com.br">IE RJ Ipanema</option>
            <option value="macae@ieintercambio.com.br">IE RJ Macaé</option>
            <option value="niteroi@ieintercambio.com.br">IE RJ Niterói</option>
            <option value="petropolis@ieintercambio.com.br">IE RJ Petrópolis</option>
            <option value="natal@ieintercambio.com.br">IE Natal</option>
            <option value="caxiasdosul@ieintercambio.com.br">IE RS Caxias do Sul</option>
            <option value="portoalegre@ieintercambio.com.br">IE RS Porto Alegre</option>
            <option value="florianopolis@ieintercambio.com.br">IE SC Florianópolis</option>
            <option value="bauru@ieintercambio.com.br">IE SP Bauru</option>
            <option value="campinas@ieintercambio.com.br">IE SP Campinas</option>
            <option value="higienopolis@ieintercambio.com.br">IE SP Higienópolis</option>
            <option value="moema@ieintercambio.com.br">IE SP Moema</option>
            <option value="morumbi@ieintercambio.com.br">IE SP Morumbi</option>
            <option value="paraiso@ieintercambio.com.br">IE SP Paraíso</option>
            <option value="paulista@ieintercambio.com.br">IE SP Paulista</option>
            <option value="piracicaba@ieintercambio.com.br">IE SP Piracicaba</option>
            <option value="santana@ieintercambio.com.br">IE SP Santana</option>
            <option value="santoamaro@ieintercambio.com.br">IE SP Santo Amaro</option>
            <option value="sbc@ieintercambio.com.br">IE SP São Bernardo do Campo</option>
            <option value="saocarlos@ieintercambio.com.br">IE SP São Carlos</option>
            <option value="sjc@ieintercambio.com.br">IE SP São José dos Campos</option>
            <option value="sorocaba@ieintercambio.com.br">IE SP Sorocaba</option>
            <option value="taubate@ieintercambio.com.br">IE SP Taubaté</option>
            </select>
        </div>
                <br />                            
        <input type="hidden" name="enviar" value="send" />
        <input type="submit" name="Enviar" value="Enviar" id="submit"/>
        
</form>


</div>

</center>
</body>
</html>
