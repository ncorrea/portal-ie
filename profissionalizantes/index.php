<?php include 'conn.php'?>
<?php require_once("func.php");?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt" xml:lang="pt" manifest="prof.appcache">
<head>
<meta http-equiv="Cache-control" content="private">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="Cursos Profissionalizantes IE em Gestão, Design Gráfico, Direito, Turismo, Negócios, Marketing e TI. Solicite seu Orçamento"/>
<meta name="keywords" content="orçamento, curso, profissionalizante, contabilidade, finanças, direito, design, gráfico, ti, marketing, negócios, gestão, turismo, intercâmbio, cultural, estados unidos, EUA, canadá, inglaterra, austrália, irlanda"/>
<meta name="author" content="IE Intercâmbio no Exterior"/>
    <!-- Google Code for Hotsite - Profissionalizantes Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 961181818;
    var google_conversion_language = "en";
    var google_conversion_format = "2";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "6S44CN7l7goQ-vCpygM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/961181818/?label=6S44CN7l7goQ-vCpygM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>    
<title>Cursos Profissionalizantes IE - Solicite seu Orçamento</title>
<link href="http://portaliefiles.s3-sa-east-1.amazonaws.com/profissionalizantes/css/estilo.css" type="text/css" media="screen" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="http://portaliefiles.s3-sa-east-1.amazonaws.com/profissionalizantes/css/print.css" media="print">
<link rel="stylesheet" href="http://portaliefiles.s3-sa-east-1.amazonaws.com/profissionalizantes/js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
<link rel="stylesheet" href="http://portaliefiles.s3-sa-east-1.amazonaws.com/profissionalizantes/js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.6.3.min.js" type="text/javascript"></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
<script src="js/jquery.animate-colors-min.js" type="text/javascript"></script>
<script src="js/jquery.skitter.min.js" type="text/javascript"></script>
<script src="js/jquery.skitter.js" type="text/javascript"></script>
<script src="js/highlight.js" type="text/javascript"></script>
<script type="text/javascript">_atrk_opts={atrk_acct:"qNcph1aMQV00Ov",domain:"ieintercambio.com.br",dynamic:true};(function(){var a=document.createElement("script");a.type="text/javascript";a.async=true;a.src="https://d31qbv1cthcecs.cloudfront.net/atrk.js";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b)})();</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=qNcph1aMQV00Ov" style="display:none" height="1" width="1" alt="" /></noscript>
<script type="text/javascript">$(document).ready(function(){var a={};if(document.location.search){var d=document.location.search.split("=");var c=d[0].replace("?","");var b=d[1];if(c=="animation"){a.animation=b}else{if(c=="type_navigation"){if(b=="dots_preview"){$(".border_box").css({marginBottom:"40px"});a.dots=true;a.preview=true}else{a[b]=true;if(b=="dots"){$(".border_box").css({marginBottom:"40px"})}}}}}$(".box_skitter_large").skitter(a);$("pre.code").highlight({source:1,zebra:1,indent:"space",list:"ol"})});</script>
<script language="javascript" type="text/javascript">var divID="box_curso";function CollapseExpand(){var a=document.getElementById(divID);var b=a.className;if(a.className=="divEscondida"){a.className="divVisivel"}}</script>
<script type="text/javascript">_atrk_opts={atrk_acct:"qNcph1aMQV00Ov",domain:"ieintercambio.com.br",dynamic:true};(function(){var a=document.createElement("script");a.type="text/javascript";a.async=true;a.src="https://d31qbv1cthcecs.cloudfront.net/atrk.js";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b)})();</script>
<img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=qNcph1aMQV00Ov" style="display:none" height="1" width="1" alt=""/>
<style>.carregando{color:#666;display:none}</style>
</head>

<body>
<div id="fb-root"></div>
<script>/*<![CDATA[*/(function(e,a,f){var c,b=e.getElementsByTagName(a)[0];if(e.getElementById(f)){return}c=e.createElement(a);c.id=f;c.src="//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=206272076050907";b.parentNode.insertBefore(c,b)}(document,"script","facebook-jssdk"));/*]]>*/</script>
<script type="text/javascript">(function(d,e,j,h,f,c,b){d.GoogleAnalyticsObject=f;d[f]=d[f]||function(){(d[f].q=d[f].q||[]).push(arguments)},d[f].l=1*new Date();c=e.createElement(j),b=e.getElementsByTagName(j)[0];c.async=1;c.src=h;b.parentNode.insertBefore(c,b)})(window,document,"script","//www.google-analytics.com/analytics.js","ga");ga("create","UA-3881937-17","ieintercambio.com.br");ga("send","pageview");</script>
<img src="http://d35a0muf0ntvw9.cloudfront.net/profissionalizantes/images/background.jpg" alt="background" id="full-screen-background-image" />
<center>
<div id="tudo">
<div id="logo"><img src="http://d35a0muf0ntvw9.cloudfront.net/profissionalizantes/images/logo.png" width="674" height="105" alt="logo ie" usemap="#Map2" border="0"/>
<map name="Map2" id="Map2">
<area shape="rect" coords="0,3,109,102" href="http://www.ieintercambio.com.br" alt="logo" target="_blank" />
</map>
</div>
<div id="social_box">
<div class="fb-like" data-href="http://www.ieintercambio.com.br/profissionalizantes" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.ieintercambio.com.br/profissionalizantes" data-via="ieintercambio" data-lang="pt" data-related="ieintercambio"></a>
<script>!function(f,a,g){var e,b=f.getElementsByTagName(a)[0],c=/^http:/.test(f.location)?"http":"https";if(!f.getElementById(g)){e=f.createElement(a);e.id=g;e.src=c+"://platform.twitter.com/widgets.js";b.parentNode.insertBefore(e,b)}}(document,"script","twitter-wjs");</script>
</div>
<div id="webdoor">
<div id="slider">
<div class="box_skitter box_skitter_large">
<ul>
<li><a href="http://www.ieintercambio.com.br/promocoes" target="_blank"><img src="http://d3636tg8eqszps.cloudfront.net/profissionalizantes/banner01.jpg" width="800" height="255" alt="carreira" class="fade" border="5px"/></a><div class="label_text"><p></p></div></li>
<li><a href="#fade"><img src="http://www.ieintercambio.com.br/profissionalizantes/images/banner02.jpg" width="800" height="255" alt="mala de viagem" class="fade" /></a><div class="label_text"><p></p></div></li>
<li><a href="#fade"><img src="http://www.ieintercambio.com.br/profissionalizantes/images/banner03.jpg" width="800" height="255" alt="paris" class="fade" /></a><div class="label_text"><p></p></div></li>
<li><a href="#fade"><img src="http://d35a0muf0ntvw9.cloudfront.net/profissionalizantes/images/banner04.jpg" width="800" height="255" alt="avião" class="fade" /></a><div class="label_text"><p></p></div></li>
</ul>
</div>
</div>
</div>
<div id="centro">
<h1><div id="area_box">
<span class="titulo">Area:</span><br />
<select name="cod_area" id="cod_area">
<option value="">-- Escolha uma área --</option>
<?php
							$sql = "SELECT cod_area, nome
									FROM area
									ORDER BY nome";
							$res = mysql_query( $sql );
							while ( $row = mysql_fetch_assoc( $res ) ) {
								echo '<option value="'.$row['cod_area'].'">'.(utf8_encode($row['nome'])).'</option>';
							}
						?>
</select>
</div>
<div id="pais_box">
<span class="titulo">País:</span><br/>
<span class="carregando"><img src="http://d35a0muf0ntvw9.cloudfront.net/profissionalizantes/images/loading2.gif" width="25" height="25" alt="carregando" class="fade" /></span>
<select name="pais" id="pais">
<option value="">-- Escolha uma pais --</option>
<script type="text/javascript">/*<![CDATA[*/$(function(){$("#cod_area").change(function(){if($(this).val()){$("#pais").hide();$(".carregando").show();$.getJSON("paises.ajax.php?search=",{cod_area:$(this).val(),ajax:"true"},function(b){var a='<option value=""></option>';for(var c=0;c<b.length;c++){a+='<option value="'+b[c].cod_master+'">'+b[c].pais+"</option>"}$("#pais").html(a).show();$(".carregando").hide()})}else{$("#pais").html('<option value="">– Escolha um pais –</option>')}})});/*]]>*/</script>
</select>
</div>
<div id="curso_box">
<span class="titulo">Curso:</span><br/>
<span class="carregando"><img src="http://d35a0muf0ntvw9.cloudfront.net/profissionalizantes/images/loading2.gif" width="25" height="25" alt="carregando" class="fade" /></span>
<select name="curso" id="curso" onchange="return CollapseExpand()">
<option value="">-- Escolha uma curso --</option>
<script type="text/javascript">/*<![CDATA[*/$(function(){$("#cod_area").change(function(){if($(this).val()){$("#curso").hide();$(".carregando").show();$.getJSON("cursos.ajax.php?search=",{cod_area:$(this).val(),ajax:"true"},function(b){var a='<option value=""></option>';for(var c=0;c<b.length;c++){a+='<option value="'+b[c].cod_curso+'">'+b[c].nome+"</option>"}$("#curso").html(a).show();$(".carregando").hide()})}else{$("#curso").html('<option value="">– Escolha um curso –</option>')}})});$(function(){$("#pais").change(function(){if($(this).val()){$("#curso").hide();$(".carregando").show();$.getJSON("cursos.ajax2.php?search=",{cod_master:$(this).val(),ajax:"true"},function(b){var a='<option value=""></option>';for(var c=0;c<b.length;c++){a+='<option value="'+b[c].cod_curso+'">'+b[c].nome+"</option>"}$("#curso").html(a).show();$(".carregando").hide()})}else{$("#curso").html('<option value="">– Escolha um curso –</option>')}})});/*]]>*/</script>
</select>
</div></h1>
</div>
<div id="linha"></div>
<div id="box_curso" class="divEscondida">
<div id="box01">
<span class="carregando">Aguarde, carregando...</span>
<div id="nome">
<script type="text/javascript">/*<![CDATA[*/$(function(){$("#curso").change(function(){if($(this).val()){$("#nome").hide();$(".carregando").show();$.getJSON("conteudo.ajax.php?search=",{cod_curso:$(this).val(),ajax:"true"},function(b){var a='<span class="texto"><br /><strong>Curso: </strong></span>';for(var c=0;c<b.length;c++){a+='<span class="texto">'+b[c].nome+"</span>"}$("#nome").html(a).show();$(".carregando").hide()})}else{$("#nome").html("- Escolha um curso -")}})});/*]]>*/</script>
</div>
<span class="carregando">Aguarde, carregando...</span>
<h2><div id="apresentacao">
<script type="text/javascript">/*<![CDATA[*/$(function(){$("#curso").change(function(){if($(this).val()){$("#apresentacao").hide();$(".carregando").show();$.getJSON("conteudo.ajax.php?search=",{cod_curso:$(this).val(),ajax:"true"},function(b){var a='<img src="http://d35a0muf0ntvw9.cloudfront.net/profissionalizantes/images/apresentacao.png" /> <span class="titulo"> Apresentação: </span> <br />';for(var c=0;c<b.length;c++){a+='<span class="texto">'+b[c].apresentacao+"</span>"}$("#apresentacao").html(a).show();$(".carregando").hide()})}else{$("#apresentacao").html("- Escolha um curso -")}})});/*]]>*/</script>
</div></h2>
<div id="formularios">
<span class="titulo"><img src="http://portaliefiles.s3-sa-east-1.amazonaws.com/profissionalizantes/images/fale.png" />Fale Conosco</span>
<script type="text/javascript" src="js/form-validation.js"></script>
<form action="http://www.ieintercambio.com.br/profissionalizantes/index.php" name="formulario" id="contactForm" method="post" enctype="multipart/form-data">
<?php
                                    if (isset($_POST['enviar']) && $_POST['enviar'] == 'send') {
                                        $agencia = $_POST['agencia'];
                                        $estado = estadoFranquia($agencia);
 										$fullName = $_POST["nome"];
 										$pieces = explode(" ", $fullName);
                                        $data['nome'] = $pieces[0] ;
                                        $data['sobrenome'] = $pieces[1];
                                        $data['data_nascimento'] = '';
                                        $data['email'] = $_POST["email"];
                                        $data['ddd'] = $_POST["ddd"];
                                        $data['telefone'] = $_POST["telefone"];
                                        $data['ddd2'] = '';
                                        $data['telefone2'] = '';		
                                        $data['cidade'] = '';
                                        $data['estado'] = $estado;
                                        $data['agencia'] = agenciaFranquia($agencia);
                                        $data['interesse'] =  '6'; //Facebook
                                        $data['produto'] = array('2');
                                        $data['travel_quantity'] = '0';
                                        $data['sexo'] = '1';
                                        $data['comentario'] = $_POST["comments"];
                                        $data['emailfranquia'] = $agencia;
                                         if(empty($data['nome'])) {
                                             $retorno = "<script>alert('Informe seu nome');</script>";
                                         }elseif (empty($data['email'])) {
                                             $retorno = "<script>alert('Informe seu e-mail');</script>";
                                         }elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                                             $retorno = "<script>alert('Informe um e-mail válido');</script>";
                                         }elseif (empty($data['ddd'])) {
                                             $retorno = "<script>alert('Digite DDD');</script>";
                                         }elseif (empty($data['telefone'])) {
                                             $retorno = "<script>alert('Digite o telefone');</script>";
                                         }elseif (empty($data['comentario'])) {
                                             $retorno = "<script>alert('Digite um comentário');</script>";
                                         }if (empty($retorno)) {
                                                //Faz o envio do e-mail e insere no webservice
                                                $data['comentario'] ="Produto de interesse: Cursos Profissionalizantes<br/> " . $_POST["comments"];
                                                enviaMail($data);
                                                $saida = WebServiceSpecta($data);
                                                //Faz o unset para limpar o formulário                                            
                                                unset($data);
     											echo "<script>alert('Formulário enviado com sucesso!');</script>";
                                         } else {
                                             echo "<span class=\"textoform\">$retorno</span>";
                                         }
                                    }
                                    ?>
<div>
<span class="texto">Nome: </span>
<input name="nome" id="nome" type="text" class="form-poshytip" title="Entre com seu nome" />
</div>
<div>
<span class="texto">E-mail: </span>
<input name="email" id="email" type="text" class="form-poshytip" title="Entre com seu e-mail" />
</div>
<div>
<span class="texto">Telefone: </span>
<input name="ddd" id="ddd" type="text" class="form-poshytip" title="Entre com seu DDD" style="width:35px" />
<input name="telefone" id="telefone" type="text" class="form-poshytip" title="Entre com seu telefone" style="width:270px" />
</div>
<div>
<span class="texto">Agencia de preferência: </span>

<select name="agencia" id="agencia">
<option value="">-- Escolha a agência --</option>
<?php
							$sql = "SELECT email, nome
									FROM agencia
									ORDER BY nome";
							$res = mysql_query( $sql );
							while ( $row = mysql_fetch_assoc( $res ) ) {
								echo '<option value="'.$row['email'].'">'.(utf8_encode($row['nome'])).'</option>';
							}
						?>
</select>
</div>
<div>
<span class="texto">Mensagem: </span>
<textarea name="comments" id="comments" rows="5" cols="20" class="form-poshytip" title="Entre com sua mensagem"></textarea>
</div>
<input type="hidden" name="enviar" value="send" />
<input type="submit" name="Enviar" value="Enviar" id="submit"/>
</form>
</div>
</div>
<div id="box2">
<h3><div id="duracao">
<script type="text/javascript">/*<![CDATA[*/$(function(){$("#curso").change(function(){if($(this).val()){$("#duracao").hide();$(".carregando").show();$.getJSON("conteudo.ajax.php?search=",{cod_curso:$(this).val(),ajax:"true"},function(b){var a='<img src="http://d35a0muf0ntvw9.cloudfront.net/profissionalizantes/images/duracao.png" alt="duração" /> <span class="titulo"> Duração: </span> <br/>';for(var c=0;c<b.length;c++){a+='<span class="texto">'+b[c].duracao+"</span>"}$("#duracao").html(a).show();$(".carregando").hide()})}else{$("#duracao").html("- Escolha um curso -")}})});/*]]>*/</script>
</div>
<br/>
<div id="prerequi">
<script type="text/javascript">/*<![CDATA[*/$(function(){$("#curso").change(function(){if($(this).val()){$("#prerequi").hide();$(".carregando").show();$.getJSON("conteudo.ajax.php?search=",{cod_curso:$(this).val(),ajax:"true"},function(b){var a='<img src="http://d35a0muf0ntvw9.cloudfront.net/profissionalizantes/images/pre-requi.png" alt="pre_requisitos"/> <span class="titulo"> Pré-requisitos: </span> <br/>';for(var c=0;c<b.length;c++){a+='<span class="texto">'+b[c].prerequi+"</span>"}$("#prerequi").html(a).show();$(".carregando").hide()})}else{$("#prerequi").html("- Escolha um curso -")}})});/*]]>*/</script>
</div>
<br/>
<div id="escola">
<script type="text/javascript">/*<![CDATA[*/$(function(){$("#curso").change(function(){if($(this).val()){$("#escola").hide();$(".carregando").show();$.getJSON("conteudo.ajax.php?search=",{cod_curso:$(this).val(),ajax:"true"},function(b){var a='<img src="http://d35a0muf0ntvw9.cloudfront.net/profissionalizantes/images/escola.png" alt="escola"/> <span class="titulo"> Escola: </span> <br/>';for(var c=0;c<b.length;c++){a+='<span class="texto">'+b[c].escola+"</span>"}$("#escola").html(a).show();$(".carregando").hide()})}else{$("#escola").html("- Escolha um curso -")}})});/*]]>*/</script>
</div>
<br/>
<div id="investimento">
<script type="text/javascript">/*<![CDATA[*/$(function(){$("#curso").change(function(){if($(this).val()){$("#investimento").hide();$(".carregando").show();$.getJSON("conteudo.ajax.php?search=",{cod_curso:$(this).val(),ajax:"true"},function(b){var a='<img src="http://d35a0muf0ntvw9.cloudfront.net/profissionalizantes/images/investimento.png" alt="investimento"/> <span class="titulo"> Investimento: </span> <br/>';for(var c=0;c<b.length;c++){a+='<span class="texto">'+b[c].investimento+"</span>"}$("#investimento").html(a).show();$(".carregando").hide()})}else{$("#investimento").html("- Escolha um curso -")}})});/*]]>*/</script>
</div>
<br/>
</div></h3>
</div>
<div id="base"><img src="http://d35a0muf0ntvw9.cloudfront.net/profissionalizantes/images/base.gif" width="800" height="53" alt="rodapé" usemap="#Map" border="0" />
<map name="Map" id="Map"><area shape="rect" coords="452,4,781,49" href="http://www.ieintercambio.com.br" alt="portal" target="_blank" /><area shape="rect" coords="222,10,254,44" href="http://pinterest.com/ieintercambio/" alt="pinterest" target="_blank" /><area shape="rect" coords="180,10,212,44" href="http://www.youtube.com/ieintercambio" alt="youtube" target="_blank" /><area shape="rect" coords="138,9,170,43" href="https://www.facebook.com/IEintercambio/app_168188869963563" alt="facebook" target="_blank" /><area shape="rect" coords="96,10,128,44" href="https://plus.google.com/116944547482760833313/posts" alt="google+" target="_blank" /><area shape="rect" coords="54,11,86,45" href="https://twitter.com/#!/ieintercambio" alt="twitter" target="_blank" />
<area shape="rect" coords="12,11,44,45" href="https://www.facebook.com/IEintercambio" alt="facebook" target="_blank" />
</map>
</div>
</div>
</center>
</body>
</html>