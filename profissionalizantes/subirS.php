<?php
error_reporting(E_ALL ^ E_NOTICE);
require_once 'excel_reader2.php';
$data = new Spreadsheet_Excel_Reader("Prospects.xls");

function chamaSpecta($data){
	$row = 2;
	while($data->val($row, 1) != ""){
		$retorno = WebServiceSpecta($data, $row);		
		$row = $row + 1;
	}
	return $row;
}

function retornaProdutos($produtos){
			$i = 0;
			//echo  "Produtos: " . $produtos;
			$elementos = explode(",", $produtos);
        	while ($i < count($elementos)){
        		$elementos[$i] = Trim($elementos[$i]);
        		$i = $i + 1;
        	}
        	//var_dump($elementos);
        	//echo  "<br><br>";
        	return $elementos;
}
                
function enviaPost($url, $fields) {
			//echo "Os parametros sao : " . $fields . "<br>";
			
			$fields_string = http_build_query($fields);

            //open connection
            $ch = curl_init();
			
            //set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    
            //execute post
            $result = curl_exec($ch);
            
            //close connection
            curl_close($ch);

            return $result;
    }
    
    
        function dtArrumar($data) {
            $data = str_replace("/","",$data);
            $dia = substr ( $data, 0, 2 );
            $mes = substr ( $data, 2, 2 );
            $ano = substr ( $data, 4, 4 );
            $arrumado = $ano . $mes . $dia;
            return $arrumado;
        }
        
        function descobreColuna($data, $nome_coluna){
        	$linha = 1;
        	$i = 1;
        	while ($data->val($linha, $i) != ""){
        		if ($data->val($linha, $i) == $nome_coluna){
        			return $i;
        		}
        		$i = $i + 1;
        	}
        	return -1;
        }

function WebServiceSpecta($data, $row){
			//echo "Linha " . $row . "<br>";
			$first_name = descobreColuna($data, "first_name");
			$last_name = descobreColuna($data, "last_name");
			$birthdate = descobreColuna($data, "birthdate");
			$email = descobreColuna($data, "email");
			$phone = descobreColuna($data, "phone");
			$phone2 = descobreColuna($data, "phone2");
			$city = descobreColuna($data, "city");
			$state = descobreColuna($data, "state");
			$franchise = descobreColuna($data, "franchise");
			$know_type = descobreColuna($data, "know_type");
			$travel_quantity = descobreColuna($data, "travel_quantity");
			$gender = descobreColuna($data, "gender");
			$products = descobreColuna($data, "products");
			$comment = descobreColuna($data, "comment");
			$list = descobreColuna($data, "list");
			$employee = descobreColuna($data, "employee");
			
			
            $dados= array( array(
                    "first_name" => utf8_encode(ucwords(strtolower($data->val($row,$first_name)))),
                    "last_name" =>utf8_encode(ucwords(strtolower($data->val($row,$last_name)))),
                    "birthdate" => dtArrumar($data->val($row,$birthdate)),
                    "email" => utf8_encode($data->val($row,$email)),
                    "phone" => utf8_encode($data->val($row,$phone)),
                    "phone2" => utf8_encode($data->val($row,$phone2)),
                    "city" => urlencode($data->val($row,$city)),
                    "state" => urlencode($data->val($row,$state)),
                    "franchise" => urlencode($data->val($row,$franchise)),
                    "know_type" => urlencode($data->val($row,$know_type)),
                    "travel_quantity" => urlencode($data->val($row,$travel_quantity)),
                    "gender" => urlencode($data->val($row,$gender)),
                    "products" => retornaProdutos($data->val($row,$products)),
                    "comment" => utf8_encode($data->val($row,$comment)),
                    "list" => urlencode($data->val($row,$list)),
                    "employee" => urlencode($data->val($row,$employee))
                )
            );
        	//echo  "<br><br>";
			//var_dump($dados);
			
            $retorno = enviaPost('https://saie.spectacloud.net/server/saie_prospection/saieProspects/importProspectsFromSite', $dados);
            
            $fp = fopen("debug.txt", "a");
            fwrite($fp, "\nRetorno:" . print_r($retorno, true));
            fclose($fp);
                
            echo "Linha " . $row . " : " . $retorno . "<br>";    
            return $retorno;
        }
        
        
        
        
        



?>

<html>
<head>
<style>
table.excel {
	border-style:ridge;
	border-width:1;
	border-collapse:collapse;
	font-family:sans-serif;
	font-size:12px;
}
table.excel thead th, table.excel tbody th {
	background:#CCCCCC;
	border-style:ridge;
	border-width:1;
	text-align: center;
	vertical-align:bottom;
}
table.excel tbody th {
	text-align:center;
	width:20px;
}
table.excel tbody td {
	vertical-align:bottom;
}
table.excel tbody td {
    padding: 0 3px;
	border: 1px solid #EEEEEE;
}
</style>
</head>

<body>
<?php echo chamaSpecta($data); ?>
</body>
</html>
