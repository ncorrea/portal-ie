<?php include 'conn.php'?>
<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	
	$cod_master = mysql_real_escape_string( $_REQUEST['cod_master'] );

	$cod_cursos = array();

	$sql = "SELECT pais, cod_area, nome, cod_curso, cod_pais, cod_master
			FROM curso
			WHERE cod_master=$cod_master";
			
	$res = mysql_query( $sql );
	while ( $row = mysql_fetch_assoc( $res ) ) {
		$cod_cursos[] = array(
			'cod_curso'	=> (utf8_encode($row['cod_curso'])),
			'nome'			=> (utf8_encode($row['nome'])),
		);
	}

	echo( json_encode( $cod_cursos ) );