Drupal.locale = { 'pluralFormula': function ($n) { return Number(($n!=1)); }, 'strings': {"":{"Not published":"N\u00e3o publicado","Edit":"Editar","Hide shortcuts":"Esconder atalhos","Disabled":"Desativado","Enabled":"Ativado","(active tab)":"(aba ativa)","Show":"Exibir","Not customizable":"N\u00e3o \u00e9 personaliz\u00e1vel","Customize dashboard":"Personalizar painel","Requires a title":"T\u00edtulo requerido","Don\u0027t display post information":"N\u00e3o exibir informa\u00e7\u00f5es de postagem","An AJAX HTTP error occurred.":"Ocorreu um erro HTTP no AJAX","HTTP Result Code: !status":"C\u00f3digo do Resultado HTTP:  !status","An AJAX HTTP request terminated abnormally.":"Uma requisi\u00e7\u00e3o HTTP AJAX terminou de forma anormal.","Debugging information follows.":"Estas s\u00e3o as informa\u00e7\u00f5es de depura\u00e7\u00e3o.","Path: !uri":"Caminho: !url","StatusText: !statusText":"Texto de Status: !statusText","ResponseText: !responseText":"Texto de Resposta: !responseText","ReadyState: !readyState":"ReadyState: !readyState","Show shortcuts":"Mostrar atalhos","Select all rows in this table":"Selecionar todas as linhas da tabela","Deselect all rows in this table":"Desmarcar todas as linhas da tabela","Configure":"Configurar","Hide summary":"Ocultar sum\u00e1rio","Edit summary":"Editar resumo","Hide":"Ocultar","Not in menu":"Fora do menu","New revision":"Nova revis\u00e3o","No revision":"Sem revis\u00e3o","By @name on @date":"Por @name em @date","By @name":"Por @name","Alias: @alias":"URL Alternativa: @alias","No alias":"Nenhuma URL alternativa","@number comments per page":"@number coment\u00e1rios por p\u00e1gina","Autocomplete popup":"Popup de autocompletar","Searching for matches...":"Procurando por dados correspondentes...","Not restricted":"Sem restri\u00e7\u00f5es","Restricted to certain pages":"Restrito para certas p\u00e1ginas","The changes to these blocks will not be saved until the \u003Cem\u003ESave blocks\u003C\/em\u003E button is clicked.":"As altera\u00e7\u00f5es nesses blocos n\u00e3o v\u00e3o ser salvas enquanto o bot\u00e3o \u003Cem\u003ESalvar Blocos\u003C\/em\u003E n\u00e3o for clicado.","The block cannot be placed in this region.":"O bloco n\u00e3o pode ser colocado nessa regi\u00e3o.","Re-order rows by numerical weight instead of dragging.":"Re-ordernar as linhas por campos n\u00famericos de peso ao inv\u00e9s de arrastar-e-soltar.","Show row weights":"Exibir pesos das linhas","Hide row weights":"Ocultar pesos das linhas","Drag to re-order":"Arraste para reordenar","Changes made in this table will not be saved until the form is submitted.":"Mudan\u00e7as feitas nesta tabela n\u00e3o ser\u00e3o salvas at\u00e9 que o formul\u00e1rio seja enviado.","Please wait...":"Por favor, espere um pouco...","The selected file %filename cannot be uploaded. Only files with the following extensions are allowed: %extensions.":"O arquivo selecionado %filename n\u00e3o p\u00f4de ser transferido. Somente arquivos com as seguintes extens\u00f5es s\u00e3o permitidos: %extensions.","Done":"Conclu\u00eddo","This permission is inherited from the authenticated user role.":"Essa permiss\u00e3o \u00e9 herdada do papel de usu\u00e1rio autenticado."}} };;

/**
 * @file
 *
 * Javascript specific to facebook connect pages.  This means pages
 * which are not canvas pages, and where fb_connect.module has
 * initialized the facebook api. The user may or may not have
 * authorized the app, this javascript will still be loaded.
 *
 * Note (!) much of the work done here is deprecated, and moved to fb.js
 * (where code works equally well on both connect pages and canvas
 * pages).  If your app needs the features here, please report your
 * use case to our issue queue (http://drupal.org/project/issues/fb),
 * otherwise these features may go away...
 */
(function ($) {
  Drupal.behaviors.fb_connect = {
    attach: function(context, settings) {
      // Logout of facebook when logging out of drupal.
      jQuery("a[href^='" + Drupal.settings.basePath + "user/logout']", context).click(FB_Connect.logoutHandler);
      jQuery("a[href^='" + Drupal.settings.fb_connect.front_url + "user/logout']", context).click(FB_Connect.logoutHandler); // front_url includes language.  I.e. "/en/"

      // Support markup for dialog boxes.
      FB_Connect.enablePopups(context);

      var events = jQuery(document).data('events');
      if (!events || !events.fb_session_change) {
        jQuery(document).bind('fb_session_change', FB_Connect.sessionChangeHandler);
      }
    }
  };
})(jQuery);

FB_Connect = function(){};

// JQuery pseudo-event handler.
FB_Connect.sessionChangeHandler = function(context, status) {
  jQuery('.block-fb_connect')
    .ajaxStart(function() {
      // This is an attempt to trigger the drupal progress indicator.  Not convinced that it works.
      jQuery(this).wrap('<div class="bar filled"></div>').wrap('<div class="bar filled"></div>');
    })
    .ajaxStop(function() {
      //jQuery(this).html('');
      // unwrap() not defined.
      jQuery(this).parent().removeClass('bar filled').parent().removeClass('bar filled');
    })
  ;

  // Call the default handler, too.
  FB_JS.sessionChangeHandler(context, status);
};

// click handler
FB_Connect.logoutHandler = function(event) {
  // If we need to reload, go to front page.
  Drupal.settings.fb.reload_url = Drupal.settings.fb_connect.front_url;

  if (typeof(FB) != 'undefined') {
    try {
      FB.logout(function () {
        // Logged out of facebook.  Session change event will log us out of drupal and
      });
      // Facebook's invalid cookies persist if third-party cookies disabled.
      // Let's try to clean up the mess.
      // @TODO: is this still needed with newer oauth SDK???
      //FB_JS.deleteCookie('fbs_' + Drupal.settings.fb.apikey, '/', ''); // apikey

      if (FB.getUserID()) { // @TODO: still needed with newer oauth SDK???
        // Facebook needs more time to log us out. (http://drupal.org/node/1164048)
        return false;
      }
    }
    catch (e) {
      return false;
    }
  }
  else {
    return false;
  }
};

/**
 * Move new dialogs to visible part of screen.
 **/
FB_Connect.centerPopups = function() {
  var scrollTop = $(window).scrollTop();
  $('.fb_dialog:not(.fb_popup_centered)').each(function() {
    var offset = $(this).offset();
    if (offset.left == 0) {
      // This is some facebook cruft that cannot be centered.
    }
    else if (offset.top < 0) {
      // Not yet visible, don't center.
    }
    else if (offset.top < scrollTop) {
      $(this).css('top', offset.top + scrollTop + 'px');
      $(this).addClass('fb_popup_centered'); // Don't move this dialog again.
    }
  });
};


FB_Connect.enablePopups = function(context) {
  // Support for easy fbml popup markup which degrades when javascript not enabled.
  // Markup is subject to change.  Currently...
  // <div class=fb_fbml_popup_wrap><a title="POPUP TITLE">LINK MARKUP</a><div class=fb_fbml_popup><fb:SOME FBML>...</fb:SOME FBML></div></div>
  jQuery('.fb_fbml_popup:not(.fb_fbml_popup-processed)', context).addClass('fb_fbml_popup-processed').prev().each(
    function() {
      this.fbml_popup = $(this).next().html();
      this.fbml_popup_width = parseInt($(this).next().attr('width'));
      this.fbml_popup_height = parseInt($(this).next().attr('height'));
      //console.log("stored fbml_popup markup: " + this.fbml_popup); // debug
      $(this).next().remove(); // Remove FBML so facebook does not expand it.
    })
    // Handle clicks on the link element.
    .bind('click',
	  function (e) {
	    var popup;
	    //console.log('Clicked!  Will show ' + this.fbml_popup); // debug

	    // http://forum.developers.facebook.net/viewtopic.php?pid=243983
	    var size = FB.UIServer.Methods["fbml.dialog"].size;
	    if (this.fbml_popup_width) {
	      size.width=this.fbml_popup_width;
	    }
	    if (this.fbml_popup_height) {
	      size.height=this.fbml_popup_height;
	    }
	    FB.UIServer.Methods['fbml.dialog'].size = size;

	    // http://forum.developers.facebook.net/viewtopic.php?id=74743
	    var markup = this.fbml_popup;
	    if ($(this).attr('title')) {
	      markup = '<fb:header icon="true" decoration="add_border">' + $(this).attr('title') + '</fb:header>' + this.fbml_popup;
	    }
	    var dialog = {
	      method: 'fbml.dialog', // triple-secret undocumented feature.
	      display: 'dialog',
	      fbml: markup,
	      width: this.fbml_popup_width,
	      height: this.fbml_popup_height
	    };
	    var popup = FB.ui(dialog, function (response) {
	      console.log(response);
	    });

	    // Start a timer to keep popups centered.
	    // @TODO - avoid starting timer more than once.
	    window.setInterval(FB_Connect.centerPopups, 500);

	    e.preventDefault();
	  })
    .parent().show();
};

;
/**
 * Javascript helpers for Facebook Streams.  Loaded by fb_stream.module.
 */
FB_Stream = function(){};

/**
 * Display a stream dialog on Facebook Connect pages, via
 * http://developers.facebook.com/docs/reference/javascript/FB.ui
 *
 * @param json is the json-encoded output of fb_stream_get_stream_dialog_data().
 */
FB_Stream.stream_publish = function(json) {
  var data_array = Drupal.parseJson(json);
  var len = data_array.length;
  for (var i=0; i < len; i++) {
    var data = data_array[i];
    data.method = 'stream.publish';
    FB.ui(data);
  }
};;
jQuery(document).ready(function($) {

	if(Drupal.settings.request_path == 'estudar/curso-de-idiomas/destaque')
	{

		var id = '2';
		var verbo = 'Eu quero';
		var produto = 'Curso de Idiomas';
		var subproduto = $('.view-cursos-idiomas-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-cursos-idiomas-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-cursos-idiomas-destaques-pagina .link').attr('href',url_contato);

		});

	}
	else if(Drupal.settings.request_path == 'estudar/high-school/modalidades')
	{

		var id = '5';
		var verbo = 'Eu quero';
		var produto = 'High School';	
		var subproduto = '';	
		var url_contato = '';	

		for(i = 1;i<=3; i++)
		{
			var subproduto = $('.field-name-body .col0'+i+' h5').html();

			url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.field-name-body .col0'+i+' .boxlink a').attr('href',url_contato);
		}

	}
	else if(Drupal.settings.request_path == 'estudar/high-school/destaques')
	{

		var id = '5';
		var verbo = 'Eu quero';
		var produto = 'High School';
		var subproduto = $('.view-high-school-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-high-school-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-high-school-destaques-pagina .link').attr('href',url_contato);

		});

	}
	else if(Drupal.settings.request_path == 'estudar/teen-experience/destaques')
	{

		var id = '6';
		var verbo = 'Eu quero';
		var produto = 'Teen Experience';
		var subproduto = $('.view-teen-experience-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-teen-experience-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-teen-experience-destaques-pagina .link').attr('href',url_contato);

		});

	}	
	else if(Drupal.settings.request_path == 'estudar/study-and-work/destaques')
	{

		var id = '2';
		var verbo = 'Eu quero';
		var produto = 'Study and Work - Estudar';
		var subproduto = $('.view-study-and-work-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-study-and-work-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-study-and-work-destaques-pagina .link').attr('href',url_contato);

		});

	}		
	else if(Drupal.settings.request_path == 'trabalhar/study-and-work/destaques')
	{

		var id = '2';
		var verbo = 'Eu quero';
		var produto = 'Study and Work - Trabalhar';
		var subproduto = $('.view-study-and-work-destaques-pagina-trabalhar .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-study-and-work-destaques-pagina-trabalhar .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-study-and-work-destaques-pagina-trabalhar .link').attr('href',url_contato);

		});

	}		
	else if(Drupal.settings.request_path == 'estudar/seguro-iac')
	{

		var id = '9';
		var verbo = 'Eu quero';
		var produto = 'Seguro Viagem IAC';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}
	else if(Drupal.settings.request_path == 'trabalhar/seguro-iac')
	{

		var id = '9';
		var verbo = 'Eu quero';
		var produto = 'Seguro Viagem IAC';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}	
	else if(Drupal.settings.request_path == 'estudar/visa-travel-money')
	{

		var id = '12';
		var verbo = 'Eu quero';
		var produto = 'Visa Travel Money';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}
	else if(Drupal.settings.request_path == 'trabalhar/visa-travel-money')
	{

		var id = '12';
		var verbo = 'Eu quero';
		var produto = 'Visa Travel Money';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}		
	else if(Drupal.settings.request_path == 'conhecer/visa-travel-money')
	{

		var id = '12';
		var verbo = 'Eu quero';
		var produto = 'Visa Travel Money';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}		
	else if(Drupal.settings.request_path == 'conhecer/mochilao/destaques')
	{

		var id = '6';
		var verbo = 'Eu quero';
		var produto = 'Trip Experience';
		var subproduto = $('.view-mochiloes-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-mochiloes-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-mochiloes-destaques-pagina .link').attr('href',url_contato);

		});

	}	
	else
	{

		var regExp = '';
		var path = Drupal.settings.request_path;
		var split_url = Drupal.settings.request_path.split('/');

		// Work Experience
		regExp = /trabalhar\/workexperience/g;	
		
		if (regExp.test(path)) 
		{
		    
			var id = '3';
			var verbo = 'Eu quero';
			var produto = 'Work Experience';	
			var subproduto = '';	
			var url_contato = '';	

			for(i = 1;i<=3; i++)
			{
				var subproduto = $('.field-name-field-opcoes .col0'+i+' h5').html()+' - '+split_url[3];

				url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

				$('.field-name-field-opcoes .col0'+i+' .boxlink a').attr('href',url_contato);
			}			

		}	

		// Conhecer país
		regExp = /conhecer\/pais/g;
		if (regExp.test(path)) 
		{

			var id = '2';
			var verbo = 'Quero ir';
			var produto = 'Conhecer país';	
			var subproduto = $('h3').html().trim();	
			var url_contato = '';				

			url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.col02 .link').attr('href',url_contato);
		}	

		// Conhecer cidade
		regExp = /conhecer\/cidade/g;
		if (regExp.test(path)) 
		{

			var id = '2';
			var verbo = 'Quero ir';
			var produto = 'Conhecer cidade';	
			var subproduto = $('h3').html().trim();
			var url_contato = '';				

			url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.col02 .link').attr('href',url_contato);

		}				

		// Promoção Interna
		regExp = /promocoes\/pais/;
		if (regExp.test(path)) 
		{

			var id = '2'; // Mesmo que o Curso de Idiomas
			var verbo = 'Eu quero';
			var produto = 'Promoção';	
			var subproduto = '';
			var empresa = '';
			var url_contato = '';				

			$('.view-promoces .views-row').each(function(){

				empresa = $(this).children('.views-field-field-promocoes-empresa').children('.field-content').html();
				subproduto = $(this).children('.views-field-field-promocoes-cidade').children('.field-content').html()+' - '+empresa;

				url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

				$(this).children('.views-field-field-promocoes-valor').append('<a href="'+url_contato+'">eu quero</a>');
				
			});			
		}					

	}



});;
/* Promo��es */

jQuery(document).ready(function($) {

	var url = window.location.href;	
	var getUrl = url.toString();	
	var urlArray = getUrl.split("/");
	var urlFinal = urlArray[4];	
	
	if(urlFinal != null){

	$('#block-system-main').append('<div id="promo-dez"><h4>Investimento</h4><p class="x10">INTERC&Aring;MBIO EM AT&Eacute; <span style="font-size:41px;">24x</span></p><p class="cidade">Kgic - Toronto</p><ul class="info"><li>4 semanas de cursos de ingl&ecirc;s geral(28h/semana).</li><li>4 semanas de acomoda&ccedil;&atilde;o em dormit&oacute;rio com quarto compartilhado, sem refei&ccedil;&otilde;es.</li><li>Inclui taxas da escola.</li></ul><p class="investimento">Investimento:</p>	<div class="p1"><div class="border-yellow"></div><div class="text"><span>CAD</span> 1.640,00 (R$ 3.509,60)</div></div>	<div class="p2"><div class="ou">ou</div><div class="border-yellow"></div><div class="text"><span>10x</span> de R$ 385,71*</div></div>	<div class="p3"><div class="ou">ou</div><div class="border-yellow"></div><div class="text"><span>10x</span> de R$ 385,71*</div></div><p class="aviso dois">*Valores e condi&ccedil;&otilde;es sujeitos a mudan&ccedil;a sem pr&eacute;vio aviso.</p><div class="euquero"><a href="javascript:void(0)">Eu quero</a></div></div><div class="clear"></div>');
		
	$('.view-promoces .views-row .views-field-field-promocoes-valor').each(function(){
		// $(this).append('<a href="javascript:void(0)">eu quero</a>');
	});
	
	
	trocaValores(0);
	$('.view-promoces .views-row:first').addClass('ativo');	
	
	$('.view-promoces .views-row').click(function(){	
		var index = $(this).index();
		trocaValores(index);
		$('html, body').animate({
         scrollTop: $("#promo-dez").offset().top
     }, 1000);
	});
		
	function trocaValores(indice){
	
		$('.view-promoces .views-row').removeClass("ativo");
		$('.view-promoces .views-row:eq('+indice+')').addClass("ativo");
		
		var empresa = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-empresa div').text();
		var body_texto = $('.view-promoces .views-row:eq('+indice+') .views-field-body div p').text();
		var legenda = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-legenda div').text();
		var valorCad = $('.view-promoces .views-row:eq('+indice+') .views-field-field-valor-cad div').text();
		var promocaoValor = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-valor div').text();
		var link_item = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-valor a').attr('href');		
		var cidade = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-cidade div').text();
		var preco = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-valor div').text();
		var parcela_um = $('.view-promoces .views-row:eq('+indice+') .views-field-field-parcelamento-1 div').text();
		var parcela_dois = $('.view-promoces .views-row:eq('+indice+') .views-field-field-parcelamento-2 div').text();
		

		if(parcela_um == 0 || parcela_um == ''){
			$('#promo-dez .p2').hide();			
		}else{
			array_parcUm = parcela_um.split("x");	
			$('#promo-dez .p2 .text').empty().append('<span>'+array_parcUm[0]+'x</span>'+array_parcUm[1]);
			$('#promo-dez .p2').show();
		}
		if(parcela_dois == 0 || parcela_dois == ''){
			$('#promo-dez .p3').hide();			
		}else{
			array_parcDois = parcela_dois.split("x");
			$('#promo-dez .p3 .text').empty().append('<span>'+array_parcDois[0]+'x</span>'+array_parcDois[1]);
			$('.aviso').removeClass('dois');
			$('#promo-dez .p3').show();
		}		
		
		valorCad = valorCad.replace(".",",");		
		valorCad = valorCad.replace(" ",".");		
		
		var corpo = body_texto.split(">");
		
		$('#promo-dez .cidade').empty().append(empresa+' - '+cidade);	
		$('#promo-dez .p1 .text').empty().append('<span>'+legenda+'</span> '+valorCad+' ('+promocaoValor+')');
		$('#promo-dez .euquero a').attr('href',link_item);		
		
		$('#promo-dez .info').empty();
		for(x=0; x<corpo.length; x++){
			$('#promo-dez .info').append('<li>'+corpo[x]+'</li>');
		}
		
	}
	
	}else{		
	
		$('.view-promooes-destaque-pagina .views-row').each(function(){
			var link = $(this).find('.views-field-field-promopagina-link div a').attr('href');	
			var conteudo_imagem = $(this).find('.views-field-field-promopagina-imagem div').html();
			$(this).find('.views-field-field-promopagina-imagem div').empty().append('<a href="'+link+'">'+conteudo_imagem+'</a>');			
		});		
	}
	
});;
