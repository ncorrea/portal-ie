/*
	Masked Input plugin for jQuery
	Copyright (c) 2007-2013 Josh Bush (digitalbush.com)
	Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
	Version: 1.3.1
*/
(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);;
/*
 * jQuery FlexSlider v2.1
 * http://www.woothemes.com/flexslider/
 *
 * Copyright 2012 WooThemes
 * Free to use under the GPLv2 license.
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Contributing author: Tyler Smith (@mbmufffin)
 */

;(function ($) {

  //FlexSlider: Object Instance
  $.flexslider = function(el, options) {
    var slider = $(el),
        vars = $.extend({}, $.flexslider.defaults, options),
        namespace = vars.namespace,
        touch = ("ontouchstart" in window) || window.DocumentTouch && document instanceof DocumentTouch,
        eventType = (touch) ? "touchend" : "click",
        vertical = vars.direction === "vertical",
        reverse = vars.reverse,
        carousel = (vars.itemWidth > 0),
        fade = vars.animation === "fade",
        asNav = vars.asNavFor !== "",
        methods = {};

    // Store a reference to the slider object
    $.data(el, "flexslider", slider);

    // Privat slider methods
    methods = {
      init: function() {
        slider.animating = false;
        slider.currentSlide = vars.startAt;
        slider.animatingTo = slider.currentSlide;
        slider.atEnd = (slider.currentSlide === 0 || slider.currentSlide === slider.last);
        slider.containerSelector = vars.selector.substr(0,vars.selector.search(' '));
        slider.slides = $(vars.selector, slider);
        slider.container = $(slider.containerSelector, slider);
        slider.count = slider.slides.length;
        // SYNC:
        slider.syncExists = $(vars.sync).length > 0;
        // SLIDE:
        if (vars.animation === "slide") vars.animation = "swing";
        slider.prop = (vertical) ? "top" : "marginLeft";
        slider.args = {};
        // SLIDESHOW:
        slider.manualPause = false;
        // TOUCH/USECSS:
        slider.transitions = !vars.video && !fade && vars.useCSS && (function() {
          var obj = document.createElement('div'),
              props = ['perspectiveProperty', 'WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
          for (var i in props) {
            if ( obj.style[ props[i] ] !== undefined ) {
              slider.pfx = props[i].replace('Perspective','').toLowerCase();
              slider.prop = "-" + slider.pfx + "-transform";
              return true;
            }
          }
          return false;
        }());
        // CONTROLSCONTAINER:
        if (vars.controlsContainer !== "") slider.controlsContainer = $(vars.controlsContainer).length > 0 && $(vars.controlsContainer);
        // MANUAL:
        if (vars.manualControls !== "") slider.manualControls = $(vars.manualControls).length > 0 && $(vars.manualControls);

        // RANDOMIZE:
        if (vars.randomize) {
          slider.slides.sort(function() { return (Math.round(Math.random())-0.5); });
          slider.container.empty().append(slider.slides);
        }

        slider.doMath();

        // ASNAV:
        if (asNav) methods.asNav.setup();

        // INIT
        slider.setup("init");

        // CONTROLNAV:
        if (vars.controlNav) methods.controlNav.setup();

        // DIRECTIONNAV:
        if (vars.directionNav) methods.directionNav.setup();

        // KEYBOARD:
        if (vars.keyboard && ($(slider.containerSelector).length === 1 || vars.multipleKeyboard)) {
          $(document).bind('keyup', function(event) {
            var keycode = event.keyCode;
            if (!slider.animating && (keycode === 39 || keycode === 37)) {
              var target = (keycode === 39) ? slider.getTarget('next') :
                           (keycode === 37) ? slider.getTarget('prev') : false;
              slider.flexAnimate(target, vars.pauseOnAction);
            }
          });
        }
        // MOUSEWHEEL:
        if (vars.mousewheel) {
          slider.bind('mousewheel', function(event, delta, deltaX, deltaY) {
            event.preventDefault();
            var target = (delta < 0) ? slider.getTarget('next') : slider.getTarget('prev');
            slider.flexAnimate(target, vars.pauseOnAction);
          });
        }

        // PAUSEPLAY
        if (vars.pausePlay) methods.pausePlay.setup();

        // SLIDSESHOW
        if (vars.slideshow) {
          if (vars.pauseOnHover) {
            slider.hover(function() {
              if (!slider.manualPlay && !slider.manualPause) slider.pause();
            }, function() {
              if (!slider.manualPause && !slider.manualPlay) slider.play();
            });
          }
          // initialize animation
          (vars.initDelay > 0) ? setTimeout(slider.play, vars.initDelay) : slider.play();
        }

        // TOUCH
        if (touch && vars.touch) methods.touch();

        // FADE&&SMOOTHHEIGHT || SLIDE:
        if (!fade || (fade && vars.smoothHeight)) $(window).bind("resize focus", methods.resize);


        // API: start() Callback
        setTimeout(function(){
          vars.start(slider);
        }, 200);
      },
      asNav: {
        setup: function() {
          slider.asNav = true;
          slider.animatingTo = Math.floor(slider.currentSlide/slider.move);
          slider.currentItem = slider.currentSlide;
          slider.slides.removeClass(namespace + "active-slide").eq(slider.currentItem).addClass(namespace + "active-slide");
          slider.slides.click(function(e){
            e.preventDefault();
            var $slide = $(this),
                target = $slide.index();
            if (!$(vars.asNavFor).data('flexslider').animating && !$slide.hasClass('active')) {
              slider.direction = (slider.currentItem < target) ? "next" : "prev";
              slider.flexAnimate(target, vars.pauseOnAction, false, true, true);
            }
          });
        }
      },
      controlNav: {
        setup: function() {
          if (!slider.manualControls) {
            methods.controlNav.setupPaging();
          } else { // MANUALCONTROLS:
            methods.controlNav.setupManual();
          }
        },
        setupPaging: function() {
          var type = (vars.controlNav === "thumbnails") ? 'control-thumbs' : 'control-paging',
              j = 1,
              item;

          slider.controlNavScaffold = $('<ol class="'+ namespace + 'control-nav ' + namespace + type + '"></ol>');

          if (slider.pagingCount > 1) {
            for (var i = 0; i < slider.pagingCount; i++) {
              item = (vars.controlNav === "thumbnails") ? '<img src="' + slider.slides.eq(i).attr("data-thumb") + '"/>' : '<a>' + j + '</a>';
              slider.controlNavScaffold.append('<li>' + item + '</li>');
              j++;
            }
          }

          // CONTROLSCONTAINER:
          (slider.controlsContainer) ? $(slider.controlsContainer).append(slider.controlNavScaffold) : slider.append(slider.controlNavScaffold);
          methods.controlNav.set();

          methods.controlNav.active();

          slider.controlNavScaffold.delegate('a, img', eventType, function(event) {
            event.preventDefault();
            var $this = $(this),
                target = slider.controlNav.index($this);

            if (!$this.hasClass(namespace + 'active')) {
              slider.direction = (target > slider.currentSlide) ? "next" : "prev";
              slider.flexAnimate(target, vars.pauseOnAction);
            }
          });
          // Prevent iOS click event bug
          if (touch) {
            slider.controlNavScaffold.delegate('a', "click touchstart", function(event) {
              event.preventDefault();
            });
          }
        },
        setupManual: function() {
          slider.controlNav = slider.manualControls;
          methods.controlNav.active();

          slider.controlNav.live(eventType, function(event) {
            event.preventDefault();
            var $this = $(this),
                target = slider.controlNav.index($this);

            if (!$this.hasClass(namespace + 'active')) {
              (target > slider.currentSlide) ? slider.direction = "next" : slider.direction = "prev";
              slider.flexAnimate(target, vars.pauseOnAction);
            }
          });
          // Prevent iOS click event bug
          if (touch) {
            slider.controlNav.live("click touchstart", function(event) {
              event.preventDefault();
            });
          }
        },
        set: function() {
          var selector = (vars.controlNav === "thumbnails") ? 'img' : 'a';
          slider.controlNav = $('.' + namespace + 'control-nav li ' + selector, (slider.controlsContainer) ? slider.controlsContainer : slider);
        },
        active: function() {
          slider.controlNav.removeClass(namespace + "active").eq(slider.animatingTo).addClass(namespace + "active");
        },
        update: function(action, pos) {
          if (slider.pagingCount > 1 && action === "add") {
            slider.controlNavScaffold.append($('<li><a>' + slider.count + '</a></li>'));
          } else if (slider.pagingCount === 1) {
            slider.controlNavScaffold.find('li').remove();
          } else {
            slider.controlNav.eq(pos).closest('li').remove();
          }
          methods.controlNav.set();
          (slider.pagingCount > 1 && slider.pagingCount !== slider.controlNav.length) ? slider.update(pos, action) : methods.controlNav.active();
        }
      },
      directionNav: {
        setup: function() {
          var directionNavScaffold = $('<ul class="' + namespace + 'direction-nav"><li><a class="' + namespace + 'prev" href="#">' + vars.prevText + '</a></li><li><a class="' + namespace + 'next" href="#">' + vars.nextText + '</a></li></ul>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            $(slider.controlsContainer).append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider.controlsContainer);
          } else {
            slider.append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider);
          }

          methods.directionNav.update();

          slider.directionNav.bind(eventType, function(event) {
            event.preventDefault();
            var target = ($(this).hasClass(namespace + 'next')) ? slider.getTarget('next') : slider.getTarget('prev');
            slider.flexAnimate(target, vars.pauseOnAction);
          });
          // Prevent iOS click event bug
          if (touch) {
            slider.directionNav.bind("click touchstart", function(event) {
              event.preventDefault();
            });
          }
        },
        update: function() {
          var disabledClass = namespace + 'disabled';
          if (slider.pagingCount === 1) {
            slider.directionNav.addClass(disabledClass);
          } else if (!vars.animationLoop) {
            if (slider.animatingTo === 0) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "prev").addClass(disabledClass);
            } else if (slider.animatingTo === slider.last) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "next").addClass(disabledClass);
            } else {
              slider.directionNav.removeClass(disabledClass);
            }
          } else {
            slider.directionNav.removeClass(disabledClass);
          }
        }
      },
      pausePlay: {
        setup: function() {
          var pausePlayScaffold = $('<div class="' + namespace + 'pauseplay"><a></a></div>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            slider.controlsContainer.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider.controlsContainer);
          } else {
            slider.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider);
          }

          methods.pausePlay.update((vars.slideshow) ? namespace + 'pause' : namespace + 'play');

          slider.pausePlay.bind(eventType, function(event) {
            event.preventDefault();
            if ($(this).hasClass(namespace + 'pause')) {
              slider.manualPause = true;
              slider.manualPlay = false;
              slider.pause();
            } else {
              slider.manualPause = false;
              slider.manualPlay = true;
              slider.play();
            }
          });
          // Prevent iOS click event bug
          if (touch) {
            slider.pausePlay.bind("click touchstart", function(event) {
              event.preventDefault();
            });
          }
        },
        update: function(state) {
          (state === "play") ? slider.pausePlay.removeClass(namespace + 'pause').addClass(namespace + 'play').text(vars.playText) : slider.pausePlay.removeClass(namespace + 'play').addClass(namespace + 'pause').text(vars.pauseText);
        }
      },
      touch: function() {
        var startX,
          startY,
          offset,
          cwidth,
          dx,
          startT,
          scrolling = false;

        el.addEventListener('touchstart', onTouchStart, false);
        function onTouchStart(e) {
          if (slider.animating) {
            e.preventDefault();
          } else if (e.touches.length === 1) {
            slider.pause();
            // CAROUSEL:
            cwidth = (vertical) ? slider.h : slider. w;
            startT = Number(new Date());
            // CAROUSEL:
            offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                     (carousel && reverse) ? slider.limit - (((slider.itemW + vars.itemMargin) * slider.move) * slider.animatingTo) :
                     (carousel && slider.currentSlide === slider.last) ? slider.limit :
                     (carousel) ? ((slider.itemW + vars.itemMargin) * slider.move) * slider.currentSlide :
                     (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
            startX = (vertical) ? e.touches[0].pageY : e.touches[0].pageX;
            startY = (vertical) ? e.touches[0].pageX : e.touches[0].pageY;

            el.addEventListener('touchmove', onTouchMove, false);
            el.addEventListener('touchend', onTouchEnd, false);
          }
        }

        function onTouchMove(e) {
          dx = (vertical) ? startX - e.touches[0].pageY : startX - e.touches[0].pageX;
          scrolling = (vertical) ? (Math.abs(dx) < Math.abs(e.touches[0].pageX - startY)) : (Math.abs(dx) < Math.abs(e.touches[0].pageY - startY));

          if (!scrolling || Number(new Date()) - startT > 500) {
            e.preventDefault();
            if (!fade && slider.transitions) {
              if (!vars.animationLoop) {
                dx = dx/((slider.currentSlide === 0 && dx < 0 || slider.currentSlide === slider.last && dx > 0) ? (Math.abs(dx)/cwidth+2) : 1);
              }
              slider.setProps(offset + dx, "setTouch");
            }
          }
        }

        function onTouchEnd(e) {
          // finish the touch by undoing the touch session
          el.removeEventListener('touchmove', onTouchMove, false);

          if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
            var updateDx = (reverse) ? -dx : dx,
                target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

            if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth/2)) {
              slider.flexAnimate(target, vars.pauseOnAction);
            } else {
              if (!fade) slider.flexAnimate(slider.currentSlide, vars.pauseOnAction, true);
            }
          }
          el.removeEventListener('touchend', onTouchEnd, false);
          startX = null;
          startY = null;
          dx = null;
          offset = null;
        }
      },
      resize: function() {
        if (!slider.animating && slider.is(':visible')) {
          if (!carousel) slider.doMath();

          if (fade) {
            // SMOOTH HEIGHT:
            methods.smoothHeight();
          } else if (carousel) { //CAROUSEL:
            slider.slides.width(slider.computedW);
            slider.update(slider.pagingCount);
            slider.setProps();
          }
          else if (vertical) { //VERTICAL:
            slider.viewport.height(slider.h);
            slider.setProps(slider.h, "setTotal");
          } else {
            // SMOOTH HEIGHT:
            if (vars.smoothHeight) methods.smoothHeight();
            slider.newSlides.width(slider.computedW);
            slider.setProps(slider.computedW, "setTotal");
          }
        }
      },
      smoothHeight: function(dur) {
        if (!vertical || fade) {
          var $obj = (fade) ? slider : slider.viewport;
          (dur) ? $obj.animate({"height": slider.slides.eq(slider.animatingTo).height()}, dur) : $obj.height(slider.slides.eq(slider.animatingTo).height());
        }
      },
      sync: function(action) {
        var $obj = $(vars.sync).data("flexslider"),
            target = slider.animatingTo;

        switch (action) {
          case "animate": $obj.flexAnimate(target, vars.pauseOnAction, false, true); break;
          case "play": if (!$obj.playing && !$obj.asNav) { $obj.play(); } break;
          case "pause": $obj.pause(); break;
        }
      }
    }

    // public methods
    slider.flexAnimate = function(target, pause, override, withSync, fromNav) {

      if (asNav && slider.pagingCount === 1) slider.direction = (slider.currentItem < target) ? "next" : "prev";

      if (!slider.animating && (slider.canAdvance(target, fromNav) || override) && slider.is(":visible")) {
        if (asNav && withSync) {
          var master = $(vars.asNavFor).data('flexslider');
          slider.atEnd = target === 0 || target === slider.count - 1;
          master.flexAnimate(target, true, false, true, fromNav);
          slider.direction = (slider.currentItem < target) ? "next" : "prev";
          master.direction = slider.direction;

          if (Math.ceil((target + 1)/slider.visible) - 1 !== slider.currentSlide && target !== 0) {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            target = Math.floor(target/slider.visible);
          } else {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            return false;
          }
        }

        slider.animating = true;
        slider.animatingTo = target;
        // API: before() animation Callback
        vars.before(slider);

        // SLIDESHOW:
        if (pause) slider.pause();

        // SYNC:
        if (slider.syncExists && !fromNav) methods.sync("animate");

        // CONTROLNAV
        if (vars.controlNav) methods.controlNav.active();

        // !CAROUSEL:
        // CANDIDATE: slide active class (for add/remove slide)
        if (!carousel) slider.slides.removeClass(namespace + 'active-slide').eq(target).addClass(namespace + 'active-slide');

        // INFINITE LOOP:
        // CANDIDATE: atEnd
        slider.atEnd = target === 0 || target === slider.last;

        // DIRECTIONNAV:
        if (vars.directionNav) methods.directionNav.update();

        if (target === slider.last) {
          // API: end() of cycle Callback
          vars.end(slider);
          // SLIDESHOW && !INFINITE LOOP:
          if (!vars.animationLoop) slider.pause();
        }

        // SLIDE:
        if (!fade) {
          var dimension = (vertical) ? slider.slides.filter(':first').height() : slider.computedW,
              margin, slideString, calcNext;

          // INFINITE LOOP / REVERSE:
          if (carousel) {
            margin = (vars.itemWidth > slider.w) ? vars.itemMargin * 2 : vars.itemMargin;
            calcNext = ((slider.itemW + margin) * slider.move) * slider.animatingTo;
            slideString = (calcNext > slider.limit && slider.visible !== 1) ? slider.limit : calcNext;
          } else if (slider.currentSlide === 0 && target === slider.count - 1 && vars.animationLoop && slider.direction !== "next") {
            slideString = (reverse) ? (slider.count + slider.cloneOffset) * dimension : 0;
          } else if (slider.currentSlide === slider.last && target === 0 && vars.animationLoop && slider.direction !== "prev") {
            slideString = (reverse) ? 0 : (slider.count + 1) * dimension;
          } else {
            slideString = (reverse) ? ((slider.count - 1) - target + slider.cloneOffset) * dimension : (target + slider.cloneOffset) * dimension;
          }
          slider.setProps(slideString, "", vars.animationSpeed);
          if (slider.transitions) {
            if (!vars.animationLoop || !slider.atEnd) {
              slider.animating = false;
              slider.currentSlide = slider.animatingTo;
            }
            slider.container.unbind("webkitTransitionEnd transitionend");
            slider.container.bind("webkitTransitionEnd transitionend", function() {
              slider.wrapup(dimension);
            });
          } else {
            slider.container.animate(slider.args, vars.animationSpeed, vars.easing, function(){
              slider.wrapup(dimension);
            });
          }
        } else { // FADE:
          if (!touch) {
            slider.slides.eq(slider.currentSlide).fadeOut(vars.animationSpeed, vars.easing);
            slider.slides.eq(target).fadeIn(vars.animationSpeed, vars.easing, slider.wrapup);
          } else {
            slider.slides.eq(slider.currentSlide).css({ "opacity": 0, "zIndex": 1 });
            slider.slides.eq(target).css({ "opacity": 1, "zIndex": 2 });

            slider.slides.unbind("webkitTransitionEnd transitionend");
            slider.slides.eq(slider.currentSlide).bind("webkitTransitionEnd transitionend", function() {
              // API: after() animation Callback
              vars.after(slider);
            });

            slider.animating = false;
            slider.currentSlide = slider.animatingTo;
          }
        }
        // SMOOTH HEIGHT:
        if (vars.smoothHeight) methods.smoothHeight(vars.animationSpeed);
      }
    }
    slider.wrapup = function(dimension) {
      // SLIDE:
      if (!fade && !carousel) {
        if (slider.currentSlide === 0 && slider.animatingTo === slider.last && vars.animationLoop) {
          slider.setProps(dimension, "jumpEnd");
        } else if (slider.currentSlide === slider.last && slider.animatingTo === 0 && vars.animationLoop) {
          slider.setProps(dimension, "jumpStart");
        }
      }
      slider.animating = false;
      slider.currentSlide = slider.animatingTo;
      // API: after() animation Callback
      vars.after(slider);
    }

    // SLIDESHOW:
    slider.animateSlides = function() {
      if (!slider.animating) slider.flexAnimate(slider.getTarget("next"));
    }
    // SLIDESHOW:
    slider.pause = function() {
      clearInterval(slider.animatedSlides);
      slider.playing = false;
      // PAUSEPLAY:
      if (vars.pausePlay) methods.pausePlay.update("play");
      // SYNC:
      if (slider.syncExists) methods.sync("pause");
    }
    // SLIDESHOW:
    slider.play = function() {
      slider.animatedSlides = setInterval(slider.animateSlides, vars.slideshowSpeed);
      slider.playing = true;
      // PAUSEPLAY:
      if (vars.pausePlay) methods.pausePlay.update("pause");
      // SYNC:
      if (slider.syncExists) methods.sync("play");
    }
    slider.canAdvance = function(target, fromNav) {
      // ASNAV:
      var last = (asNav) ? slider.pagingCount - 1 : slider.last;
      return (fromNav) ? true :
             (asNav && slider.currentItem === slider.count - 1 && target === 0 && slider.direction === "prev") ? true :
             (asNav && slider.currentItem === 0 && target === slider.pagingCount - 1 && slider.direction !== "next") ? false :
             (target === slider.currentSlide && !asNav) ? false :
             (vars.animationLoop) ? true :
             (slider.atEnd && slider.currentSlide === 0 && target === last && slider.direction !== "next") ? false :
             (slider.atEnd && slider.currentSlide === last && target === 0 && slider.direction === "next") ? false :
             true;
    }
    slider.getTarget = function(dir) {
      slider.direction = dir;
      if (dir === "next") {
        return (slider.currentSlide === slider.last) ? 0 : slider.currentSlide + 1;
      } else {
        return (slider.currentSlide === 0) ? slider.last : slider.currentSlide - 1;
      }
    }

    // SLIDE:
    slider.setProps = function(pos, special, dur) {
      var target = (function() {
        var posCheck = (pos) ? pos : ((slider.itemW + vars.itemMargin) * slider.move) * slider.animatingTo,
            posCalc = (function() {
              if (carousel) {
                return (special === "setTouch") ? pos :
                       (reverse && slider.animatingTo === slider.last) ? 0 :
                       (reverse) ? slider.limit - (((slider.itemW + vars.itemMargin) * slider.move) * slider.animatingTo) :
                       (slider.animatingTo === slider.last) ? slider.limit : posCheck;
              } else {
                switch (special) {
                  case "setTotal": return (reverse) ? ((slider.count - 1) - slider.currentSlide + slider.cloneOffset) * pos : (slider.currentSlide + slider.cloneOffset) * pos;
                  case "setTouch": return (reverse) ? pos : pos;
                  case "jumpEnd": return (reverse) ? pos : slider.count * pos;
                  case "jumpStart": return (reverse) ? slider.count * pos : pos;
                  default: return pos;
                }
              }
            }());
            return (posCalc * -1) + "px";
          }());

      if (slider.transitions) {
        target = (vertical) ? "translate3d(0," + target + ",0)" : "translate3d(" + target + ",0,0)";
        dur = (dur !== undefined) ? (dur/1000) + "s" : "0s";
        slider.container.css("-" + slider.pfx + "-transition-duration", dur);
      }

      slider.args[slider.prop] = target;
      if (slider.transitions || dur === undefined) slider.container.css(slider.args);
    }

    slider.setup = function(type) {
      // SLIDE:
      if (!fade) {
        var sliderOffset, arr;

        if (type === "init") {
          slider.viewport = $('<div class="' + namespace + 'viewport"></div>').css({"overflow": "hidden", "position": "relative"}).appendTo(slider).append(slider.container);
          // INFINITE LOOP:
          slider.cloneCount = 0;
          slider.cloneOffset = 0;
          // REVERSE:
          if (reverse) {
            arr = $.makeArray(slider.slides).reverse();
            slider.slides = $(arr);
            slider.container.empty().append(slider.slides);
          }
        }
        // INFINITE LOOP && !CAROUSEL:
        if (vars.animationLoop && !carousel) {
          slider.cloneCount = 2;
          slider.cloneOffset = 1;
          // clear out old clones
          if (type !== "init") slider.container.find('.clone').remove();
          slider.container.append(slider.slides.first().clone().addClass('clone')).prepend(slider.slides.last().clone().addClass('clone'));
        }
        slider.newSlides = $(vars.selector, slider);

        sliderOffset = (reverse) ? slider.count - 1 - slider.currentSlide + slider.cloneOffset : slider.currentSlide + slider.cloneOffset;
        // VERTICAL:
        if (vertical && !carousel) {
          slider.container.height((slider.count + slider.cloneCount) * 200 + "%").css("position", "absolute").width("100%");
          setTimeout(function(){
            slider.newSlides.css({"display": "block"});
            slider.doMath();
            slider.viewport.height(slider.h);
            slider.setProps(sliderOffset * slider.h, "init");
          }, (type === "init") ? 100 : 0);
        } else {
          slider.container.width((slider.count + slider.cloneCount) * 200 + "%");
          slider.setProps(sliderOffset * slider.computedW, "init");
          setTimeout(function(){
            slider.doMath();
            slider.newSlides.css({"width": slider.computedW, "float": "left", "display": "block"});
            // SMOOTH HEIGHT:
            if (vars.smoothHeight) methods.smoothHeight();
          }, (type === "init") ? 100 : 0);
        }
      } else { // FADE:
        slider.slides.css({"width": "100%", "float": "left", "marginRight": "-100%", "position": "relative"});
        if (type === "init") {
          if (!touch) {
            slider.slides.eq(slider.currentSlide).fadeIn(vars.animationSpeed, vars.easing);
          } else {
            slider.slides.css({ "opacity": 0, "display": "block", "webkitTransition": "opacity " + vars.animationSpeed / 1000 + "s ease", "zIndex": 1 }).eq(slider.currentSlide).css({ "opacity": 1, "zIndex": 2});
          }
        }
        // SMOOTH HEIGHT:
        if (vars.smoothHeight) methods.smoothHeight();
      }
      // !CAROUSEL:
      // CANDIDATE: active slide
      if (!carousel) slider.slides.removeClass(namespace + "active-slide").eq(slider.currentSlide).addClass(namespace + "active-slide");
    }

    slider.doMath = function() {
      var slide = slider.slides.first(),
          slideMargin = vars.itemMargin,
          minItems = vars.minItems,
          maxItems = vars.maxItems;

      slider.w = slider.width();
      slider.h = slide.height();
      slider.boxPadding = slide.outerWidth() - slide.width();

      // CAROUSEL:
      if (carousel) {
        slider.itemT = vars.itemWidth + slideMargin;
        slider.minW = (minItems) ? minItems * slider.itemT : slider.w;
        slider.maxW = (maxItems) ? maxItems * slider.itemT : slider.w;
        slider.itemW = (slider.minW > slider.w) ? (slider.w - (slideMargin * minItems))/minItems :
                       (slider.maxW < slider.w) ? (slider.w - (slideMargin * maxItems))/maxItems :
                       (vars.itemWidth > slider.w) ? slider.w : vars.itemWidth;
        slider.visible = Math.floor(slider.w/(slider.itemW + slideMargin));
        slider.move = (vars.move > 0 && vars.move < slider.visible ) ? vars.move : slider.visible;
        slider.pagingCount = Math.ceil(((slider.count - slider.visible)/slider.move) + 1);
        slider.last =  slider.pagingCount - 1;
        slider.limit = (slider.pagingCount === 1) ? 0 :
                       (vars.itemWidth > slider.w) ? ((slider.itemW + (slideMargin * 2)) * slider.count) - slider.w - slideMargin : ((slider.itemW + slideMargin) * slider.count) - slider.w - slideMargin;
      } else {
        slider.itemW = slider.w;
        slider.pagingCount = slider.count;
        slider.last = slider.count - 1;
      }
      slider.computedW = slider.itemW - slider.boxPadding;
    }

    slider.update = function(pos, action) {
      slider.doMath();

      // update currentSlide and slider.animatingTo if necessary
      if (!carousel) {
        if (pos < slider.currentSlide) {
          slider.currentSlide += 1;
        } else if (pos <= slider.currentSlide && pos !== 0) {
          slider.currentSlide -= 1;
        }
        slider.animatingTo = slider.currentSlide;
      }

      // update controlNav
      if (vars.controlNav && !slider.manualControls) {
        if ((action === "add" && !carousel) || slider.pagingCount > slider.controlNav.length) {
          methods.controlNav.update("add");
        } else if ((action === "remove" && !carousel) || slider.pagingCount < slider.controlNav.length) {
          if (carousel && slider.currentSlide > slider.last) {
            slider.currentSlide -= 1;
            slider.animatingTo -= 1;
          }
          methods.controlNav.update("remove", slider.last);
        }
      }
      // update directionNav
      if (vars.directionNav) methods.directionNav.update();

    }

    slider.addSlide = function(obj, pos) {
      var $obj = $(obj);

      slider.count += 1;
      slider.last = slider.count - 1;

      // append new slide
      if (vertical && reverse) {
        (pos !== undefined) ? slider.slides.eq(slider.count - pos).after($obj) : slider.container.prepend($obj);
      } else {
        (pos !== undefined) ? slider.slides.eq(pos).before($obj) : slider.container.append($obj);
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.update(pos, "add");

      // update slider.slides
      slider.slides = $(vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      //FlexSlider: added() Callback
      vars.added(slider);
    }
    slider.removeSlide = function(obj) {
      var pos = (isNaN(obj)) ? slider.slides.index($(obj)) : obj;

      // update count
      slider.count -= 1;
      slider.last = slider.count - 1;

      // remove slide
      if (isNaN(obj)) {
        $(obj, slider.slides).remove();
      } else {
        (vertical && reverse) ? slider.slides.eq(slider.last).remove() : slider.slides.eq(obj).remove();
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.doMath();
      slider.update(pos, "remove");

      // update slider.slides
      slider.slides = $(vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      // FlexSlider: removed() Callback
      vars.removed(slider);
    }

    //FlexSlider: Initialize
    methods.init();
  }

  //FlexSlider: Default Settings
  $.flexslider.defaults = {
    namespace: "flex-",             //{NEW} String: Prefix string attached to the class of every element generated by the plugin
    selector: ".slides > li",       //{NEW} Selector: Must match a simple pattern. '{container} > {slide}' -- Ignore pattern at your own peril
    animation: "fade",              //String: Select your animation type, "fade" or "slide"
    easing: "swing",               //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
    direction: "horizontal",        //String: Select the sliding direction, "horizontal" or "vertical"
    reverse: false,                 //{NEW} Boolean: Reverse the animation direction
    animationLoop: true,             //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
    smoothHeight: false,            //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode
    startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
    slideshow: true,                //Boolean: Animate slider automatically
    slideshowSpeed: 7000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
    animationSpeed: 600,            //Integer: Set the speed of animations, in milliseconds
    initDelay: 0,                   //{NEW} Integer: Set an initialization delay, in milliseconds
    randomize: false,               //Boolean: Randomize slide order

    // Usability features
    pauseOnAction: true,            //Boolean: Pause the slideshow when interacting with control elements, highly recommended.
    pauseOnHover: false,            //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
    useCSS: true,                   //{NEW} Boolean: Slider will use CSS3 transitions if available
    touch: true,                    //{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
    video: false,                   //{NEW} Boolean: If using video in the slider, will prevent CSS3 3D Transforms to avoid graphical glitches

    // Primary Controls
    controlNav: true,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
    directionNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
    prevText: "Previous",           //String: Set the text for the "previous" directionNav item
    nextText: "Next",               //String: Set the text for the "next" directionNav item

    // Secondary Navigation
    keyboard: true,                 //Boolean: Allow slider navigating via keyboard left/right keys
    multipleKeyboard: false,        //{NEW} Boolean: Allow keyboard navigation to affect multiple sliders. Default behavior cuts out keyboard navigation with more than one slider present.
    mousewheel: false,              //{UPDATED} Boolean: Requires jquery.mousewheel.js (https://github.com/brandonaaron/jquery-mousewheel) - Allows slider navigating via mousewheel
    pausePlay: false,               //Boolean: Create pause/play dynamic element
    pauseText: "Pause",             //String: Set the text for the "pause" pausePlay item
    playText: "Play",               //String: Set the text for the "play" pausePlay item

    // Special properties
    controlsContainer: "",          //{UPDATED} jQuery Object/Selector: Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be $(".flexslider-container"). Property is ignored if given element is not found.
    manualControls: "",             //{UPDATED} jQuery Object/Selector: Declare custom control navigation. Examples would be $(".flex-control-nav li") or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
    sync: "",                       //{NEW} Selector: Mirror the actions performed on this slider with another slider. Use with care.
    asNavFor: "",                   //{NEW} Selector: Internal property exposed for turning the slider into a thumbnail navigation for another slider

    // Carousel Options
    itemWidth: 0,                   //{NEW} Integer: Box-model width of individual carousel items, including horizontal borders and padding.
    itemMargin: 0,                  //{NEW} Integer: Margin between carousel items.
    minItems: 0,                    //{NEW} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
    maxItems: 0,                    //{NEW} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
    move: 0,                        //{NEW} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.

    // Callback API
    start: function(){},            //Callback: function(slider) - Fires when the slider loads the first slide
    before: function(){},           //Callback: function(slider) - Fires asynchronously with each slider animation
    after: function(){},            //Callback: function(slider) - Fires after each slider animation completes
    end: function(){},              //Callback: function(slider) - Fires when the slider reaches the last slide (asynchronous)
    added: function(){},            //{NEW} Callback: function(slider) - Fires after a slide is added
    removed: function(){}           //{NEW} Callback: function(slider) - Fires after a slide is removed
  }


  //FlexSlider: Plugin Function
  $.fn.flexslider = function(options) {
    if (options === undefined) options = {};

    if (typeof options === "object") {
      return this.each(function() {
        var $this = $(this),
            selector = (options.selector) ? options.selector : ".slides > li",
            $slides = $this.find(selector);

        if ($slides.length === 1) {
          $slides.fadeIn(400);
          if (options.start) options.start($this);
        } else if ($this.data('flexslider') == undefined) {
          new $.flexslider(this, options);
        }
      });
    } else {
      // Helper strings to quickly perform functions on the slider
      var $slider = $(this).data('flexslider');
      switch (options) {
        case "play": $slider.play(); break;
        case "pause": $slider.pause(); break;
        case "next": $slider.flexAnimate($slider.getTarget("next"), true); break;
        case "prev":
        case "previous": $slider.flexAnimate($slider.getTarget("prev"), true); break;
        default: if (typeof options === "number") $slider.flexAnimate(options, true);
      }
    }
  }

})(jQuery);
;
/*mousewheel*/
(function(a){function d(b){var c=b||window.event,d=[].slice.call(arguments,1),e=0,f=!0,g=0,h=0;return b=a.event.fix(c),b.type="mousewheel",c.wheelDelta&&(e=c.wheelDelta/120),c.detail&&(e=-c.detail/3),h=e,c.axis!==undefined&&c.axis===c.HORIZONTAL_AXIS&&(h=0,g=-1*e),c.wheelDeltaY!==undefined&&(h=c.wheelDeltaY/120),c.wheelDeltaX!==undefined&&(g=-1*c.wheelDeltaX/120),d.unshift(b,e,g,h),(a.event.dispatch||a.event.handle).apply(this,d)}var b=["DOMMouseScroll","mousewheel"];if(a.event.fixHooks)for(var c=b.length;c;)a.event.fixHooks[b[--c]]=a.event.mouseHooks;a.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var a=b.length;a;)this.addEventListener(b[--a],d,!1);else this.onmousewheel=d},teardown:function(){if(this.removeEventListener)for(var a=b.length;a;)this.removeEventListener(b[--a],d,!1);else this.onmousewheel=null}},a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})})(jQuery);
/*TweenLite*/
(function(a){"use strict";var e,f,g,h,b=a.GreenSockGlobals||a,c=function(a){var e,c=a.split("."),d=b;for(e=0;c.length>e;e++)d[c[e]]=d=d[c[e]]||{};return d},d=c("com.greensock"),i={},j=function(d,e,f,g){this.sc=i[d]?i[d].sc:[],i[d]=this,this.gsClass=null,this.func=f;var h=[];this.check=function(k){for(var n,o,p,q,l=e.length,m=l;--l>-1;)(n=i[e[l]]||new j(e[l],[])).gsClass?(h[l]=n.gsClass,m--):k&&n.sc.push(this);if(0===m&&f)for(o=("com.greensock."+d).split("."),p=o.pop(),q=c(o.join("."))[p]=this.gsClass=f.apply(f,h),g&&(b[p]=q,"function"==typeof define&&define.amd?define((a.GreenSockAMDPath?a.GreenSockAMDPath+"/":"")+d.split(".").join("/"),[],function(){return q}):"undefined"!=typeof module&&module.exports&&(module.exports=q)),l=0;this.sc.length>l;l++)this.sc[l].check()},this.check(!0)},k=a._gsDefine=function(a,b,c,d){return new j(a,b,c,d)},l=d._class=function(a,b,c){return b=b||function(){},k(a,[],function(){return b},c),b},m=[0,0,1,1],n=[],o=l("easing.Ease",function(a,b,c,d){this._func=a,this._type=c||0,this._power=d||0,this._params=b?m.concat(b):m},!0),p=o.map={},q=o.register=function(a,b,c,e){for(var i,j,k,m,f=b.split(","),g=f.length,h=(c||"easeIn,easeOut,easeInOut").split(",");--g>-1;)for(j=f[g],i=e?l("easing."+j,null,!0):d.easing[j]||{},k=h.length;--k>-1;)m=h[k],p[j+"."+m]=p[m+j]=i[m]=a.getRatio?a:a[m]||new a};for(g=o.prototype,g._calcEnd=!1,g.getRatio=function(a){if(this._func)return this._params[0]=a,this._func.apply(null,this._params);var b=this._type,c=this._power,d=1===b?1-a:2===b?a:.5>a?2*a:2*(1-a);return 1===c?d*=d:2===c?d*=d*d:3===c?d*=d*d*d:4===c&&(d*=d*d*d*d),1===b?1-d:2===b?d:.5>a?d/2:1-d/2},e=["Linear","Quad","Cubic","Quart","Quint,Strong"],f=e.length;--f>-1;)g=e[f]+",Power"+f,q(new o(null,null,1,f),g,"easeOut",!0),q(new o(null,null,2,f),g,"easeIn"+(0===f?",easeNone":"")),q(new o(null,null,3,f),g,"easeInOut");p.linear=d.easing.Linear.easeIn,p.swing=d.easing.Quad.easeInOut;var r=l("events.EventDispatcher",function(a){this._listeners={},this._eventTarget=a||this});g=r.prototype,g.addEventListener=function(a,b,c,d,e){e=e||0;var h,i,f=this._listeners[a],g=0;for(null==f&&(this._listeners[a]=f=[]),i=f.length;--i>-1;)h=f[i],h.c===b?f.splice(i,1):0===g&&e>h.pr&&(g=i+1);f.splice(g,0,{c:b,s:c,up:d,pr:e})},g.removeEventListener=function(a,b){var d,c=this._listeners[a];if(c)for(d=c.length;--d>-1;)if(c[d].c===b)return c.splice(d,1),void 0},g.dispatchEvent=function(a){var b=this._listeners[a];if(b)for(var e,c=b.length,d=this._eventTarget;--c>-1;)e=b[c],e.up?e.c.call(e.s||d,{type:a,target:d}):e.c.call(e.s||d)};var s=a.requestAnimationFrame,t=a.cancelAnimationFrame,u=Date.now||function(){return(new Date).getTime()};for(e=["ms","moz","webkit","o"],f=e.length;--f>-1&&!s;)s=a[e[f]+"RequestAnimationFrame"],t=a[e[f]+"CancelAnimationFrame"]||a[e[f]+"CancelRequestAnimationFrame"];l("Ticker",function(b,c){var g,h,i,j,k,d=this,e=u(),f=c!==!1&&s,l=function(){null!=i&&(f&&t?t(i):a.clearTimeout(i),i=null)},m=function(a){d.time=(u()-e)/1e3,(!g||d.time>=k||a===!0)&&(d.frame++,k=d.time>k?d.time+j-(d.time-k):d.time+j-.001,d.time+.001>k&&(k=d.time+.001),d.dispatchEvent("tick")),a!==!0&&(i=h(m))};r.call(d),this.time=this.frame=0,this.tick=function(){m(!0)},this.fps=function(a){return arguments.length?(g=a,j=1/(g||60),k=this.time+j,h=0===g?function(){}:f&&s?s:function(a){return setTimeout(a,1e3*(k-d.time)+1>>0||1)},l(),i=h(m),void 0):g},this.useRAF=function(a){return arguments.length?(l(),f=a,d.fps(g),void 0):f},d.fps(b),setTimeout(function(){f&&!i&&d.useRAF(!1)},1e3)}),g=d.Ticker.prototype=new d.events.EventDispatcher,g.constructor=d.Ticker;var v=l("core.Animation",function(a,b){if(this.vars=b||{},this._duration=this._totalDuration=a||0,this._delay=Number(this.vars.delay)||0,this._timeScale=1,this._active=this.vars.immediateRender===!0,this.data=this.vars.data,this._reversed=this.vars.reversed===!0,I){h||(w.tick(),h=!0);var c=this.vars.useFrames?H:I;c.add(this,c._time),this.vars.paused&&this.paused(!0)}}),w=v.ticker=new d.Ticker;g=v.prototype,g._dirty=g._gc=g._initted=g._paused=!1,g._totalTime=g._time=0,g._rawPrevTime=-1,g._next=g._last=g._onUpdate=g._timeline=g.timeline=null,g._paused=!1,g.play=function(a,b){return arguments.length&&this.seek(a,b),this.reversed(!1),this.paused(!1)},g.pause=function(a,b){return arguments.length&&this.seek(a,b),this.paused(!0)},g.resume=function(a,b){return arguments.length&&this.seek(a,b),this.paused(!1)},g.seek=function(a,b){return this.totalTime(Number(a),b!==!1)},g.restart=function(a,b){return this.reversed(!1),this.paused(!1),this.totalTime(a?-this._delay:0,b!==!1)},g.reverse=function(a,b){return arguments.length&&this.seek(a||this.totalDuration(),b),this.reversed(!0),this.paused(!1)},g.render=function(){},g.invalidate=function(){return this},g._enabled=function(a,b){return this._gc=!a,this._active=a&&!this._paused&&this._totalTime>0&&this._totalTime<this._totalDuration,b!==!0&&(a&&null==this.timeline?this._timeline.add(this,this._startTime-this._delay):a||null==this.timeline||this._timeline._remove(this,!0)),!1},g._kill=function(){return this._enabled(!1,!1)},g.kill=function(a,b){return this._kill(a,b),this},g._uncache=function(a){for(var b=a?this:this.timeline;b;)b._dirty=!0,b=b.timeline;return this},g.eventCallback=function(a,b,c,d){if(null==a)return null;if("on"===a.substr(0,2)){if(1===arguments.length)return this.vars[a];if(null==b)delete this.vars[a];else if(this.vars[a]=b,this.vars[a+"Params"]=c,this.vars[a+"Scope"]=d,c)for(var e=c.length;--e>-1;)"{self}"===c[e]&&(c=this.vars[a+"Params"]=c.concat(),c[e]=this);"onUpdate"===a&&(this._onUpdate=b)}return this},g.delay=function(a){return arguments.length?(this._timeline.smoothChildTiming&&this.startTime(this._startTime+a-this._delay),this._delay=a,this):this._delay},g.duration=function(a){return arguments.length?(this._duration=this._totalDuration=a,this._uncache(!0),this._timeline.smoothChildTiming&&this._time>0&&this._time<this._duration&&0!==a&&this.totalTime(this._totalTime*(a/this._duration),!0),this):(this._dirty=!1,this._duration)},g.totalDuration=function(a){return this._dirty=!1,arguments.length?this.duration(a):this._totalDuration},g.time=function(a,b){return arguments.length?(this._dirty&&this.totalDuration(),a>this._duration&&(a=this._duration),this.totalTime(a,b)):this._time},g.totalTime=function(a,b){if(!arguments.length)return this._totalTime;if(this._timeline){if(0>a&&(a+=this.totalDuration()),this._timeline.smoothChildTiming&&(this._dirty&&this.totalDuration(),a>this._totalDuration&&(a=this._totalDuration),this._startTime=(this._paused?this._pauseTime:this._timeline._time)-(this._reversed?this._totalDuration-a:a)/this._timeScale,this._timeline._dirty||this._uncache(!1),!this._timeline._active))for(var c=this._timeline;c._timeline;)c.totalTime(c._totalTime,!0),c=c._timeline;this._gc&&this._enabled(!0,!1),this._totalTime!==a&&this.render(a,b,!1)}return this},g.startTime=function(a){return arguments.length?(a!==this._startTime&&(this._startTime=a,this.timeline&&this.timeline._sortChildren&&this.timeline.add(this,a-this._delay)),this):this._startTime},g.timeScale=function(a){if(!arguments.length)return this._timeScale;if(a=a||1e-6,this._timeline&&this._timeline.smoothChildTiming){var b=this._pauseTime||0===this._pauseTime?this._pauseTime:this._timeline._totalTime;this._startTime=b-(b-this._startTime)*this._timeScale/a}return this._timeScale=a,this._uncache(!1)},g.reversed=function(a){return arguments.length?(a!=this._reversed&&(this._reversed=a,this.totalTime(this._totalTime,!0)),this):this._reversed},g.paused=function(a){return arguments.length?(a!=this._paused&&this._timeline&&(!a&&this._timeline.smoothChildTiming&&(this._startTime+=this._timeline.rawTime()-this._pauseTime,this._uncache(!1)),this._pauseTime=a?this._timeline.rawTime():null,this._paused=a,this._active=!this._paused&&this._totalTime>0&&this._totalTime<this._totalDuration),this._gc&&(a||this._enabled(!0,!1)),this):this._paused};var x=l("core.SimpleTimeline",function(a){v.call(this,0,a),this.autoRemoveChildren=this.smoothChildTiming=!0});g=x.prototype=new v,g.constructor=x,g.kill()._gc=!1,g._first=g._last=null,g._sortChildren=!1,g.add=function(a,b){var e,f;if(a._startTime=Number(b||0)+a._delay,a._paused&&this!==a._timeline&&(a._pauseTime=a._startTime+(this.rawTime()-a._startTime)/a._timeScale),a.timeline&&a.timeline._remove(a,!0),a.timeline=a._timeline=this,a._gc&&a._enabled(!0,!0),e=this._last,this._sortChildren)for(f=a._startTime;e&&e._startTime>f;)e=e._prev;return e?(a._next=e._next,e._next=a):(a._next=this._first,this._first=a),a._next?a._next._prev=a:this._last=a,a._prev=e,this._timeline&&this._uncache(!0),this},g.insert=g.add,g._remove=function(a,b){return a.timeline===this&&(b||a._enabled(!1,!0),a.timeline=null,a._prev?a._prev._next=a._next:this._first===a&&(this._first=a._next),a._next?a._next._prev=a._prev:this._last===a&&(this._last=a._prev),this._timeline&&this._uncache(!0)),this},g.render=function(a,b){var e,d=this._first;for(this._totalTime=this._time=this._rawPrevTime=a;d;)e=d._next,(d._active||a>=d._startTime&&!d._paused)&&(d._reversed?d.render((d._dirty?d.totalDuration():d._totalDuration)-(a-d._startTime)*d._timeScale,b,!1):d.render((a-d._startTime)*d._timeScale,b,!1)),d=e},g.rawTime=function(){return this._totalTime};var y=l("TweenLite",function(a,b,c){if(v.call(this,b,c),null==a)throw"Cannot tween an undefined reference.";this.target=a="string"!=typeof a?a:y.selector(a)||a,this._overwrite=null==this.vars.overwrite?G[y.defaultOverwrite]:"number"==typeof this.vars.overwrite?this.vars.overwrite>>0:G[this.vars.overwrite];var e,f,d=a.jquery||"function"==typeof a.each&&a[0]&&a[0].nodeType&&a[0].style;if((d||a instanceof Array)&&"number"!=typeof a[0])for(this._targets=d&&!a.slice?A(a):a.slice(0),this._propLookup=[],this._siblings=[],e=0;this._targets.length>e;e++)f=this._targets[e],f?"string"!=typeof f?"function"==typeof f.each&&f[0]&&f[0].nodeType&&f[0].style?(this._targets.splice(e--,1),this._targets=this._targets.concat(A(f))):(this._siblings[e]=J(f,this,!1),1===this._overwrite&&this._siblings[e].length>1&&K(f,this,null,1,this._siblings[e])):(f=this._targets[e--]=y.selector(f),"string"==typeof f&&this._targets.splice(e+1,1)):this._targets.splice(e--,1);else this._propLookup={},this._siblings=J(a,this,!1),1===this._overwrite&&this._siblings.length>1&&K(a,this,null,1,this._siblings);(this.vars.immediateRender||0===b&&0===this._delay&&this.vars.immediateRender!==!1)&&this.render(-this._delay,!1,!0)},!0),z=function(a){return"function"==typeof a.each&&a[0]&&a[0].nodeType&&a[0].style},A=function(a){var b=[];return a.each(function(){b.push(this)}),b},B=function(a,b){var d,c={};for(d in a)F[d]||d in b&&"x"!==d&&"y"!==d&&"width"!==d&&"height"!==d||!(!C[d]||C[d]&&C[d]._autoCSS)||(c[d]=a[d],delete a[d]);a.css=c};g=y.prototype=new v,g.constructor=y,g.kill()._gc=!1,g.ratio=0,g._firstPT=g._targets=g._overwrittenProps=null,g._notifyPluginsOfEnabled=!1,y.version="1.8.4",y.defaultEase=g._ease=new o(null,null,1,1),y.defaultOverwrite="auto",y.ticker=w,y.selector=a.$||a.jQuery||function(b){return a.$?(y.selector=a.$,a.$(b)):a.document?a.document.getElementById("#"===b.charAt(0)?b.substr(1):b):b};var C=y._plugins={},D=y._tweenLookup={},E=0,F={ease:1,delay:1,overwrite:1,onComplete:1,onCompleteParams:1,onCompleteScope:1,useFrames:1,runBackwards:1,startAt:1,onUpdate:1,onUpdateParams:1,onUpdateScope:1,onStart:1,onStartParams:1,onStartScope:1,onReverseComplete:1,onReverseCompleteParams:1,onReverseCompleteScope:1,onRepeat:1,onRepeatParams:1,onRepeatScope:1,easeParams:1,yoyo:1,orientToBezier:1,immediateRender:1,repeat:1,repeatDelay:1,data:1,paused:1,reversed:1,autoCSS:1},G={none:0,all:1,auto:2,concurrent:3,allOnStart:4,preexisting:5,"true":1,"false":0},H=v._rootFramesTimeline=new x,I=v._rootTimeline=new x;I._startTime=w.time,H._startTime=w.frame,I._active=H._active=!0,v._updateRoot=function(){if(I.render((w.time-I._startTime)*I._timeScale,!1,!1),H.render((w.frame-H._startTime)*H._timeScale,!1,!1),!(w.frame%120)){var a,b,c;for(c in D){for(b=D[c].tweens,a=b.length;--a>-1;)b[a]._gc&&b.splice(a,1);0===b.length&&delete D[c]}}},w.addEventListener("tick",v._updateRoot);var J=function(a,b,c){var e,f,d=a._gsTweenID;if(D[d||(a._gsTweenID=d="t"+E++)]||(D[d]={target:a,tweens:[]}),b&&(e=D[d].tweens,e[f=e.length]=b,c))for(;--f>-1;)e[f]===b&&e.splice(f,1);return D[d].tweens},K=function(a,b,c,d,e){var f,g,h,i;if(1===d||d>=4){for(i=e.length,f=0;i>f;f++)if((h=e[f])!==b)h._gc||h._enabled(!1,!1)&&(g=!0);else if(5===d)break;return g}var n,j=b._startTime+1e-10,k=[],l=0,m=0===b._duration;for(f=e.length;--f>-1;)(h=e[f])===b||h._gc||h._paused||(h._timeline!==b._timeline?(n=n||L(b,0,m),0===L(h,n,m)&&(k[l++]=h)):j>=h._startTime&&h._startTime+h.totalDuration()/h._timeScale+1e-10>j&&((m||!h._initted)&&2e-10>=j-h._startTime||(k[l++]=h)));for(f=l;--f>-1;)h=k[f],2===d&&h._kill(c,a)&&(g=!0),(2!==d||!h._firstPT&&h._initted)&&h._enabled(!1,!1)&&(g=!0);return g},L=function(a,b,c){for(var d=a._timeline,e=d._timeScale,f=a._startTime;d._timeline;){if(f+=d._startTime,e*=d._timeScale,d._paused)return-100;d=d._timeline}return f/=e,f>b?f-b:c&&f===b||!a._initted&&2e-10>f-b?1e-10:(f+=a.totalDuration()/a._timeScale/e)>b?0:f-b-1e-10};g._init=function(){var c,d,e,a=this.vars,b=a.ease;if(a.startAt&&(a.startAt.overwrite=0,a.startAt.immediateRender=!0,y.to(this.target,0,a.startAt)),this._ease=b?b instanceof o?a.easeParams instanceof Array?b.config.apply(b,a.easeParams):b:"function"==typeof b?new o(b,a.easeParams):p[b]||y.defaultEase:y.defaultEase,this._easeType=this._ease._type,this._easePower=this._ease._power,this._firstPT=null,this._targets)for(c=this._targets.length;--c>-1;)this._initProps(this._targets[c],this._propLookup[c]={},this._siblings[c],this._overwrittenProps?this._overwrittenProps[c]:null)&&(d=!0);else d=this._initProps(this.target,this._propLookup,this._siblings,this._overwrittenProps);if(d&&y._onPluginEvent("_onInitAllProps",this),this._overwrittenProps&&null==this._firstPT&&"function"!=typeof this.target&&this._enabled(!1,!1),a.runBackwards)for(e=this._firstPT;e;)e.s+=e.c,e.c=-e.c,e=e._next;this._onUpdate=a.onUpdate,this._initted=!0},g._initProps=function(a,b,c,d){var e,f,g,h,i,j,k;if(null==a)return!1;this.vars.css||a.style&&a.nodeType&&C.css&&this.vars.autoCSS!==!1&&B(this.vars,a);for(e in this.vars){if(F[e]){if(("onStartParams"===e||"onUpdateParams"===e||"onCompleteParams"===e||"onReverseCompleteParams"===e||"onRepeatParams"===e)&&(i=this.vars[e]))for(f=i.length;--f>-1;)"{self}"===i[f]&&(i=this.vars[e]=i.concat(),i[f]=this)}else if(C[e]&&(h=new C[e])._onInitTween(a,this.vars[e],this)){for(this._firstPT=j={_next:this._firstPT,t:h,p:"setRatio",s:0,c:1,f:!0,n:e,pg:!0,pr:h._priority},f=h._overwriteProps.length;--f>-1;)b[h._overwriteProps[f]]=this._firstPT;(h._priority||h._onInitAllProps)&&(g=!0),(h._onDisable||h._onEnable)&&(this._notifyPluginsOfEnabled=!0)}else this._firstPT=b[e]=j={_next:this._firstPT,t:a,p:e,f:"function"==typeof a[e],n:e,pg:!1,pr:0},j.s=j.f?a[e.indexOf("set")||"function"!=typeof a["get"+e.substr(3)]?e:"get"+e.substr(3)]():parseFloat(a[e]),k=this.vars[e],j.c="string"==typeof k&&"="===k.charAt(1)?parseInt(k.charAt(0)+"1",10)*Number(k.substr(2)):Number(k)-j.s||0;j&&j._next&&(j._next._prev=j)}return d&&this._kill(d,a)?this._initProps(a,b,c,d):this._overwrite>1&&this._firstPT&&c.length>1&&K(a,this,b,this._overwrite,c)?(this._kill(b,a),this._initProps(a,b,c,d)):g},g.render=function(a,b,c){var e,f,g,d=this._time;if(a>=this._duration)this._totalTime=this._time=this._duration,this.ratio=this._ease._calcEnd?this._ease.getRatio(1):1,this._reversed||(e=!0,f="onComplete"),0===this._duration&&((0===a||0>this._rawPrevTime)&&this._rawPrevTime!==a&&(c=!0),this._rawPrevTime=a);else if(0>=a)this._totalTime=this._time=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0,(0!==d||0===this._duration&&this._rawPrevTime>0)&&(f="onReverseComplete",e=this._reversed),0>a?(this._active=!1,0===this._duration&&(this._rawPrevTime>=0&&(c=!0),this._rawPrevTime=a)):this._initted||(c=!0);else if(this._totalTime=this._time=a,this._easeType){var h=a/this._duration,i=this._easeType,j=this._easePower;(1===i||3===i&&h>=.5)&&(h=1-h),3===i&&(h*=2),1===j?h*=h:2===j?h*=h*h:3===j?h*=h*h*h:4===j&&(h*=h*h*h*h),this.ratio=1===i?1-h:2===i?h:.5>a/this._duration?h/2:1-h/2}else this.ratio=this._ease.getRatio(a/this._duration);if(this._time!==d||c){for(this._initted||(this._init(),!e&&this._time&&(this.ratio=this._ease.getRatio(this._time/this._duration))),this._active||this._paused||(this._active=!0),0===d&&this.vars.onStart&&(0!==this._time||0===this._duration)&&(b||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||n)),g=this._firstPT;g;)g.f?g.t[g.p](g.c*this.ratio+g.s):g.t[g.p]=g.c*this.ratio+g.s,g=g._next;this._onUpdate&&(b||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||n)),f&&(this._gc||(e&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),b||this.vars[f]&&this.vars[f].apply(this.vars[f+"Scope"]||this,this.vars[f+"Params"]||n)))}},g._kill=function(a,b){if("all"===a&&(a=null),null==a&&(null==b||b===this.target))return this._enabled(!1,!1);b="string"!=typeof b?b||this._targets||this.target:y.selector(b)||b;var c,d,e,f,g,h,i,j;if((b instanceof Array||z(b))&&"number"!=typeof b[0])for(c=b.length;--c>-1;)this._kill(a,b[c])&&(h=!0);else{if(this._targets){for(c=this._targets.length;--c>-1;)if(b===this._targets[c]){g=this._propLookup[c]||{},this._overwrittenProps=this._overwrittenProps||[],d=this._overwrittenProps[c]=a?this._overwrittenProps[c]||{}:"all";break}}else{if(b!==this.target)return!1;g=this._propLookup,d=this._overwrittenProps=a?this._overwrittenProps||{}:"all"}if(g){i=a||g,j=a!==d&&"all"!==d&&a!==g&&(null==a||a._tempKill!==!0);for(e in i)(f=g[e])&&(f.pg&&f.t._kill(i)&&(h=!0),f.pg&&0!==f.t._overwriteProps.length||(f._prev?f._prev._next=f._next:f===this._firstPT&&(this._firstPT=f._next),f._next&&(f._next._prev=f._prev),f._next=f._prev=null),delete g[e]),j&&(d[e]=1)}}return h},g.invalidate=function(){return this._notifyPluginsOfEnabled&&y._onPluginEvent("_onDisable",this),this._firstPT=null,this._overwrittenProps=null,this._onUpdate=null,this._initted=this._active=this._notifyPluginsOfEnabled=!1,this._propLookup=this._targets?{}:[],this},g._enabled=function(a,b){if(a&&this._gc)if(this._targets)for(var c=this._targets.length;--c>-1;)this._siblings[c]=J(this._targets[c],this,!0);else this._siblings=J(this.target,this,!0);return v.prototype._enabled.call(this,a,b),this._notifyPluginsOfEnabled&&this._firstPT?y._onPluginEvent(a?"_onEnable":"_onDisable",this):!1},y.to=function(a,b,c){return new y(a,b,c)},y.from=function(a,b,c){return c.runBackwards=!0,c.immediateRender!==!1&&(c.immediateRender=!0),new y(a,b,c)},y.fromTo=function(a,b,c,d){return d.startAt=c,c.immediateRender&&(d.immediateRender=!0),new y(a,b,d)},y.delayedCall=function(a,b,c,d,e){return new y(b,0,{delay:a,onComplete:b,onCompleteParams:c,onCompleteScope:d,onReverseComplete:b,onReverseCompleteParams:c,onReverseCompleteScope:d,immediateRender:!1,useFrames:e,overwrite:0})},y.set=function(a,b){return new y(a,0,b)},y.killTweensOf=y.killDelayedCallsTo=function(a,b){for(var c=y.getTweensOf(a),d=c.length;--d>-1;)c[d]._kill(b,a)},y.getTweensOf=function(a){if(null!=a){a="string"!=typeof a?a:y.selector(a)||a;var b,c,d,e;if((a instanceof Array||z(a))&&"number"!=typeof a[0]){for(b=a.length,c=[];--b>-1;)c=c.concat(y.getTweensOf(a[b]));for(b=c.length;--b>-1;)for(e=c[b],d=b;--d>-1;)e===c[d]&&c.splice(b,1)}else for(c=J(a).concat(),b=c.length;--b>-1;)c[b]._gc&&c.splice(b,1);return c}};var M=l("plugins.TweenPlugin",function(a,b){this._overwriteProps=(a||"").split(","),this._propName=this._overwriteProps[0],this._priority=b||0},!0);if(g=M.prototype,M.version=12,M.API=2,g._firstPT=null,g._addTween=function(a,b,c,d,e,f){var g,h;null!=d&&(g="number"==typeof d||"="!==d.charAt(1)?Number(d)-c:parseInt(d.charAt(0)+"1",10)*Number(d.substr(2)))&&(this._firstPT=h={_next:this._firstPT,t:a,p:b,s:c,c:g,f:"function"==typeof a[b],n:e||b,r:f},h._next&&(h._next._prev=h))},g.setRatio=function(a){for(var c,b=this._firstPT;b;)c=b.c*a+b.s,b.r&&(c=c+(c>0?.5:-.5)>>0),b.f?b.t[b.p](c):b.t[b.p]=c,b=b._next},g._kill=function(a){if(null!=a[this._propName])this._overwriteProps=[];else for(var b=this._overwriteProps.length;--b>-1;)null!=a[this._overwriteProps[b]]&&this._overwriteProps.splice(b,1);for(var c=this._firstPT;c;)null!=a[c.n]&&(c._next&&(c._next._prev=c._prev),c._prev?(c._prev._next=c._next,c._prev=null):this._firstPT===c&&(this._firstPT=c._next)),c=c._next;return!1},g._roundProps=function(a,b){for(var c=this._firstPT;c;)(a[this._propName]||null!=c.n&&a[c.n.split(this._propName+"_").join("")])&&(c.r=b),c=c._next},y._onPluginEvent=function(a,b){var d,c=b._firstPT;if("_onInitAllProps"===a){for(var e,f,g,h;c;){for(h=c._next,e=f;e&&e.pr>c.pr;)e=e._next;(c._prev=e?e._prev:g)?c._prev._next=c:f=c,(c._next=e)?e._prev=c:g=c,c=h}c=b._firstPT=f}for(;c;)c.pg&&"function"==typeof c.t[a]&&c.t[a]()&&(d=!0),c=c._next;return d},M.activate=function(a){for(var b=a.length;--b>-1;)a[b].API===M.API&&(y._plugins[(new a[b])._propName]=a[b]);return!0},e=a._gsQueue){for(f=0;e.length>f;f++)e[f]();for(g in i)i[g].func||a.console.log("GSAP encountered missing dependency: com.greensock."+g)}h=!1})(window);
/*CSSPlugin*/
(window._gsQueue||(window._gsQueue=[])).push(function(){"use strict";window._gsDefine("plugins.CSSPlugin",["plugins.TweenPlugin","TweenLite"],function(a){var d,e,f,g,c=function(){a.call(this,"css"),this._overwriteProps.length=0},h={},i=c.prototype=new a("css");i.constructor=c,c.version="1.8.4",c.API=2,c.defaultTransformPerspective=0,i="px",c.suffixMap={top:i,right:i,bottom:i,left:i,width:i,height:i,fontSize:i,padding:i,margin:i,perspective:i};var G,H,I,J,K,L,j=/(?:\d|\-\d|\.\d|\-\.\d)+/g,k=/(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,l=/(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,m=/[^\d\-\.]/g,n=/(?:\d|\-|\+|=|#|\.)*/g,o=/opacity *= *([^)]*)/,p=/opacity:([^;]*)/,q=/alpha\(opacity *=.+?\)/i,r=/([A-Z])/g,s=/-([a-z])/gi,t=/(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,u=function(a,b){return b.toUpperCase()},v=/(?:Left|Right|Width)/i,w=/(M11|M12|M21|M22)=[\d\-\.e]+/gi,x=/progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,y=Math.PI/180,z=180/Math.PI,A={},B=document,C=B.createElement("div"),D=B.createElement("img"),E=c._internals={_specialProps:h},F=navigator.userAgent,M=function(){var c,a=F.indexOf("Android"),b=B.createElement("div");return I=-1!==F.indexOf("Safari")&&-1===F.indexOf("Chrome")&&(-1===a||Number(F.substr(a+8,1))>3),K=I&&6>Number(F.substr(F.indexOf("Version/")+8,1)),J=-1!==F.indexOf("Firefox"),/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(F),L=parseFloat(RegExp.$1),b.innerHTML="<a style='top:1px;opacity:.55;'>a</a>",c=b.getElementsByTagName("a")[0],c?/^0.55/.test(c.style.opacity):!1}(),N=function(a){return o.test("string"==typeof a?a:(a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?parseFloat(RegExp.$1)/100:1},O=function(a){window.console&&console.log(a)},P="",Q="",R=function(a,b){b=b||C;var d,e,c=b.style;if(void 0!==c[a])return a;for(a=a.charAt(0).toUpperCase()+a.substr(1),d=["O","Moz","ms","Ms","Webkit"],e=5;--e>-1&&void 0===c[d[e]+a];);return e>=0?(Q=3===e?"ms":d[e],P="-"+Q.toLowerCase()+"-",Q+a):null},S=B.defaultView?B.defaultView.getComputedStyle:function(){},T=c.getStyle=function(a,b,c,d,e){var f;return M||"opacity"!==b?(!d&&a.style[b]?f=a.style[b]:(c=c||S(a,null))?(a=c.getPropertyValue(b.replace(r,"-$1").toLowerCase()),f=a||c.length?a:c[b]):a.currentStyle&&(c=a.currentStyle,f=c[b]),null==e||f&&"none"!==f&&"auto"!==f&&"auto auto"!==f?f:e):N(a)},U=function(a,b,c){var f,g,d={},e=a._gsOverwrittenClassNamePT;if(e&&!c){for(;e;)e.setRatio(0),e=e._next;a._gsOverwrittenClassNamePT=null}if(b=b||S(a,null))if(f=b.length)for(;--f>-1;)d[b[f].replace(s,u)]=b.getPropertyValue(b[f]);else for(f in b)d[f]=b[f];else if(b=a.currentStyle||a.style)for(f in b)d[f.replace(s,u)]=b[f];return M||(d.opacity=N(a)),g=wb(a,b,!1),d.rotation=g.rotation*z,d.skewX=g.skewX*z,d.scaleX=g.scaleX,d.scaleY=g.scaleY,d.x=g.x,d.y=g.y,vb&&(d.z=g.z,d.rotationX=g.rotationX*z,d.rotationY=g.rotationY*z,d.scaleZ=g.scaleZ),d.filters&&delete d.filters,d},V=function(a,b,c,d){var g,h,i,e={},f=a.style;for(h in c)"cssText"!==h&&"length"!==h&&isNaN(h)&&b[h]!==(g=c[h])&&-1===h.indexOf("Origin")&&("number"==typeof g||"string"==typeof g)&&(e[h]=""!==g&&"auto"!==g&&"none"!==g||"string"!=typeof b[h]||""===b[h].replace(m,"")?g:0,void 0!==f[h]&&(i=new jb(f,h,f[h],i)));if(d)for(h in d)"className"!==h&&(e[h]=d[h]);return{difs:e,firstMPT:i}},W={width:["Left","Right"],height:["Top","Bottom"]},X=["marginLeft","marginRight","marginTop","marginBottom"],Y=function(a,b,c){var d=parseFloat("width"===b?a.offsetWidth:a.offsetHeight),e=W[b],f=e.length;for(c=c||S(a,null);--f>-1;)d-=parseFloat(T(a,"padding"+e[f],c,!0))||0,d-=parseFloat(T(a,"border"+e[f]+"Width",c,!0))||0;return d},Z=function(a,b,c,d,e){if("px"===d||!d)return c;if("auto"===d||!c)return 0;var j,f=v.test(b),g=a,h=C.style,i=0>c;return i&&(c=-c),"%"===d&&-1!==b.indexOf("border")?j=c/100*(f?a.clientWidth:a.clientHeight):(h.cssText="border-style:solid; border-width:0; position:absolute; line-height:0;","%"!==d&&"em"!==d&&g.appendChild?h[f?"borderLeftWidth":"borderTopWidth"]=c+d:(g=a.parentNode||B.body,h[f?"width":"height"]=c+d),g.appendChild(C),j=parseFloat(C[f?"offsetWidth":"offsetHeight"]),g.removeChild(C),0!==j||e||(j=Z(a,b,c,d,!0))),i?-j:j},$=function(a,b){(null==a||""===a||"auto"===a||"auto auto"===a)&&(a="0 0");var c=a.split(" "),d=-1!==a.indexOf("left")?"0%":-1!==a.indexOf("right")?"100%":c[0],e=-1!==a.indexOf("top")?"0%":-1!==a.indexOf("bottom")?"100%":c[1];return null==e?e="0":"center"===e&&(e="50%"),("center"===d||isNaN(parseFloat(d)))&&(d="50%"),b&&(b.oxp=-1!==d.indexOf("%"),b.oyp=-1!==e.indexOf("%"),b.oxr="="===d.charAt(1),b.oyr="="===e.charAt(1),b.ox=parseFloat(d.replace(m,"")),b.oy=parseFloat(e.replace(m,""))),d+" "+e+(c.length>2?" "+c[2]:"")},_=function(a,b){return"string"==typeof a&&"="===a.charAt(1)?parseInt(a.charAt(0)+"1",10)*parseFloat(a.substr(2)):parseFloat(a)-parseFloat(b)},ab=function(a,b){return null==a?b:"string"==typeof a&&"="===a.charAt(1)?parseInt(a.charAt(0)+"1",10)*Number(a.substr(2))+b:parseFloat(a)},bb=function(a,b){if(null==a)return b;var c=-1===a.indexOf("rad")?y:1,d="="===a.charAt(1);return a=Number(a.replace(m,""))*c,d?a+b:a},cb=function(a,b){var c="number"==typeof a?a*y:bb(a,b),d=(c-b)%(2*Math.PI);return d!==d%Math.PI&&(d+=Math.PI*(0>d?2:-2)),b+d},db={aqua:[0,255,255],lime:[0,255,0],silver:[192,192,192],black:[0,0,0],maroon:[128,0,0],teal:[0,128,128],blue:[0,0,255],navy:[0,0,128],white:[255,255,255],fuchsia:[255,0,255],olive:[128,128,0],yellow:[255,255,0],orange:[255,165,0],gray:[128,128,128],purple:[128,0,128],green:[0,128,0],red:[255,0,0],pink:[255,192,203],cyan:[0,255,255],transparent:[255,255,255,0]},eb=function(a){if(!a||""===a)return db.black;if(db[a])return db[a];if("number"==typeof a)return[a>>16,255&a>>8,255&a];if("#"===a.charAt(0)){if(4===a.length){var b=a.charAt(1),c=a.charAt(2),d=a.charAt(3);a="#"+b+b+c+c+d+d}return a=parseInt(a.substr(1),16),[a>>16,255&a>>8,255&a]}return a=a.match(j)||db.transparent,a[0]=Number(a[0]),a[1]=Number(a[1]),a[2]=Number(a[2]),a.length>3&&(a[3]=Number(a[3])),a},fb="(?:\\b(?:(?:rgb|rgba)\\(.+?\\))|\\B#.+?\\b";for(i in db)fb+="|"+i+"\\b";fb=RegExp(fb+")","gi");var gb=function(a,b,c){if(null==a)return function(a){return a};var d=b?(a.match(fb)||[""])[0]:"",e=a.split(d).join("").match(l)||[],f=a.substr(0,a.indexOf(e[0])),g=")"===a.charAt(a.length-1)?")":"",h=-1!==a.indexOf(" ")?" ":",",i=e.length,k=i>0?e[0].replace(j,""):"";return b?function(a){"number"==typeof a&&(a+=k);var b=(a.match(fb)||[d])[0],j=a.split(b).join("").match(l)||[],m=j.length;if(i>m--)for(;i>++m;)j[m]=c?j[(m-1)/2>>0]:e[m];return f+j.join(h)+h+b+g}:function(a){"number"==typeof a&&(a+=k);var b=a.match(l)||[],d=b.length;if(i>d--)for(;i>++d;)b[d]=c?b[(d-1)/2>>0]:e[d];return f+b.join(h)+g}},hb=function(a){return a=a.split(","),function(b,c,d,e,f,g,h){var j,i=(c+"").split(" ");for(h={},j=0;4>j;j++)h[a[j]]=i[j]=i[j]||i[(j-1)/2>>0];return e.parse(b,h,f,g)}},jb=(E._setPluginRatio=function(a){this.plugin.setRatio(a);for(var f,g,h,i,b=this.data,c=b.proxy,d=b.firstMPT,e=1e-6;d;)f=c[d.v],d.r?f=f>0?f+.5>>0:f-.5>>0:e>f&&f>-e&&(f=0),d.t[d.p]=f,d=d._next;if(b.autoRotate&&(b.autoRotate.rotation=c.rotation),1===a)for(d=b.firstMPT;d;){if(g=d.t,g.type){if(1===g.type){for(i=g.xs0+g.s+g.xs1,h=1;g.l>h;h++)i+=g["xn"+h]+g["xs"+(h+1)];g.e=i}}else g.e=g.s+g.xs0;d=d._next}},function(a,b,c,d,e){this.t=a,this.p=b,this.v=c,this.r=e,d&&(d._prev=this,this._next=d)}),lb=(E._parseToProxy=function(a,b,c,d,e,f){var l,m,n,o,p,g=d,h={},i={},j=c._transform,k=A;for(c._transform=null,A=b,d=p=c.parse(a,b,d,e),A=k,f&&(c._transform=j,g&&(g._prev=null,g._prev&&(g._prev._next=null)));d&&d!==g;){if(1>=d.type&&(m=d.p,i[m]=d.s+d.c,h[m]=d.s,f||(o=new jb(d,"s",m,o,d.r),d.c=0),1===d.type))for(l=d.l;--l>0;)n="xn"+l,m=d.p+"_"+n,i[m]=d.data[n],h[m]=d[n],f||(o=new jb(d,n,m,o,d.rxp[n]));d=d._next}return{proxy:h,end:i,firstMPT:o,pt:p}},E.CSSPropTween=function(a,b,c,e,f,h,i,j,k,l,m){this.t=a,this.p=b,this.s=c,this.c=e,this.n=i||"css_"+b,a instanceof lb||g.push(this.n),this.r=j,this.type=h||0,k&&(this.pr=k,d=!0),this.b=void 0===l?c:l,this.e=void 0===m?c+e:m,f&&(this._next=f,f._prev=this)}),mb=c.parseComplex=function(a,b,c,d,e,f,g,h,i,l){g=new lb(a,b,0,0,g,l?2:1,null,!1,h,c,d);var q,r,s,t,u,v,w,x,y,z,A,B,m=c.split(", ").join(",").split(" "),n=(d+"").split(", ").join(",").split(" "),o=m.length,p=G!==!1;for(o!==n.length&&(m=(f||"").split(" "),o=m.length),g.plugin=i,g.setRatio=l,q=0;o>q;q++)if(t=m[q],u=n[q],x=parseFloat(t),x||0===x)g.appendXtra("",x,_(u,x),u.replace(k,""),p&&-1!==u.indexOf("px"),!0);else if(e&&("#"===t.charAt(0)||0===t.indexOf("rgb")||db[t]))t=eb(t),u=eb(u),y=t.length+u.length>6,y&&!M&&0===u[3]?(g["xs"+g.l]+=g.l?" transparent":"transparent",g.e=g.e.split(n[q]).join("transparent")):(M||(y=!1),g.appendXtra(y?"rgba(":"rgb(",t[0],u[0]-t[0],",",!0,!0).appendXtra("",t[1],u[1]-t[1],",",!0).appendXtra("",t[2],u[2]-t[2],y?",":")",!0),y&&(t=4>t.length?1:t[3],g.appendXtra("",t,(4>u.length?1:u[3])-t,")",!1)));else if(v=t.match(j)){if(w=u.match(k),!w||w.length!==v.length)return g;for(s=0,r=0;v.length>r;r++)A=v[r],z=t.indexOf(A,s),g.appendXtra(t.substr(s,z-s),Number(A),_(w[r],A),"",p&&"px"===t.substr(z+A.length,2),0===r),s=z+A.length;g["xs"+g.l]+=t.substr(s)}else g["xs"+g.l]+=g.l?" "+t:t;if(-1!==d.indexOf("=")&&g.data){for(B=g.xs0+g.data.s,q=1;g.l>q;q++)B+=g["xs"+q]+g.data["xn"+q];g.e=B+g["xs"+q]}return g.l||(g.type=-1,g.xs0=g.e),g.xfirst||g},nb=9;for(i=lb.prototype,i.l=i.pr=0;--nb>0;)i["xn"+nb]=0,i["xs"+nb]="";i.xs0="",i._next=i._prev=i.xfirst=i.data=i.plugin=i.setRatio=i.rxp=null,i.appendXtra=function(a,b,c,d,e,f){var g=this,h=g.l;return g["xs"+h]+=f&&h?" "+a:a||"",c||0===h||g.plugin?(g.l++,g.type=g.setRatio?2:1,g["xs"+g.l]=d||"",h>0?(g.data["xn"+h]=b+c,g.rxp["xn"+h]=e,g["xn"+h]=b,g.plugin||(g.xfirst=new lb(g,"xn"+h,b,c,g.xfirst||g,0,g.n,e,g.pr),g.xfirst.xs0=0),g):(g.data={s:b+c},g.rxp={},g.s=b,g.c=c,g.r=e,g)):(g["xs"+h]+=b+(d||""),g)};var ob=function(a,b,c,d,e,f,g){this.p=d?R(a)||a:a,h[a]=h[this.p]=this,this.format=f||gb(b,e),c&&(this.parse=c),this.clrs=e,this.dflt=b,this.pr=g||0},pb=E._registerComplexSpecialProp=function(a,b,c,d,e,f,g){for(var k,h=a.split(","),i=b instanceof Array?b:[b],j=h.length;--j>-1;)k=new ob(h[j],i[j],c,d&&0===j,e,f,g)},qb=function(a){if(!h[a]){var b=a.charAt(0).toUpperCase()+a.substr(1)+"Plugin";pb(a,null,function(a,c,d,e,f,g,i){var j=(window.GreenSockGlobals||window).com.greensock.plugins[b];return j?(j._cssRegister(),h[d].parse(a,c,d,e,f,g,i)):(O("Error: "+b+" js file not loaded."),f)})}};i=ob.prototype,i.parseComplex=function(a,b,c,d,e,f){return mb(a,this.p,b,c,this.clrs,this.dflt,d,this.pr,e,f)},i.parse=function(a,b,c,d,e,g){return this.parseComplex(a.style,this.format(T(a,c,f,!1,this.dflt)),this.format(b),e,g)},c.registerSpecialProp=function(a,b,c){pb(a,null,function(a,d,e,f,g,h){var j=new lb(a,e,0,0,g,2,e,!1,c);return j.plugin=h,j.setRatio=b(a,d,f._tween,e),j},!1,!1,null,c)};var rb=["scaleX","scaleY","scaleZ","x","y","z","skewX","rotation","rotationX","rotationY","perspective"],sb=R("transform"),tb=P+"transform",ub=R("transformOrigin"),vb=null!==R("perspective"),wb=function(a,b,d){var l,m,n,o,p,q,r,s,t,u,v,x,e=d?a._gsTransform||{skewY:0}:{skewY:0},f=0>e.scaleX,g=2e-5,h=1e5,i=-Math.PI+1e-4,j=Math.PI-1e-4,k=vb?parseFloat(T(a,ub,b,!1,"0 0 0").split(" ")[2])||e.zOrigin||0:0;for(sb?l=T(a,tb,b,!0):a.currentStyle&&(l=a.currentStyle.filter.match(w),l=l&&4===l.length?l[0].substr(4)+","+Number(l[2].substr(4))+","+Number(l[1].substr(4))+","+l[3].substr(4)+","+(e?e.x:0)+","+(e?e.y:0):null),m=(l||"").match(/(?:\-|\b)[\d\-\.e]+\b/gi)||[],n=m.length;--n>-1;)o=Number(m[n]),m[n]=(o*h+(0>o?-.5:.5)>>0)/h;if(16===m.length){var y=m[8],z=m[9],A=m[10],B=m[12],C=m[13],D=m[14];if(e.zOrigin&&(D=-e.zOrigin,B=y*D-m[12],C=z*D-m[13],D=A*D+e.zOrigin-m[14]),!d||B!==e.x||C!==e.y||D!==e.z){var P,Q,R,S,U,V,W,X,E=m[0],F=m[1],G=m[2],H=m[3],I=m[4],J=m[5],K=m[6],L=m[7],M=m[11],N=e.rotationX=Math.atan2(K,A),O=i>N||N>j;N&&(U=Math.cos(-N),V=Math.sin(-N),P=I*U+y*V,Q=J*U+z*V,R=K*U+A*V,S=L*U+M*V,y=I*-V+y*U,z=J*-V+z*U,A=K*-V+A*U,M=L*-V+M*U,I=P,J=Q,K=R),N=e.rotationY=Math.atan2(y,E),N&&(W=i>N||N>j,U=Math.cos(-N),V=Math.sin(-N),P=E*U-y*V,Q=F*U-z*V,R=G*U-A*V,S=H*U-M*V,z=F*V+z*U,A=G*V+A*U,M=H*V+M*U,E=P,F=Q,G=R),N=e.rotation=Math.atan2(F,J),N&&(X=i>N||N>j,U=Math.cos(-N),V=Math.sin(-N),E=E*U+I*V,Q=F*U+J*V,J=F*-V+J*U,K=G*-V+K*U,F=Q),X&&O?e.rotation=e.rotationX=0:X&&W?e.rotation=e.rotationY=0:W&&O&&(e.rotationY=e.rotationX=0),e.scaleX=(Math.sqrt(E*E+F*F)*h+.5>>0)/h,e.scaleY=(Math.sqrt(J*J+z*z)*h+.5>>0)/h,e.scaleZ=(Math.sqrt(K*K+A*A)*h+.5>>0)/h,e.skewX=0,e.perspective=M?1/(0>M?-M:M):0,e.x=B,e.y=C,e.z=D}}else if(!vb||0===m.length||e.x!==m[4]||e.y!==m[5]||!e.rotationX&&!e.rotationY){var Y=m.length>=6,Z=Y?m[0]:1,$=m[1]||0,_=m[2]||0,ab=Y?m[3]:1;e.x=m[4]||0,e.y=m[5]||0,p=Math.sqrt(Z*Z+$*$),q=Math.sqrt(ab*ab+_*_),r=Z||$?Math.atan2($,Z):e.rotation||0,s=_||ab?Math.atan2(_,ab)+r:e.skewX||0,t=p-Math.abs(e.scaleX||0),u=q-Math.abs(e.scaleY||0),Math.abs(s)>Math.PI/2&&Math.abs(s)<1.5*Math.PI&&(f?(p*=-1,s+=0>=r?Math.PI:-Math.PI,r+=0>=r?Math.PI:-Math.PI):(q*=-1,s+=0>=s?Math.PI:-Math.PI)),v=(r-e.rotation)%Math.PI,x=(s-e.skewX)%Math.PI,(void 0===e.skewX||t>g||-g>t||u>g||-g>u||v>i&&j>v&&0!==v*h>>0||x>i&&j>x&&0!==x*h>>0)&&(e.scaleX=p,e.scaleY=q,e.rotation=r,e.skewX=s),vb&&(e.rotationX=e.rotationY=e.z=0,e.perspective=parseFloat(c.defaultTransformPerspective)||0,e.scaleZ=1)}e.zOrigin=k;for(n in e)g>e[n]&&e[n]>-g&&(e[n]=0);return d&&(a._gsTransform=e),e},xb=function(a){var l,m,b=this.data,c=-b.rotation,d=c+b.skewX,e=1e5,f=(Math.cos(c)*b.scaleX*e>>0)/e,g=(Math.sin(c)*b.scaleX*e>>0)/e,h=(Math.sin(d)*-b.scaleY*e>>0)/e,i=(Math.cos(d)*b.scaleY*e>>0)/e,j=this.t.style,k=this.t.currentStyle;if(k){m=g,g=-h,h=-m,l=k.filter,j.filter="";var v,w,p=this.t.offsetWidth,q=this.t.offsetHeight,r="absolute"!==k.position,s="progid:DXImageTransform.Microsoft.Matrix(M11="+f+", M12="+g+", M21="+h+", M22="+i,t=b.x,u=b.y;if(null!=b.ox&&(v=(b.oxp?.01*p*b.ox:b.ox)-p/2,w=(b.oyp?.01*q*b.oy:b.oy)-q/2,t+=v-(v*f+w*g),u+=w-(v*h+w*i)),r)v=p/2,w=q/2,s+=", Dx="+(v-(v*f+w*g)+t)+", Dy="+(w-(v*h+w*i)+u)+")";else{var z,A,B,y=8>L?1:-1;for(v=b.ieOffsetX||0,w=b.ieOffsetY||0,b.ieOffsetX=Math.round((p-((0>f?-f:f)*p+(0>g?-g:g)*q))/2+t),b.ieOffsetY=Math.round((q-((0>i?-i:i)*q+(0>h?-h:h)*p))/2+u),nb=0;4>nb;nb++)A=X[nb],z=k[A],m=-1!==z.indexOf("px")?parseFloat(z):Z(this.t,A,parseFloat(z),z.replace(n,""))||0,B=m!==b[A]?2>nb?-b.ieOffsetX:-b.ieOffsetY:2>nb?v-b.ieOffsetX:w-b.ieOffsetY,j[A]=(b[A]=Math.round(m-B*(0===nb||2===nb?1:y)))+"px";s+=", sizingMethod='auto expand')"}j.filter=-1!==l.indexOf("DXImageTransform.Microsoft.Matrix(")?l.replace(x,s):s+" "+l,(0===a||1===a)&&1===f&&0===g&&0===h&&1===i&&(r&&-1===s.indexOf("Dx=0, Dy=0")||o.test(l)&&100!==parseFloat(RegExp.$1)||-1===l.indexOf("gradient(")&&j.removeAttribute("filter"))}},yb=function(){var x,y,z,A,B,C,D,E,F,b=this.data,c=this.t.style,d=b.perspective,e=b.scaleX,f=0,g=0,h=0,i=0,j=b.scaleY,k=0,l=0,m=0,n=0,o=b.scaleZ,p=0,q=0,r=0,s=d?-1/d:0,t=b.rotation,u=b.zOrigin,v=",",w=1e5;J&&(D=T(this.t,"top",null,!1,"0"),E=parseFloat(D)||0,F=D.substr((E+"").length),b._ffFix=!b._ffFix,c.top=(b._ffFix?E+.05:E-.05)+(""===F?"px":F)),(t||b.skewX)&&(z=e*Math.cos(t),A=j*Math.sin(t),t-=b.skewX,f=e*-Math.sin(t),j*=Math.cos(t),e=z,i=A),t=b.rotationY,t&&(x=Math.cos(t),y=Math.sin(t),z=e*x,A=i*x,B=o*-y,C=s*-y,g=e*y,k=i*y,o*=x,s*=x,e=z,i=A,m=B,q=C),t=b.rotationX,t&&(x=Math.cos(t),y=Math.sin(t),z=f*x+g*y,A=j*x+k*y,B=n*x+o*y,C=r*x+s*y,g=f*-y+g*x,k=j*-y+k*x,o=n*-y+o*x,s=r*-y+s*x,f=z,j=A,n=B,r=C),u&&(p-=u,h=g*p,l=k*p,p=o*p+u),h+=b.x,l+=b.y,p=((p+b.z)*w>>0)/w,c[sb]="matrix3d("+(e*w>>0)/w+v+(i*w>>0)/w+v+(m*w>>0)/w+v+(q*w>>0)/w+v+(f*w>>0)/w+v+(j*w>>0)/w+v+(n*w>>0)/w+v+(r*w>>0)/w+v+(g*w>>0)/w+v+(k*w>>0)/w+v+(o*w>>0)/w+v+(s*w>>0)/w+v+(h*w>>0)/w+v+(l*w>>0)/w+v+p+v+(d?1+-p/d:1)+")"},zb=function(){var d,e,f,g,h,i,j,k,b=this.data,c=this.t;J&&(d=T(c,"top",null,!1,"0"),e=parseFloat(d)||0,f=d.substr((e+"").length),b._ffFix=!b._ffFix,c.style.top=(b._ffFix?e+.05:e-.05)+(""===f?"px":f)),b.rotation||b.skewX?(g=b.rotation,h=g-b.skewX,i=1e5,j=b.scaleX*i,k=b.scaleY*i,c.style[sb]="matrix("+(Math.cos(g)*j>>0)/i+","+(Math.sin(g)*j>>0)/i+","+(Math.sin(h)*-k>>0)/i+","+(Math.cos(h)*k>>0)/i+","+b.x+","+b.y+")"):c.style[sb]="matrix("+b.scaleX+",0,0,"+b.scaleY+","+b.x+","+b.y+")"};pb("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,transformPerspective",null,function(a,b,c,d,e,g,h){if(d._transform)return e;var n,o,p,q,r,s,t,i=d._transform=wb(a,f,!0),j=a.style,k=1e-6,l=rb.length,m=h;for("string"==typeof m.transform&&sb?(q=j.cssText,j[sb]=m.transform,j.display="block",n=wb(a,null,!1),j.cssText=q):"object"==typeof m&&(o=null!=m.rotation?m.rotation:null!=m.rotationZ?m.rotationZ:i.rotation*z,n={scaleX:ab(null!=m.scaleX?m.scaleX:m.scale,i.scaleX),scaleY:ab(null!=m.scaleY?m.scaleY:m.scale,i.scaleY),scaleZ:ab(null!=m.scaleZ?m.scaleZ:m.scale,i.scaleZ),x:ab(m.x,i.x),y:ab(m.y,i.y),z:ab(m.z,i.z),perspective:ab(m.transformPerspective,i.perspective)},n.rotation=null!=m.shortRotation||null!=m.shortRotationZ?cb(m.shortRotation||m.shortRotationZ||0,i.rotation):"number"==typeof o?o*y:bb(o,i.rotation),vb&&(n.rotationX=null!=m.shortRotationX?cb(m.shortRotationX,i.rotationX):"number"==typeof m.rotationX?m.rotationX*y:bb(m.rotationX,i.rotationX),n.rotationY=null!=m.shortRotationY?cb(m.shortRotationY,i.rotationY):"number"==typeof m.rotationY?m.rotationY*y:bb(m.rotationY,i.rotationY),k>n.rotationX&&n.rotationX>-k&&(n.rotationX=0),k>n.rotationY&&n.rotationY>-k&&(n.rotationY=0)),n.skewX=null==m.skewX?i.skewX:"number"==typeof m.skewX?m.skewX*y:bb(m.skewX,i.skewX),n.skewY=null==m.skewY?i.skewY:"number"==typeof m.skewY?m.skewY*y:bb(m.skewY,i.skewY),(p=n.skewY-i.skewY)&&(n.skewX+=p,n.rotation+=p),k>n.skewY&&n.skewY>-k&&(n.skewY=0),k>n.skewX&&n.skewX>-k&&(n.skewX=0),k>n.rotation&&n.rotation>-k&&(n.rotation=0)),s=i.z||i.rotationX||i.rotationY||n.z||n.rotationX||n.rotationY||n.perspective,s||null==m.scale||(n.scaleZ=1);--l>-1;)c=rb[l],r=n[c]-i[c],(r>k||-k>r||null!=A[c])&&(t=!0,e=new lb(i,c,i[c],r,e),e.xs0=0,e.plugin=g,d._overwriteProps.push(e.n));return r=m.transformOrigin,(r||vb&&s&&i.zOrigin)&&(sb?(t=!0,r=(r||T(a,c,f,!1,"50% 50%"))+"",c=ub,e=new lb(j,c,0,0,e,-1,"css_transformOrigin"),e.b=j[c],e.plugin=g,vb?(q=i.zOrigin,r=r.split(" "),i.zOrigin=(r.length>2?parseFloat(r[2]):q)||0,e.xs0=e.e=j[c]=r[0]+" "+(r[1]||"50%")+" 0px",e=new lb(i,"zOrigin",0,0,e,-1,e.n),e.b=q,e.xs0=e.e=i.zOrigin):e.xs0=e.e=j[c]=r):$(r+"",i)),t&&(d._transformType=s||3===this._transformType?3:2),e},!0),pb("boxShadow","0px 0px 0px 0px #999",function(a,b,c,d,e,g){var h=-1!==(b+"").indexOf("inset")?" inset":"";return this.parseComplex(a.style,this.format(T(a,this.p,f,!1,this.dflt))+h,this.format(b)+h,e,g)},!0,!0),pb("borderRadius","0px",function(a,b,c,d,g){b=this.format(b);var k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,i=["borderTopLeftRadius","borderTopRightRadius","borderBottomRightRadius","borderBottomLeftRadius"],j=a.style;for(s=parseFloat(a.offsetWidth),t=parseFloat(a.offsetHeight),k=b.split(" "),l=0;i.length>l;l++)this.p.indexOf("border")&&(i[l]=R(i[l])),o=n=T(a,i[l],f,!1,"0px"),-1!==o.indexOf(" ")&&(n=o.split(" "),o=n[0],n=n[1]),p=m=k[l],q=parseFloat(o),v=o.substr((q+"").length),w="="===p.charAt(1),w?(r=parseInt(p.charAt(0)+"1",10),p=p.substr(2),r*=parseFloat(p),u=p.substr((r+"").length-(0>r?1:0))||""):(r=parseFloat(p),u=p.substr((r+"").length)),""===u&&(u=e[c]||v),u!==v&&(x=Z(a,"borderLeft",q,v),y=Z(a,"borderTop",q,v),"%"===u?(o=100*(x/s)+"%",n=100*(y/t)+"%"):"em"===u?(z=Z(a,"borderLeft",1,"em"),o=x/z+"em",n=y/z+"em"):(o=x+"px",n=y+"px"),w&&(p=parseFloat(o)+r+u,m=parseFloat(n)+r+u)),g=mb(j,i[l],o+" "+n,p+" "+m,!1,"0px",g);return g},!0,!1,gb("0px 0px 0px 0px",!1,!0)),pb("backgroundPosition","0 0",function(a,b,c,d,e,g){var l,m,n,o,p,q,h="background-position",i=f||S(a,null),j=this.format((i?L?i.getPropertyValue(h+"-x")+" "+i.getPropertyValue(h+"-y"):i.getPropertyValue(h):a.currentStyle.backgroundPositionX+" "+a.currentStyle.backgroundPositionY)||"0 0"),k=this.format(b);if(-1!==j.indexOf("%")!=(-1!==k.indexOf("%"))&&(q=T(a,"backgroundImage").replace(t,""),q&&"none"!==q)){for(l=j.split(" "),m=k.split(" "),D.setAttribute("src",q),n=2;--n>-1;)j=l[n],o=-1!==j.indexOf("%"),o!==(-1!==m[n].indexOf("%"))&&(p=0===n?a.offsetWidth-D.width:a.offsetHeight-D.height,l[n]=o?parseFloat(j)/100*p+"px":100*(parseFloat(j)/p)+"%");j=l.join(" ")}return this.parseComplex(a.style,j,k,e,g)},!1,!1,$),pb("backgroundSize","0 0",null,!1,!1,$),pb("perspective","0px",null,!0),pb("perspectiveOrigin","50% 50%",null,!0),pb("transformStyle","preserve-3d",null,!0),pb("backfaceVisibility","visible",null,!0),pb("margin",null,hb("marginTop,marginRight,marginBottom,marginLeft")),pb("padding",null,hb("paddingTop,paddingRight,paddingBottom,paddingLeft")),pb("clip","rect(0px,0px,0px,0px)"),pb("textShadow","0px 0px 0px #999",null,!1,!0),pb("autoRound,strictUnits",null,function(a,b,c,d,e){return e}),pb("border","0px solid #000",function(a,b,c,d,e,g){return this.parseComplex(a.style,this.format(T(a,"borderTopWidth",f,!1,"0px")+" "+T(a,"borderTopStyle",f,!1,"solid")+" "+T(a,"borderTopColor",f,!1,"#000")),this.format(b),e,g)},!1,!0,function(a){var b=a.split(" ");return b[0]+" "+(b[1]||"solid")+" "+(a.match(fb)||["#000"])[0]});var Ab=function(a){var e,b=this.t,c=b.filter,d=this.s+this.c*a>>0;100===d&&(-1===c.indexOf("atrix(")&&-1===c.indexOf("radient(")?(b.removeAttribute("filter"),e=!T(this.data,"filter")):(b.filter=c.replace(q,""),e=!0)),e||(this.xn1&&(b.filter=c=c||"alpha(opacity=100)"),-1===c.indexOf("opacity")?b.filter+=" alpha(opacity="+d+")":b.filter=c.replace(o,"opacity="+d))};pb("opacity,alpha,autoAlpha","1",function(a,b,c,d,e,g){var j,h=parseFloat(T(a,"opacity",f,!1,"1")),i=a.style;return b=parseFloat(b),"autoAlpha"===c&&(j=T(a,"visibility",f),1===h&&"hidden"===j&&0!==b&&(h=0),e=new lb(i,"visibility",0,0,e,-1,null,!1,0,0!==h?"visible":"hidden",0===b?"hidden":"visible"),e.xs0="visible",d._overwriteProps.push(e.n)),M?e=new lb(i,"opacity",h,b-h,e):(e=new lb(i,"opacity",100*h,100*(b-h),e),e.xn1="autoAlpha"===c?1:0,i.zoom=1,e.type=2,e.b="alpha(opacity="+e.s+")",e.e="alpha(opacity="+(e.s+e.c)+")",e.data=a,e.plugin=g,e.setRatio=Ab),e});var Bb=function(a){if(1===a||0===a){this.t.className=1===a?this.e:this.b;for(var b=this.data,c=this.t.style,d=c.removeProperty?"removeProperty":"removeAttribute";b;)b.v?c[b.p]=b.v:c[d](b.p.replace(r,"-$1").toLowerCase()),b=b._next}else this.t.className!==this.b&&(this.t.className=this.b)};pb("className",null,function(a,b,c,e,g,h,i){var l,m,j=a.className,k=a.style.cssText;return g=e._classNamePT=new lb(a,c,0,0,g,2),g.setRatio=Bb,g.pr=-11,d=!0,g.b=j,g.e="="!==b.charAt(1)?b:"+"===b.charAt(0)?j+" "+b.substr(2):j.split(b.substr(2)).join(""),e._tween._duration&&(m=U(a,f,!0),a.className=g.e,l=V(a,m,U(a),i),a.className=j,g.data=l.firstMPT,a.style.cssText=k,g=g.xfirst=e.parse(a,l.difs,g,h)),g});var Cb=function(a){if((1===a||0===a)&&this.data._totalTime===this.data._totalDuration)for(var i,b="all"===this.e,c=this.t.style,d=b?c.cssText.split(";"):this.e.split(","),e=c.removeProperty?"removeProperty":"removeAttribute",f=d.length,g=h.transform.parse;--f>-1;)i=d[f],b&&(i=i.substr(0,i.indexOf(":")).split(" ").join("")),h[i]&&(i=h[i].parse===g?sb:h[i].p),i&&c[e](i.replace(r,"-$1").toLowerCase())};for(pb("clearProps",null,function(a,b,c,e,f){return f=new lb(a,c,0,0,f,2),f.setRatio=Cb,f.e=b,f.pr=-10,f.data=e._tween,d=!0,f}),i="bezier,throwProps,physicsProps,physics2D".split(","),nb=i.length;nb--;)qb(i[nb]);return i=c.prototype,i._firstPT=null,i._onInitTween=function(a,b,h){if(!a.nodeType)return!1;this._target=a,this._tween=h,this._vars=b,G=b.autoRound,d=!1,e=b.suffixMap||c.suffixMap,f=S(a,""),g=this._overwriteProps;var j,k,l,m,n,o,q,r,s,i=a.style;if(H&&""===i.zIndex&&(j=T(a,"zIndex",f),("auto"===j||""===j)&&(i.zIndex=0)),"string"==typeof b&&(m=i.cssText,j=U(a,f),i.cssText=m+";"+b,j=V(a,j,U(a)).difs,!M&&p.test(b)&&(j.opacity=parseFloat(RegExp.$1)),b=j,i.cssText=m),this._firstPT=k=this.parse(a,b,null),this._transformType){for(s=3===this._transformType,sb?I&&(H=!0,""===i.zIndex&&(q=T(a,"zIndex",f),("auto"===q||""===q)&&(i.zIndex=0)),K&&(i.WebkitBackfaceVisibility=this._vars.WebkitBackfaceVisibility||(s?"visible":"hidden"))):i.zoom=1,l=k;l&&l._next;)l=l._next;r=new lb(a,"transform",0,0,null,2),this._linkCSSP(r,null,l),r.setRatio=s&&vb?yb:sb?zb:xb,r.data=this._transform||wb(a,f,!0),g.pop()}if(d){for(;k;){for(o=k._next,l=m;l&&l.pr>k.pr;)l=l._next;(k._prev=l?l._prev:n)?k._prev._next=k:m=k,(k._next=l)?l._prev=k:n=k,k=o}this._firstPT=m}return!0},i.parse=function(a,b,c,d){var i,j,k,l,m,n,o,p,q,r,g=a.style;for(i in b)n=b[i],j=h[i],j?c=j.parse(a,n,i,this,c,d,b):(m=T(a,i,f)+"",q="string"==typeof n,"color"===i||"fill"===i||"stroke"===i||-1!==i.indexOf("Color")||q&&!n.indexOf("rgb")?(q||(n=eb(n),n=(n.length>3?"rgba(":"rgb(")+n.join(",")+")"),c=mb(g,i,m,n,!0,"transparent",c,0,d)):!q||-1===n.indexOf(" ")&&-1===n.indexOf(",")?(k=parseFloat(m),o=k||0===k?m.substr((k+"").length):"",(""===m||"auto"===m)&&("width"===i||"height"===i?(k=Y(a,i,f),o="px"):(k="opacity"!==i?0:1,o="")),r=q&&"="===n.charAt(1),r?(l=parseInt(n.charAt(0)+"1",10),n=n.substr(2),l*=parseFloat(n),p=n.substr((l+"").length-(0>l?1:0))||""):(l=parseFloat(n),p=q?n.substr((l+"").length)||"":""),""===p&&(p=e[i]||o),n=l||0===l?(r?l+k:l)+p:b[i],o!==p&&""!==p&&(l||0===l)&&(k||0===k)&&(k=Z(a,i,k,o),"%"===p?(k/=Z(a,i,100,"%")/100,k>100&&(k=100),b.strictUnits!==!0&&(m=k+"%")):"em"===p?k/=Z(a,i,1,"em"):(l=Z(a,i,l,p),p="px"),r&&(l||0===l)&&(n=l+k+p)),r&&(l+=k),!k&&0!==k||!l&&0!==l?n||"NaN"!=n+""&&null!=n?(c=new lb(g,i,l||k||0,0,c,-1,"css_"+i,!1,0,m,n),c.xs0="display"===i&&"none"===n?m:n):O("invalid "+i+" tween value. "):(c=new lb(g,i,k,l-k,c,0,"css_"+i,G!==!1&&("px"===p||"zIndex"===i),0,m,n),c.xs0=p)):c=mb(g,i,m,n,!0,null,c,0,d)),d&&c&&!c.plugin&&(c.plugin=d);return c},i.setRatio=function(a){var d,e,f,b=this._firstPT,c=1e-6;if(1!==a||this._tween._time!==this._tween._duration&&0!==this._tween._time)if(a||this._tween._time!==this._tween._duration&&0!==this._tween._time||this._tween._rawPrevTime===-1e-6)for(;b;){if(d=b.c*a+b.s,b.r?d=d>0?d+.5>>0:d-.5>>0:c>d&&d>-c&&(d=0),b.type)if(1===b.type)if(f=b.l,2===f)b.t[b.p]=b.xs0+d+b.xs1+b.xn1+b.xs2;else if(3===f)b.t[b.p]=b.xs0+d+b.xs1+b.xn1+b.xs2+b.xn2+b.xs3;else if(4===f)b.t[b.p]=b.xs0+d+b.xs1+b.xn1+b.xs2+b.xn2+b.xs3+b.xn3+b.xs4;else if(5===f)b.t[b.p]=b.xs0+d+b.xs1+b.xn1+b.xs2+b.xn2+b.xs3+b.xn3+b.xs4+b.xn4+b.xs5;else{for(e=b.xs0+d+b.xs1,f=1;b.l>f;f++)e+=b["xn"+f]+b["xs"+(f+1)];b.t[b.p]=e}else-1===b.type?b.t[b.p]=b.xs0:b.setRatio&&b.setRatio(a);else b.t[b.p]=d+b.xs0;b=b._next}else for(;b;)2!==b.type?b.t[b.p]=b.b:b.setRatio(a),b=b._next;else for(;b;)2!==b.type?b.t[b.p]=b.e:b.setRatio(a),b=b._next},i._enableTransforms=function(a){this._transformType=a||3===this._transformType?3:2},i._linkCSSP=function(a,b,c,d){return a&&(b&&(b._prev=a),a._next&&(a._next._prev=a._prev),c?c._next=a:d||null!==this._firstPT||(this._firstPT=a),a._prev?a._prev._next=a._next:this._firstPT===a&&(this._firstPT=a._next),a._next=b,a._prev=c),a},i._kill=function(b){var e,f,g,c=b,d=!1;if(b.css_autoAlpha||b.css_alpha){c={};for(f in b)c[f]=b[f];c.css_opacity=1,c.css_autoAlpha&&(c.css_visibility=1)}return b.css_className&&(e=this._classNamePT)&&(g=e.xfirst,g&&g._prev?this._linkCSSP(g._prev,e._next,g._prev._prev):g===this._firstPT&&(this._firstPT=null),e._next&&this._linkCSSP(e._next,e._next._next,g._prev),this._target._gsOverwrittenClassNamePT=this._linkCSSP(e,this._target._gsOverwrittenClassNamePT),this._classNamePT=null,d=!0),a.prototype._kill.call(this,c)||d},a.activate([c]),c},!0)}),window._gsDefine&&window._gsQueue.pop()();
/*custom scrollbar*/
(function(b){var a={init:function(c){var e={set_width:false,set_height:false,horizontalScroll:false,scrollInertia:950,mouseWheel:true,mouseWheelPixels:"auto",autoDraggerLength:true,autoHideScrollbar:false,scrollButtons:{enable:false,scrollType:"continuous",scrollSpeed:"auto",scrollAmount:40},advanced:{updateOnBrowserResize:true,updateOnContentResize:false,autoExpandHorizontalScroll:false,autoScrollOnFocus:true},callbacks:{onScrollStart:function(){},onScroll:function(){},onTotalScroll:function(){},onTotalScrollBack:function(){},onTotalScrollOffset:0,onTotalScrollBackOffset:0,whileScrolling:function(){}},theme:"light"},c=b.extend(true,e,c);b(document).data("mCS-is-touch-device",false);if(d()){b(document).data("mCS-is-touch-device",true)}function d(){return !!("ontouchstart" in window)?1:0}return this.each(function(){var l=b(this);if(c.set_width){l.css("width",c.set_width)}if(c.set_height){l.css("height",c.set_height)}if(!b(document).data("mCustomScrollbar-index")){b(document).data("mCustomScrollbar-index","1")}else{var s=parseInt(b(document).data("mCustomScrollbar-index"));b(document).data("mCustomScrollbar-index",s+1)}l.wrapInner("<div class='mCustomScrollBox mCS-"+c.theme+"' id='mCSB_"+b(document).data("mCustomScrollbar-index")+"' style='position:relative; height:100%; overflow:hidden; max-width:100%;' />").addClass("mCustomScrollbar _mCS_"+b(document).data("mCustomScrollbar-index"));var f=l.children(".mCustomScrollBox");if(c.horizontalScroll){f.addClass("mCSB_horizontal").wrapInner("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />");var j=f.children(".mCSB_h_wrapper");j.wrapInner("<div class='mCSB_container' style='position:absolute; left:0;' />").children(".mCSB_container").css({width:j.children().outerWidth(),position:"relative"}).unwrap()}else{f.wrapInner("<div class='mCSB_container' style='position:relative; top:0;' />")}var n=f.children(".mCSB_container");if(b(document).data("mCS-is-touch-device")){n.addClass("mCS_touch")}n.after("<div class='mCSB_scrollTools' style='position:absolute;'><div class='mCSB_draggerContainer'><div class='mCSB_dragger' style='position:absolute;' oncontextmenu='return false;'><div class='mCSB_dragger_bar' style='position:relative;'></div></div><div class='mCSB_draggerRail'></div></div></div>");var k=f.children(".mCSB_scrollTools"),g=k.children(".mCSB_draggerContainer"),p=g.children(".mCSB_dragger");if(c.horizontalScroll){p.data("minDraggerWidth",p.width())}else{p.data("minDraggerHeight",p.height())}if(c.scrollButtons.enable){if(c.horizontalScroll){k.prepend("<a class='mCSB_buttonLeft' oncontextmenu='return false;'></a>").append("<a class='mCSB_buttonRight' oncontextmenu='return false;'></a>")}else{k.prepend("<a class='mCSB_buttonUp' oncontextmenu='return false;'></a>").append("<a class='mCSB_buttonDown' oncontextmenu='return false;'></a>")}}f.bind("scroll",function(){if(!l.is(".mCS_disabled")){f.scrollTop(0).scrollLeft(0)}});l.data({mCS_Init:true,mCustomScrollbarIndex:b(document).data("mCustomScrollbar-index"),horizontalScroll:c.horizontalScroll,scrollInertia:c.scrollInertia,scrollEasing:Power4.easeOut,mouseWheel:c.mouseWheel,mouseWheelPixels:c.mouseWheelPixels,autoDraggerLength:c.autoDraggerLength,autoHideScrollbar:c.autoHideScrollbar,scrollButtons_enable:c.scrollButtons.enable,scrollButtons_scrollType:c.scrollButtons.scrollType,scrollButtons_scrollSpeed:c.scrollButtons.scrollSpeed,scrollButtons_scrollAmount:c.scrollButtons.scrollAmount,autoExpandHorizontalScroll:c.advanced.autoExpandHorizontalScroll,autoScrollOnFocus:c.advanced.autoScrollOnFocus,onScrollStart_Callback:c.callbacks.onScrollStart,onScroll_Callback:c.callbacks.onScroll,onTotalScroll_Callback:c.callbacks.onTotalScroll,onTotalScrollBack_Callback:c.callbacks.onTotalScrollBack,onTotalScroll_Offset:c.callbacks.onTotalScrollOffset,onTotalScrollBack_Offset:c.callbacks.onTotalScrollBackOffset,whileScrolling_Callback:c.callbacks.whileScrolling,bindEvent_scrollbar_drag:false,bindEvent_content_touch:false,bindEvent_scrollbar_click:false,bindEvent_mousewheel:false,bindEvent_buttonsContinuous_y:false,bindEvent_buttonsContinuous_x:false,bindEvent_buttonsPixels_y:false,bindEvent_buttonsPixels_x:false,bindEvent_focusin:false,bindEvent_autoHideScrollbar:false,mCSB_buttonScrollRight:false,mCSB_buttonScrollLeft:false,mCSB_buttonScrollDown:false,mCSB_buttonScrollUp:false});if(c.horizontalScroll){if(l.css("max-width")!=="none"){if(!c.advanced.updateOnContentResize){c.advanced.updateOnContentResize=true}}}else{if(l.css("max-height")!=="none"){var r=false,q=parseInt(l.css("max-height"));if(l.css("max-height").indexOf("%")>=0){r=q,q=l.parent().height()*r/100}l.css("overflow","hidden");f.css("max-height",q)}}l.mCustomScrollbar("update");if(c.advanced.updateOnBrowserResize){var h,i=b(window).width(),t=b(window).height();b(window).resize(function(){if(h){clearTimeout(h)}h=setTimeout(function(){if(!l.is(".mCS_disabled")&&!l.is(".mCS_destroyed")){var v=b(window).width(),u=b(window).height();if(i!==v||t!==u){if(l.css("max-height")!=="none"&&r){f.css("max-height",l.parent().height()*r/100)}l.mCustomScrollbar("update");i=v;t=u}}},150)})}if(c.advanced.updateOnContentResize){var o;if(c.horizontalScroll){var m=n.outerWidth()}else{var m=n.outerHeight()}o=setInterval(function(){if(c.horizontalScroll){if(c.advanced.autoExpandHorizontalScroll){n.css({position:"absolute",width:"auto"}).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({width:n.outerWidth(),position:"relative"}).unwrap()}var u=n.outerWidth()}else{var u=n.outerHeight()}if(u!=m){l.mCustomScrollbar("update");m=u}},300)}})},update:function(){var l=b(this),i=l.children(".mCustomScrollBox"),o=i.children(".mCSB_container");o.removeClass("mCS_no_scrollbar");l.removeClass("mCS_disabled mCS_destroyed");i.scrollTop(0).scrollLeft(0);var w=i.children(".mCSB_scrollTools"),m=w.children(".mCSB_draggerContainer"),k=m.children(".mCSB_dragger");if(l.data("horizontalScroll")){var y=w.children(".mCSB_buttonLeft"),r=w.children(".mCSB_buttonRight"),d=i.width();if(l.data("autoExpandHorizontalScroll")){o.css({position:"absolute",width:"auto"}).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({width:o.outerWidth(),position:"relative"}).unwrap()}var x=o.outerWidth()}else{var u=w.children(".mCSB_buttonUp"),e=w.children(".mCSB_buttonDown"),p=i.height(),g=o.outerHeight()}if(g>p&&!l.data("horizontalScroll")){w.css("display","block");var q=m.height();if(l.data("autoDraggerLength")){var s=Math.round(p/g*q),j=k.data("minDraggerHeight");if(s<=j){k.css({height:j})}else{if(s>=q-10){var n=q-10;k.css({height:n})}else{k.css({height:s})}}k.children(".mCSB_dragger_bar").css({"line-height":k.height()+"px"})}var z=k.height(),v=(g-p)/(q-z);l.data("scrollAmount",v).mCustomScrollbar("scrolling",i,o,m,k,u,e,y,r);var B=Math.abs(o.position().top);l.mCustomScrollbar("scrollTo",B,{scrollInertia:0})}else{if(x>d&&l.data("horizontalScroll")){w.css("display","block");var f=m.width();if(l.data("autoDraggerLength")){var h=Math.round(d/x*f),A=k.data("minDraggerWidth");if(h<=A){k.css({width:A})}else{if(h>=f-10){var c=f-10;k.css({width:c})}else{k.css({width:h})}}}var t=k.width(),v=(x-d)/(f-t);l.data("scrollAmount",v).mCustomScrollbar("scrolling",i,o,m,k,u,e,y,r);var B=Math.abs(o.position().left);l.mCustomScrollbar("scrollTo",B,{scrollInertia:0})}else{i.unbind("mousewheel focusin");if(l.data("horizontalScroll")){k.add(o).css("left",0)}else{k.add(o).css("top",0)}w.css("display","none");o.addClass("mCS_no_scrollbar");l.data({bindEvent_mousewheel:false,bindEvent_focusin:false})}}},scrolling:function(f,n,k,h,u,c,w,t){var i=b(this);if(!i.data("bindEvent_scrollbar_drag")){var l,m;if(window.navigator.msPointerEnabled){h.bind("MSPointerDown",function(F){F.preventDefault();i.data({on_drag:true});h.addClass("mCSB_dragger_onDrag");var E=b(this),H=E.offset(),D=F.originalEvent.pageX-H.left,G=F.originalEvent.pageY-H.top;if(D<E.width()&&D>0&&G<E.height()&&G>0){l=G;m=D}});b(document).bind("MSPointerMove."+i.data("mCustomScrollbarIndex"),function(F){F.preventDefault();if(i.data("on_drag")){var E=h,H=E.offset(),D=F.originalEvent.pageX-H.left,G=F.originalEvent.pageY-H.top;B(l,m,G,D)}}).bind("MSPointerUp."+i.data("mCustomScrollbarIndex"),function(x){x.preventDefault();i.data({on_drag:false});h.removeClass("mCSB_dragger_onDrag")})}else{h.bind("mousedown touchstart",function(F){F.preventDefault();F.stopImmediatePropagation();var E=b(this),I=E.offset(),D,H;if(F.type==="touchstart"){var G=F.originalEvent.touches[0]||F.originalEvent.changedTouches[0];D=G.pageX-I.left;H=G.pageY-I.top}else{i.data({on_drag:true});h.addClass("mCSB_dragger_onDrag");D=F.pageX-I.left;H=F.pageY-I.top}if(D<E.width()&&D>0&&H<E.height()&&H>0){l=H;m=D}}).bind("touchmove",function(F){F.preventDefault();F.stopImmediatePropagation();var I=F.originalEvent.touches[0]||F.originalEvent.changedTouches[0],E=b(this),H=E.offset(),D=I.pageX-H.left,G=I.pageY-H.top;B(l,m,G,D)});b(document).bind("mousemove."+i.data("mCustomScrollbarIndex"),function(F){F.preventDefault();if(i.data("on_drag")){var E=h,H=E.offset(),D=F.pageX-H.left,G=F.pageY-H.top;B(l,m,G,D)}}).bind("mouseup."+i.data("mCustomScrollbarIndex"),function(x){x.preventDefault();i.data({on_drag:false});h.removeClass("mCSB_dragger_onDrag")})}i.data({bindEvent_scrollbar_drag:true})}function B(E,F,G,D){if(i.data("horizontalScroll")){i.mCustomScrollbar("scrollTo",(h.position().left-(F))+D,{moveDragger:true,trigger:"internal"})}else{i.mCustomScrollbar("scrollTo",(h.position().top-(E))+G,{moveDragger:true,trigger:"internal"})}}if(b(document).data("mCS-is-touch-device")){if(!i.data("bindEvent_content_touch")){var j,z,p,q,s,A,C;n.bind("touchstart",function(x){x.stopImmediatePropagation();j=x.originalEvent.touches[0]||x.originalEvent.changedTouches[0];z=b(this);p=z.offset();s=j.pageX-p.left;q=j.pageY-p.top;A=q;C=s});n.bind("touchmove",function(x){x.preventDefault();x.stopImmediatePropagation();j=x.originalEvent.touches[0]||x.originalEvent.changedTouches[0];z=b(this).parent();p=z.offset();s=j.pageX-p.left;q=j.pageY-p.top;if(i.data("horizontalScroll")){i.mCustomScrollbar("scrollTo",C-s,{trigger:"internal"})}else{i.mCustomScrollbar("scrollTo",A-q,{trigger:"internal"})}})}}if(!i.data("bindEvent_scrollbar_click")){k.bind("click",function(D){var x=(D.pageY-k.offset().top)*i.data("scrollAmount"),y=b(D.target);if(i.data("horizontalScroll")){x=(D.pageX-k.offset().left)*i.data("scrollAmount")}if(y.hasClass("mCSB_draggerContainer")||y.hasClass("mCSB_draggerRail")){i.mCustomScrollbar("scrollTo",x,{trigger:"internal"})}});i.data({bindEvent_scrollbar_click:true})}if(i.data("mouseWheel")){if(!i.data("bindEvent_mousewheel")){f.bind("mousewheel",function(F,H){var E,D=i.data("mouseWheelPixels"),x=Math.abs(n.position().top),G=h.position().top,y=k.height()-h.height();if(H<0){H=-1}else{H=1}if(D==="auto"){D=100+Math.round(i.data("scrollAmount")/2)}if(i.data("horizontalScroll")){G=h.position().left;y=k.width()-h.width();x=Math.abs(n.position().left)}if((H>0&&G!==0)||(H<0&&G!==y)){F.preventDefault();F.stopImmediatePropagation()}E=x-(H*D);i.mCustomScrollbar("scrollTo",E,{trigger:"internal"})});i.data({bindEvent_mousewheel:true})}}if(i.data("scrollButtons_enable")){if(i.data("scrollButtons_scrollType")==="pixels"){if(i.data("horizontalScroll")){t.add(w).unbind("mousedown touchstart MSPointerDown mouseup MSPointerUp mouseout MSPointerOut touchend",g,e);i.data({bindEvent_buttonsContinuous_x:false});if(!i.data("bindEvent_buttonsPixels_x")){t.bind("click",function(x){x.preventDefault();o(Math.abs(n.position().left)+i.data("scrollButtons_scrollAmount"))});w.bind("click",function(x){x.preventDefault();o(Math.abs(n.position().left)-i.data("scrollButtons_scrollAmount"))});i.data({bindEvent_buttonsPixels_x:true})}}else{c.add(u).unbind("mousedown touchstart MSPointerDown mouseup MSPointerUp mouseout MSPointerOut touchend",g,e);i.data({bindEvent_buttonsContinuous_y:false});if(!i.data("bindEvent_buttonsPixels_y")){c.bind("click",function(x){x.preventDefault();o(Math.abs(n.position().top)+i.data("scrollButtons_scrollAmount"))});u.bind("click",function(x){x.preventDefault();o(Math.abs(n.position().top)-i.data("scrollButtons_scrollAmount"))});i.data({bindEvent_buttonsPixels_y:true})}}function o(x){if(!h.data("preventAction")){h.data("preventAction",true);i.mCustomScrollbar("scrollTo",x,{trigger:"internal"})}}}else{if(i.data("horizontalScroll")){t.add(w).unbind("click");i.data({bindEvent_buttonsPixels_x:false});if(!i.data("bindEvent_buttonsContinuous_x")){t.bind("mousedown touchstart MSPointerDown",function(y){y.preventDefault();var x=v();i.data({mCSB_buttonScrollRight:setInterval(function(){i.mCustomScrollbar("scrollTo",Math.abs(n.position().left)+x,{trigger:"internal"})},17)})});var g=function(x){x.preventDefault();clearInterval(i.data("mCSB_buttonScrollRight"))};t.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",g);w.bind("mousedown touchstart MSPointerDown",function(y){y.preventDefault();var x=v();i.data({mCSB_buttonScrollLeft:setInterval(function(){i.mCustomScrollbar("scrollTo",Math.abs(n.position().left)-x,{trigger:"internal"})},17)})});var e=function(x){x.preventDefault();clearInterval(i.data("mCSB_buttonScrollLeft"))};w.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",e);i.data({bindEvent_buttonsContinuous_x:true})}}else{c.add(u).unbind("click");i.data({bindEvent_buttonsPixels_y:false});if(!i.data("bindEvent_buttonsContinuous_y")){c.bind("mousedown touchstart MSPointerDown",function(y){y.preventDefault();var x=v();i.data({mCSB_buttonScrollDown:setInterval(function(){i.mCustomScrollbar("scrollTo",Math.abs(n.position().top)+x,{trigger:"internal"})},17)})});var r=function(x){x.preventDefault();clearInterval(i.data("mCSB_buttonScrollDown"))};c.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",r);u.bind("mousedown touchstart MSPointerDown",function(y){y.preventDefault();var x=v();i.data({mCSB_buttonScrollUp:setInterval(function(){i.mCustomScrollbar("scrollTo",Math.abs(n.position().top)-x,{trigger:"internal"})},17)})});var d=function(x){x.preventDefault();clearInterval(i.data("mCSB_buttonScrollUp"))};u.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",d);i.data({bindEvent_buttonsContinuous_y:true})}}function v(){var x=i.data("scrollButtons_scrollSpeed");if(i.data("scrollButtons_scrollSpeed")==="auto"){x=Math.round((i.data("scrollInertia")+100)/20)}return x}}}if(i.data("autoScrollOnFocus")){if(!i.data("bindEvent_focusin")){f.bind("focusin",function(){f.scrollTop(0).scrollLeft(0);var x=b(document.activeElement);if(x.is("input,textarea,select,button,a[tabindex],area,object")){var E=n.position().top,y=x.position().top,D=f.height()-x.outerHeight();if(i.data("horizontalScroll")){E=n.position().left;y=x.position().left;D=f.width()-x.outerWidth()}if(E+y<0||E+y>D){i.mCustomScrollbar("scrollTo",y,{trigger:"internal"})}}});i.data({bindEvent_focusin:true})}}if(i.data("autoHideScrollbar")){if(!i.data("bindEvent_autoHideScrollbar")){f.bind("mouseenter",function(x){f.addClass("mCS-mouse-over").children(".mCSB_scrollTools").stop().animate({opacity:1},"fast")}).bind("mouseleave touchend",function(x){f.removeClass("mCS-mouse-over");if(x.type==="mouseleave"){f.children(".mCSB_scrollTools").stop().animate({opacity:0},"fast")}});i.data({bindEvent_autoHideScrollbar:true})}}},scrollTo:function(c,d){var h=b(this),n={moveDragger:false,trigger:"external",callbacks:true,scrollInertia:h.data("scrollInertia"),scrollEasing:h.data("scrollEasing")},d=b.extend(n,d),o,e=h.children(".mCustomScrollBox"),j=e.children(".mCSB_container"),q=e.children(".mCSB_scrollTools"),i=q.children(".mCSB_draggerContainer"),g=i.children(".mCSB_dragger"),s=draggerSpeed=d.scrollInertia,p,r,l,k;h.data({mCS_trigger:d.trigger});if(d.scrollInertia>0){s=draggerSpeed=d.scrollInertia/1000}if(h.data("mCS_Init")){d.callbacks=false}if(c||c===0){if(typeof(c)==="number"){if(d.moveDragger){o=c;if(h.data("horizontalScroll")){c=g.position().left*h.data("scrollAmount")}else{c=g.position().top*h.data("scrollAmount")}draggerSpeed=0}else{o=c/h.data("scrollAmount")}}else{if(typeof(c)==="string"){var t;if(c==="top"){t=0}else{if(c==="bottom"&&!h.data("horizontalScroll")){t=j.outerHeight()-e.height()}else{if(c==="left"){t=0}else{if(c==="right"&&h.data("horizontalScroll")){t=j.outerWidth()-e.width()}else{if(c==="first"){t=h.find(".mCSB_container").find(":first")}else{if(c==="last"){t=h.find(".mCSB_container").find(":last")}else{t=h.find(c)}}}}}}if(t.length===1){if(h.data("horizontalScroll")){c=t.position().left}else{c=t.position().top}o=c/h.data("scrollAmount")}else{o=c=t}}}if(h.data("horizontalScroll")){if(h.data("onTotalScrollBack_Offset")){r=-h.data("onTotalScrollBack_Offset")}if(h.data("onTotalScroll_Offset")){k=e.width()-j.outerWidth()+h.data("onTotalScroll_Offset")}if(o<0){o=c=0;clearInterval(h.data("mCSB_buttonScrollLeft"));if(!r){p=true}}else{if(o>=i.width()-g.width()){o=i.width()-g.width();c=e.width()-j.outerWidth();clearInterval(h.data("mCSB_buttonScrollRight"));if(!k){l=true}}else{c=-c}}TweenLite.to(g,draggerSpeed,{css:{left:Math.round(o)},overwrite:"all",ease:d.scrollEasing});TweenLite.to(j,s,{css:{left:Math.round(c)},overwrite:"all",ease:d.scrollEasing,onStart:function(){if(d.callbacks&&!h.data("mCS_tweenRunning")){u("onScrollStart")}if(h.data("autoHideScrollbar")){m()}},onUpdate:function(){if(d.callbacks){u("whileScrolling")}},onComplete:function(){if(d.callbacks){u("onScroll");if(p||(r&&j.position().left>=r)){u("onTotalScrollBack")}if(l||(k&&j.position().left<=k)){u("onTotalScroll")}}g.data("preventAction",false);h.data("mCS_tweenRunning",false);if(h.data("autoHideScrollbar")){f()}}})}else{if(h.data("onTotalScrollBack_Offset")){r=-h.data("onTotalScrollBack_Offset")}if(h.data("onTotalScroll_Offset")){k=e.height()-j.outerHeight()+h.data("onTotalScroll_Offset")}if(o<0){o=c=0;clearInterval(h.data("mCSB_buttonScrollUp"));if(!r){p=true}}else{if(o>=i.height()-g.height()){o=i.height()-g.height();c=e.height()-j.outerHeight();clearInterval(h.data("mCSB_buttonScrollDown"));if(!k){l=true}}else{c=-c}}TweenLite.to(g,draggerSpeed,{css:{top:Math.round(o)},overwrite:"all",ease:d.scrollEasing});TweenLite.to(j,s,{css:{top:Math.round(c)},overwrite:"all",ease:d.scrollEasing,onStart:function(){if(d.callbacks&&!h.data("mCS_tweenRunning")){u("onScrollStart")}if(h.data("autoHideScrollbar")){m()}},onUpdate:function(){if(d.callbacks){u("whileScrolling")}},onComplete:function(){if(d.callbacks){u("onScroll");if(p||(r&&j.position().top>=r)){u("onTotalScrollBack")}if(l||(k&&j.position().top<=k)){u("onTotalScroll")}}g.data("preventAction",false);h.data("mCS_tweenRunning",false);if(h.data("autoHideScrollbar")){f()}}})}if(h.data("mCS_Init")){h.data({mCS_Init:false})}}function u(v){this.mcs={top:j.position().top,left:j.position().left,draggerTop:g.position().top,draggerLeft:g.position().left,topPct:Math.round((100*Math.abs(j.position().top))/Math.abs(j.outerHeight()-e.height())),leftPct:Math.round((100*Math.abs(j.position().left))/Math.abs(j.outerWidth()-e.width()))};switch(v){case"onScrollStart":h.data("mCS_tweenRunning",true).data("onScrollStart_Callback").call(h,this.mcs);break;case"whileScrolling":h.data("whileScrolling_Callback").call(h,this.mcs);break;case"onScroll":h.data("onScroll_Callback").call(h,this.mcs);break;case"onTotalScrollBack":h.data("onTotalScrollBack_Callback").call(h,this.mcs);break;case"onTotalScroll":h.data("onTotalScroll_Callback").call(h,this.mcs);break}}function m(){TweenLite.to(q,0.15,{css:{opacity:1},ease:Power2.easeIn})}function f(){if(!e.hasClass("mCS-mouse-over")){TweenLite.to(q,0.15,{css:{opacity:0},ease:Power2.easeIn})}}},stop:function(){var e=b(this),c=e.children().children(".mCSB_container"),d=e.children().children().children().children(".mCSB_dragger");TweenLite.killTweensOf(c);TweenLite.killTweensOf(d)},disable:function(c){var h=b(this),d=h.children(".mCustomScrollBox"),f=d.children(".mCSB_container"),e=d.children(".mCSB_scrollTools"),g=e.children().children(".mCSB_dragger");d.unbind("mousewheel focusin mouseenter mouseleave touchend");f.unbind("touchstart touchmove");if(c){if(h.data("horizontalScroll")){g.add(f).css("left",0)}else{g.add(f).css("top",0)}}e.css("display","none");f.addClass("mCS_no_scrollbar");h.data({bindEvent_mousewheel:false,bindEvent_focusin:false,bindEvent_content_touch:false,bindEvent_autoHideScrollbar:false}).addClass("mCS_disabled")},destroy:function(){var c=b(this);c.removeClass("mCustomScrollbar _mCS_"+c.data("mCustomScrollbarIndex")).addClass("mCS_destroyed").children().children(".mCSB_container").unwrap().children().unwrap().siblings(".mCSB_scrollTools").remove();b(document).unbind("mousemove."+c.data("mCustomScrollbarIndex")+" mouseup."+c.data("mCustomScrollbarIndex")+" MSPointerMove."+c.data("mCustomScrollbarIndex")+" MSPointerUp."+c.data("mCustomScrollbarIndex"))}};b.fn.mCustomScrollbar=function(c){if(a[c]){return a[c].apply(this,Array.prototype.slice.call(arguments,1))}else{if(typeof c==="object"||!c){return a.init.apply(this,arguments)}else{b.error("Method "+c+" does not exist")}}}})(jQuery);;
/* 
== malihu jquery custom scrollbars plugin == 
version: 2.7 
author: malihu (http://manos.malihu.gr) 
plugin home: http://manos.malihu.gr/jquery-custom-content-scroller 
*/
(function($){
	/*plugin script*/
	var methods={
		init:function(options){
			var defaults={ 
				set_width:false, /*optional element width: boolean, pixels, percentage*/
				set_height:false, /*optional element height: boolean, pixels, percentage*/
				horizontalScroll:false, /*scroll horizontally: boolean*/
				scrollInertia:950, /*scrolling inertia: integer (milliseconds)*/
				mouseWheel:true, /*mousewheel support: boolean*/
				mouseWheelPixels:"auto", /*mousewheel pixels amount: integer, "auto"*/
				autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
				autoHideScrollbar:false, /*auto-hide scrollbar when idle*/
				scrollButtons:{ /*scroll buttons*/
					enable:false, /*scroll buttons support: boolean*/
					scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
					scrollSpeed:"auto", /*scroll buttons continuous scrolling speed: integer, "auto"*/
					scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/
				},
				advanced:{
					updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
					updateOnContentResize:false, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
					autoExpandHorizontalScroll:false, /*auto-expand width for horizontal scrolling: boolean*/
					autoScrollOnFocus:true /*auto-scroll on focused elements: boolean*/
				},
				callbacks:{
					onScrollStart:function(){}, /*user custom callback function on scroll start event*/
					onScroll:function(){}, /*user custom callback function on scroll event*/
					onTotalScroll:function(){}, /*user custom callback function on scroll end reached event*/
					onTotalScrollBack:function(){}, /*user custom callback function on scroll begin reached event*/
					onTotalScrollOffset:0, /*scroll end reached offset: integer (pixels)*/
					onTotalScrollBackOffset:0, /*scroll begin reached offset: integer (pixels)*/
					whileScrolling:function(){} /*user custom callback function on scrolling event*/
				},
				theme:"light" /*"light", "dark", "light-2", "dark-2", "light-thick", "dark-thick", "light-thin", "dark-thin"*/
			},
			options=$.extend(true,defaults,options);
			/*check for touch device*/
			$(document).data("mCS-is-touch-device",false);
			if(is_touch_device()){
				$(document).data("mCS-is-touch-device",true); 
			}
			function is_touch_device(){
				return !!("ontouchstart" in window) ? 1 : 0;
			}
			return this.each(function(){
				var $this=$(this);
				/*set element width/height, create markup for custom scrollbars, add classes*/
				if(options.set_width){
					$this.css("width",options.set_width);
				}
				if(options.set_height){
					$this.css("height",options.set_height);
				}
				if(!$(document).data("mCustomScrollbar-index")){
					$(document).data("mCustomScrollbar-index","1");
				}else{
					var mCustomScrollbarIndex=parseInt($(document).data("mCustomScrollbar-index"));
					$(document).data("mCustomScrollbar-index",mCustomScrollbarIndex+1);
				}
				$this.wrapInner("<div class='mCustomScrollBox"+" mCS-"+options.theme+"' id='mCSB_"+$(document).data("mCustomScrollbar-index")+"' style='position:relative; height:100%; overflow:hidden; max-width:100%;' />").addClass("mCustomScrollbar _mCS_"+$(document).data("mCustomScrollbar-index"));
				var mCustomScrollBox=$this.children(".mCustomScrollBox");
				if(options.horizontalScroll){
					mCustomScrollBox.addClass("mCSB_horizontal").wrapInner("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />");
					var mCSB_h_wrapper=mCustomScrollBox.children(".mCSB_h_wrapper");
					mCSB_h_wrapper.wrapInner("<div class='mCSB_container' style='position:absolute; left:0;' />").children(".mCSB_container").css({"width":mCSB_h_wrapper.children().outerWidth(),"position":"relative"}).unwrap();
				}else{
					mCustomScrollBox.wrapInner("<div class='mCSB_container' style='position:relative; top:0;' />");
				}
				var mCSB_container=mCustomScrollBox.children(".mCSB_container");
				if($(document).data("mCS-is-touch-device")){
					mCSB_container.addClass("mCS_touch");
				}
				mCSB_container.after("<div class='mCSB_scrollTools' style='position:absolute;'><div class='mCSB_draggerContainer'><div class='mCSB_dragger' style='position:absolute;' oncontextmenu='return false;'><div class='mCSB_dragger_bar' style='position:relative;'></div></div><div class='mCSB_draggerRail'></div></div></div>");
				var mCSB_scrollTools=mCustomScrollBox.children(".mCSB_scrollTools"),
					mCSB_draggerContainer=mCSB_scrollTools.children(".mCSB_draggerContainer"),
					mCSB_dragger=mCSB_draggerContainer.children(".mCSB_dragger");
				if(options.horizontalScroll){
					mCSB_dragger.data("minDraggerWidth",mCSB_dragger.width());
				}else{
					mCSB_dragger.data("minDraggerHeight",mCSB_dragger.height());
				}
				if(options.scrollButtons.enable){
					if(options.horizontalScroll){
						mCSB_scrollTools.prepend("<a class='mCSB_buttonLeft' oncontextmenu='return false;'></a>").append("<a class='mCSB_buttonRight' oncontextmenu='return false;'></a>");
					}else{
						mCSB_scrollTools.prepend("<a class='mCSB_buttonUp' oncontextmenu='return false;'></a>").append("<a class='mCSB_buttonDown' oncontextmenu='return false;'></a>");
					}
				}
				/*mCustomScrollBox scrollTop and scrollLeft is always 0 to prevent browser focus scrolling*/
				mCustomScrollBox.bind("scroll",function(){
					if(!$this.is(".mCS_disabled")){ /*native focus scrolling for disabled scrollbars*/
						mCustomScrollBox.scrollTop(0).scrollLeft(0);
					}
				});
				/*store options, global vars/states, intervals*/
				$this.data({
					/*init state*/
					"mCS_Init":true,
					/*instance index*/
					"mCustomScrollbarIndex":$(document).data("mCustomScrollbar-index"),
					/*option parameters*/
					"horizontalScroll":options.horizontalScroll,
					"scrollInertia":options.scrollInertia,
					"scrollEasing":Power4.easeOut,
					"mouseWheel":options.mouseWheel,
					"mouseWheelPixels":options.mouseWheelPixels,
					"autoDraggerLength":options.autoDraggerLength,
					"autoHideScrollbar":options.autoHideScrollbar,
					"scrollButtons_enable":options.scrollButtons.enable,
					"scrollButtons_scrollType":options.scrollButtons.scrollType,
					"scrollButtons_scrollSpeed":options.scrollButtons.scrollSpeed,
					"scrollButtons_scrollAmount":options.scrollButtons.scrollAmount,
					"autoExpandHorizontalScroll":options.advanced.autoExpandHorizontalScroll,
					"autoScrollOnFocus":options.advanced.autoScrollOnFocus,
					"onScrollStart_Callback":options.callbacks.onScrollStart,
					"onScroll_Callback":options.callbacks.onScroll,
					"onTotalScroll_Callback":options.callbacks.onTotalScroll,
					"onTotalScrollBack_Callback":options.callbacks.onTotalScrollBack,
					"onTotalScroll_Offset":options.callbacks.onTotalScrollOffset,
					"onTotalScrollBack_Offset":options.callbacks.onTotalScrollBackOffset,
					"whileScrolling_Callback":options.callbacks.whileScrolling,
					/*events binding state*/
					"bindEvent_scrollbar_drag":false,
					"bindEvent_content_touch":false,
					"bindEvent_scrollbar_click":false,
					"bindEvent_mousewheel":false,
					"bindEvent_buttonsContinuous_y":false,
					"bindEvent_buttonsContinuous_x":false,
					"bindEvent_buttonsPixels_y":false,
					"bindEvent_buttonsPixels_x":false,
					"bindEvent_focusin":false,
					"bindEvent_autoHideScrollbar":false,
					/*buttons intervals*/
					"mCSB_buttonScrollRight":false,
					"mCSB_buttonScrollLeft":false,
					"mCSB_buttonScrollDown":false,
					"mCSB_buttonScrollUp":false
				});
				/*max-width/max-height*/
				if(options.horizontalScroll){
					if($this.css("max-width")!=="none"){
						if(!options.advanced.updateOnContentResize){ /*needs updateOnContentResize*/
							options.advanced.updateOnContentResize=true;
						}
					}
				}else{
					if($this.css("max-height")!=="none"){
						var percentage=false,maxHeight=parseInt($this.css("max-height"));
						if($this.css("max-height").indexOf("%")>=0){
							percentage=maxHeight,
							maxHeight=$this.parent().height()*percentage/100;
						}
						$this.css("overflow","hidden");
						mCustomScrollBox.css("max-height",maxHeight);
					}
				}
				$this.mCustomScrollbar("update");
				/*window resize fn (for layouts based on percentages)*/
				if(options.advanced.updateOnBrowserResize){
					var mCSB_resizeTimeout,currWinWidth=$(window).width(),currWinHeight=$(window).height();
					$(window).resize(function(){
						if(mCSB_resizeTimeout){
							clearTimeout(mCSB_resizeTimeout);
						}
						mCSB_resizeTimeout=setTimeout(function(){
							if(!$this.is(".mCS_disabled") && !$this.is(".mCS_destroyed")){
								var winWidth=$(window).width(),winHeight=$(window).height();
								if(currWinWidth!==winWidth || currWinHeight!==winHeight){ /*ie8 fix*/
									if($this.css("max-height")!=="none" && percentage){
										mCustomScrollBox.css("max-height",$this.parent().height()*percentage/100);
									}
									$this.mCustomScrollbar("update");
									currWinWidth=winWidth; currWinHeight=winHeight;
								}
							}
						},150);
					});
				}
				/*content resize fn (for dynamically generated content)*/
				if(options.advanced.updateOnContentResize){
					var mCSB_onContentResize;
					if(options.horizontalScroll){
						var mCSB_containerOldSize=mCSB_container.outerWidth();
					}else{
						var mCSB_containerOldSize=mCSB_container.outerHeight();
					}
					mCSB_onContentResize=setInterval(function(){
						if(options.horizontalScroll){
							if(options.advanced.autoExpandHorizontalScroll){
								mCSB_container.css({"position":"absolute","width":"auto"}).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({"width":mCSB_container.outerWidth(),"position":"relative"}).unwrap();
							}
							var mCSB_containerNewSize=mCSB_container.outerWidth();
						}else{
							var mCSB_containerNewSize=mCSB_container.outerHeight();
						}
						if(mCSB_containerNewSize!=mCSB_containerOldSize){
							$this.mCustomScrollbar("update");
							mCSB_containerOldSize=mCSB_containerNewSize;
						}
					},300);
				}
			});
		},
		update:function(){
			var $this=$(this),
				mCustomScrollBox=$this.children(".mCustomScrollBox"),
				mCSB_container=mCustomScrollBox.children(".mCSB_container");
			mCSB_container.removeClass("mCS_no_scrollbar");
			$this.removeClass("mCS_disabled mCS_destroyed");
			mCustomScrollBox.scrollTop(0).scrollLeft(0); /*reset scrollTop/scrollLeft to prevent browser focus scrolling*/
			var mCSB_scrollTools=mCustomScrollBox.children(".mCSB_scrollTools"),
				mCSB_draggerContainer=mCSB_scrollTools.children(".mCSB_draggerContainer"),
				mCSB_dragger=mCSB_draggerContainer.children(".mCSB_dragger");
			if($this.data("horizontalScroll")){
				var mCSB_buttonLeft=mCSB_scrollTools.children(".mCSB_buttonLeft"),
					mCSB_buttonRight=mCSB_scrollTools.children(".mCSB_buttonRight"),
					mCustomScrollBoxW=mCustomScrollBox.width();
				if($this.data("autoExpandHorizontalScroll")){
					mCSB_container.css({"position":"absolute","width":"auto"}).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({"width":mCSB_container.outerWidth(),"position":"relative"}).unwrap();
				}
				var mCSB_containerW=mCSB_container.outerWidth();
			}else{
				var mCSB_buttonUp=mCSB_scrollTools.children(".mCSB_buttonUp"),
					mCSB_buttonDown=mCSB_scrollTools.children(".mCSB_buttonDown"),
					mCustomScrollBoxH=mCustomScrollBox.height(),
					mCSB_containerH=mCSB_container.outerHeight();
			}
			if(mCSB_containerH>mCustomScrollBoxH && !$this.data("horizontalScroll")){ /*content needs vertical scrolling*/
				mCSB_scrollTools.css("display","block");
				var mCSB_draggerContainerH=mCSB_draggerContainer.height();
				/*auto adjust scrollbar dragger length analogous to content*/
				if($this.data("autoDraggerLength")){
					var draggerH=Math.round(mCustomScrollBoxH/mCSB_containerH*mCSB_draggerContainerH),
						minDraggerH=mCSB_dragger.data("minDraggerHeight");
					if(draggerH<=minDraggerH){ /*min dragger height*/
						mCSB_dragger.css({"height":minDraggerH});
					}else if(draggerH>=mCSB_draggerContainerH-10){ /*max dragger height*/
						var mCSB_draggerContainerMaxH=mCSB_draggerContainerH-10;
						mCSB_dragger.css({"height":mCSB_draggerContainerMaxH});
					}else{
						mCSB_dragger.css({"height":draggerH});
					}
					mCSB_dragger.children(".mCSB_dragger_bar").css({"line-height":mCSB_dragger.height()+"px"});
				}
				var mCSB_draggerH=mCSB_dragger.height(),
				/*calculate and store scroll amount, add scrolling*/
					scrollAmount=(mCSB_containerH-mCustomScrollBoxH)/(mCSB_draggerContainerH-mCSB_draggerH);
				$this.data("scrollAmount",scrollAmount).mCustomScrollbar("scrolling",mCustomScrollBox,mCSB_container,mCSB_draggerContainer,mCSB_dragger,mCSB_buttonUp,mCSB_buttonDown,mCSB_buttonLeft,mCSB_buttonRight);
				/*scroll*/
				var mCSB_containerP=Math.abs(mCSB_container.position().top);
				$this.mCustomScrollbar("scrollTo",mCSB_containerP,{scrollInertia:0});
			}else if(mCSB_containerW>mCustomScrollBoxW && $this.data("horizontalScroll")){ /*content needs horizontal scrolling*/
				mCSB_scrollTools.css("display","block");
				var mCSB_draggerContainerW=mCSB_draggerContainer.width();
				/*auto adjust scrollbar dragger length analogous to content*/
				if($this.data("autoDraggerLength")){
					var draggerW=Math.round(mCustomScrollBoxW/mCSB_containerW*mCSB_draggerContainerW),
						minDraggerW=mCSB_dragger.data("minDraggerWidth");
					if(draggerW<=minDraggerW){ /*min dragger height*/
						mCSB_dragger.css({"width":minDraggerW});
					}else if(draggerW>=mCSB_draggerContainerW-10){ /*max dragger height*/
						var mCSB_draggerContainerMaxW=mCSB_draggerContainerW-10;
						mCSB_dragger.css({"width":mCSB_draggerContainerMaxW});
					}else{
						mCSB_dragger.css({"width":draggerW});
					}
				}
				var mCSB_draggerW=mCSB_dragger.width(),
				/*calculate and store scroll amount, add scrolling*/
					scrollAmount=(mCSB_containerW-mCustomScrollBoxW)/(mCSB_draggerContainerW-mCSB_draggerW);
				$this.data("scrollAmount",scrollAmount).mCustomScrollbar("scrolling",mCustomScrollBox,mCSB_container,mCSB_draggerContainer,mCSB_dragger,mCSB_buttonUp,mCSB_buttonDown,mCSB_buttonLeft,mCSB_buttonRight);
				/*scroll*/
				var mCSB_containerP=Math.abs(mCSB_container.position().left);
				$this.mCustomScrollbar("scrollTo",mCSB_containerP,{scrollInertia:0});
			}else{ /*content does not need scrolling*/
				/*unbind events, reset content position, hide scrollbars, remove classes*/
				mCustomScrollBox.unbind("mousewheel focusin");
				if($this.data("horizontalScroll")){
					mCSB_dragger.add(mCSB_container).css("left",0);
				}else{
					mCSB_dragger.add(mCSB_container).css("top",0);
				}
				mCSB_scrollTools.css("display","none");
				mCSB_container.addClass("mCS_no_scrollbar");
				$this.data({"bindEvent_mousewheel":false,"bindEvent_focusin":false});
			}
		},
		scrolling:function(mCustomScrollBox,mCSB_container,mCSB_draggerContainer,mCSB_dragger,mCSB_buttonUp,mCSB_buttonDown,mCSB_buttonLeft,mCSB_buttonRight){
			var $this=$(this);
			/*scrollbar drag scrolling*/
			if(!$this.data("bindEvent_scrollbar_drag")){
				var mCSB_draggerDragY,mCSB_draggerDragX;
				if(window.navigator.msPointerEnabled){ /*MSPointer*/
					mCSB_dragger.bind("MSPointerDown",function(e){
						e.preventDefault();
						$this.data({"on_drag":true}); mCSB_dragger.addClass("mCSB_dragger_onDrag");
						var elem=$(this),
							elemOffset=elem.offset(),
							x=e.originalEvent.pageX-elemOffset.left,
							y=e.originalEvent.pageY-elemOffset.top;
						if(x<elem.width() && x>0 && y<elem.height() && y>0){
							mCSB_draggerDragY=y;
							mCSB_draggerDragX=x;
						}
					});
					$(document).bind("MSPointerMove."+$this.data("mCustomScrollbarIndex"),function(e){
						e.preventDefault();
						if($this.data("on_drag")){
							var elem=mCSB_dragger,
								elemOffset=elem.offset(),
								x=e.originalEvent.pageX-elemOffset.left,
								y=e.originalEvent.pageY-elemOffset.top;
							scrollbarDrag(mCSB_draggerDragY,mCSB_draggerDragX,y,x);
						}
					}).bind("MSPointerUp."+$this.data("mCustomScrollbarIndex"),function(e){
						e.preventDefault();
						$this.data({"on_drag":false}); mCSB_dragger.removeClass("mCSB_dragger_onDrag");
					});
				}else{ /*mouse/touch*/
					mCSB_dragger.bind("mousedown touchstart",function(e){
						e.preventDefault(); e.stopImmediatePropagation();
						var	elem=$(this),elemOffset=elem.offset(),x,y;
						if(e.type==="touchstart"){
							var touch=e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
							x=touch.pageX-elemOffset.left; y=touch.pageY-elemOffset.top;
						}else{
							$this.data({"on_drag":true}); mCSB_dragger.addClass("mCSB_dragger_onDrag");
							x=e.pageX-elemOffset.left; y=e.pageY-elemOffset.top;
						}
						if(x<elem.width() && x>0 && y<elem.height() && y>0){
							mCSB_draggerDragY=y; mCSB_draggerDragX=x;
						}
					}).bind("touchmove",function(e){
						e.preventDefault(); e.stopImmediatePropagation();
						var touch=e.originalEvent.touches[0] || e.originalEvent.changedTouches[0],
							elem=$(this),
							elemOffset=elem.offset(),
							x=touch.pageX-elemOffset.left,
							y=touch.pageY-elemOffset.top;
						scrollbarDrag(mCSB_draggerDragY,mCSB_draggerDragX,y,x);
					});
					$(document).bind("mousemove."+$this.data("mCustomScrollbarIndex"),function(e){
						e.preventDefault();
						if($this.data("on_drag")){
							var elem=mCSB_dragger,
								elemOffset=elem.offset(),
								x=e.pageX-elemOffset.left,
								y=e.pageY-elemOffset.top;
							scrollbarDrag(mCSB_draggerDragY,mCSB_draggerDragX,y,x);
						}
					}).bind("mouseup."+$this.data("mCustomScrollbarIndex"),function(e){
						e.preventDefault();
						$this.data({"on_drag":false}); mCSB_dragger.removeClass("mCSB_dragger_onDrag");
					});
				}
				$this.data({"bindEvent_scrollbar_drag":true});
			}
			function scrollbarDrag(mCSB_draggerDragY,mCSB_draggerDragX,y,x){
				if($this.data("horizontalScroll")){
					$this.mCustomScrollbar("scrollTo",(mCSB_dragger.position().left-(mCSB_draggerDragX))+x,{moveDragger:true,trigger:"internal"});
				}else{
					$this.mCustomScrollbar("scrollTo",(mCSB_dragger.position().top-(mCSB_draggerDragY))+y,{moveDragger:true,trigger:"internal"});
				}
			}
			/*content touch-drag*/
			if($(document).data("mCS-is-touch-device")){
				if(!$this.data("bindEvent_content_touch")){
					var touch,
						elem,elemOffset,y,x,mCSB_containerTouchY,mCSB_containerTouchX;
					mCSB_container.bind("touchstart",function(e){
						e.stopImmediatePropagation();
						touch=e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
						elem=$(this);
						elemOffset=elem.offset();
						x=touch.pageX-elemOffset.left;
						y=touch.pageY-elemOffset.top;
						mCSB_containerTouchY=y;
						mCSB_containerTouchX=x;
					});
					mCSB_container.bind("touchmove",function(e){
						e.preventDefault(); e.stopImmediatePropagation();
						touch=e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
						elem=$(this).parent();
						elemOffset=elem.offset();
						x=touch.pageX-elemOffset.left;
						y=touch.pageY-elemOffset.top;
						if($this.data("horizontalScroll")){
							$this.mCustomScrollbar("scrollTo",mCSB_containerTouchX-x,{trigger:"internal"});
						}else{
							$this.mCustomScrollbar("scrollTo",mCSB_containerTouchY-y,{trigger:"internal"});
						}
					});
				}
			}
			/*dragger rail click scrolling*/
			if(!$this.data("bindEvent_scrollbar_click")){
				mCSB_draggerContainer.bind("click",function(e){
					var scrollToPos=(e.pageY-mCSB_draggerContainer.offset().top)*$this.data("scrollAmount"),target=$(e.target);
					if($this.data("horizontalScroll")){
						scrollToPos=(e.pageX-mCSB_draggerContainer.offset().left)*$this.data("scrollAmount");
					}
					if(target.hasClass("mCSB_draggerContainer") || target.hasClass("mCSB_draggerRail")){
						$this.mCustomScrollbar("scrollTo",scrollToPos,{trigger:"internal"});
					}
				});
				$this.data({"bindEvent_scrollbar_click":true});
			}
			/*mousewheel scrolling*/
			if($this.data("mouseWheel")){
				if(!$this.data("bindEvent_mousewheel")){
					mCustomScrollBox.bind("mousewheel",function(e,delta){
						var scrollTo,mouseWheelPixels=$this.data("mouseWheelPixels"),absPos=Math.abs(mCSB_container.position().top),
							draggerPos=mCSB_dragger.position().top,limit=mCSB_draggerContainer.height()-mCSB_dragger.height();
						if(delta<0){delta=-1;}else{delta=1;}
						if(mouseWheelPixels==="auto"){
							mouseWheelPixels=100+Math.round($this.data("scrollAmount")/2);
						}
						if($this.data("horizontalScroll")){
							draggerPos=mCSB_dragger.position().left; 
							limit=mCSB_draggerContainer.width()-mCSB_dragger.width();
							absPos=Math.abs(mCSB_container.position().left);
						}
						if((delta>0 && draggerPos!==0) || (delta<0 && draggerPos!==limit)){e.preventDefault(); e.stopImmediatePropagation();}
						scrollTo=absPos-(delta*mouseWheelPixels);
						$this.mCustomScrollbar("scrollTo",scrollTo,{trigger:"internal"});
					});
					$this.data({"bindEvent_mousewheel":true});
				}
			}
			/*buttons scrolling*/
			if($this.data("scrollButtons_enable")){
				if($this.data("scrollButtons_scrollType")==="pixels"){ /*scroll by pixels*/
					if($this.data("horizontalScroll")){
						mCSB_buttonRight.add(mCSB_buttonLeft).unbind("mousedown touchstart MSPointerDown mouseup MSPointerUp mouseout MSPointerOut touchend",mCSB_buttonRight_stop,mCSB_buttonLeft_stop);
						$this.data({"bindEvent_buttonsContinuous_x":false});
						if(!$this.data("bindEvent_buttonsPixels_x")){
							/*scroll right*/
							mCSB_buttonRight.bind("click",function(e){
								e.preventDefault();
								PixelsScrollTo(Math.abs(mCSB_container.position().left)+$this.data("scrollButtons_scrollAmount"));
							});
							/*scroll left*/
							mCSB_buttonLeft.bind("click",function(e){
								e.preventDefault();
								PixelsScrollTo(Math.abs(mCSB_container.position().left)-$this.data("scrollButtons_scrollAmount"));
							});
							$this.data({"bindEvent_buttonsPixels_x":true});
						}
					}else{
						mCSB_buttonDown.add(mCSB_buttonUp).unbind("mousedown touchstart MSPointerDown mouseup MSPointerUp mouseout MSPointerOut touchend",mCSB_buttonRight_stop,mCSB_buttonLeft_stop);
						$this.data({"bindEvent_buttonsContinuous_y":false});
						if(!$this.data("bindEvent_buttonsPixels_y")){
							/*scroll down*/
							mCSB_buttonDown.bind("click",function(e){
								e.preventDefault();
								PixelsScrollTo(Math.abs(mCSB_container.position().top)+$this.data("scrollButtons_scrollAmount"));
							});
							/*scroll up*/
							mCSB_buttonUp.bind("click",function(e){
								e.preventDefault();
								PixelsScrollTo(Math.abs(mCSB_container.position().top)-$this.data("scrollButtons_scrollAmount"));
							});
							$this.data({"bindEvent_buttonsPixels_y":true});
						}
					}
					function PixelsScrollTo(to){
						if(!mCSB_dragger.data("preventAction")){
							mCSB_dragger.data("preventAction",true);
							$this.mCustomScrollbar("scrollTo",to,{trigger:"internal"});
						}
					}
				}else{ /*continuous scrolling*/
					if($this.data("horizontalScroll")){
						mCSB_buttonRight.add(mCSB_buttonLeft).unbind("click");
						$this.data({"bindEvent_buttonsPixels_x":false});
						if(!$this.data("bindEvent_buttonsContinuous_x")){
							/*scroll right*/
							mCSB_buttonRight.bind("mousedown touchstart MSPointerDown",function(e){
								e.preventDefault();
								var scrollButtonsSpeed=ScrollButtonsSpeed();
								$this.data({"mCSB_buttonScrollRight":setInterval(function(){
									$this.mCustomScrollbar("scrollTo",Math.abs(mCSB_container.position().left)+scrollButtonsSpeed,{trigger:"internal"});
								},17)});
							});
							var mCSB_buttonRight_stop=function(e){
								e.preventDefault(); clearInterval($this.data("mCSB_buttonScrollRight"));
							}
							mCSB_buttonRight.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",mCSB_buttonRight_stop);
							/*scroll left*/
							mCSB_buttonLeft.bind("mousedown touchstart MSPointerDown",function(e){
								e.preventDefault();
								var scrollButtonsSpeed=ScrollButtonsSpeed();
								$this.data({"mCSB_buttonScrollLeft":setInterval(function(){
									$this.mCustomScrollbar("scrollTo",Math.abs(mCSB_container.position().left)-scrollButtonsSpeed,{trigger:"internal"});
								},17)});
							});	
							var mCSB_buttonLeft_stop=function(e){
								e.preventDefault(); clearInterval($this.data("mCSB_buttonScrollLeft"));
							}
							mCSB_buttonLeft.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",mCSB_buttonLeft_stop);
							$this.data({"bindEvent_buttonsContinuous_x":true});
						}
					}else{
						mCSB_buttonDown.add(mCSB_buttonUp).unbind("click");
						$this.data({"bindEvent_buttonsPixels_y":false});
						if(!$this.data("bindEvent_buttonsContinuous_y")){
							/*scroll down*/
							mCSB_buttonDown.bind("mousedown touchstart MSPointerDown",function(e){
								e.preventDefault();
								var scrollButtonsSpeed=ScrollButtonsSpeed();
								$this.data({"mCSB_buttonScrollDown":setInterval(function(){
									$this.mCustomScrollbar("scrollTo",Math.abs(mCSB_container.position().top)+scrollButtonsSpeed,{trigger:"internal"});
								},17)});
							});
							var mCSB_buttonDown_stop=function(e){
								e.preventDefault(); clearInterval($this.data("mCSB_buttonScrollDown"));
							}
							mCSB_buttonDown.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",mCSB_buttonDown_stop);
							/*scroll up*/
							mCSB_buttonUp.bind("mousedown touchstart MSPointerDown",function(e){
								e.preventDefault();
								var scrollButtonsSpeed=ScrollButtonsSpeed();
								$this.data({"mCSB_buttonScrollUp":setInterval(function(){
									$this.mCustomScrollbar("scrollTo",Math.abs(mCSB_container.position().top)-scrollButtonsSpeed,{trigger:"internal"});
								},17)});
							});	
							var mCSB_buttonUp_stop=function(e){
								e.preventDefault(); clearInterval($this.data("mCSB_buttonScrollUp"));
							}
							mCSB_buttonUp.bind("mouseup touchend MSPointerUp mouseout MSPointerOut",mCSB_buttonUp_stop);
							$this.data({"bindEvent_buttonsContinuous_y":true});
						}
					}
					function ScrollButtonsSpeed(){
						var speed=$this.data("scrollButtons_scrollSpeed");
						if($this.data("scrollButtons_scrollSpeed")==="auto"){
							speed=Math.round(($this.data("scrollInertia")+100)/20);
						}
						return speed;
					}
				}
			}
			/*scrolling on element focus (e.g. via TAB key)*/
			if($this.data("autoScrollOnFocus")){
				if(!$this.data("bindEvent_focusin")){
					mCustomScrollBox.bind("focusin",function(){
						mCustomScrollBox.scrollTop(0).scrollLeft(0);
						var focusedElem=$(document.activeElement);
						if(focusedElem.is("input,textarea,select,button,a[tabindex],area,object")){
							var mCSB_containerPos=mCSB_container.position().top,
								focusedElemPos=focusedElem.position().top,
								visibleLimit=mCustomScrollBox.height()-focusedElem.outerHeight();
							if($this.data("horizontalScroll")){
								mCSB_containerPos=mCSB_container.position().left;
								focusedElemPos=focusedElem.position().left;
								visibleLimit=mCustomScrollBox.width()-focusedElem.outerWidth();
							}
							if(mCSB_containerPos+focusedElemPos<0 || mCSB_containerPos+focusedElemPos>visibleLimit){
								$this.mCustomScrollbar("scrollTo",focusedElemPos,{trigger:"internal"});
							}
						}
					});
					$this.data({"bindEvent_focusin":true});
				}
			}
			/*auto-hide scrollbar*/
			if($this.data("autoHideScrollbar")){
				if(!$this.data("bindEvent_autoHideScrollbar")){
					mCustomScrollBox.bind("mouseenter",function(e){
						mCustomScrollBox.addClass("mCS-mouse-over").children(".mCSB_scrollTools").stop().animate({opacity:1},"fast");
					}).bind("mouseleave touchend",function(e){
						mCustomScrollBox.removeClass("mCS-mouse-over");
						if(e.type==="mouseleave"){mCustomScrollBox.children(".mCSB_scrollTools").stop().animate({opacity:0},"fast");}
					});
					$this.data({"bindEvent_autoHideScrollbar":true});
				}
			}
		},
		scrollTo:function(scrollTo,options){
			var $this=$(this),
				defaults={
					moveDragger:false,
					trigger:"external",
					callbacks:true,
					scrollInertia:$this.data("scrollInertia"),
					scrollEasing:$this.data("scrollEasing")
				},
				options=$.extend(defaults,options),
				draggerScrollTo,
				mCustomScrollBox=$this.children(".mCustomScrollBox"),
				mCSB_container=mCustomScrollBox.children(".mCSB_container"),
				mCSB_scrollTools=mCustomScrollBox.children(".mCSB_scrollTools"),
				mCSB_draggerContainer=mCSB_scrollTools.children(".mCSB_draggerContainer"),
				mCSB_dragger=mCSB_draggerContainer.children(".mCSB_dragger"),
				contentSpeed=draggerSpeed=options.scrollInertia,
				scrollBeginning,scrollBeginningOffset,totalScroll,totalScrollOffset;
			$this.data({"mCS_trigger":options.trigger});
			if(options.scrollInertia>0){contentSpeed=draggerSpeed=options.scrollInertia/1000;}
			if($this.data("mCS_Init")){options.callbacks=false;}
			if(scrollTo || scrollTo===0){
				if(typeof(scrollTo)==="number"){ /*if integer, scroll by number of pixels*/
					if(options.moveDragger){ /*scroll dragger*/
						draggerScrollTo=scrollTo;
						if($this.data("horizontalScroll")){
							scrollTo=mCSB_dragger.position().left*$this.data("scrollAmount");
						}else{
							scrollTo=mCSB_dragger.position().top*$this.data("scrollAmount");
						}
						draggerSpeed=0;
					}else{ /*scroll content by default*/
						draggerScrollTo=scrollTo/$this.data("scrollAmount");
					}
				}else if(typeof(scrollTo)==="string"){ /*if string, scroll by element position*/
					var target;
					if(scrollTo==="top"){ /*scroll to top*/
						target=0;
					}else if(scrollTo==="bottom" && !$this.data("horizontalScroll")){ /*scroll to bottom*/
						target=mCSB_container.outerHeight()-mCustomScrollBox.height();
					}else if(scrollTo==="left"){ /*scroll to left*/
						target=0;
					}else if(scrollTo==="right" && $this.data("horizontalScroll")){ /*scroll to right*/
						target=mCSB_container.outerWidth()-mCustomScrollBox.width();
					}else if(scrollTo==="first"){ /*scroll to first element position*/
						target=$this.find(".mCSB_container").find(":first");
					}else if(scrollTo==="last"){ /*scroll to last element position*/
						target=$this.find(".mCSB_container").find(":last");
					}else{ /*scroll to element position*/
						target=$this.find(scrollTo);
					}
					if(target.length===1){ /*if such unique element exists, scroll to it*/
						if($this.data("horizontalScroll")){
							scrollTo=target.position().left;
						}else{
							scrollTo=target.position().top;
						}
						draggerScrollTo=scrollTo/$this.data("scrollAmount");
					}else{
						draggerScrollTo=scrollTo=target;
					}
				}
				/*scroll to*/
				if($this.data("horizontalScroll")){
					if($this.data("onTotalScrollBack_Offset")){ /*scroll beginning offset*/
						scrollBeginningOffset=-$this.data("onTotalScrollBack_Offset");
					}
					if($this.data("onTotalScroll_Offset")){ /*total scroll offset*/
						totalScrollOffset=mCustomScrollBox.width()-mCSB_container.outerWidth()+$this.data("onTotalScroll_Offset");
					}
					if(draggerScrollTo<0){ /*scroll start position*/
						draggerScrollTo=scrollTo=0; clearInterval($this.data("mCSB_buttonScrollLeft"));
						if(!scrollBeginningOffset){scrollBeginning=true;}
					}else if(draggerScrollTo>=mCSB_draggerContainer.width()-mCSB_dragger.width()){ /*scroll end position*/
						draggerScrollTo=mCSB_draggerContainer.width()-mCSB_dragger.width();
						scrollTo=mCustomScrollBox.width()-mCSB_container.outerWidth(); clearInterval($this.data("mCSB_buttonScrollRight"));
						if(!totalScrollOffset){totalScroll=true;}
					}else{scrollTo=-scrollTo;}
					/*scrolling animation*/
					TweenLite.to(mCSB_dragger,draggerSpeed,{css:{left:Math.round(draggerScrollTo)},overwrite:"all",ease:options.scrollEasing});
					TweenLite.to(mCSB_container,contentSpeed,{css:{left:Math.round(scrollTo)},overwrite:"all",ease:options.scrollEasing,
						onStart:function(){
							if(options.callbacks && !$this.data("mCS_tweenRunning")){callbacks("onScrollStart");}
							if($this.data("autoHideScrollbar")){showScrollbar();}
						},
						onUpdate:function(){
							if(options.callbacks){callbacks("whileScrolling");}
						},
						onComplete:function(){
							if(options.callbacks){
								callbacks("onScroll");
								if(scrollBeginning || (scrollBeginningOffset && mCSB_container.position().left>=scrollBeginningOffset)){callbacks("onTotalScrollBack");}
								if(totalScroll || (totalScrollOffset && mCSB_container.position().left<=totalScrollOffset)){callbacks("onTotalScroll");}
							}
							mCSB_dragger.data("preventAction",false); $this.data("mCS_tweenRunning",false);
							if($this.data("autoHideScrollbar")){hideScrollbar();}
						}
					});
				}else{
					if($this.data("onTotalScrollBack_Offset")){ /*scroll beginning offset*/
						scrollBeginningOffset=-$this.data("onTotalScrollBack_Offset");
					}
					if($this.data("onTotalScroll_Offset")){ /*total scroll offset*/
						totalScrollOffset=mCustomScrollBox.height()-mCSB_container.outerHeight()+$this.data("onTotalScroll_Offset");
					}
					if(draggerScrollTo<0){ /*scroll start position*/
						draggerScrollTo=scrollTo=0; clearInterval($this.data("mCSB_buttonScrollUp"));
						if(!scrollBeginningOffset){scrollBeginning=true;}
					}else if(draggerScrollTo>=mCSB_draggerContainer.height()-mCSB_dragger.height()){ /*scroll end position*/
						draggerScrollTo=mCSB_draggerContainer.height()-mCSB_dragger.height();
						scrollTo=mCustomScrollBox.height()-mCSB_container.outerHeight(); clearInterval($this.data("mCSB_buttonScrollDown"));
						if(!totalScrollOffset){totalScroll=true;}
					}else{scrollTo=-scrollTo;}
					/*scrolling animation*/
					TweenLite.to(mCSB_dragger,draggerSpeed,{css:{top:Math.round(draggerScrollTo)},overwrite:"all",ease:options.scrollEasing});
					TweenLite.to(mCSB_container,contentSpeed,{css:{top:Math.round(scrollTo)},overwrite:"all",ease:options.scrollEasing,
						onStart:function(){
							if(options.callbacks && !$this.data("mCS_tweenRunning")){callbacks("onScrollStart");}
							if($this.data("autoHideScrollbar")){showScrollbar();}
						},
						onUpdate:function(){
							if(options.callbacks){callbacks("whileScrolling");}
						},
						onComplete:function(){
							if(options.callbacks){
								callbacks("onScroll");
								if(scrollBeginning || (scrollBeginningOffset && mCSB_container.position().top>=scrollBeginningOffset)){callbacks("onTotalScrollBack");}
								if(totalScroll || (totalScrollOffset && mCSB_container.position().top<=totalScrollOffset)){callbacks("onTotalScroll");}
							}
							mCSB_dragger.data("preventAction",false); $this.data("mCS_tweenRunning",false);
							if($this.data("autoHideScrollbar")){hideScrollbar();}
						}
					});
				}
				if($this.data("mCS_Init")){$this.data({"mCS_Init":false});}
			}
			/*callbacks*/
			function callbacks(cb){
				this.mcs={
					top:mCSB_container.position().top,left:mCSB_container.position().left,
					draggerTop:mCSB_dragger.position().top,draggerLeft:mCSB_dragger.position().left,
					topPct:Math.round((100*Math.abs(mCSB_container.position().top))/Math.abs(mCSB_container.outerHeight()-mCustomScrollBox.height())),
					leftPct:Math.round((100*Math.abs(mCSB_container.position().left))/Math.abs(mCSB_container.outerWidth()-mCustomScrollBox.width()))
				};
				switch(cb){
					/*start scrolling callback*/
					case "onScrollStart":
						$this.data("mCS_tweenRunning",true).data("onScrollStart_Callback").call($this,this.mcs);
						break;
					case "whileScrolling":
						$this.data("whileScrolling_Callback").call($this,this.mcs);
						break;
					case "onScroll":
						$this.data("onScroll_Callback").call($this,this.mcs);
						break;
					case "onTotalScrollBack":
						$this.data("onTotalScrollBack_Callback").call($this,this.mcs);
						break;
					case "onTotalScroll":
						$this.data("onTotalScroll_Callback").call($this,this.mcs);
						break;
				}
			}
			/*hide/show scrollbar*/
			function showScrollbar(){
				TweenLite.to(mCSB_scrollTools,0.15,{css:{opacity:1},ease:Power2.easeIn});
			}
			function hideScrollbar(){
				if(!mCustomScrollBox.hasClass("mCS-mouse-over")){TweenLite.to(mCSB_scrollTools,0.15,{css:{opacity:0},ease:Power2.easeIn});}
			}
		},
		stop:function(){
			var $this=$(this),
				mCSB_container=$this.children().children(".mCSB_container"),
				mCSB_dragger=$this.children().children().children().children(".mCSB_dragger");
			TweenLite.killTweensOf(mCSB_container);
			TweenLite.killTweensOf(mCSB_dragger);
		},
		disable:function(resetScroll){
			var $this=$(this),
				mCustomScrollBox=$this.children(".mCustomScrollBox"),
				mCSB_container=mCustomScrollBox.children(".mCSB_container"),
				mCSB_scrollTools=mCustomScrollBox.children(".mCSB_scrollTools"),
				mCSB_dragger=mCSB_scrollTools.children().children(".mCSB_dragger");
			mCustomScrollBox.unbind("mousewheel focusin mouseenter mouseleave touchend");
			mCSB_container.unbind("touchstart touchmove")
			if(resetScroll){
				if($this.data("horizontalScroll")){
					mCSB_dragger.add(mCSB_container).css("left",0);
				}else{
					mCSB_dragger.add(mCSB_container).css("top",0);
				}
			}
			mCSB_scrollTools.css("display","none");
			mCSB_container.addClass("mCS_no_scrollbar");
			$this.data({"bindEvent_mousewheel":false,"bindEvent_focusin":false,"bindEvent_content_touch":false,"bindEvent_autoHideScrollbar":false}).addClass("mCS_disabled");
		},
		destroy:function(){
			var $this=$(this);
			$this.removeClass("mCustomScrollbar _mCS_"+$this.data("mCustomScrollbarIndex")).addClass("mCS_destroyed").children().children(".mCSB_container").unwrap().children().unwrap().siblings(".mCSB_scrollTools").remove();
			$(document).unbind("mousemove."+$this.data("mCustomScrollbarIndex")+" mouseup."+$this.data("mCustomScrollbarIndex")+" MSPointerMove."+$this.data("mCustomScrollbarIndex")+" MSPointerUp."+$this.data("mCustomScrollbarIndex"));
		}
	}
	/*plugin dependencies*/
	$.event.special.mousewheel || document.write('<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.0.6/jquery.mousewheel.min.js"><\/script>');
	(window.com && window.com.greensock && window.com.greensock.TweenLite) || document.write('<script src="//cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenLite.min.js"><\/script>');
	(window.com && window.com.greensock && window.com.greensock.plugins && window.com.greensock.plugins.CSSPlugin) || document.write('<script src="//cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/CSSPlugin.min.js"><\/script>');
	/*plugin fn*/
	$.fn.mCustomScrollbar=function(method){
		if(methods[method]){
			return methods[method].apply(this,Array.prototype.slice.call(arguments,1));
		}else if(typeof method==="object" || !method){
			return methods.init.apply(this,arguments);
		}else{
			$.error("Method "+method+" does not exist");
		}
	};
})(jQuery);;
jQuery(document).ready(function($) {

	
	
// --------------- ## HEADER ## ------------------

	
	//Mostra e esconde a label do campo de busca
	$('.form-busca .input-busca').focus(function(){
		$('.form-busca label').stop().fadeOut(400);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('label').stop().fadeIn(400);
		}
	});

	//Hover do menu principal	
	$('.menu-principal ul.primeiro-nivel li').mouseenter(function(){		
		$(this).css('height', '53').find('.segundo-nivel').show();
	}).mouseleave(function(){
		$(this).css('height', '45').find('.segundo-nivel').hide();
		$('.menu-principal ul.primeiro-nivel li.l03 .segundo-nivel').css({'top':'0'}).removeClass('segundo-nivel-footer');				
	});
	
	$('.menu-principal ul.primeiro-nivel li.l03 .segundo-nivel').prepend('<span class="close">X</span>');
	$('.menu-principal ul.primeiro-nivel li:eq(1)').addClass('li-trabalhar');	

	//Divide o menu de 3º nível	
	
	var getUrl = window.location.href;
	var arrayUrl = getUrl.split("/");
	var indiceUrl = arrayUrl[3].toString();
		
	
	if(indiceUrl == 'iexplorer'){
		var listaTotal = $('.menu-level-3 ul.menu li').size();	
		var listaFinal = ''	;
		listaTotal--;

		for (i=1; i<=listaTotal; i++){		
			var pegaLi = $('.menu-level-3 ul.menu li:eq('+i+')').html();
			listaFinal += '<li>'+pegaLi+'</li>';

			$('.menu-level-3 ul.menu li:eq('+i+')').hide();
		}

		var listaFinal = '<li><a href="http://site.ieintercambio.com.br/iexplorer/destinos/america">América</a></li><li><a href="http://site.ieintercambio.com.br/iexplorer/destinos/europa">Europa</a></li>';

		$('.menu-level-3 .menu').after('<div class="menu_33"><span>NAVEGAR POR CONTINENTE</span><ul class="menu_sel">'+listaFinal+'</ul></div>');
		$('.menu_sel').prependTo('#content');
	}
	
	
	$('.menu_33 span').click(function(){
		$('.menu_sel').slideToggle('fast');
	});

	//$('#content .menu_sel li a').removeClass('active');
	/*$('#content .menu_sel li a').click(function(){
		$('#content .menu_sel li a').removeClass('active');
		$(this).addClass('active');
	});*/

	
	//adiciona o ativo ao submenu de 3º nível
	$('.menu-level-3 ul.menu li.active').append('<div class="ativo">botão ativo</div>');

	//Centraliza o submenu de 3º nível
	var tamTotal = 450;
	var tamAtual = $('.page-title .fltrgt').width();
	var pxMargin = parseInt(tamTotal-tamAtual)/2;

	$('.page-title .fltrgt').css('margin-right', pxMargin);


	//WEBDOOR
	// Trata os títulos do webdoor
	$('.webdoor .views-row .textos div').each(function(){
		var pegaTexto = $(this).find('p.titulo').text();
		var quebraTexto = pegaTexto.split(' ');

		var qtd_itens = quebraTexto.length;
	//	console.log(qtd_itens);
		var frase_final = '';

		for(i=0; i<qtd_itens; i++){

			if(quebraTexto[i] == 'intercâmbios'){
				frase_final += '<strong>intercâmbios</strong> ';
			}else{
				frase_final += quebraTexto[i]+' ';  
			}
		}	
	//	console.log(frase_final);
		$(this).find('p.titulo').empty();
		$(this).find('p.titulo').html(frase_final);
	});


	//Ativa o primeiro item do webdoor.
	$('.webdoor .item-webdoor:eq(0)').addClass('wAtivo').find('.textos div:eq(0)').addClass('dados-ativo');

	//adiciona ID para identificar a imagem, o texto e o bullet com a div principal
	var qtd = $('.webdoor .item-webdoor').size();
	for(i=0; i<qtd; i++){		
		$('.bullets .navegacao').append('<li class="item'+i+'" rel="item'+i+'">item'+i+'</li>');
		$('.webdoor .item-webdoor:eq('+i+')').addClass('item'+i+'').find('.imagem img').attr('id', 'item'+i+'');		
	}

	//Ativa a primeira bullet
	$('.navegacao li:eq(0)').addClass('ativo');

	//executa a função que monta as thumbs
	montaThumbs();

	//Navegação por bullet
	$('.navegacao li').click(function(){			

		if(!$(this).hasClass('ativo')){

			var pegaRel = $(this).attr('class');

			$('.view-id-webdoors .view-content div').removeClass('wAtivo').find('.textos div:eq(0)').removeClass('dados-ativo');
			$('.view-id-webdoors').find('.'+pegaRel+'').addClass('wAtivo').find('.textos div:eq(0)').addClass('dados-ativo');
			montaThumbs();
			$('.navegacao li').removeClass('ativo');
			$(this).addClass('ativo');

		}

	});

	
	//navegação por thumbs
	function montaThumbs() {
		//Identifica o primeiro e o último item.
		var qtdWebdoor = ($('.webdoor .item-webdoor').size())-1;
		$('.webdoor .item-webdoor:eq(0)').attr('id', 'primeiro');
		$('.webdoor .item-webdoor:eq('+qtdWebdoor+')').attr('id', 'ultimo');

		//Verifica se é o primeiro ou o último item.
		var identificaId = $('.webdoor .wAtivo').attr('id');

		//Gera e aplica a thumb para navegação do anterior e do próximo.
		if(identificaId == "primeiro"){
			var pegaNextThumb = $('.webdoor .wAtivo').next('.item-webdoor').find('.imagem img').clone();
			var pegaPrevThumb = $('.webdoor').find('#ultimo').find('.imagem img').clone();
		}else if(identificaId == "ultimo"){
			var pegaNextThumb = $('.webdoor').find('#primeiro').find('.imagem img').clone();
			var pegaPrevThumb = $('.webdoor .wAtivo').prev('.item-webdoor').find('.imagem img').clone();
		}else{
			var pegaNextThumb = $('.webdoor .wAtivo').next('.item-webdoor').find('.imagem img').clone();
			var pegaPrevThumb = $('.webdoor .wAtivo').prev('.item-webdoor').find('.imagem img').clone();
		}
		
		//Aplica a thumb
		$('#thumbPrev').html(pegaPrevThumb);
		$('#thumbNext').html(pegaNextThumb);
	}
	
	//hover das thumbs
	$('#thumbPrev').hover(function(){
		$(this).append('<span class="itemPrev"></span>');
		$(this).children('span').stop().animate({right: '0'}, 300);
	}).mouseleave(function(){
		$(this).children('span').stop().animate({right: '-135px'}, 300, function(){
			$('#thumbPrev').find('span').remove();
		});
	});

	$('#thumbNext').hover(function(){
		$(this).append('<span class="itemNext"></span>');
		$(this).children('span').stop().animate({left: '0'}, 300);
	}).mouseleave(function(){
		$(this).children('span').stop().animate({left: '-135px'}, 300, function(){
			$('#thumbNext').find('span').remove();
		});
	});

	//Navegação por thumbnail
	$('.thumbs .thumWebdoor').click(function(){
		var pegaRel = $(this).children('img').attr('id');
		
		$('.view-id-webdoors .view-content div').removeClass('wAtivo').find('.textos div:eq(0)').removeClass('dados-ativo');
		$('.view-id-webdoors').find('.'+pegaRel+'').addClass('wAtivo').find('.textos div:eq(0)').addClass('dados-ativo');
		
		$('.navegacao li').removeClass('ativo');
		$('.navegacao li.'+pegaRel+'').addClass('ativo');
		montaThumbs()
	});

	//Identifica o primeiro e o último item.
		var qtdWebdoor = ($('.webdoor .item-webdoor').size())-1;
		$('.navegacao li:eq(0)').attr('id', 'primeiro');
		$('.navegacao li:eq('+qtdWebdoor+')').attr('id', 'ultimo');
	
	//aqui vem a troca de webdoor com timeout
	function trocaWebdoor(){
		var identificaId = $('.navegacao li.ativo').attr('id');
		var pegaRel = $('.navegacao li.ativo').next('li').attr('rel');

		if(identificaId == 'ultimo'){
			$('.webdoor div.wAtivo').removeClass('wAtivo').find('.textos div:eq(0)').removeClass('dados-ativo');
			$('.webdoor .item-webdoor:eq(0)').addClass('wAtivo').find('.textos div:eq(0)').addClass('dados-ativo');
			$('.navegacao li').removeClass('ativo');
			$('.navegacao li:eq(0)').addClass('ativo');
		}else{
			$('.webdoor div.wAtivo').removeClass('wAtivo').find('.textos div:eq(0)').removeClass('dados-ativo');
			$('.view-id-webdoors').find('.'+pegaRel+'').addClass('wAtivo').find('.textos div:eq(0)').addClass('dados-ativo');
			$('.navegacao li.ativo').next('li').addClass('ativo').prev('li').removeClass('ativo');
		}
	}

	// troca automática do webdoor
	setInterval(function() {
      trocaWebdoor()
      montaThumbs()
	}, 10000);



// --------------- ## CONTENT ## ------------------

	//Detalhes dos Boxes
	$('#box3col .col01').prepend('<span class="bicot"></span><span class="bicob"></span>');

	// PÁGINA CIDADES INTERNA
	if($('.node-cidades')[0]){
		$('.field-name-field-cidades-cidade .field-items').prepend('<div class="boxIcon">&nbsp;</div>');
		$('.field-name-field-cidades-imagem-1').append('<div class="espaco2">&nbsp;</div>');
	}

	// PÁGINA PROMOÇÕES
	if($('.view-promooes-destaque-pagina')[0]){
		$('.view-promooes-destaque-pagina .views-row').append('<div class="bread">&nbsp;</div>');
		$('.view-promooes-destaque-pagina .views-row:nth-child(2n)').css('margin-right', '0');
	}

	if($('.view-id-promoces')[0]){
		$('.view-id-promoces .views-row').append('<div class="bread">&nbsp;</div>');
		$('.view-id-promoces .views-row .views-field-field-promocoes-imagem .field-content').append('<div class="dat">&nbsp;</div>');

		var pegaPais = $('.view-id-promoces .views-row:eq(0) .views-field-field-promocoes-pais .field-content');
		$('.galeria20').prepend('<h3>'+pegaPais.html()+'</h3>');
		$('.view-id-promoces .views-row').each(function(){
			$(this).find('.views-field-field-promocoes-pais .field-content').remove();
		});
	}

	// PÁGINA FEIRA DE CONTRATAÇÃO
	if($('.view-feira-de-contratacao')[0]){
		$('.views-field-field-feiras-datas').prepend('<span>Datas e <strong>Locais</strong></span>');

		$('.views-field-field-feiras-datas li').each(function(){
			var pegaTexto = $(this).find('p').text();
			var quebraTexto = pegaTexto.split('-');
			$(this).find('p').remove();
			
			$(this).prepend('<div class="datas"><strong>'+quebraTexto[0]+'</strong>'+quebraTexto[1]+'</div>');
		});
	}

	// Promoções da Home
	$('#block-views-promocoes-destaque-block .view-promocoes-destaque').before('<div id="box1col"><div class="fltlft boxIcon icone boxcolor"></div><div class="fltlft boxTitle">PROMOÇÕES  IE</div><div class="clear"></div><div class="fltlft boxBar"></div><div class="fltlft box"></div></div>');

	// Monta o player do bloco TV IE
	if($('.view-tv-ie')[0]){
		
		var getUrlVideo = $('.view-tv-ie .views-row .views-field-field-video .field-content').text();
		var indexItem = getUrlVideo.indexOf('&list');
		//alert(indexItem);			
		
		if(indexItem < 0){
			var idVIdeo = getUrlVideo.split('=');
			$('.view-tv-ie .views-row .views-field-field-video .field-content').html('<iframe id="video-home" width="287px" height="195px" src="http://www.youtube.com/embed/'+idVIdeo[1]+'?wmode=transparent" frameborder="0" allowfullscreen></iframe></div>');
		}else{			
			var testeUrl = getUrlVideo.split('&list');			
			var idLista = testeUrl[0].split('?v=');			
			$('.view-tv-ie .views-row .views-field-field-video .field-content').html('<iframe id="video-home" width="287px" height="195px" src="http://www.youtube.com/embed/'+idLista[1]+'?wmode=transparent" frameborder="0" allowfullscreen></iframe></div>');
		}		
	}
	
	// Move o conteúdo do Vídeo pra dentro do box	
	$('.view-tv-ie .view-content').prependTo('.view-tv-ie #box1col .fltlft.box');

	// Galeria dos mochilões
	if($('.view-mochiloes')[0]){
		var qtdItens = $('.view-mochiloes .views-row').size();
		var qtdDivs = qtdItens/4; 

		if(qtdDivs > 1 && qtdDivs < 2){
			qtdDivs = 2;
		}else if(qtdDivs > 2 && qtdDivs < 3){
			qtdDivs = 3;
		}else if(qtdDivs > 3 && qtdDivs < 4){
			qtdDivs = 4;
		}else if(qtdDivs > 4 && qtdDivs < 5){
			qtdDivs = 5;
		}else if(qtdDivs > 5 && qtdDivs < 6){
			qtdDivs = 6;
		}else if(qtdDivs > 6 && qtdDivs < 7){
			qtdDivs = 7;
		}

		for(i=0;i<qtdDivs;i++){
			$('.view-mochiloes .view-content').append('<div class="grupo grupo'+i+'"></div>');
		}

		if($('.grupo0')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(0)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(1)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(2)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(3)').clone();

			$('.view-mochiloes .grupo0').append(linha0);
			$('.view-mochiloes .grupo0').append(linha1);
			$('.view-mochiloes .grupo0').append(linha2);
			$('.view-mochiloes .grupo0').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}

		if($('.grupo1')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(4)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(5)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(6)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(7)').clone();

			$('.view-mochiloes .grupo1').append(linha0);
			$('.view-mochiloes .grupo1').append(linha1);
			$('.view-mochiloes .grupo1').append(linha2);
			$('.view-mochiloes .grupo1').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}
				
		if($('.grupo2')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(8)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(9)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(10)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(11)').clone();

			$('.view-mochiloes .grupo2').append(linha0);
			$('.view-mochiloes .grupo2').append(linha1);
			$('.view-mochiloes .grupo2').append(linha2);
			$('.view-mochiloes .grupo2').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}

		if($('.grupo3')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(12)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(13)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(14)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(15)').clone();

			$('.view-mochiloes .grupo3').append(linha0);
			$('.view-mochiloes .grupo3').append(linha1);
			$('.view-mochiloes .grupo3').append(linha2);
			$('.view-mochiloes .grupo3').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}

		if($('.grupo4')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(16)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(17)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(18)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(19)').clone();

			$('.view-mochiloes .grupo4').append(linha0);
			$('.view-mochiloes .grupo4').append(linha1);
			$('.view-mochiloes .grupo4').append(linha2);
			$('.view-mochiloes .grupo4').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}

		if($('.grupo5')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(20)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(21)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(22)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(23)').clone();

			$('.view-mochiloes .grupo5').append(linha0);
			$('.view-mochiloes .grupo5').append(linha1);
			$('.view-mochiloes .grupo5').append(linha2);
			$('.view-mochiloes .grupo5').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}

		if($('.grupo6')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(24)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(25)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(26)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(27)').clone();

			$('.view-mochiloes .grupo6').append(linha0);
			$('.view-mochiloes .grupo6').append(linha1);
			$('.view-mochiloes .grupo6').append(linha2);
			$('.view-mochiloes .grupo6').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');

			//cria o slide dos mochilões
			$('.view-mochiloes .view-content').before('<div class="navegacao"></div>');
		}

		$('.view-mochiloes').append('<div class="navegacao"></div>');
		var qtdGrupos = $('.view-mochiloes .view-content .grupo').size();
		
		$('.view-mochiloes .view-content .grupo:eq(0)').addClass('grupoAtivo');

		for(k=0;k<qtdGrupos;k++){
			$('.view-mochiloes .navegacao').append('<div class="bullet">'+k+'</div>');
		}

		$('.view-mochiloes .navegacao .bullet:eq(0)').addClass('ativo');

		$('.view-mochiloes .view-content').addClass('conteudo');

		$('.view-mochiloes .navegacao .bullet').click(function(){

			var pegaIndiceBullet = $(this).index();

			$('.view-mochiloes .navegacao .bullet').removeClass('ativo');
			$('.view-mochiloes .view-content .grupo').removeClass('grupoAtivo');

			$(this).addClass('ativo');
			$('.view-mochiloes .view-content .grupo'+pegaIndiceBullet+'').addClass('grupoAtivo');

			var tamPadrao = 261;
			var vaiPos = tamPadrao*pegaIndiceBullet;
			$('.view-mochiloes .conteudo').animate({left: -vaiPos}, 300);

		});
	}


	
	// Destinos
	
	var getUrl = window.location.href;
	var newUrl = getUrl.toString();
	var arrayUrl = newUrl.split("/");	
	
	if(arrayUrl[5] == 'destinos'){
		$('#box2col .boxlink .link').addClass('destinos');
	}
	
	// Viajar
	if(arrayUrl[4] == 'cidade'){
		$('.page-title .adjust .fltrgt').append('<span class="voltar-cidades"><a href="javascript:history.go(-1)"><< Voltar</a></span>');
	}
	
	

// --------------- ## FORMULÁRIOS ## ------------------

	// FORM FALE CONOSCO
	$('#webform-client-form-55 input[type=text], #webform-client-form-55 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

	$('#webform-client-form-55 #edit-submitted-telefone-box-ddd-telefone').mask('99', {placeholder: ""});
	$('#webform-client-form-55 #edit-submitted-telefone-box-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 


	// FORM RECLAMAÇÕES
	$('#webform-client-form-61 input[type=text], #webform-client-form-61 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

    $('#webform-client-form-61 #edit-submitted-telefone-ddd').mask('99', {placeholder: ""});
	$('#webform-client-form-61 #edit-submitted-telefone-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: " "});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: " "});
        }  
    }); 


    // FORM DÚVIDAS E SUGESTÕES
    $('#webform-client-form-60 input[type=text], #webform-client-form-60 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

    $('#webform-client-form-60 #edit-submitted-telefone-fieldset-ddd').mask('99', {placeholder: ""});
	$('#webform-client-form-60 #edit-submitted-telefone-fieldset-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 
    

    // FORM SEJA UM FRANQUEADO
    $('#webform-client-form-57 input[type=text], #webform-client-form-57 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

    $('#webform-client-form-57 #edit-submitted-dados-pessoais-cpf').mask('999.999.999-99', {placeholder: ""});
    $('#webform-client-form-57 #edit-submitted-dados-pessoais-rg').mask('99.999.999-9', {placeholder: ""});
    $('#webform-client-form-57 #edit-submitted-dados-pessoais-rg').mask('99.999.999-9', {placeholder: ""});

    $('#webform-client-form-57 #edit-submitted-dados-para-contato-telefone-fixo-ddd-fixo').mask('99', {placeholder: ""});
	$('#webform-client-form-57 #edit-submitted-dados-para-contato-telefone-fixo-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 

    $('#webform-client-form-57 #edit-submitted-dados-para-contato-telefone-celular-ddd-fixo').mask('99', {placeholder: ""});
	$('#webform-client-form-57 #edit-submitted-dados-para-contato-telefone-celular-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 


    // FORM FALE COM O PRESIDENTE
    $('#webform-client-form-62 input[type=text], #webform-client-form-62 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

    $('#webform-client-form-62 #edit-submitted-telefone-ddd').mask('99', {placeholder: ""});
	$('#webform-client-form-62 #edit-submitted-telefone-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 

    // FORM TRABALHE CONOSCO
    $('#webform-client-form-59 input[type=text], #webform-client-form-59 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

	$('#webform-client-form-59 #webform-component-data-de-nascimento input[type=text]').focus(function(){
		$(this).next('.field-suffix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).next('.field-suffix').stop().fadeIn(400);
		}
	});


    $('#webform-client-form-59 #edit-submitted-data-de-nascimento-dia').mask('99', {placeholder: ""});
    $('#webform-client-form-59 #edit-submitted-data-de-nascimento-mes').mask('99', {placeholder: ""});
    $('#webform-client-form-59 #edit-submitted-data-de-nascimento-ano').mask('9999', {placeholder: ""});

    $('#webform-client-form-59 #edit-submitted-telefone-fieldset-ddd').mask('99', {placeholder: ""});
	$('#webform-client-form-59 #edit-submitted-telefone-fieldset-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 

    if($('#webform-client-form-369')[0])
    {
    	if($('#edit-previous')[0])
    	{
    		
    	}
    	else
    	{
    		$('#node-369').css('height','940px');
    	}
    }

	//
	if($('#block-block-12')[0]){

		//Coloca a galeria de cidades dentro do body
		// var i = 0;
		var pegaLista = $('.view-id-cidades').clone();
		$('.view-id-cidades').remove();
		$('.field-name-field-paises-saiba-mais .field-item #box2colsmall02 .col01').append(pegaLista);
		$('.col01 .view-id-cidades .views-row:nth-child(2n)').after('<div class="clear"></div>');

		$('.col01 .view-id-cidades').flexslider({
			selector: ".view-content > .views-row",
			animation: "slide",
			direction: "vertical",
			itemWidth: 155,
			itemMargin: 7,
			minItems: 4,
			maxItems: 4,
			slideshow: false,
			move: 2
		});


		// var itens = pegaLista.find('.views-row').html();
		// var qtdItensGaleria = pegaLista.find('.views-row').size();

		// while(i<qtdItensGaleria){
		//  $('.field-name-field-paises-saiba-mais .field-item #box2colsmall02 .col01').append('<div class="grupo"></div>');
		//  for(e=0; e<3; e++){
		//   $('.field-name-field-paises-saiba-mais .field-item #box2colsmall02 .col01 .grupo:eq('+e+')').append(''+itens+':eq('+e+')');
		//  }
		//  i++;
		// }

		// $('.field-name-field-paises-saiba-mais .field-item #box2colsmall02 .col01').append(pegaLista);
		// $('#block-block-12').remove();
	}
	

	if($('#block-lojas-lojas-buscaloja').length){
		$('#block-lojas-lojas-buscaloja').prepend('<div class="fltlft boxIcon icone boxcolor"></div><div class="fltlft boxTitle">Agências IE</div><div class="clear"></div>');
	}



	
	//pegando o titulo do Pais e montando o botão de Contato enviado o param do pais
	if($('#box1colbig')){
			// corrigindo e passando param para contato
			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI($('#box1colbig h4').text());
		    // $(this).find('.col02 .link').attr('href',url_contato);
			// $(this).find('.col02 .link').html('Quero ir para <br>'+$('#box1colbig h4').text());
	
	}	
	

	// pegando o titulo de IE Explorer e montando o link para contato
	if($('.node .node-page .clearfix')){		


			if($('.page-node a.active-trail.active').html() == 'Modalidades')
			{
				
				$('.fltlft h5').each(function(index){

					var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI($(this).text());

					// $(this).parents('.fltlft').children('.boxlink').children('a').html('Eu quero');
					// $(this).parents('.fltlft').children('.boxlink').children('a').attr('href',url_contato);

				});

			}
			else if($('.page-node .menu-dif li[rel=field-name-field-opcoes] a').html() == 'Tipos de Programa')
			{
				
				$('.fltlft h5').each(function(index){

					var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI($(this).text());

					// $(this).parents('.fltlft').children('.boxlink').children('a').html('Eu quero');
					// $(this).parents('.fltlft').children('.boxlink').children('a').attr('href',url_contato);

				});

			}
			else
			{
				// corrigindo e passando param para contato
				var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI($('.fltlft h3').text());
			    
				// $(this).find('.fltlft .link').attr('href',url_contato);		
						
				$(this).find('.fltlft .link').html('Eu quero');
			}

			

	}


	// Para a página Work Experience Tipos de Programa
	$('.page-node .menu-dif li.active a.active')



// --------------- ## FOOTER ## ------------------

	//adiciona classes para tratamento de estilos no menu do footer
	var qtdItemFooter = $('#block-menu-menu-footer-menu .menu:eq(0) .expanded').size();
	for(i=0; i<=qtdItemFooter;i++){
		//adiciona classe para identificar cada item principal do menu
		$('#block-menu-menu-footer-menu .menu:eq(0) .expanded:eq('+i+')').addClass('item'+i+'');
		// Remove o link do item principal do menu do footer
		$('#block-menu-menu-footer-menu ul.menu:eq(0) li.expanded:eq('+i+') a:eq(0)').addClass('noclick').click(function(){ return false; });
	}

	//Adiciona classe de tratamento de estilo no footer
	$('.footer-2 ul.footerLeft li:eq(1)').addClass('link02');

	//localiza sua agência
	var browser = navigator.appName;	
	//alert(browser);
	if(browser != 'Microsoft Internet Explorer'){
		$('.footer-2 .link02').hover(
			function(){
				$(this).children('a').addClass('ativo');
				$(this).children('.boxForm').stop().fadeTo(700, 1, function() {
					$(this).children('.boxForm').show();
				});		
		},
			function(){
				$(this).children('a').removeClass('ativo');
				$(this).children('.boxForm').stop().fadeTo(700, 0, function() {
					$('.boxForm').hide();
				});
			}
		);
	}else{
		$('.boxForm .bt_fechar').show();			
		
		$('.footer-2 .link02').hover(
			function(){
				$(this).children('a').addClass('ativo');
				$(this).children('.boxForm').stop().fadeTo(700, 1, function() {
					$(this).children('.boxForm').show();
				});		
		},
			function(){
			}
		);
		
		$('.boxForm .bt_fechar').click(function(){ $('.boxForm').hide(); $('.footer-2 .link02 a').removeClass('ativo'); });			
	}
	
	$('#block-menu-menu-footer-menu .menu .menu .last a').attr('target','_blank');
	
	
	// ---
	// Menu aparecendo no Footer
	// ---
	
	var bodyHeight = $('body').height();
	var topMenu = bodyHeight - 510;
		
	$('#footer .item2 .menu .first a').attr('href','javascript:void(0)');
	
	$('#footer .item2 .menu .first a').hover(
		  function () {
			$('#header .menu-principal ul li.l03 dl').addClass('segundo-nivel-footer');			
			$('.segundo-nivel-footer').css({'top':topMenu});			
			$('.segundo-nivel-footer').show();			
		  }, 
		  function () { return false; }
	);
	
	$('#header .menu-principal ul li.l03 dl.segundo-nivel-footer .close').live('click',function(){
		$(this).parent().removeClass('segundo-nivel-footer').hide();		
	});
	
	
});;
jQuery(document).ready(function($) {

		$(".scrool").mCustomScrollbar({
			horizontalScroll:true,
			scrollButtons:{
				enable:true
			}
		});
	
	
	jQuery('<div>').addClass('pelicula').appendTo('li.views-row-1');

	var qtd = jQuery('.block-views .item-list ul li.item-curso').size();

	for(i=0; i<=qtd; i++){

		var contaAltura = jQuery('.block-views .item-list ul li:eq('+i+') .title_curso a').height();

		if(contaAltura == 12){
			jQuery('.block-views .item-list ul li:eq('+i+') .title_curso a').css('padding-top', '16px');
		}else if(contaAltura == 24) {
			jQuery('.block-views .item-list ul li:eq('+i+') .title_curso a').css('padding-top', '11px');
		}else{
			jQuery('.block-views .item-list ul li:eq('+i+') .title_curso a').css('padding-top', '4px');
		}
	}

	//Monta a galeria das internas de cursos
	function galeriaInterna(viewClass, blockId){

		//adiciona o rel para tratamento
		$('.'+viewClass+' .views-row').each(function(){
			var titulo = $(this).find('.views-field-title span').text();
			var qtdTitulo = titulo.length;
			
			// corrigindo e passando param para contato
			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI(titulo);
		    // $(this).find('.link').attr('href',url_contato);
		    // $(this).find('.link').html('Quero ir para <span>"'+titulo+'"</span>');
			
		
			for(i=0; i<qtdTitulo; i++){
				if(titulo.indexOf(" ") != -1){
				 titulo = titulo.replace(" ", "");
				}
			}
			
			$(this).attr('id', titulo);

		});

		//monta a galeria
		var qtdCursos = $('.'+viewClass+' .views-row').size();
		for(i=0; i<qtdCursos; i++){
			var pegaId = $('.'+viewClass+' .views-row:eq('+i+')').attr('id');
			var pegaThumb = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-field-cursos-imagem img').attr('src');
			var pegaTitle = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-title span').text();
			var pegaNid = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-nid span').text();
			$('.'+viewClass+' .views-row:eq('+i+')').addClass(''+pegaNid+'');

			//Adiciona as li's a ul da galeria
			$('#'+blockId+' .galeria03 ul').append('<li id="'+pegaNid+'" rel="'+pegaId+'"><img src="'+pegaThumb+'" alt=""><div class="title_curso">'+pegaTitle+'</div></li>');
		}

		//mostra sempre o primeiro registro | Adiciona a classe ativa ao primeiro registro da galeria
		$('.galeria03 ul li:eq(0)').addClass('ativo');
		$('.'+viewClass+' .views-row:eq(0)').show();

		//clique das thumbs
		$('.galeria03 ul li').live('click', function(){
			var pegaRel = $(this).attr('rel');

			$('.galeria03 ul li').removeClass('ativo');
			$('.'+viewClass+' .views-row').hide();
			
			$(this).addClass('ativo');
			$('.'+viewClass+' #'+pegaRel+'').show();
		});


		//Coloca a Imagem e o nome da escola no lugar correto e depois remove da posição original
		$('.'+viewClass+' .views-row').each(function(){
			var pegaImg = $(this).find('.views-field-field-cursos-imagem').html();
			var pegaNome = $(this).find('.views-field-field-cursos-escola').html();
			var local = $(this).find('#box3col .col01');
			var localNome = $(this).find('#box3col .col03');

			local.append(pegaImg);
			localNome.append(pegaNome);
			$(this).find('.views-field-field-cursos-imagem').remove();
			$(this).find('.views-field-field-cursos-escola').remove();
		});

		
		
		//Pega a URL da página
		var url = window.location.href;
		var seParametro = url.split("?");
		
		//Verifica se a URL está com parâmetro | Se estiver com parâmetro,
		//pega o nid do li e exibe o view row com a classe igual ao nid.
		//Se não tiver parâmetro exibe o primeiro registro e ativa a primeira li da galeria.

		

		
		if(seParametro.length > 1){

			var nid = seParametro[1].split("=");
			$('.galeria03 ul li').removeClass('ativo');
			$('.'+viewClass+' .views-row').hide();

			$('.galeria03 ul li#'+nid[1]+'').addClass('ativo');
			$('.'+viewClass+' .'+nid[1]+'').show();
		}else{
			$('.'+viewClass+' .views-row:eq(0)').show();
			$('.galeria03 ul li:eq(0)').addClass('ativo');
		}
		
		
	}

	//Monta a galeria das internas de destinos
	function galeriaDestinos(viewClass, blockId){

		//adiciona o rel para tratamento
		$('.field-name-body .'+viewClass+' .views-row').each(function(){
			var titulo = $(this).find('.views-field-title span').text();
			var qtdTitulo = titulo.length;
			
			// corrigindo e passando param para contato
			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI(titulo);
		    // $(this).find('.link').attr('href',url_contato);
		    // $(this).find('.link').html('Quero ir para <span>"'+titulo+'"</span>');
		    $(this).find('.link').html('Quero ir');
			
		
			for(i=0; i<qtdTitulo; i++){
				if(titulo.indexOf(" ") != -1){
				 titulo = titulo.replace(" ", "");
				}
			}
			
			$(this).attr('id', titulo);

		});

		//monta a galeria
		var qtdCursos = $('.field-name-body .'+viewClass+' .views-row').size();
		for(i=0; i<qtdCursos; i++){
			var pegaId = $('.field-name-body .'+viewClass+' .views-row:eq('+i+')').attr('id');
			var pegaThumb = $('.field-name-body .'+viewClass+' .views-row:eq('+i+')').find('.views-field-field-destinos-imagem img').attr('src');
			var pegaTitle = $('.field-name-body .'+viewClass+' .views-row:eq('+i+')').find('.views-field-title span').text();
			var pegaNid = $('.field-name-body .'+viewClass+' .views-row:eq('+i+')').find('.views-field-nid span').text();
			$('.field-name-body .'+viewClass+' .views-row:eq('+i+')').addClass('nid-'+pegaNid+'');
			$('.field-name-body .'+viewClass+' .views-row:eq('+i+')').attr('nid',pegaNid);

			//Adiciona as li's a ul da galeria
			$('#'+blockId+' .galeria03 ul').append('<li id="'+pegaNid+'" rel="'+pegaId+'"><img src="'+pegaThumb+'" alt=""><div class="title_curso">'+pegaTitle+'</div></li>');
		}

		//mostra sempre o primeiro registro | Adiciona a classe ativa ao primeiro registro da galeria
		$('.galeria03 ul li:eq(0)').addClass('ativo');
		$('.field-name-body .'+viewClass+' .views-row:eq(0)').show();

		//clique das thumbs
		$('.galeria03 ul li').live('click', function(){

			var pegaRel = $(this).attr('rel');
			var pegaNid = $(this).attr('id');

			$('.galeria03 ul li').removeClass('ativo');
			$('.field-name-body .'+viewClass+' .views-row').hide();
			
			$(this).addClass('ativo');
			// $('.'+viewClass+' #'+pegaRel+'').show();
			$('div[nid='+pegaNid+']').show();

			$('#block-block-13').hide();
			// alert('.'+viewClass+' #'+pegaRel+'');
		});


		//Coloca a Imagem no lugar correto e depois remove da posição original
		$('.'+viewClass+' .views-row').each(function(){
			var pegaImg = $(this).find('.views-field-field-destinos-imagem').html();
			var local = $(this).find('#box3col .col01');

			local.append(pegaImg);
			$(this).find('.views-field-field-destinos-imagem').remove();
		});

		//Pega a URL da página
		var url = window.location.href;
		var seParametro = url.split("?");
		
		//Verifica se a URL está com parâmetro | Se estiver com parâmetro,
		//pega o nid do li e exibe o view row com a classe igual ao nid.
		//Se não tiver parâmetro exibe o primeiro registro e ativa a primeira li da galeria.
		if(seParametro.length > 1){

			var nid = seParametro[1].split("=");
			$('.galeria03 ul li').removeClass('ativo');
			$('.'+viewClass+' .views-row').hide();

			$('.galeria03 ul li#'+nid[1]+'').addClass('ativo');
			$('.'+viewClass+' .'+nid[1]+'').show();
			
		}else{
			$('.'+viewClass+' .views-row:eq(0)').show();
			$('.galeria03 ul li:eq(0)').addClass('ativo');
		}
		
	}
	
 

		//Monta a galeria dos mochilões
	function galeriaMochiloes(viewClass, blockId){

		//adiciona o rel para tratamento
		$('.'+viewClass+' .views-row').each(function(){
			var titulo = $(this).find('.views-field-title span').text();
			var qtdTitulo = titulo.length;
			
			// corrigindo e passando param para contato
			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI(titulo);
		    // $(this).find('.link').attr('href',url_contato);
			// $(this).find('.link').html('Quero ir para <span>"'+titulo+'"</span>');
			$(this).find('.link').html('Quero ir');
			
			
			for(i=0; i<qtdTitulo; i++){
				if(titulo.indexOf(" ") != -1){
				 titulo = titulo.replace(" ", "");
				}
			}
			
			$(this).attr('id', titulo);

		});

		//monta a galeria
		var qtdCursos = $('.'+viewClass+' .views-row').size();
		for(i=0; i<qtdCursos; i++){
			var pegaId = $('.'+viewClass+' .views-row:eq('+i+')').attr('id');
			var pegaThumb = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-field-mochiloes-imagem img').attr('src');
			var pegaTitle = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-title span').text();
			var pegaNid = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-nid span').text();
			$('.'+viewClass+' .views-row:eq('+i+')').addClass(''+pegaNid+'');

			//Adiciona as li's a ul da galeria
			$('#'+blockId+' .galeria03 ul').append('<li id="'+pegaNid+'" rel="'+pegaId+'"><img src="'+pegaThumb+'" alt=""><div class="title_curso">'+pegaTitle+'</div></li>');
		}

		//mostra sempre o primeiro registro | Adiciona a classe ativa ao primeiro registro da galeria
		$('.galeria03 ul li:eq(0)').addClass('ativo');
		$('.'+viewClass+' .views-row:eq(0)').show();
		
		/* Insere o primeiro vídeo */
			if($('#block-block-9').length > 0){
							
				var urlVideo = $('.view-mochiloes-destaques-pagina .views-row:eq(0) .views-field-field-mochiloes-video div').text();
				var getId = urlVideo.split("?v=");
				var idVideo = getId[1];			
							
				$('#box2col .col01').prepend('<iframe width="580" height="376" src="http://www.youtube.com/embed/'+idVideo+'?rel=0" frameborder="0" allowfullscreen></iframe>');

			}
		
		//clique das thumbs
		$('.galeria03 ul li').live('click', function(){
			var pegaRel = $(this).attr('rel');

			$('.galeria03 ul li').removeClass('ativo');
			$('.'+viewClass+' .views-row').hide();
			
			$(this).addClass('ativo');
			$('.'+viewClass+' #'+pegaRel+'').show();
			
			
			/* Carrega o video no box2 abaixo */
			if($('#block-block-9').length > 0){
	
			var ref = $(this).index();
						
			var urlVideo = $('.view-mochiloes-destaques-pagina .views-row:eq('+ref+') .views-field-field-mochiloes-video div').text();
			var getId = urlVideo.split("?v=");
			var idVideo = getId[1];			
			
			$('#box2col .col01').find('iframe').remove();			
			$('#box2col .col01').prepend('<iframe width="580" height="376" src="http://www.youtube.com/embed/'+idVideo+'?rel=0" frameborder="0" allowfullscreen></iframe>');			
		
			}
		});

		//Pega a URL da página
		var url = window.location.href;
		var seParametro = url.split("?");
		
		//Verifica se a URL está com parâmetro | Se estiver com parâmetro,
		//pega o nid do li e exibe o view row com a classe igual ao nid.
		//Se não tiver parâmetro exibe o primeiro registro e ativa a primeira li da galeria.
		if(seParametro.length > 1){
			var nid = seParametro[1].split("=");
			$('.galeria03 ul li').removeClass('ativo');
			$('.'+viewClass+' .views-row').hide();

			$('.galeria03 ul li#'+nid[1]+'').addClass('ativo');
			$('.'+viewClass+' .'+nid[1]+'').show();
		}else{
			$('.'+viewClass+' .views-row:eq(0)').show();
			$('.galeria03 ul li:eq(0)').addClass('ativo');
		}
	}

	//Curso de idiomas - destaques
	if($('.view-cursos-idiomas-destaques-pagina')[0]){
		var minhaViewClass = "view-cursos-idiomas-destaques-pagina";
		var meuBlockId = "block-block-3";
		galeriaInterna(minhaViewClass, meuBlockId);
	}
	
	//Curso de idiomas - Profissionalizantes
	if($('.view-cursos-idiomas-profissionalizantes-pagina')[0]){
		var minhaViewClass = "view-cursos-idiomas-profissionalizantes-pagina";
		var meuBlockId = "block-block-4";
		galeriaInterna(minhaViewClass, meuBlockId);
	}

	//Teen Experience - Destaques
	if($('.view-teen-experience-destaques-pagina')[0]){
		var minhaViewClass = "view-teen-experience-destaques-pagina";
		var meuBlockId = "block-block-5";
		galeriaInterna(minhaViewClass, meuBlockId);
	}

	//High School - Destaques
	if($('.view-high-school-destaques-pagina')[0]){
		var minhaViewClass = "view-high-school-destaques-pagina";
		var meuBlockId = "block-block-6";
		galeriaInterna(minhaViewClass, meuBlockId);
	}

	//Study And Work | Estudar - Destaques
	if($('.view-study-and-work-destaques-pagina')[0]){
		var minhaViewClass = "view-study-and-work-destaques-pagina";
		var meuBlockId = "block-block-7";
		galeriaInterna(minhaViewClass, meuBlockId);
	}

	//Study And Work | Trabalhar - Destaques
	if($('.view-study-and-work-destaques-pagina-trabalhar')[0]){
		var minhaViewClass = "view-study-and-work-destaques-pagina-trabalhar";
		var meuBlockId = "block-block-8";
		galeriaInterna(minhaViewClass, meuBlockId);
	}

	//
	if($('.view-destinos-por-continente')[0]){
		var minhaViewClass = "view-destinos-por-continente";
		// var meuBlockId = "block-block-7";
		var meuBlockId = "block-block-20";
		galeriaDestinos(minhaViewClass, meuBlockId);
		
		$('.galeria03').append('<a href="javascript:void(0)" class="btLeft"></a><a href="javascript:void(0)" class="btRight"></a>');

		// monta o vídeo

		/*
			var getUrlVideo = $('.view-destinos-por-continente .views-field-field-destinos-video .field-content').html();
			var idVIdeo = getUrlVideo.split('=');
			$('.view-destinos-por-continente .views-field-body .col01').append('<iframe width="580px" height="380px" src="http://www.youtube.com/embed/'+idVIdeo[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');						
		*/

		$('.view-destinos-por-continente .view-content .views-row').each(function(){

			var getUrlVideo = $(this).children('.views-field-field-destinos-video').children('.field-content').html();
			var idVIdeo = getUrlVideo.split('=');

			$(this).children('.views-field-body').children('.field-content').children('#box2col').children('.col01').append('<iframe width="580px" height="380px" src="http://www.youtube.com/embed/'+idVIdeo[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');			

		});


	}	

	//Mochilões
	if($('.view-id-mochiloes_destaques_pagina')[0]){
		// monta a visualização
	 	var minhaViewClass = "view-id-mochiloes_destaques_pagina";
	 	var meuBlockId = "block-block-9";
		galeriaMochiloes(minhaViewClass, meuBlockId);
		
		// monta o vídeo
		var getUrlVideo = $('.view-id-mochiloes_destaques_pagina .views-field-field-mochiloes-video .field-content').html();
		var idVIdeo = getUrlVideo.split('=');
		$('.view-id-mochiloes_destaques_pagina .views-field-body .col01').append('<iframe width="580px" height="380px" src="http://www.youtube.com/embed/'+idVIdeo[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');

		//monta o mapa
		var getMapUrl = $('.view-id-mochiloes_destaques_pagina .views-field-field-embed-mapa .field-content').html();
		$(this).find('.views-field-field-embed-mapa').html('<div id="mapviewer"><iframe id="map" Name="mapFrame" scrolling="no" width="910" height="442" frameborder="0" src="http://br.bing.com/maps/embed/?'+getMapUrl+'w=910&amp;h=442"></iframe></div>');
	}

	
	if($('.galeria03').length){		
		//Scroll das galerias internas quando necessário
		// verifica a qtd para ver se ativa o scroll
		
		var qtdCompara = $('.galeria03 .scroll li').size();

		if( qtdCompara > 5){
			// Função que faz o slide funcionar
			var largura_item = 122;
			var qtd_items = $('.galeria03 .scroll li').size();
			var limite = qtd_items - 5;
			var posicao_scroll = parseInt($('.galeria03 .container').css('marginLeft'));
			
			var contador = 0;
			/*$('.galeria03 a.btRight').click(function(){
				posicao_scroll = parseInt($('.galeria03 .container').css('marginLeft'));
				if(contador == limite){
					return false;		
				}else{								
					contador++;
					$(this).hide();
					$('.galeria03 .container').stop().animate({
						marginLeft : posicao_scroll-largura_item
					}, 300, function(){
						if(contador != limite){
							$(this).show();
						}
					});				
				}	
			});
			$('.galeria03 a.btLeft').click(function(){				
				posicao_scroll = parseInt($('.galeria03 .container').css('marginLeft'));
				if(contador == 0){
					return false;
				}else{
					contador--;
					$(this).hide();
					$('.galeria03 .container').stop().animate({
						marginLeft : posicao_scroll+largura_item
					}, 300, function(){
						if(contador != 0){
							$(this).show();
						}						
					});				
				}		
			});*/
			$('.galeria03 a.btRight').click(function(){
				posicao_scroll = parseInt($('.galeria03 .container').css('marginLeft'));				
				if(contador == limite){
					$(this).hide();
					return false;		
				}else{			
					contador++;
					$('.btLeft').fadeTo(10,1);										
					$(this).hide();
					$('.galeria03 .container').stop().animate({
						marginLeft : posicao_scroll-largura_item
					}, 400, function(){												
						if(contador == limite && qtd_items > 5){
							//$('.btRight').fadeTo(10,0.5);							
							$('.btLeft').fadeTo(10,1);
						}else if(contador == limite){
							//$('.btRight').fadeTo(10,0.5);							
						}else{
							$('.btRight').show();
						}						
					});				
				}	
			});
			$('.galeria03 a.btLeft').click(function(){
				posicao_scroll = parseInt($('.galeria03 .container').css('marginLeft'));
				if(contador == 0){
					$(this).hide();
					return false;
				}else{			
					contador--;					
					$('.btRight').fadeTo(10,1);
					$(this).hide();	
					$('.galeria03 .container').stop().animate({
						marginLeft : posicao_scroll+largura_item
					}, 400, function(){							
						if(contador == 0 && qtd_items > 5){
							//$('.btLeft').fadeTo(10,0.5);							
							$('.btRight').fadeTo(10,1);
						}else if(contador == 0){
							//$('.btLeft').fadeTo(10,0.5);							
						}else{
							$('.btLeft').show();
						}				
					});				
				}		
			});
		}else{
			$('.galeria03 a.btRight').css('display', 'none');
			$('.galeria03 a.btLeft').css('display', 'none');
		}	
	}


	if($('.galeria20').length > 0){
		//Scroll das galerias internas quando necessário
		// verifica a qtd para ver se ativa o scrool
		var qtdCompara = $('.galeria20 .scroll .views-row').size();
		if( qtdCompara > 4){
			// Função que faz o slide funcionar
			var largura_item = 215;
			var qtd_items = $('.galeria20 .scroll .views-row').size();
			var limite = qtd_items - 4;
			var posicao_scroll = parseInt($('.galeria20 .container').css('marginLeft'));
			
			var contador = 0;
			$('.galeria20 a.btRight').click(function(){
				posicao_scroll = parseInt($('.galeria20 .container').css('marginLeft'));
				if(contador == limite){
					return false;		
				}else{			
					contador++;
					$('.galeria20 .container').animate({
						marginLeft : posicao_scroll-largura_item
					}, 600, function(){
						if(contador == limite && qtd_items > 4){
							$('.btRight').fadeTo(10,0.1);
							$('.btLeft').fadeTo(10,1);
						}else if(contador == limite){
							$('.btRight').fadeTo(10,0.1);
						}				
					});				
				}	
			});
			$('.galeria20 a.btLeft').click(function(){
				posicao_scroll = parseInt($('.galeria20 .container').css('marginLeft'));
				if(contador == 0){
					return false;
				}else{			
					contador--;
					$('.galeria20 .container').animate({
						marginLeft : posicao_scroll+largura_item
					}, 600, function(){
						if(contador == 0 && qtd_items > 4){
							$('.btLeft').fadeTo(10,0.1);
							$('.btRight').fadeTo(10,1);
						}else if(contador == 0){
							$('.btLeft').fadeTo(10,0.1);
						}				
					});				
				}		
			});
		}else{
			$('.galeria20 a.btRight').css('display', 'none');
			$('.galeria20 a.btLeft').css('display', 'none');
		}	
	}

// GALERIA da Pagina Feira de Contratação
	if($('.view-feira-de-contratacao')[0]){
		$('#block-views-empresas-block').remove();
		$('#block-block-19').css({'background':' url(http://www.ieintercambio.com.br/sites/all/themes/iesite/img/bg_setas_feiras.png) 0 268px no-repeat'});
		$('.view-feira-de-contratacao').append('<a class="btLeft"></a><a class="btRight"></a>');
		
		var status = $('.views-field-status span').text();
		var url = window.location.href;
		var splitUrl = url.split("/");
		
		
		if(status == 'On'){
			if(splitUrl[4] == 'trainee'){
				$('#block-block-19').show();
				$('#block-block-16').css({'top':'750px'});
			}	
		}else{
			if(splitUrl[4] == 'trainee'){
				$('#block-block-19').hide();
				$('#block-block-16').css({'top':'350px'});
			}
		}
		
		if(status == 'On'){
			if(splitUrl[4] == 'workexperience'){		
				$('#block-block-19').show();
				$('#block-block-16').css({'top':'395px'});
			}	
		}else{
			if(splitUrl[4] == 'workexperience'){
				$('#block-block-19').hide();
				$('#block-block-16').css({'top':'0'});
			}	
		}

		
		//Scroll das galerias internas quando necessário
		// verifica a qtd para ver se ativa o scroll
		var qtdCompara = $('.views-field-field-feiras-empresas .views-row').size();
		if( qtdCompara > 5){
			// Função que faz o slide funcionar
			var largura_item = $('.views-field-field-feiras-empresas .views-row').width();
			var larguraFinal = largura_item + 10;
			var qtd_items = $('.views-field-field-feiras-empresas .views-row').size();
			var limite = qtd_items - 5;
			$('.views-field-field-feiras-empresas').children().addClass('conteudo');
			var posicao_scroll = parseInt($('.views-field-field-feiras-empresas .conteudo').css('marginLeft'));
			
			
			var contador = 0;
			$('.view-feira-de-contratacao a.btRight').click(function(){
				posicao_scroll = parseInt($('.views-field-field-feiras-empresas .conteudo').css('marginLeft'));				
				if(contador == limite){
					$(this).fadeTo(10,0.5);
					return false;		
				}else{			
					contador++;
					$('.btLeft').fadeTo(10,1);										
					$(this).hide();										
					$('.views-field-field-feiras-empresas .conteudo').stop().animate({
						marginLeft : posicao_scroll-larguraFinal
					}, 400, function(){												
						if(contador == limite && qtd_items > 5){
							//$('.btRight').fadeTo(10,0.5);							
							$('.btLeft').fadeTo(10,1);
						}else if(contador == limite){
							//$('.btRight').fadeTo(10,0.5);							
						}else{
							$('.btRight').show();
						}						
					});				
				}	
			});
			$('.view-feira-de-contratacao a.btLeft').click(function(){
				posicao_scroll = parseInt($('.views-field-field-feiras-empresas .conteudo').css('marginLeft'));
				if(contador == 0){
					$(this).fadeTo(10,0.5);	
					return false;
				}else{			
					contador--;					
					$('.btRight').fadeTo(10,1);
					$(this).hide();	
					$('.views-field-field-feiras-empresas .conteudo').stop().animate({
						marginLeft : posicao_scroll+larguraFinal
					}, 400, function(){							
						if(contador == 0 && qtd_items > 5){
							//$('.btLeft').fadeTo(10,0.5);							
							$('.btRight').fadeTo(10,1);
						}else if(contador == 0){
							//$('.btLeft').fadeTo(10,0.5);							
						}else{
							$('.btLeft').show();
						}				
					});				
				}		
			});
		}else{
			$('.view-feira-de-contratacao a.btRight').css('opacity', '.3');
			$('.view-feira-de-contratacao a.btLeft').css('opacity', '.3');
		}

		$('.view-feira-de-contratacao .views-field-nothing').hide();
		$('.view-feira-de-contratacao .views-field-field-feiras-empresas .views-row').mouseenter(function(){
				$(this).find('.views-field-nothing').show().append('<div class="seta"></div>');
				//$('.view-feira-de-contratacao .views-field-field-feiras-empresas .views-row:eq(5)').find('.views-field-nothing').css('left', '-103px');
				//$('.view-feira-de-contratacao .views-field-field-feiras-empresas .views-row:eq(5)').find('.views-field-nothing .seta').css('left', '145px');
			}).mouseleave(function(){
				$(this).find('.views-field-nothing').hide();
				$('.views-field-nothing .seta').remove();
			});
	}


	//galeria sem filtros
	if($('#block-block-15').length > 0){	

		var ativo = true;
		var indice = 1;
		var qtdFotos = $('#block-block-15 .view-fotos-sem-filtro-por-pais .views-row').size();			
		var qtdVideos = $('#block-block-15 .view-videos-sem-filtro-por-pais .views-row').size();
		qtdFotos--;
		qtdVideos--;
		
		$('#block-block-15 .view-fotos-sem-filtro-por-pais').mouseenter(function(){ativo = false;});
		$('#block-block-15 .view-fotos-sem-filtro-por-pais').mouseleave(function(){ativo = true;});
		
		$('#block-block-15 .view-videos-sem-filtro-por-pais').mouseenter(function(){ativo = false;});
		$('#block-block-15 .view-videos-sem-filtro-por-pais').mouseleave(function(){ativo = true;});
		
		var interval = window.setInterval(function(){			
			if(ativo){
				if(indice != qtdFotos ){	
					$('#block-block-15 .view-fotos-sem-filtro-por-pais .mCSB_container img:eq('+indice+')').click();
					$('#block-block-15 .view-videos-sem-filtro-por-pais .mCSB_container img:eq('+indice+')').click();
					indice++;
				}else{
					indice = 0;
				}			
			}
			//alert(contaClique);
		},5000);			
		
		
		//Bullets de navegação 		
			var qtdItensExperiencia = $('.view-experiencias-sem-filtro-por-pais .views-row').size();
			for(k=0;k<qtdItensExperiencia;k++){
				$('.view-experiencias-sem-filtro-por-pais .view-footer .navegacao').append('<div class="bullet">'+k+'</div>');
			}

			$('.view-experiencias-sem-filtro-por-pais .view-footer .navegacao .bullet:eq(0)').addClass('ativo');

			$('.navegacao .bullet').click(function(){
			
				if( $(this).parent().parent().hasClass('barraExperiencias') ){
							
					var pegaIndiceBullet = $(this).index();
					$('.barraExperiencias .navegacao .bullet').removeClass('ativo');
					$(this).addClass('ativo');

					var tamPadrao = $('.view-experiencias-sem-filtro-por-pais .views-row').width();
					var vaiPos = tamPadrao*pegaIndiceBullet;
					$('.view-experiencias-sem-filtro-por-pais .view-content').animate({left: -vaiPos}, 300);
			
				}else{
				
					var pegaIndiceBullet = $(this).index();
					$(' .navegacao .bullet').removeClass('ativo');
					$(this).addClass('ativo');

					var tamPadrao = $('.view-mochiloes .grupo').width();
					var vaiPos = tamPadrao*pegaIndiceBullet;
					$('.view-mochiloes .conteudo').animate({left: -vaiPos}, 300);
					
				}
			
			});
				

		// monta o vídeo e a thumb
			
			//adiciona a div que faz o scroll das fotos
				$('.view-fotos-sem-filtro-por-pais').append('<div class="thumbImage"></div>');

			//verifica a quantidade de itens
				var qtdItensFotos = $('.view-fotos-sem-filtro-por-pais .views-row').size();

			//monta as thumbs das fotos
				for(f=0;f<qtdItensFotos;f++){
					var fazThumb = $('.view-fotos-sem-filtro-por-pais .views-row:eq('+f+') .views-field-field-fotos-imagem .field-content').html();
					$('.view-fotos-sem-filtro-por-pais .thumbImage').append(fazThumb);
				}

			// clique das thumbs
				$('.view-fotos-sem-filtro-por-pais .thumbImage img').click(function(){
					var indice = $(this).index();
					$('.view-fotos-sem-filtro-por-pais .views-row').fadeTo(300, 0);	
					$('.view-fotos-sem-filtro-por-pais .views-row:eq('+indice+')').fadeTo(300, 1);
				});

			//verifica a quantidade de itens
				var qtdItensVideos = $('.view-videos-sem-filtro-por-pais .views-row').size();

			//adiciona a div que faz o scroll dos vídeos
				$('.view-videos-sem-filtro-por-pais').append('<div class="thumbVideos"></div>');

			//adiciona a div com o player
				var getUrlVideoInicial = $('.view-videos-sem-filtro-por-pais .views-row:eq(0) .views-field-field-url-do-video .field-content').html();
				if(getUrlVideoInicial != null){
					var idVIdeoInicial = getUrlVideoInicial.split('&amp;list');
					if(idVIdeoInicial[0] != null){						
						var idVideoLista = idVIdeoInicial[0].split('?v=');						
						$('.view-videos-sem-filtro-por-pais').append('<div class="playerVideo"><iframe width="283px" height="204px" src="http://www.youtube.com/embed/'+idVideoLista[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');
					}else{						
						idVIdeoInicial = getUrlVideoInicial.split('?v=');
						$('.view-videos-sem-filtro-por-pais').append('<div class="playerVideo"><iframe width="283px" height="204px" src="http://www.youtube.com/embed/'+idVIdeoInicial[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');
					}
					
				}

			//puxa a thumb do video no youtube
				for(v=0;v<qtdItensVideos;v++){
				 	var getUrlVideo = $('.view-videos-sem-filtro-por-pais .views-row:eq('+v+') .views-field-field-url-do-video .field-content').html();	
				 	var idVIdeo = getUrlVideo.split('&amp;list');
					if(idVIdeo[0] != null){
						var idVideoL = idVIdeo[0].split('?v=');
						$('.view-videos-sem-filtro-por-pais .thumbVideos').append('<img src="http://img.youtube.com/vi/'+idVideoL[1]+'/0.jpg">');						
						$('.view-videos-sem-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+idVideoL[1]+'?rel=0');
					}else{
						idVIdeo = getUrlVideo.split('?v=');
						$('.view-videos-sem-filtro-por-pais .thumbVideos').append('<img src="http://img.youtube.com/vi/'+idVideoL[1]+'/0.jpg">');						
						$('.view-videos-sem-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+idVideoL[1]+'?rel=0');
					}
				}

			// Clique das thumbs dos vídeos
				$('.view-videos-sem-filtro-por-pais .thumbVideos img').click(function(){
					// alert('clique do thumb');
					var pegaSrc = $(this).attr('src');
					var geraId = pegaSrc.split('/');

					$('.view-videos-sem-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+geraId[4]+'?rel=0');
					
				});

		//inicia a visualização da galeria
			$('.view-fotos-sem-filtro-por-pais .views-row:eq(0)').show();
			
			$('.galeriaPrincipal .link a:eq(0)').addClass('ativo');
			$('.view-fotos-sem-filtro-por-pais').show();
			$('.view-videos-sem-filtro-por-pais').hide();

		//troca a visualização de fotos para vídeos
			$('.galeriaPrincipal .link a').click(function(){

				var pegaId = $(this).attr('id');
				
				$('.galeriaPrincipal .link a').removeClass('ativo');
				$(this).addClass('ativo');

				if(pegaId == "fotos"){
					$('.galeriaPrincipal .foto').show();
					$('.galeriaPrincipal .video').hide();

					$('.view-fotos-sem-filtro-por-pais').show();
					$('.view-videos-sem-filtro-por-pais').hide();
				}else{
					$('.galeriaPrincipal .foto').hide();
					$('.galeriaPrincipal .video').show();
					
					$('.view-videos-sem-filtro-por-pais').show();
					$('.view-fotos-sem-filtro-por-pais').hide();				
				}
			});

		//inicia o scroll das fotos e dos vídeos
		$(window).load(function(){
			$(".thumbImage, .thumbVideos").mCustomScrollbar({
				horizontalScroll:true,
				set_width:false,
                set_height:false,
				advanced:{
					updateOnBrowserResize:false, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                    autoExpandHorizontalScroll:true /*auto expand width for horizontal scrolling: boolean*/
				}
			});

		});
	}

	//galeria com filtros
	if($('#block-block-16').length > 0){
	
		var ativo = true;
		var indice = 1;
		var qtdFotos = $('#block-block-16 .view-fotos-com-filtro-por-pais .views-row').size();			
		var qtdVideos = $('#block-block-16 .view-videos-com-filtro-por-pais .views-row').size();
		qtdFotos--;
		qtdVideos--;
		
		$('#block-block-16 .view-fotos-com-filtro-por-pais').mouseenter(function(){ativo = false;});
		$('#block-block-16 .view-fotos-com-filtro-por-pais').mouseleave(function(){ativo = true;});
		
		$('#block-block-16 .view-videos-com-filtro-por-pais').mouseenter(function(){ativo = false;});
		$('#block-block-16 .view-videos-com-filtro-por-pais').mouseleave(function(){ativo = true;});
		
		var interval = window.setInterval(function(){			
			if(ativo){
				if(indice != qtdFotos ){	
					$('#block-block-16 .view-fotos-com-filtro-por-pais .mCSB_container img:eq('+indice+')').click();
					$('#block-block-16 .view-videos-com-filtro-por-pais .mCSB_container img:eq('+indice+')').click();
					indice++;
				}else{
					indice = 0;
				}			
			}
			//alert(contaClique);
		},5000);
		
		//Bullets de navegação 		
			var qtdItensExperiencia = $('.view-experiencias-com-filtro-por-pais .views-row').size();
			for(k=0;k<qtdItensExperiencia;k++){
				$('.view-experiencias-com-filtro-por-pais .view-footer .navegacao').append('<div class="bullet">'+k+'</div>');
			}

			$('.view-experiencias-com-filtro-por-pais .view-footer .navegacao .bullet:eq(0)').addClass('ativo');

			$('.view-experiencias-com-filtro-por-pais .view-content').addClass('conteudo2');

			$('.navegacao .bullet').click(function(){
			
				if( $(this).parent().parent().hasClass('barraExperiencias') ){

					var pegaIndiceBullet = $(this).index();
					$('.barraExperiencias .navegacao .bullet').removeClass('ativo');
					$(this).addClass('ativo');

					var tamPadrao = $('.view-experiencias-com-filtro-por-pais .views-row').width();
					var vaiPos = tamPadrao*pegaIndiceBullet;
					$('.view-experiencias-com-filtro-por-pais .conteudo2').animate({left: -vaiPos}, 300);
				
				}else{
				
					var pegaIndiceBullet = $(this).index();
					$('.view-mochiloes .navegacao .bullet').removeClass('ativo');
					$(this).addClass('ativo');

					var tamPadrao = $('.view-mochiloes .grupo').width();
					var vaiPos = tamPadrao*pegaIndiceBullet;
					$('.view-mochiloes .conteudo').animate({left: -vaiPos}, 300);
				
				}
			
			});
					
		
		// monta o video e a thumb
			
			//adiciona a div que faz o scroll das fotos
				$('.view-fotos-com-filtro-por-pais').append('<div class="thumbImage"></div>');

			//verifica a quantidade de itens
				var qtdItensFotos = $('.view-fotos-com-filtro-por-pais .views-row').size();

			//monta as thumbs das fotos
				for(f=0;f<qtdItensFotos;f++){
					var fazThumb = $('.view-fotos-com-filtro-por-pais .views-row:eq('+f+') .views-field-field-fotos-imagem .field-content').html();
					$('.view-fotos-com-filtro-por-pais .thumbImage').append(fazThumb);
				}

			// clique das thumbs
				$('.view-fotos-com-filtro-por-pais .thumbImage img').click(function(){
					var indice = $(this).index();
					$('.view-fotos-com-filtro-por-pais .views-row').fadeTo(300, 0);	
					$('.view-fotos-com-filtro-por-pais .views-row:eq('+indice+')').fadeTo(300, 1);
				});

			//verifica a quantidade de itens
				var qtdItensVideos = $('.view-videos-com-filtro-por-pais .views-row').size();

			//adiciona a div que faz o scroll dos vídeos
				$('.view-videos-com-filtro-por-pais').append('<div class="thumbVideos"></div>');

			//adiciona a div com o player
				var getUrlVideoInicial = $('.view-videos-com-filtro-por-pais .views-row:eq(0) .views-field-field-url-do-video .field-content').html();
				if(getUrlVideoInicial != null){
					var idVIdeoInicial = getUrlVideoInicial.split('&amp;list');
					if(idVIdeoInicial[0] != null){						
						var idVideoLista = idVIdeoInicial[0].split('?v=');						
						$('.view-videos-com-filtro-por-pais').append('<div class="playerVideo"><iframe width="283px" height="204px" src="http://www.youtube.com/embed/'+idVideoLista[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');
					}else{						
						idVIdeoInicial = getUrlVideoInicial.split('?v=');
						$('.view-videos-com-filtro-por-pais').append('<div class="playerVideo"><iframe width="283px" height="204px" src="http://www.youtube.com/embed/'+idVIdeoInicial[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');
					}
					
				}

			//puxa a thumb do video no youtube
				for(v=0;v<qtdItensVideos;v++){
				 	var getUrlVideo = $('.view-videos-com-filtro-por-pais .views-row:eq('+v+') .views-field-field-url-do-video .field-content').html();	
				 	var idVIdeo = getUrlVideo.split('&amp;list');
					if(idVIdeo[0] != null){
						var idVideoL = idVIdeo[0].split('?v=');
						$('.view-videos-com-filtro-por-pais .thumbVideos').append('<img src="http://img.youtube.com/vi/'+idVideoL[1]+'/0.jpg">');						
						$('.view-videos-com-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+idVideoL[1]+'?rel=0');
					}else{
						idVIdeo = getUrlVideo.split('?v=');
						$('.view-videos-com-filtro-por-pais .thumbVideos').append('<img src="http://img.youtube.com/vi/'+idVideoL[1]+'/0.jpg">');						
						$('.view-videos-com-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+idVideoL[1]+'?rel=0');
					}
				}

			// Clique das thumbs dos videos
				$('.view-videos-com-filtro-por-pais .thumbVideos img').click(function(){
					// alert('clique do thumb');
					var pegaSrc = $(this).attr('src');
					var geraId = pegaSrc.split('/');

					$('.view-videos-com-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+geraId[4]+'?rel=0');
					
				});

		//inicia a visualização da galeria
			$('.view-fotos-com-filtro-por-pais .views-row:eq(0)').show();
			
			$('.galeriaPrincipal .link a:eq(0)').addClass('ativo');
			$('.view-fotos-com-filtro-por-pais').show();
			$('.view-videos-com-filtro-por-pais').hide();

		//troca a visualização de fotos para vídeos
			$('.galeriaPrincipal .link a').click(function(){

				var pegaId = $(this).attr('id');
				
				$('.galeriaPrincipal .link a').removeClass('ativo');
				$(this).addClass('ativo');

				if(pegaId == "fotos"){
					$('.galeriaPrincipal .foto').show();
					$('.galeriaPrincipal .video').hide();

					$('.view-fotos-com-filtro-por-pais').show();
					$('.view-videos-com-filtro-por-pais').hide();
				}else{
					$('.galeriaPrincipal .foto').hide();
					$('.galeriaPrincipal .video').show();
					
					$('.view-videos-com-filtro-por-pais').show();
					$('.view-fotos-com-filtro-por-pais').hide();				
				}
			});

		//inicia o scroll das fotos e dos vídeos
		$(window).load(function(){
			$(".thumbImage, .thumbVideos").mCustomScrollbar({
				horizontalScroll:true,
				set_width:false,
                set_height:false,
				advanced:{
					updateOnBrowserResize:false, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                    autoExpandHorizontalScroll:true /*auto expand width for horizontal scrolling: boolean*/
				}
			});

		});
	}
	
	if( $('.barraExperiencias')[0] ){
		var indExp = 1;
		var qtdBullets = $('.barraExperiencias .navegacao .bullet').size();		
		var intExp = window.setInterval(function(){				
			if(qtdBullets > indExp){	
				$('.barraExperiencias .navegacao .bullet:eq('+indExp+')').click();						
				indExp++;
			}else{ indExp = 0; }								
		},4000);
	}

	if($('.view-galeria-seja-um-franqueado').length > 0){

		var qtdItens = $('.view-galeria-seja-um-franqueado .views-row').size();
		var tamPadraoInicial = $('.view-galeria-seja-um-franqueado .views-row').width();
		var vaiPosInicial = (tamPadraoInicial*qtdItens)+1;
		$('.view-galeria-seja-um-franqueado .view-content').width(vaiPosInicial);

		//Bullets de navegação 		
			for(s=0;s<qtdItens;s++){
				$('.view-galeria-seja-um-franqueado .view-footer .barraItens').append('<div class="marcador">'+s+'</div>');
			}

			$('.view-galeria-seja-um-franqueado .view-footer .barraItens .marcador:eq(0)').addClass('ativo');

			$('.barraItens .marcador').click(function(){

				var pegaIndiceBullet = $(this).index();
				$('.barraItens .marcador').removeClass('ativo');
				$(this).addClass('ativo');

				var tamPadrao = $('.view-galeria-seja-um-franqueado .views-row').width();
				var vaiPos = tamPadrao*pegaIndiceBullet;
				$('.view-galeria-seja-um-franqueado .view-content').animate({left: -(vaiPos)}, 300);

			});
	}	
	
	
});;
