Drupal.locale = { 'pluralFormula': function ($n) { return Number(($n!=1)); }, 'strings': {"":{"Not published":"N\u00e3o publicado","Edit":"Editar","Hide shortcuts":"Esconder atalhos","Disabled":"Desativado","Enabled":"Ativado","(active tab)":"(aba ativa)","Show":"Exibir","Not customizable":"N\u00e3o \u00e9 personaliz\u00e1vel","Customize dashboard":"Personalizar painel","Requires a title":"T\u00edtulo requerido","Don\u0027t display post information":"N\u00e3o exibir informa\u00e7\u00f5es de postagem","An AJAX HTTP error occurred.":"Ocorreu um erro HTTP no AJAX","HTTP Result Code: !status":"C\u00f3digo do Resultado HTTP:  !status","An AJAX HTTP request terminated abnormally.":"Uma requisi\u00e7\u00e3o HTTP AJAX terminou de forma anormal.","Debugging information follows.":"Estas s\u00e3o as informa\u00e7\u00f5es de depura\u00e7\u00e3o.","Path: !uri":"Caminho: !url","StatusText: !statusText":"Texto de Status: !statusText","ResponseText: !responseText":"Texto de Resposta: !responseText","ReadyState: !readyState":"ReadyState: !readyState","Show shortcuts":"Mostrar atalhos","Select all rows in this table":"Selecionar todas as linhas da tabela","Deselect all rows in this table":"Desmarcar todas as linhas da tabela","Configure":"Configurar","Hide summary":"Ocultar sum\u00e1rio","Edit summary":"Editar resumo","Hide":"Ocultar","Not in menu":"Fora do menu","New revision":"Nova revis\u00e3o","No revision":"Sem revis\u00e3o","By @name on @date":"Por @name em @date","By @name":"Por @name","Alias: @alias":"URL Alternativa: @alias","No alias":"Nenhuma URL alternativa","@number comments per page":"@number coment\u00e1rios por p\u00e1gina","Autocomplete popup":"Popup de autocompletar","Searching for matches...":"Procurando por dados correspondentes...","Not restricted":"Sem restri\u00e7\u00f5es","Restricted to certain pages":"Restrito para certas p\u00e1ginas","The changes to these blocks will not be saved until the \u003Cem\u003ESave blocks\u003C\/em\u003E button is clicked.":"As altera\u00e7\u00f5es nesses blocos n\u00e3o v\u00e3o ser salvas enquanto o bot\u00e3o \u003Cem\u003ESalvar Blocos\u003C\/em\u003E n\u00e3o for clicado.","The block cannot be placed in this region.":"O bloco n\u00e3o pode ser colocado nessa regi\u00e3o.","Re-order rows by numerical weight instead of dragging.":"Re-ordernar as linhas por campos n\u00famericos de peso ao inv\u00e9s de arrastar-e-soltar.","Show row weights":"Exibir pesos das linhas","Hide row weights":"Ocultar pesos das linhas","Drag to re-order":"Arraste para reordenar","Changes made in this table will not be saved until the form is submitted.":"Mudan\u00e7as feitas nesta tabela n\u00e3o ser\u00e3o salvas at\u00e9 que o formul\u00e1rio seja enviado.","Please wait...":"Por favor, espere um pouco...","The selected file %filename cannot be uploaded. Only files with the following extensions are allowed: %extensions.":"O arquivo selecionado %filename n\u00e3o p\u00f4de ser transferido. Somente arquivos com as seguintes extens\u00f5es s\u00e3o permitidos: %extensions.","Done":"Conclu\u00eddo","This permission is inherited from the authenticated user role.":"Essa permiss\u00e3o \u00e9 herdada do papel de usu\u00e1rio autenticado."}} };;

/**
 * @file
 *
 * Javascript specific to facebook connect pages.  This means pages
 * which are not canvas pages, and where fb_connect.module has
 * initialized the facebook api. The user may or may not have
 * authorized the app, this javascript will still be loaded.
 *
 * Note (!) much of the work done here is deprecated, and moved to fb.js
 * (where code works equally well on both connect pages and canvas
 * pages).  If your app needs the features here, please report your
 * use case to our issue queue (http://drupal.org/project/issues/fb),
 * otherwise these features may go away...
 */
(function ($) {
  Drupal.behaviors.fb_connect = {
    attach: function(context, settings) {
      // Logout of facebook when logging out of drupal.
      jQuery("a[href^='" + Drupal.settings.basePath + "user/logout']", context).click(FB_Connect.logoutHandler);
      jQuery("a[href^='" + Drupal.settings.fb_connect.front_url + "user/logout']", context).click(FB_Connect.logoutHandler); // front_url includes language.  I.e. "/en/"

      // Support markup for dialog boxes.
      FB_Connect.enablePopups(context);

      var events = jQuery(document).data('events');
      if (!events || !events.fb_session_change) {
        jQuery(document).bind('fb_session_change', FB_Connect.sessionChangeHandler);
      }
    }
  };
})(jQuery);

FB_Connect = function(){};

// JQuery pseudo-event handler.
FB_Connect.sessionChangeHandler = function(context, status) {
  jQuery('.block-fb_connect')
    .ajaxStart(function() {
      // This is an attempt to trigger the drupal progress indicator.  Not convinced that it works.
      jQuery(this).wrap('<div class="bar filled"></div>').wrap('<div class="bar filled"></div>');
    })
    .ajaxStop(function() {
      //jQuery(this).html('');
      // unwrap() not defined.
      jQuery(this).parent().removeClass('bar filled').parent().removeClass('bar filled');
    })
  ;

  // Call the default handler, too.
  FB_JS.sessionChangeHandler(context, status);
};

// click handler
FB_Connect.logoutHandler = function(event) {
  // If we need to reload, go to front page.
  Drupal.settings.fb.reload_url = Drupal.settings.fb_connect.front_url;

  if (typeof(FB) != 'undefined') {
    try {
      FB.logout(function () {
        // Logged out of facebook.  Session change event will log us out of drupal and
      });
      // Facebook's invalid cookies persist if third-party cookies disabled.
      // Let's try to clean up the mess.
      // @TODO: is this still needed with newer oauth SDK???
      //FB_JS.deleteCookie('fbs_' + Drupal.settings.fb.apikey, '/', ''); // apikey

      if (FB.getUserID()) { // @TODO: still needed with newer oauth SDK???
        // Facebook needs more time to log us out. (http://drupal.org/node/1164048)
        return false;
      }
    }
    catch (e) {
      return false;
    }
  }
  else {
    return false;
  }
};

/**
 * Move new dialogs to visible part of screen.
 **/
FB_Connect.centerPopups = function() {
  var scrollTop = $(window).scrollTop();
  $('.fb_dialog:not(.fb_popup_centered)').each(function() {
    var offset = $(this).offset();
    if (offset.left == 0) {
      // This is some facebook cruft that cannot be centered.
    }
    else if (offset.top < 0) {
      // Not yet visible, don't center.
    }
    else if (offset.top < scrollTop) {
      $(this).css('top', offset.top + scrollTop + 'px');
      $(this).addClass('fb_popup_centered'); // Don't move this dialog again.
    }
  });
};


FB_Connect.enablePopups = function(context) {
  // Support for easy fbml popup markup which degrades when javascript not enabled.
  // Markup is subject to change.  Currently...
  // <div class=fb_fbml_popup_wrap><a title="POPUP TITLE">LINK MARKUP</a><div class=fb_fbml_popup><fb:SOME FBML>...</fb:SOME FBML></div></div>
  jQuery('.fb_fbml_popup:not(.fb_fbml_popup-processed)', context).addClass('fb_fbml_popup-processed').prev().each(
    function() {
      this.fbml_popup = $(this).next().html();
      this.fbml_popup_width = parseInt($(this).next().attr('width'));
      this.fbml_popup_height = parseInt($(this).next().attr('height'));
      //console.log("stored fbml_popup markup: " + this.fbml_popup); // debug
      $(this).next().remove(); // Remove FBML so facebook does not expand it.
    })
    // Handle clicks on the link element.
    .bind('click',
	  function (e) {
	    var popup;
	    //console.log('Clicked!  Will show ' + this.fbml_popup); // debug

	    // http://forum.developers.facebook.net/viewtopic.php?pid=243983
	    var size = FB.UIServer.Methods["fbml.dialog"].size;
	    if (this.fbml_popup_width) {
	      size.width=this.fbml_popup_width;
	    }
	    if (this.fbml_popup_height) {
	      size.height=this.fbml_popup_height;
	    }
	    FB.UIServer.Methods['fbml.dialog'].size = size;

	    // http://forum.developers.facebook.net/viewtopic.php?id=74743
	    var markup = this.fbml_popup;
	    if ($(this).attr('title')) {
	      markup = '<fb:header icon="true" decoration="add_border">' + $(this).attr('title') + '</fb:header>' + this.fbml_popup;
	    }
	    var dialog = {
	      method: 'fbml.dialog', // triple-secret undocumented feature.
	      display: 'dialog',
	      fbml: markup,
	      width: this.fbml_popup_width,
	      height: this.fbml_popup_height
	    };
	    var popup = FB.ui(dialog, function (response) {
	      console.log(response);
	    });

	    // Start a timer to keep popups centered.
	    // @TODO - avoid starting timer more than once.
	    window.setInterval(FB_Connect.centerPopups, 500);

	    e.preventDefault();
	  })
    .parent().show();
};

;
/**
 * Javascript helpers for Facebook Streams.  Loaded by fb_stream.module.
 */
FB_Stream = function(){};

/**
 * Display a stream dialog on Facebook Connect pages, via
 * http://developers.facebook.com/docs/reference/javascript/FB.ui
 *
 * @param json is the json-encoded output of fb_stream_get_stream_dialog_data().
 */
FB_Stream.stream_publish = function(json) {
  var data_array = Drupal.parseJson(json);
  var len = data_array.length;
  for (var i=0; i < len; i++) {
    var data = data_array[i];
    data.method = 'stream.publish';
    FB.ui(data);
  }
};;
jQuery(document).ready(function($) {

	if(Drupal.settings.request_path == 'estudar/curso-de-idiomas/destaque')
	{

		var id = '2';
		var verbo = 'Eu quero';
		var produto = 'Curso de Idiomas';
		var subproduto = $('.view-cursos-idiomas-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-cursos-idiomas-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-cursos-idiomas-destaques-pagina .link').attr('href',url_contato);

		});

	}
	else if(Drupal.settings.request_path == 'estudar/high-school/modalidades')
	{

		var id = '5';
		var verbo = 'Eu quero';
		var produto = 'High School';	
		var subproduto = '';	
		var url_contato = '';	

		for(i = 1;i<=3; i++)
		{
			var subproduto = $('.field-name-body .col0'+i+' h5').html();

			url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.field-name-body .col0'+i+' .boxlink a').attr('href',url_contato);
		}

	}
	else if(Drupal.settings.request_path == 'estudar/high-school/destaques')
	{

		var id = '5';
		var verbo = 'Eu quero';
		var produto = 'High School';
		var subproduto = $('.view-high-school-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-high-school-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-high-school-destaques-pagina .link').attr('href',url_contato);

		});

	}
	else if(Drupal.settings.request_path == 'estudar/teen-experience/destaques')
	{

		var id = '6';
		var verbo = 'Eu quero';
		var produto = 'Teen Experience';
		var subproduto = $('.view-teen-experience-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-teen-experience-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-teen-experience-destaques-pagina .link').attr('href',url_contato);

		});

	}	
	else if(Drupal.settings.request_path == 'estudar/study-and-work/destaques')
	{

		var id = '2';
		var verbo = 'Eu quero';
		var produto = 'Study and Work - Estudar';
		var subproduto = $('.view-study-and-work-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-study-and-work-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-study-and-work-destaques-pagina .link').attr('href',url_contato);

		});

	}		
	else if(Drupal.settings.request_path == 'trabalhar/study-and-work/destaques')
	{

		var id = '2';
		var verbo = 'Eu quero';
		var produto = 'Study and Work - Trabalhar';
		var subproduto = $('.view-study-and-work-destaques-pagina-trabalhar .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-study-and-work-destaques-pagina-trabalhar .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-study-and-work-destaques-pagina-trabalhar .link').attr('href',url_contato);

		});

	}		
	else if(Drupal.settings.request_path == 'estudar/seguro-iac')
	{

		var id = '9';
		var verbo = 'Eu quero';
		var produto = 'Seguro Viagem IAC';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}
	else if(Drupal.settings.request_path == 'trabalhar/seguro-iac')
	{

		var id = '9';
		var verbo = 'Eu quero';
		var produto = 'Seguro Viagem IAC';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}	
	else if(Drupal.settings.request_path == 'estudar/visa-travel-money')
	{

		var id = '12';
		var verbo = 'Eu quero';
		var produto = 'Visa Travel Money';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}
	else if(Drupal.settings.request_path == 'trabalhar/visa-travel-money')
	{

		var id = '12';
		var verbo = 'Eu quero';
		var produto = 'Visa Travel Money';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}		
	else if(Drupal.settings.request_path == 'conhecer/visa-travel-money')
	{

		var id = '12';
		var verbo = 'Eu quero';
		var produto = 'Visa Travel Money';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}		
	else if(Drupal.settings.request_path == 'conhecer/mochilao/destaques')
	{

		var id = '6';
		var verbo = 'Eu quero';
		var produto = 'Trip Experience';
		var subproduto = $('.view-mochiloes-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-mochiloes-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-mochiloes-destaques-pagina .link').attr('href',url_contato);

		});

	}	
	else
	{

		var regExp = '';
		var path = Drupal.settings.request_path;
		var split_url = Drupal.settings.request_path.split('/');

		// Work Experience
		regExp = /trabalhar\/workexperience/g;	
		
		if (regExp.test(path)) 
		{
		    
			var id = '3';
			var verbo = 'Eu quero';
			var produto = 'Work Experience';	
			var subproduto = '';	
			var url_contato = '';	

			for(i = 1;i<=3; i++)
			{
				var subproduto = $('.field-name-field-opcoes .col0'+i+' h5').html()+' - '+split_url[3];

				url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

				$('.field-name-field-opcoes .col0'+i+' .boxlink a').attr('href',url_contato);
			}			

		}	

		// Conhecer país
		regExp = /conhecer\/pais/g;
		if (regExp.test(path)) 
		{

			var id = '2';
			var verbo = 'Quero ir';
			var produto = 'Conhecer país';	
			var subproduto = $('h3').html().trim();	
			var url_contato = '';				

			url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.col02 .link').attr('href',url_contato);
		}	

		// Conhecer cidade
		regExp = /conhecer\/cidade/g;
		if (regExp.test(path)) 
		{

			var id = '2';
			var verbo = 'Quero ir';
			var produto = 'Conhecer cidade';	
			var subproduto = $('h3').html().trim();
			var url_contato = '';				

			url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.col02 .link').attr('href',url_contato);

		}				

		// Promoção Interna
		regExp = /promocoes\/pais/;
		if (regExp.test(path)) 
		{

			var id = '2'; // Mesmo que o Curso de Idiomas
			var verbo = 'Eu quero';
			var produto = 'Promoção';	
			var subproduto = '';
			var empresa = '';
			var url_contato = '';				

			$('.view-promoces .views-row').each(function(){

				empresa = $(this).children('.views-field-field-promocoes-empresa').children('.field-content').html();
				subproduto = $(this).children('.views-field-field-promocoes-cidade').children('.field-content').html()+' - '+empresa;

				url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

				$(this).children('.views-field-field-promocoes-valor').append('<a href="'+url_contato+'">eu quero</a>');
				
			});			
		}					

	}



});;
jQuery(document).ready(function($) {

	$('#block-views-clippings-block').appendTo('.bloco-clippings');
	$('#block-views-parcerias-bloco-block').appendTo('.bloco-parcerias');
	
	$('#block-views-clippings-block .views-row').after('<div class="clear"></div>');
	$('.bloco-parcerias').after('<div class="clear"></div>');
	
	$('.bloco-clippings').append('<div class="img-rodape"></div>');
	
	
	// Monta datas
	
	var arrayMes = ['','jan','fev','mar','abr','mai','jun','jul','ago','set','out','nov','dez'];
	
	$('.view-clippings .date-display-single').each(function(){
		
		var getMes = $(this).text();
		var numMes = getMes.split("/");
		var diaAtual = numMes[1].toString();
		var mesAtual = numMes[0];
		
		if(mesAtual < 10){
			mesAtual.toString();
			mesAtual = mesAtual.substring(1,2);
		}
		
		$(this).empty();
		$(this).prepend(diaAtual+'<strong>'+arrayMes[mesAtual]+'</strong>');				
		
	});
	
	
	$('.view-parcerias-bloco .views-row').each(function(){
		var getTitle = $(this).find('.views-field-title').html();
		var getSubtitle = $(this).find('.views-field-field-subtitulo-parcerias').html();
		
		/*
		$(this).find('.views-field-title').remove();
		$(this).find('.views-field-field-subtitulo-parcerias').remove();
		$(this).find('.views-field-title').body();
		*/
		
		$(this).prepend('<div class="conteudo">'+getTitle+getSubtitle+'</div>');
	});
	
		
	$('#edit-submitted-estado').change(function(){
		$('#webform-component-cidade').remove();
		$(this).parent().css({'height':'74px'});				
	});
	
	
	
});;
(function ($) {

/**
 * A progressbar object. Initialized with the given id. Must be inserted into
 * the DOM afterwards through progressBar.element.
 *
 * method is the function which will perform the HTTP request to get the
 * progress bar state. Either "GET" or "POST".
 *
 * e.g. pb = new progressBar('myProgressBar');
 *      some_element.appendChild(pb.element);
 */
Drupal.progressBar = function (id, updateCallback, method, errorCallback) {
  var pb = this;
  this.id = id;
  this.method = method || 'GET';
  this.updateCallback = updateCallback;
  this.errorCallback = errorCallback;

  // The WAI-ARIA setting aria-live="polite" will announce changes after users
  // have completed their current activity and not interrupt the screen reader.
  this.element = $('<div class="progress" aria-live="polite"></div>').attr('id', id);
  this.element.html('<div class="bar"><div class="filled"></div></div>' +
                    '<div class="percentage"></div>' +
                    '<div class="message">&nbsp;</div>');
};

/**
 * Set the percentage and status message for the progressbar.
 */
Drupal.progressBar.prototype.setProgress = function (percentage, message) {
  if (percentage >= 0 && percentage <= 100) {
    $('div.filled', this.element).css('width', percentage + '%');
    $('div.percentage', this.element).html(percentage + '%');
  }
  $('div.message', this.element).html(message);
  if (this.updateCallback) {
    this.updateCallback(percentage, message, this);
  }
};

/**
 * Start monitoring progress via Ajax.
 */
Drupal.progressBar.prototype.startMonitoring = function (uri, delay) {
  this.delay = delay;
  this.uri = uri;
  this.sendPing();
};

/**
 * Stop monitoring progress via Ajax.
 */
Drupal.progressBar.prototype.stopMonitoring = function () {
  clearTimeout(this.timer);
  // This allows monitoring to be stopped from within the callback.
  this.uri = null;
};

/**
 * Request progress data from server.
 */
Drupal.progressBar.prototype.sendPing = function () {
  if (this.timer) {
    clearTimeout(this.timer);
  }
  if (this.uri) {
    var pb = this;
    // When doing a post request, you need non-null data. Otherwise a
    // HTTP 411 or HTTP 406 (with Apache mod_security) error may result.
    $.ajax({
      type: this.method,
      url: this.uri,
      data: '',
      dataType: 'json',
      success: function (progress) {
        // Display errors.
        if (progress.status == 0) {
          pb.displayError(progress.data);
          return;
        }
        // Update display.
        pb.setProgress(progress.percentage, progress.message);
        // Schedule next timer.
        pb.timer = setTimeout(function () { pb.sendPing(); }, pb.delay);
      },
      error: function (xmlhttp) {
        pb.displayError(Drupal.ajaxError(xmlhttp, pb.uri));
      }
    });
  }
};

/**
 * Display errors on the page.
 */
Drupal.progressBar.prototype.displayError = function (string) {
  var error = $('<div class="messages error"></div>').html(string);
  $(this.element).before(error).hide();

  if (this.errorCallback) {
    this.errorCallback(this);
  }
};

})(jQuery);
;
/**
 * @file
 * Provides JavaScript additions to the managed file field type.
 *
 * This file provides progress bar support (if available), popup windows for
 * file previews, and disabling of other file fields during Ajax uploads (which
 * prevents separate file fields from accidentally uploading files).
 */

(function ($) {

/**
 * Attach behaviors to managed file element upload fields.
 */
Drupal.behaviors.fileValidateAutoAttach = {
  attach: function (context, settings) {
    if (settings.file && settings.file.elements) {
      $.each(settings.file.elements, function(selector) {
        var extensions = settings.file.elements[selector];
        $(selector, context).bind('change', {extensions: extensions}, Drupal.file.validateExtension);
      });
    }
  },
  detach: function (context, settings) {
    if (settings.file && settings.file.elements) {
      $.each(settings.file.elements, function(selector) {
        $(selector, context).unbind('change', Drupal.file.validateExtension);
      });
    }
  }
};

/**
 * Attach behaviors to the file upload and remove buttons.
 */
Drupal.behaviors.fileButtons = {
  attach: function (context) {
    $('input.form-submit', context).bind('mousedown', Drupal.file.disableFields);
    $('div.form-managed-file input.form-submit', context).bind('mousedown', Drupal.file.progressBar);
  },
  detach: function (context) {
    $('input.form-submit', context).unbind('mousedown', Drupal.file.disableFields);
    $('div.form-managed-file input.form-submit', context).unbind('mousedown', Drupal.file.progressBar);
  }
};

/**
 * Attach behaviors to links within managed file elements.
 */
Drupal.behaviors.filePreviewLinks = {
  attach: function (context) {
    $('div.form-managed-file .file a, .file-widget .file a', context).bind('click',Drupal.file.openInNewWindow);
  },
  detach: function (context){
    $('div.form-managed-file .file a, .file-widget .file a', context).unbind('click', Drupal.file.openInNewWindow);
  }
};

/**
 * File upload utility functions.
 */
Drupal.file = Drupal.file || {
  /**
   * Client-side file input validation of file extensions.
   */
  validateExtension: function (event) {
    // Remove any previous errors.
    $('.file-upload-js-error').remove();

    // Add client side validation for the input[type=file].
    var extensionPattern = event.data.extensions.replace(/,\s*/g, '|');
    if (extensionPattern.length > 1 && this.value.length > 0) {
      var acceptableMatch = new RegExp('\\.(' + extensionPattern + ')$', 'gi');
      if (!acceptableMatch.test(this.value)) {
        var error = Drupal.t("The selected file %filename cannot be uploaded. Only files with the following extensions are allowed: %extensions.", {
          '%filename': this.value,
          '%extensions': extensionPattern.replace(/\|/g, ', ')
        });
        $(this).closest('div.form-managed-file').prepend('<div class="messages error file-upload-js-error">' + error + '</div>');
        this.value = '';
        return false;
      }
    }
  },
  /**
   * Prevent file uploads when using buttons not intended to upload.
   */
  disableFields: function (event){
    var clickedButton = this;

    // Only disable upload fields for Ajax buttons.
    if (!$(clickedButton).hasClass('ajax-processed')) {
      return;
    }

    // Check if we're working with an "Upload" button.
    var $enabledFields = [];
    if ($(this).closest('div.form-managed-file').length > 0) {
      $enabledFields = $(this).closest('div.form-managed-file').find('input.form-file');
    }

    // Temporarily disable upload fields other than the one we're currently
    // working with. Filter out fields that are already disabled so that they
    // do not get enabled when we re-enable these fields at the end of behavior
    // processing. Re-enable in a setTimeout set to a relatively short amount
    // of time (1 second). All the other mousedown handlers (like Drupal's Ajax
    // behaviors) are excuted before any timeout functions are called, so we
    // don't have to worry about the fields being re-enabled too soon.
    // @todo If the previous sentence is true, why not set the timeout to 0?
    var $fieldsToTemporarilyDisable = $('div.form-managed-file input.form-file').not($enabledFields).not(':disabled');
    $fieldsToTemporarilyDisable.attr('disabled', 'disabled');
    setTimeout(function (){
      $fieldsToTemporarilyDisable.attr('disabled', false);
    }, 1000);
  },
  /**
   * Add progress bar support if possible.
   */
  progressBar: function (event) {
    var clickedButton = this;
    var $progressId = $(clickedButton).closest('div.form-managed-file').find('input.file-progress');
    if ($progressId.length) {
      var originalName = $progressId.attr('name');

      // Replace the name with the required identifier.
      $progressId.attr('name', originalName.match(/APC_UPLOAD_PROGRESS|UPLOAD_IDENTIFIER/)[0]);

      // Restore the original name after the upload begins.
      setTimeout(function () {
        $progressId.attr('name', originalName);
      }, 1000);
    }
    // Show the progress bar if the upload takes longer than half a second.
    setTimeout(function () {
      $(clickedButton).closest('div.form-managed-file').find('div.ajax-progress-bar').slideDown();
    }, 500);
  },
  /**
   * Open links to files within forms in a new window.
   */
  openInNewWindow: function (event) {
    $(this).attr('target', '_blank');
    window.open(this.href, 'filePreview', 'toolbar=0,scrollbars=1,location=1,statusbar=1,menubar=0,resizable=1,width=500,height=550');
    return false;
  }
};

})(jQuery);
;

/**
 * JavaScript behaviors for the front-end display of webforms.
 */

(function ($) {

Drupal.behaviors.webform = Drupal.behaviors.webform || {};

Drupal.behaviors.webform.attach = function(context) {
  // Calendar datepicker behavior.
  Drupal.webform.datepicker(context);
};

Drupal.webform = Drupal.webform || {};

Drupal.webform.datepicker = function(context) {
  $('div.webform-datepicker').each(function() {
    var $webformDatepicker = $(this);
    var $calendar = $webformDatepicker.find('input.webform-calendar');
    var startDate = $calendar[0].className.replace(/.*webform-calendar-start-(\d{4}-\d{2}-\d{2}).*/, '$1').split('-');
    var endDate = $calendar[0].className.replace(/.*webform-calendar-end-(\d{4}-\d{2}-\d{2}).*/, '$1').split('-');
    var firstDay = $calendar[0].className.replace(/.*webform-calendar-day-(\d).*/, '$1');
    // Convert date strings into actual Date objects.
    startDate = new Date(startDate[0], startDate[1] - 1, startDate[2]);
    endDate = new Date(endDate[0], endDate[1] - 1, endDate[2]);

    // Ensure that start comes before end for datepicker.
    if (startDate > endDate) {
      var laterDate = startDate;
      startDate = endDate;
      endDate = laterDate;
    }

    var startYear = startDate.getFullYear();
    var endYear = endDate.getFullYear();

    // Set up the jQuery datepicker element.
    $calendar.datepicker({
      dateFormat: 'yy-mm-dd',
      yearRange: startYear + ':' + endYear,
      firstDay: parseInt(firstDay),
      minDate: startDate,
      maxDate: endDate,
      onSelect: function(dateText, inst) {
        var date = dateText.split('-');
        $webformDatepicker.find('select.year, input.year').val(+date[0]);
        $webformDatepicker.find('select.month').val(+date[1]);
        $webformDatepicker.find('select.day').val(+date[2]);
      },
      beforeShow: function(input, inst) {
        // Get the select list values.
        var year = $webformDatepicker.find('select.year, input.year').val();
        var month = $webformDatepicker.find('select.month').val();
        var day = $webformDatepicker.find('select.day').val();

        // If empty, default to the current year/month/day in the popup.
        var today = new Date();
        year = year ? year : today.getFullYear();
        month = month ? month : today.getMonth() + 1;
        day = day ? day : today.getDate();

        // Make sure that the default year fits in the available options.
        year = (year < startYear || year > endYear) ? startYear : year;

        // jQuery UI Datepicker will read the input field and base its date off
        // of that, even though in our case the input field is a button.
        $(input).val(year + '-' + month + '-' + day);
      }
    });

    // Prevent the calendar button from submitting the form.
    $calendar.click(function(event) {
      $(this).focus();
      event.preventDefault();
    });
  });
}

})(jQuery);
;
