<?php


function instituicoes_admin_page() {

	$header = array(
		array('data' => 'Nome'),
		array('data' => 'Cidade'),
		array('data' => 'Data'),
		array('data' => 'Ações'),
    ) ;
	
	$rows = array() ;

    $result = db_query( "SELECT l.iid, l.titulo, c.name, l.created FROM appieintercambio_instituicao l LEFT JOIN appieintercambio_cidades c ON l.cidade = c.id ORDER BY l.created DESC" ) ;

	foreach ( $result as $r )
	{
		$row = array() ;

		$row[] = $r->titulo ;
		$row[] = $r->name ;
		$row[] = format_date($r->created) ;
		$row[] = l( 'editar', 'admin/content/instituicoes/form/'.$r->iid ) ;
		$rows[] = array( 'data' => $row ) ;
    }
	$output = "" ;
	
	$output .= theme( 'table', array( 
		'header' => $header, 
		'rows' => $rows,
		'sticky' => TRUE 
	) ) ;

	$output .= theme('pager', array( 'tags' => array() ) ) ;
	
	return $output ;
}

function instituicoes_admin_edit( $iid=null ) {
	global $instituicao ;

	$output = "" ;
	
	if ( isset($iid) )	{
		$result = db_query( "SELECT * FROM appieintercambio_instituicao where iid = :iid", array( 'iid' => $iid ) ) ;
		foreach ( $result as $instituicao )
		{
			$iid_exist = TRUE ;
		}
	}

	$formulario = drupal_get_form('instituicoes_admin_edit_form');

	$output .= drupal_render($formulario) ;

	return $output;
}

function instituicoes_admin_edit_form($form, &$form_state){
	global $instituicao;

	$fid = @$instituicao->fid;

	if ( $fid != '') {
		$arquivo = file_load($fid);
		$img = "<img src='" . file_create_url($arquivo->uri) ."' />";
	} else {
		$img = 'Selecione uma imagem';
	}
	
	$funcao = array(1 =>'Empresa', 2 =>'Escola', 3 =>'Empresa e Escola');

	$form['titulo'] = array(
		'#type' => 'textfield',
		'#title' => 'Nome da Filial',
		'#required' => TRUE,
		'#size' => 60,
		'#maxlength' => 200,
		'#weight' => 1,
		'#default_value' => @$instituicao->titulo
	);

	$form['funcao'] = array(
		'#type' => 'select',
		'#title' => 'Categoria',
		'#options' => $funcao,
		'#default_value' => @$instituicao->funcao,
		'#weight' => 2,
	);

	$form['fid'] = array(
	  '#title' => t('Logo da Instituição'),
	  '#type' => 'file',
	  '#description' => $img,
	  '#weight' => 3
	);	

	$form['fid_controle'] = array(
		'#type' => 'hidden',
		'#default_value' => @$instituicao->fid
	);

	$form['endereco'] = array(
		'#type' => 'textfield',
		'#title' => 'Endereço',
		'#required' => FALSE,
		'#size' => 100,
		'#maxlength' => 200,
		'#weight' => 4,
		'#default_value' => @$instituicao->endereco
	);
		
	$result = db_query( "SELECT p.pid, p.nome FROM appieintercambio_pais p ORDER BY p.nome ASC" ) ;

	$pais = array();

	$query = db_select('appieintercambio_cidades','c');
	$query->fields('c',array('id','name'));
	$cidades = $query->execute()->fetchAllKeyed();

	$form['cidade'] = array(
		'#type' => 'select',
		'#title' => 'Cidade',
		'#options' => $cidades,
		'#weight' => 6,
		'#default_value' => @$instituicao->cidade,
		'#required' => TRUE,
	);
	
	$form['iid'] = array(
		'#type' => 'hidden',
		'#default_value' => @$instituicao->iid
	);
	
	$form['submit'] = array(		
		'#type' => 'submit',
		'#value' => 'Salvar',
		'#weight' => 21,
	);

    if ( isset($instituicao->iid) ) {
        $form['delete'] = array(
            '#type' => 'submit',
            '#value' => 'Apagar',
            '#weight' => 22,
        );
	}
	
	return $form ;
}


/**
 * Form submission handler for lojas_admin_edit_form_submit().
 */
function instituicoes_admin_edit_form_submit( $form, &$form_state )
{

 	$file = file_save_upload('fid', array(), 'public://instituicoes_images', FILE_EXISTS_REPLACE);
   	if ($file) {
			$file->status = FILE_STATUS_PERMANENT;
		file_save($file);
		$fid = $file->fid;
   	} else {
   		$fid =  $form_state['values']['fid_controle'];
   	}   


    if( $form_state['values']['op'] == 'Salvar' ){
        // parametros
        $params = array(
            'titulo' => $form_state['values']['titulo'], 
            'endereco' => $form_state['values']['endereco'],             
            'cidade' => $form_state['values']['cidade'],                        
            'funcao' => $form_state['values']['funcao'],
            'fid' => $fid, 
        ) ;
    
        // INSERT
        if ( empty( $form_state['values']['iid'] ) || $form_state['values']['iid'] == '' )  {
            $params['created'] = time() ;
            
            // Grava
            db_query( 
                'INSERT INTO appieintercambio_instituicao ( 
                    titulo, 
                    endereco, cidade, 
                    created,funcao, fid
                ) VALUES ( 
                    :titulo, 
                    :endereco, :cidade,
                    :created, :funcao, :fid 
                )', 
                $params 
            ) ;
        }
        // UPDATE
        else {
            $params['iid'] = $form_state['values']['iid'] ;
            
            // Grava
            db_query( 
                'UPDATE appieintercambio_instituicao SET
                    titulo = :titulo, 
                    endereco = :endereco,                     
                    cidade = :cidade,                    
                    funcao = :funcao, 
                    fid = :fid
                WHERE iid = :iid', 
                $params 
            ) ;
        }
    }
    elseif ( $form_state['values']['op'] == 'Apagar' )  {
            db_query( 
                'DELETE FROM appieintercambio_instituicao WHERE iid = :iid', 
                array('iid' => $form_state['values']['iid']) 
            ) ;
    }
    
	// Joga para tela de listagem
	$form_state['redirect'] = 'admin/content/instituicoes' ;
}