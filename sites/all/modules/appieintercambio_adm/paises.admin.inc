<?php

function paises_admin_page() 
{
	$header = array(
		array('data' => 'Nome'),
		array('data' => 'Criado em'),
		array('data' => 'Ações'),
    ) ;
	
	$rows = array() ;

    $result = db_query( "SELECT l.pid, l.nome, l.CC, l.fid, l.created FROM appieintercambio_pais l ORDER BY l.created DESC" ) ;

	foreach ( $result as $r )
	{
		$row = array() ;

		$file = file_load($r->fid);

		$row[] = $r->nome ;
		$row[] = format_date($r->created) ;
		$row[] = l( 'editar', 'admin/content/paises/form/'.$r->pid ) ;
		$rows[] = array( 'data' => $row ) ;
    }
	$output = "" ;
	
	$output .= theme( 'table', array( 
		'header' => $header, 
		'rows' => $rows,
		'sticky' => TRUE 
	) ) ;

	$output .= theme('pager', array( 'tags' => array() ) ) ;
	
	return $output ;
}

function paises_admin_edit( $pid=null )
{

	global $pais ;

	$output = "" ;
	
	if ( isset($pid) )
	{
		$result = db_query( "SELECT * FROM appieintercambio_pais where pid = :pid", array( 'pid' => $pid ) ) ;
		foreach ( $result as $pais )
		{
			$pid_exist = TRUE ;
		}
	}

	$formulario = drupal_get_form('paises_admin_edit_form');

	$output .= drupal_render($formulario);

	return $output;
}

function paises_admin_edit_form( $form, &$form_state ){
	global $pais;
	$fid = @$pais->fid;

	if ( $fid != '') {
		$arquivo = file_load($fid);
		$img = "<img src='" . file_create_url($arquivo->uri) ."' />";
	} else {
		$img = 'Selecione uma imagem';
	}

	$form['nome'] = array(
		'#type' => 'textfield',
		'#title' => 'Nome',
		'#required' => TRUE,
		'#size' => 60,
		'#maxlength' => 200,
		'#weight' => 1,
		'#default_value' => @$pais->nome
	);

	$form['country_code'] = array(
		'#type' => 'textfield',
		'#title' => 'Country Code',
		'#required' => TRUE,
		'#size' => 10,
		'#maxlength' => 2,
		'#weight' => 2,
		'#default_value' => @$pais->CC,
	);

	$form['fid'] = array(
	  '#title' => t('Image Field'),
	  '#type' => 'file',
	  '#default_value' => @$pais->fid,
	  '#description' => $img,
	  '#weight' => 4
	);	
	
	$form['pid'] = array(
		'#type' => 'hidden',
		'#default_value' => @$pais->pid
	);

	$form['fid_controle'] = array(
		'#type' => 'hidden',
		'#default_value' => @$pais->fid
	);

	$form['continente_id'] = array(
		'#title' => 'Continente',
		'#type' => 'select',
		'#options' => array(1=> 'Américas', 2 => 'Europa', 3 => 'Oceania', 4 => 'África', 5 => 'Ásia'),
		'#weight' => 3,
		'#default_value' => @$pais->continente_id
	);

	$form['x'] = array(
		'#type' => 'textfield',
		'#title' => 'X ( Posição Horizonzal em PXs no Mapa )',
		'#required' => TRUE,
		'#size' => 60,
		'#maxlength' => 200,
		'#weight' => 5,
		'#default_value' => @$pais->x
	);	

	$form['y'] = array(
		'#type' => 'textfield',
		'#title' => 'Y ( Posição Vertical em PXs no Mapa )',
		'#required' => TRUE,
		'#size' => 60,
		'#maxlength' => 200,
		'#weight' => 6,
		'#default_value' => @$pais->y
	);		
	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Salvar',
		'#weight' => 7,
	);

    if ( isset($pais->pid) )
    {
        $form['delete'] = array(
            '#type' => 'submit',
            '#value' => 'Apagar',
            '#weight' => 22,
        );
	}
	
	return $form ;
}

function paises_admin_edit_form_submit( $form, &$form_state ) {

    if( $form_state['values']['op'] == 'Salvar' )  {

	   	$file = file_save_upload('fid', array(), 'public://paises_images', FILE_EXISTS_REPLACE);
	   	if ($file) {
   			$file->status = FILE_STATUS_PERMANENT;
    		file_save($file);
    		$fid = $file->fid;
	   	} else {
	   		$fid =  $form_state['values']['fid_controle'];
	   	}   	
    	
        $params = array(
            'nome' => $form_state['values']['nome'], 
            'fid' => $fid, 
            'cc' => $form_state['values']['country_code'],
            'continente_id' => $form_state['values']['continente_id'],
            'x' => $form_state['values']['x'],
            'y' => $form_state['values']['y'],
        );
    
        // INSERT
        if ( empty( $form_state['values']['pid'] ) || $form_state['values']['pid'] == '' ) {
            $params['created'] = time() ;
            
            // Grava
            db_query( 
                'INSERT INTO appieintercambio_pais ( 
                    nome, fid, cc, created, continente_id, x, y
                ) VALUES ( 
                    :nome, :fid, :cc, :created, :continente_id, :x, :y 
                )', 
                $params 
            );
        }
        // UPDATE
        else {
        	
        	$params['pid'] = $form_state['values']['pid'];
        	
        	if($file){
	        	$fid_controle = $form_state['values']['fid_controle'];           
	            
	            $file_old = file_load($fid_controle);
	            if ($file_old) {
	            	file_delete($file_old);
	            }

            }

            // Grava pais
            db_query( 
                'UPDATE appieintercambio_pais SET
                    nome = :nome, 
                    fid = :fid,
                    cc = :cc,
                    continente_id = :continente_id,
                    x = :x,
                    y = :y
                WHERE pid = :pid', 
                $params 
            ) ;
        }
    }
    elseif ( $form_state['values']['op'] == 'Apagar' ){
            db_query( 
                'DELETE FROM appieintercambio_pais WHERE pid = :pid', 
                array('pid' => $form_state['values']['pid']) 
            ) ;
    }
    
	// Joga para tela de listagem
	$form_state['redirect'] = 'admin/content/paises' ;
}