<?php

	function cidades_admin_page() {

	$header = array(		
		array('data' => 'Cidade'),		
		array('data' => 'Ações'),
    ) ;
	
	$rows = array() ;

    $result = db_query( "SELECT id,name FROM appieintercambio_cidades l WHERE id != '0' ORDER BY id ASC  " ) ;

	foreach ( $result as $r )	{
		$row = array() ;

		$row[] = $r->name ;		
		$row[] = l( 'editar', 'admin/content/cidades/form/'.$r->id ) ;
		$rows[] = array( 'data' => $row ) ;
    }

	$output = "" ;
	
	$output .= theme( 'table', array( 
		'header' => $header, 
		'rows' => $rows,
		'sticky' => TRUE 
	) ) ;

	$output .= theme('pager', array( 'tags' => array() ) ) ;
	
	return $output ;
	}
	
	function cidades_admin_edit($id = NULL){

		global $cidade;

		$output = "" ;		
		
		if ( isset($id) )	{
		$result = db_query( "SELECT * FROM appieintercambio_cidades where id = :id", array( 'id' => $id ) ) ;
			foreach ( $result as $cidade ){
				$id_exist = TRUE ;
			}
		}


		$formulario = drupal_get_form('cidades_admin_edit_form');

		$output .= drupal_render($formulario) ;

		return $output;
	}	

	function cidades_admin_edit_form($form, &$form_state){

	  drupal_add_js(drupal_get_path('module', 'appieintercambio') .'/js/jquery-ui.js', 'file');
	  drupal_add_css(drupal_get_path('module', 'appieintercambio') . '/css/jquery-ui.css');

	  drupal_add_js(
	  'jQuery(document).ready(function($){
	  	 
	  	 basepath = Drupal.settings.appieintercambio.basepath;
	     
	     $("#edit-name").autocomplete({
	          source: function (request, response) {

	            country = $(\'input[name="country_code"]\').val();
	            cidade = request.term;

	            $.ajax({
	                url: "https://dev.virtualearth.net/REST/v1/Locations",
	                dataType: "jsonp",
	                data: {
	                    key: "Aqc_aSDvyUOosa5YAwmnSkgjuIdUkZ9jHJ9F-6RDoubG12YxaSIXAf7XkAAcPczv",
	                     countryRegion: country,
	                     locality: cidade,
	                     maxResults: 2
	                },
	                jsonp: "jsonp",
	                success: function (data) {
	                    var result = data.resourceSets[0];
	                    if (result) {
	                        if (result.estimatedTotal > 0) {
	                            response($.map(result.resources, function (item) {
	                                return {
	                                    data: item,
	                                    label: item.name +  "(" + item.address.countryRegion + ")",
	                                    value: item.name
	                                }
	                            }));
	                        }
	                    }
	                }
	            });
	        },
	        minLength: 1,
	        change: function (event, ui) {
	            if (!ui.item)
	                $("#searchBox").val("");
	        },
	        select: function (event, ui) {
	            displaySelectedItem(ui.item.data);
	        }
	    });

	  function displaySelectedItem(item) {      
	      $(\'input[name = "latitude_input"]\').val(item.point.coordinates[0]);
	      $(\'input[name = "longitude_input"]\').val(item.point.coordinates[1]);
	  }

	  $("#edit-pais").change(function(){
	  	var pid = $(this).val();
	  	$.post(basepath + "admin/getCC", {"pid":pid}, function(data){   
	  		$(\'input[name="country_code"]\').val(data.country_code);
	  	});	


	  });
	  
	  });','inline');


		global $cidade;

		$paises = getPaises();

		$form['cid'] = array(
			'#type' => 'hidden',						
			'#default_value' => @$cidade->id
		);

		$form['pais'] = array(
			'#type' => 'select',
			'#title' => 'País',
			'#options' => $paises,
			'#weight' => 1,
			'#default_value' => @$cidade->pid
		);

		$form['name'] = array(
			'#type' => 'textfield',
			'#title' => 'Cidade',
			'#suffix' => '<div id="searchResult" class="ui-widget" style="margin-top: 1em;"></div>',
			'#weight' => 2,
			'#default_value' => @$cidade->name
		);

		$form['latitude_input'] = array(
			'#type' => 'hidden',			
			'#required' => TRUE,
			'#size' => 60,			
			'#weight' => 3,
			'#default_value' => @$cidade->latitude
		);

		$form['longitude_input'] = array(
			'#type' => 'hidden',			
			'#required' => TRUE,
			'#size' => 60,			
			'#weight' => 4,
			'#default_value' => @$cidade->longitude
		);	

		$form['country_code'] = array(
			'#type' => 'hidden',						
			'#size' => 60,			
			'#weight' => 4,			
		);


		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => 'Salvar',
			'#weight' => 5,
		);	

		 if ( isset($cidade->id) ) {
	        $form['delete'] = array(
	            '#type' => 'submit',
	            '#value' => 'Apagar',
	            '#weight' => 22,
	        );
		}	

		return $form;
	}

	function cidades_admin_edit_form_submit($form, &$form_state){
		$cid = $form_state['values']['cid'];

		if($cid != ''){
			$query = db_update('appieintercambio_cidades');
			$query->fields(array(
				'name' => $form_state['values']['name'],
				'latitude' => $form_state['values']['latitude_input'],
				'longitude' => $form_state['values']['longitude_input'],
				'pid' =>  $form_state['values']['pais'],
			));
			$query->condition('cid',$cid);
			$query->execute();		

			drupal_set_message(t('Cidade alterada com sucesso! "'));

		} else {

			$query = db_insert('appieintercambio_cidades');
			$query->fields(array(
				'name' => $form_state['values']['name'],
				'latitude' => $form_state['values']['latitude_input'],
				'longitude' => $form_state['values']['longitude_input'],
				'pid' =>  $form_state['values']['pais'],
			));
			$query->execute();

			drupal_set_message(t($form_state['values']['name'] . ' adicionada com sucesso! "'));
		}
	}

?>