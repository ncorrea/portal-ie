<?php
	function bing_admin_page(){

		$formulario = drupal_get_form('bing_admin_form');

		return drupal_render($formulario);
	}

	function bing_admin_form($form, &$form_state){
		$form['bingkey'] = array(
			'#title' => 'Bing Api Key',
			'#type' => 'textfield',
			'#default_value' => variable_get('bing_api_key'),
		);

		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => 'Salvar',
		);

		return $form;
	}

	function bing_admin_form_submit($form, &$form_state){

		variable_set('bing_api_key',$form_state['values']['bingkey']);
		drupal_set_message('Api Key Salva!');
	}