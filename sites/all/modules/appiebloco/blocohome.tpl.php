<script type="text/javascript">

jQuery(document).ready(function($){

    $('.getusuarios').change(function(){

        var select = $(this);
        var tipo = $(this).attr('tipo');
        var pid = $(this).val();
        
        $.ajax({
          url: Drupal.settings.basePath+'appiebloco/getusuarios/'+tipo+'/'+pid,
          cache: false,
          success: function(html){ 
                $('.'+tipo).html(html);  
            },
          complete : function() {}
        });        

    });

});

</script>

<ul class="box-querovoufui">
    <li class="quero">
        <h3>Eu Quero</h3>
        <select class="getusuarios" tipo="quequerem">            
            <option value="destinos">Destinos</option>
            <?php foreach($paises as $pais){ ?>
            <option value="<?php echo $pais['pid']; ?>"><?php echo $pais['nome']; ?></option>
            <?php } ?>
        </select>
        <div class="clear">&nbsp;</div>
        <div class="interno">
            <span></span>
            <p>Calouro de viagens? Para onde deseja ir? Trabalhar ou estudar?</p>
            <div class="pessoas quequerem">

                <?php foreach($quequerem as $_quequerem){ ?>
                <div class="row">
                    <div class="avatar"><a href="https://apps.facebook.com/ieintercambio" target="_blank"><img src="<?php echo $_quequerem['avatar']; ?>" alt="<?php echo $_quequerem['nome']; ?>" /></a></div>
                    <div class="nome"><?php echo $_quequerem['nome']; ?></div>
                    <div class="frase"><?php echo $_quequerem['frase']; ?></div>
                </div>
                <?php } ?>

            </div>
            <div class="bt"><a href="https://apps.facebook.com/ieintercambio/" target="_blank">Para onde que ir?</a></div>
        </div>
    </li>
    <li class="vou">
        <h3>Eu Vou</h3>
        <select class="getusuarios" tipo="quevao">            
            <option value="destinos">Destinos</option>
            <?php foreach($paises as $pais){ ?>
            <option value="<?php echo $pais['pid']; ?>"><?php echo $pais['nome']; ?></option>
            <?php } ?>
        </select>
        <div class="clear">&nbsp;</div>
        <div class="interno">
            <span></span>
            <p>Ponto de encontro de quem está indo. Onde morar, traslado, etc...</p>
            <div class="pessoas quevao">

                <?php foreach($quevao as $_quevao){ ?>
                <div class="row">
                    <div class="avatar"><a href="https://apps.facebook.com/ieintercambio" target="_blank"><img src="<?php echo $_quevao['avatar']; ?>" alt="<?php echo $_quevao['nome']; ?>" /></a></div>
                    <div class="nome"><?php echo $_quevao['nome']; ?></div>
                    <div class="frase"><?php echo $_quevao['frase']; ?></div>
                </div>
                <?php } ?>

            </div>
            <div class="bt">
                <a href="https://apps.facebook.com/ieintercambio/" target="_blank">Troque informações</a>
            </div>
        </div>
    </li>
    <li class="fui">
    <h3>Eu Fui</h3>
        <select class="getusuarios" tipo="queforam">            
            <option value="destinos">Destinos</option>
            <?php foreach($paises as $pais){ ?>
            <option value="<?php echo $pais['pid']; ?>"><?php echo $pais['nome']; ?></option>
            <?php } ?>
        </select>
        <div class="clear">&nbsp;</div>
        <div class="interno">
        <span></span>
        <p>Aprenda com quem já foi. Veja depoimentos e dicas com viajantes IE</p>
        <div class="pessoas queforam">

                <?php foreach($queforam as $_queforam){ ?>
                <div class="row">
                    <div class="avatar"><a href="https://apps.facebook.com/ieintercambio" target="_blank"><img src="<?php echo $_queforam['avatar']; ?>" alt="<?php echo $_queforam['nome']; ?>" /></a></div>
                    <div class="nome"><?php echo $_queforam['nome']; ?></div>
                    <div class="frase"><?php echo $_queforam['frase']; ?></div>
                </div>
                <?php } ?>

        </div>
        <div class="bt">
            <a href="https://apps.facebook.com/ieintercambio/" target="_blank">Compartilhe sua experiência</a></div>
        </div>
    </li>
</ul>