<?php if(count($usuarios) > 0){ ?>
	<?php foreach($usuarios as $usuario){ ?>
	<div class="row">
	    <div class="avatar"><img src="<?php echo $usuario['avatar']; ?>" /></div>
	    <div class="nome"><?php echo $usuario['nome']; ?></div>
	    <div class="frase"><?php echo $usuario['frase']; ?></div>
	</div>
	<?php } ?>
<?php }else{ ?>

	<div class="interno">
	<p><strong style="color:#0a4fae;">Não há ninguém cadastrado nesse país.</strong></p>
	</div>

<?php } ?>