<?php
function lojas_admin_page() 
{
	$header = array(
		array('data' => 'Nome'),
		array('data' => 'Cidade'),
		array('data' => 'UF'),
		array('data' => 'Data'),
		array('data' => 'Ações'),
    ) ;
	
	$rows = array() ;

    $result = db_query( "SELECT l.oid, l.titulo, l.cidade, l.uf, l.created FROM ie_loja l ORDER BY l.created DESC" ) ;

	foreach ( $result as $r )
	{
		$row = array() ;

		$row[] = $r->titulo ;
		$row[] = $r->cidade ;
		$row[] = $r->uf ;
		$row[] = format_date($r->created) ;
		$row[] = l( 'editar', 'admin/content/lojas/form/'.$r->oid ) ;
		$rows[] = array( 'data' => $row ) ;
    }
	$output = "" ;
	
	$output .= theme( 'table', array( 
		'header' => $header, 
		'rows' => $rows,
		'sticky' => TRUE 
	) ) ;

	$output .= theme('pager', array( 'tags' => array() ) ) ;
	
	return $output ;
}

function lojas_admin_edit( $oid=null )
{
	drupal_add_js( 'http://maps.google.com/maps/api/js?sensor=false', 'external' ) ;
	drupal_add_js( 'sites/all/modules/lojas/js/lojas.js', 'file') ;



	/*

	Resolve o problema das imagens

	$query = 'SELECT imagem FROM koni_loja WHERE imagem > 0 AND imagem NOT IN (SELECT fid FROM file_usage)';

	$resultado = db_query($query);

	$lojas = $resultado->fetchAll();

	foreach($lojas as $_loja)
	{
		echo 'INSERT INTO file_usage (fid,module,type,id,count) VALUES ('.$_loja->imagem.',"user","user",1,1); <br />';
	}

	die();
	*/



	global $loja ;
	
	$output = "" ;
	
	if ( isset($oid) )
	{
		$result = db_query( "SELECT * FROM ie_loja where oid = :oid", array( 'oid' => $oid ) ) ;
		foreach ( $result as $loja )
		{
			$oid_exist = TRUE ;
		}
	}

	$output .= drupal_render( drupal_get_form('lojas_admin_edit_form') ) ;

	return $output;
}

function lojas_admin_edit_form( $form, &$form_state )
{
	global $loja ;

	$form['saie_id'] = array(
		'#type' => 'textfield',
		'#title' => 'SAIE ID',
		'#required' => FALSE,
		'#size' => 10,
		'#maxlength' => 100,
		'#weight' => 1,
		'#default_value' => @$loja->saie_id
	);	

	$form['titulo'] = array(
		'#type' => 'textfield',
		'#title' => 'Título',
		'#required' => TRUE,
		'#size' => 60,
		'#maxlength' => 200,
		'#weight' => 1,
		'#default_value' => @$loja->titulo
	);

	$form['abreviacao'] = array(
		'#type' => 'textfield',
		'#title' => 'Abreviação',
		'#required' => TRUE,
		'#size' => 60,
		'#maxlength' => 200,
		'#weight' => 1,
		'#default_value' => @$loja->abreviacao
	);

	/*
	$form['subtitulo'] = array(
		'#type' => 'textfield',
		'#title' => 'Subtítulo',
		'#required' => FALSE,
		'#size' => 60,
		'#maxlength' => 200,
		'#weight' => 2,
		'#default_value' => @$loja->subtitulo
	);
	*/
	$form['endereco_real'] = array(
		'#type' => 'textfield',
		'#title' => 'Endereço real',
		'#required' => FALSE,
		'#size' => 100,
		'#maxlength' => 200,
		'#weight' => 3,
		'#default_value' => @$loja->endereco_real
	);	
	$form['endereco'] = array(
		'#type' => 'textfield',
		'#title' => 'Endereço para o Mapa',
		'#required' => FALSE,
		'#size' => 100,
		'#maxlength' => 200,
		'#weight' => 3,
		'#default_value' => @$loja->endereco,
		'#suffix' => '<div id="map_canvas" style="width: 622px; height: 290px; border: 1px solid #CCC; margin: 0 0 0.6em 0.2em;"></div>'
	);
	$form['bairro'] = array(
		'#type' => 'textfield',
		'#title' => 'Bairro',
		'#required' => FALSE,
		'#size' => 40,
		'#maxlength' => 100,
		'#weight' => 4,
		'#default_value' => @$loja->bairro
	);
	$form['cidade'] = array(
		'#type' => 'textfield',
		'#title' => 'Cidade',
		'#required' => FALSE,
		'#size' => 40,
		'#maxlength' => 100,
		'#weight' => 5,
		'#default_value' => @$loja->cidade
	);
	$form['uf'] = array(
		'#type' => 'select',
		'#title' => 'UF',
		'#required' => TRUE,
		'#weight' => 6,
		'#options' => array(
			'AC' => 'AC',
			'AL' => 'AL',
			'AM' => 'AM',
			'AP' => 'AP',
			'BA' => 'BA',
			'CE' => 'CE',
			'DF' => 'DF',
			'ES' => 'ES',
			'GO' => 'GO',
			'MA' => 'MA',
			'MG' => 'MG',
			'MS' => 'MS',
			'MT' => 'MT',
			'PA' => 'PA',
			'PB' => 'PB',
			'PE' => 'PE',
			'PI' => 'PI',
			'PR' => 'PR',
			'RJ' => 'RJ',
			'RN' => 'RN',
			'RO' => 'RO',
			'RR' => 'RR',
			'RS' => 'RS',
			'SC' => 'SC',
			'SE' => 'SE',
			'SP' => 'SP',
			'TO' => 'TO'
		),
		'#default_value' => @$loja->uf
	);
	/*
	$form['cep'] = array(
		'#type' => 'textfield',
		'#title' => 'CEP',
		'#required' => FALSE,
		'#size' => 12,
		'#maxlength' => 10,
		'#weight' => 7,
		'#default_value' => @$loja->cep
	);
	*/
	$form['telefone'] = array(
		'#type' => 'textfield',
		'#title' => 'Telefone',
		'#required' => FALSE,
		'#size' => 20,
		'#maxlength' => 60,
		'#weight' => 8,
		'#default_value' => @$loja->telefone
	);
	$form['email'] = array(
		'#type' => 'textfield',
		'#title' => 'E-mail',
		'#required' => FALSE,
		'#size' => 100,
		'#maxlength' => 200,
		'#weight' => 9,
		'#default_value' => @$loja->email
	);
	
	$form['atendimento'] = array(
		'#type' => 'textarea',
		'#title' => 'Horário de Atendimento',
		'#required' => FALSE,
		'#size' => 100,
		'#maxlength' => 200,
		'#weight' => 10,
		'#default_value' => @$loja->atendimento
	);
	/*
	$form['atendimento2'] = array(
		'#type' => 'hidden',
		'#title' => 'Horário de Atendimento Sábado, Domingo e Feriado',
		'#required' => FALSE,
		'#size' => 100,
		'#maxlength' => 200,
		'#weight' => 10,
		'#default_value' => @$loja->atendimento2
	);	
	
	$form['delivery'] = array(
		'#type' => 'textfield',
		'#title' => 'Delivery',
		'#required' => FALSE,
		'#size' => 100,
		'#maxlength' => 200,
		'#weight' => 10,
		'#default_value' => @$loja->delivery
	);		

	*/

	/*

	$form['fid'] = array(
		'#title' => t('Image'),
		'#type' => 'managed_file',
		'#description' => t('The uploaded image will be displayed on this page using the image style choosen below.'),
		/// '#default_value' => variable_get('image_fid', ''),
		'#default_value' => @$loja->fid,
		'#upload_location' => 'public://lojas_images/',
	);	

	*/

	/*
	$form['imagem'] = array(
		'#title' => t('Image'),
		'#type' => 'managed_file',
		'#description' => t('The uploaded image will be displayed on this page using the image style choosen below.'),
		/// '#default_value' => variable_get('image_fid', ''),
		'#default_value' => @$loja->imagem,
		'#upload_location' => 'public://lojas_images/',
	);		
	*/	

	$form['oid'] = array(
		'#type' => 'hidden',
		'#default_value' => @$loja->oid
	);
	
	$form['latitude'] = array(
		'#type' => 'hidden',
		'#default_value' => @$loja->latitude
	);
	
	$form['longitude'] = array(
		'#type' => 'hidden',
		'#default_value' => @$loja->longitude
	);
	
	// $maps = '<div id="map_canvas" style="width: 622px; height: 290px; border: 1px solid #CCC; margin: 0 0 0.6em 0.2em;"></div>' ;

	
	$form['submit'] = array(
		// '#prefix' => $maps,
		'#type' => 'submit',
		'#value' => 'Salvar',
		'#weight' => 21,
	);
    if ( isset($loja->oid) )
    {
        $form['delete'] = array(
            '#type' => 'submit',
            '#value' => 'Apagar',
            '#weight' => 22,
        );
	}
	
	return $form ;
}

/**
 * Form submission handler for lojas_admin_edit_form_submit().
 */
function lojas_admin_edit_form_submit( $form, &$form_state )
{

    if( $form_state['values']['op'] == 'Salvar' ) 
    {

		/*
	    if(strlen($form_state['values']['imagem']) > 1){

			$file = file_load($form_state['values']['imagem']);

			// Change status to permanent.
			if($file->status != FILE_STATUS_PERMANENT)
			{

				global $user;

				$file->status = FILE_STATUS_PERMANENT;	
				file_save($file);		
				file_usage_add($file, 'user', 'user', $user->uid);	
			}    		
    	}
    	*/

    	
    	
        // parametros
        $loja_params = array(
            'titulo' => $form_state['values']['titulo'], 
            'abreviacao' => $form_state['values']['abreviacao'], 
            'saie_id' => $form_state['values']['saie_id'], 
            // 'subtitulo' => $form_state['values']['subtitulo'], 
            'endereco_real' => $form_state['values']['endereco_real'], 
            'endereco' => $form_state['values']['endereco'], 
            'bairro' => $form_state['values']['bairro'], 
            'cidade' => $form_state['values']['cidade'], 
            'uf' => $form_state['values']['uf'], 
            // 'cep' => $form_state['values']['cep'], 
            'telefone' => $form_state['values']['telefone'], 
            'email' => $form_state['values']['email'], 
            'atendimento' => $form_state['values']['atendimento'], 
            // 'delivery' => $form_state['values']['delivery'], 
            // 'fid' => $form_state['values']['fid'], 
            // 'imagem' => $form_state['values']['imagem'], 
            'latitude' => $form_state['values']['latitude'], 
            'longitude' => $form_state['values']['longitude'], 
        ) ;
    
        // 'atendimento2' => $form_state['values']['atendimento2'], 	


	// INSERT
        if ( empty( $form_state['values']['oid'] ) || $form_state['values']['oid'] == '' )
        {
            $loja_params['created'] = time();      
            
            // Grava ie_loja
            /*
            db_query( 
                'INSERT INTO ie_loja ( 
                    titulo, abreviacao, saie_id,
                    endereco_real, endereco, bairro, cidade, uf, 
                    cep, telefone, email,
                    latitude, longitude, created 
                ) VALUES ( 
                    :titulo, :abreviacao, :saie_id,
                    :endereco_real, :endereco, :bairro, :cidade, :uf, 
                    :cep, :telefone, :email,
                    :latitude, :longitude, :created 
                )', 
                $loja_params 
            );
            */

			$nid = db_insert('ie_loja') 
			->fields($loja_params)
			->execute();            



        }
        // UPDATE
        else
        {
            $loja_params['oid'] = $form_state['values']['oid'] ;
            
            // Grava ie Loja
            db_query( 
                'UPDATE ie_loja SET
                    titulo = :titulo, 
                    abreviacao = :abreviacao, 
                    saie_id = :saie_id, 
                    endereco_real = :endereco_real, 
                    endereco = :endereco, 
                    bairro = :bairro, 
                    cidade = :cidade, 
                    uf = :uf, 
                    telefone = :telefone, 
                    email = :email, 
                    atendimento = :atendimento, 
                    latitude = :latitude, 
                    longitude = :longitude
                WHERE oid = :oid', 
                $loja_params 
            ) ;
        }
    }
    elseif ( $form_state['values']['op'] == 'Apagar' )
    {
		db_query( 
			'DELETE FROM ie_loja WHERE oid = :oid', 
			array('oid' => $form_state['values']['oid']) 
		) ;
    }
    
	// Joga para tela de listagem
	$form_state['redirect'] = 'admin/content/lojas' ;
}