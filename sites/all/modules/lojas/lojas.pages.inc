<?php

function lojas_pages_detalhes(){

	drupal_add_js( 'http://maps.google.com/maps/api/js?sensor=false', 'external' ) ;
	drupal_add_js( drupal_get_path('theme', 'iesite') . '/js/lojas.js', 'file' ) ;
	drupal_add_css(drupal_get_path('theme', 'iesite') . '/css/onde-estamos.css', array('group' => CSS_THEME, 'type' => 'file'));

	global $loja ;

	echo '<!-- ===> '.arg(2).' -->';	
	$result = db_query( 'SELECT * FROM ie_loja WHERE oid = :oid', array('oid' => arg(2)) ) ;

	foreach ( $result as $loja )
	{
		$oid_exist = TRUE ;
	}	

	$output = '
		<script type="text/javascript">

		  	function initialize() {
		    		var latlng = new google.maps.LatLng('.$loja->latitude.','.$loja->longitude.');
		    		var myOptions = {
		      		zoom: 17,
		      		center: latlng,
		      		mapTypeId: google.maps.MapTypeId.SATELLITE
		    		};
		    		var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

		    		var marker = new google.maps.Marker({
		    			 	position: latlng,
		    			      map: map,
		    			      title:"Hello World!"
		    			  });
		  	}

		  	jQuery(function(){

		  		initialize();

		  	});

		</script>
	';


	// $file = file_load($loja->fid)->uri;

	global $base_path;

	$server = $_SERVER['SERVER_NAME']; 
	$endereco = $_SERVER ['REQUEST_URI'];

	
	$output .= '<div class="titulo"> <div class="conteudo-fixo">';
	$output .= '</div> </div>';
	$output .= '<div class="conteudo-fixo">';
	$output .= '<div class="internaLojas">';
		$output .= '<div class="headerLoja">';
			$output .= '<div class="header">';
				//$output .= '<h3>';
				//$output .= '<span>'.$loja->cidade.'</span>';
				//$output .= ''.$loja->titulo.'';
				//$output .= '</h3>';
			$output .= '</div>';
		$output .= '</div>';

		$output .= '<div class="loja">';
				$output .= '<div class="bloco_voltar">';
					$output .= '<div class="link">';
						$output .= '<a href="'.url('sobre-a-ie/fale-conosco').'" id="link_voltar">Voltar para a página de contato</a>';
					$output .= '</div>';
				$output .= '</div>';
				
			$output .= '<div class="loja-dados">';	
			$output .= '<h4 class="title_loja">'.$loja->titulo.'</h4>';
			$output .= '<div class="dados">';
				$output .= '<div class="foto">';
				$output .= theme('image', array('path' => $base_path.path_to_theme().'/img/busca/pin_lojas.png'));
					// $output .= '<img src="'.file_create_url($file).'" />';
				$output .= '</div>';

				if(strlen($loja->endereco_real) > 0)
				{
					$plotar_endereco = $loja->endereco_real;
				}elseif(strlen($loja->endereco) >0)
				{
					$plotar_endereco = $loja->endereco;
				}

				$output .= '<div class="endereco"><strong> Endereço: </strong><br />'.$plotar_endereco.' <br /> '.$loja->bairro.' <br /> '.$loja->cidade.' - '.$loja->uf.'. <br />'.$loja->cep.'</div>';

				$output .= '<div class="horarios">';
					$output .= '<div class="horario2"><strong>Horário de funcionamento:</strong><br/>'.str_replace( array("\r\n", "\n", "\r"), "<br/>", $loja->atendimento).'</div> <br />';
					$output .= '<div class="delivery"><strong>Telefone:</strong></div>';
				$output .= '</div>';
			$output .= '</div>';
			$output .= '<div class="outrasInfos">';
				$output .= '<div class="telefone">'.$loja->telefone.'</div>';
			$output .= '</div>';	
		$output .= '</div>';
		$output .= '</div>';
		$output .= '<div class="mapa">';
		// $output .= '<h4>Localização no mapa</h4>';
		$output .= '<div id="map_canvas" style="height:450px;"></div>';
		$output .= '</div>';
	$output .= '</div>';
	$output .= '</div>';

	return $output;

}
