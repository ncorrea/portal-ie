jQuery(document).ready(function($) {

	if(Drupal.settings.request_path == 'estudar/curso-de-idiomas/destaque')
	{

		var id = '2';
		var verbo = 'Eu quero';
		var produto = 'Curso de Idiomas';
		var subproduto = $('.view-cursos-idiomas-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-cursos-idiomas-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-cursos-idiomas-destaques-pagina .link').attr('href',url_contato);

		});

	}
	else if(Drupal.settings.request_path == 'estudar/high-school/modalidades')
	{

		var id = '5';
		var verbo = 'Eu quero';
		var produto = 'High School';	
		var subproduto = '';	
		var url_contato = '';	

		for(i = 1;i<=3; i++)
		{
			var subproduto = $('.field-name-body .col0'+i+' h5').html();

			url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.field-name-body .col0'+i+' .boxlink a').attr('href',url_contato);
		}

	}
	else if(Drupal.settings.request_path == 'estudar/high-school/destaques')
	{

		var id = '5';
		var verbo = 'Eu quero';
		var produto = 'High School';
		var subproduto = $('.view-high-school-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-high-school-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-high-school-destaques-pagina .link').attr('href',url_contato);

		});

	}
	else if(Drupal.settings.request_path == 'estudar/teen-experience/destaques')
	{

		var id = '6';
		var verbo = 'Eu quero';
		var produto = 'Teen Experience';
		var subproduto = $('.view-teen-experience-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-teen-experience-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-teen-experience-destaques-pagina .link').attr('href',url_contato);

		});

	}	
	else if(Drupal.settings.request_path == 'estudar/study-and-work/destaques')
	{

		var id = '2';
		var verbo = 'Eu quero';
		var produto = 'Study and Work - Estudar';
		var subproduto = $('.view-study-and-work-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-study-and-work-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-study-and-work-destaques-pagina .link').attr('href',url_contato);

		});

	}		
	else if(Drupal.settings.request_path == 'trabalhar/study-and-work/destaques')
	{

		var id = '2';
		var verbo = 'Eu quero';
		var produto = 'Study and Work - Trabalhar';
		var subproduto = $('.view-study-and-work-destaques-pagina-trabalhar .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-study-and-work-destaques-pagina-trabalhar .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-study-and-work-destaques-pagina-trabalhar .link').attr('href',url_contato);

		});

	}		
	else if(Drupal.settings.request_path == 'estudar/seguro-iac')
	{

		var id = '9';
		var verbo = 'Eu quero';
		var produto = 'Seguro Viagem IAC';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}
	else if(Drupal.settings.request_path == 'trabalhar/seguro-iac')
	{

		var id = '9';
		var verbo = 'Eu quero';
		var produto = 'Seguro Viagem IAC';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}	
	else if(Drupal.settings.request_path == 'estudar/visa-travel-money')
	{

		var id = '12';
		var verbo = 'Eu quero';
		var produto = 'Visa Travel Money';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}
	else if(Drupal.settings.request_path == 'trabalhar/visa-travel-money')
	{

		var id = '12';
		var verbo = 'Eu quero';
		var produto = 'Visa Travel Money';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}		
	else if(Drupal.settings.request_path == 'conhecer/visa-travel-money')
	{

		var id = '12';
		var verbo = 'Eu quero';
		var produto = 'Visa Travel Money';		

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id;

		$('.page-node .link').attr('href',url_contato);		

	}		
	else if(Drupal.settings.request_path == 'conhecer/mochilao/destaques')
	{

		var id = '6';
		var verbo = 'Eu quero';
		var produto = 'Trip Experience';
		var subproduto = $('.view-mochiloes-destaques-pagina .views-field-title .field-content').eq(0).html();

		var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

		$('.view-mochiloes-destaques-pagina .link').attr('href',url_contato);

		$('.galeria03 .container li').live('click',function(){

			subproduto = $(this).children('.title_curso').eq(0).html();

			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.view-mochiloes-destaques-pagina .link').attr('href',url_contato);

		});

	}	
	else
	{

		var regExp = '';
		var path = Drupal.settings.request_path;
		var split_url = Drupal.settings.request_path.split('/');

		// Work Experience
		regExp = /trabalhar\/workexperience/g;	
		
		if (regExp.test(path)) 
		{
		    
			var id = '3';
			var verbo = 'Eu quero';
			var produto = 'Work Experience';	
			var subproduto = '';	
			var url_contato = '';	

			for(i = 1;i<=3; i++)
			{
				var subproduto = $('.field-name-field-opcoes .col0'+i+' h5').html()+' - '+split_url[3];

				url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

				$('.field-name-field-opcoes .col0'+i+' .boxlink a').attr('href',url_contato);
			}			

		}	

		// Conhecer país
		regExp = /conhecer\/pais/g;
		if (regExp.test(path)) 
		{

			var id = '2';
			var verbo = 'Quero ir';
			var produto = 'Conhecer país';	
			var subproduto = $('h3').html().trim();	
			var url_contato = '';				

			url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.col02 .link').attr('href',url_contato);
		}	

		// Conhecer cidade
		regExp = /conhecer\/cidade/g;
		if (regExp.test(path)) 
		{

			var id = '2';
			var verbo = 'Quero ir';
			var produto = 'Conhecer cidade';	
			var subproduto = $('h3').html().trim();
			var url_contato = '';				

			url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

			$('.col02 .link').attr('href',url_contato);

		}				

		// Promoção Interna
		regExp = /promocoes\/pais/;
		if (regExp.test(path)) 
		{

			var id = '2'; // Mesmo que o Curso de Idiomas
			var verbo = 'Eu quero';
			var produto = 'Promoção';	
			var subproduto = '';
			var empresa = '';
			var url_contato = '';				

			$('.view-promoces .views-row').each(function(){

				empresa = $(this).children('.views-field-field-promocoes-empresa').children('.field-content').html();
				subproduto = $(this).children('.views-field-field-promocoes-cidade').children('.field-content').html()+' - '+empresa;

				url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?verbo='+verbo+'&produto='+produto+'&saie='+id+'&subproduto='+subproduto;

				$(this).children('.views-field-field-promocoes-valor').append('<a href="'+url_contato+'">eu quero</a>');
				
			});			
		}					

	}



});