jQuery(function(){

    initialize();

    //submita busca map
    jQuery('.wrapForm_mapa input[type=image]').click(function()
    {
		initialize();
    });

});


// SCRIPT GOOGLE MAPS - inicio
var map;
var gdir;
var geocoder = null;
var addressMarker;
var icone ;
var overlays = new Array() ;
var directionsService = new google.maps.DirectionsService();

var myLati ;
var myLongi ;

function initialize() 
{
	var latlng = new google.maps.LatLng(-15, -55) ;
    var myOptions = {
		zoom: 4,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map( document.getElementById("map_canvas"), myOptions ) ;

	geocoder = new google.maps.Geocoder() ;

	gdir = new google.maps.DirectionsRenderer() ;
	gdir.setMap( map ) ;
	
	// Cria��o do nosso �cone
	if ( jQuery('input:radio[name=tipo]:checked').val() == '2' )
	{
		icone = new google.maps.MarkerImage(
			"",
			new google.maps.Size( 80, 58 ),
			new google.maps.Point( 0, 0 ),
			new google.maps.Point( 33, 50 )
		) ;
	}
	else
	{
		icone = new google.maps.MarkerImage(
			"",
			new google.maps.Size( 36, 40 ),
			new google.maps.Point( 0, 0 ),
			new google.maps.Point( 33, 50 )
		) ;
	}	
}

function showLocation()
{
	address = jQuery('#busca_endereco').val() ;

    //alert(address);

	geocoderequest = {
		address: address,
		latLng: new google.maps.LatLng( -15, -55 ),
		region: 'BR'
	}
	
    geocoder.geocode( geocoderequest, showAddress );


}

function showAddress( response, status )
{
	while( overlays[0] )
		overlays.pop().setMap( null ) ;

    if ( !response || status != google.maps.GeocoderStatus.OK ) 
	{
        alert( "Status Code:" + status ) ;
    }
    else 
	{
        point = response[0].geometry.location ;
		
		myLati = point.lat() ;
		myLongi = point.lng() ;
		
		var marker = new google.maps.Marker({
			position: point,
			title: "Sua localidade"
		});
  
		// To add the marker to the map, call setMap();
		marker.setMap( map ) ;
		
		map.setCenter( point ) ;
		map.setZoom( 13 ) ;
		
		overlays.push( marker ) ;
		
        recuperaDados() ;
    }
}

function dump(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;
	
	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";
	
	if(typeof(arr) == 'object') { //Array/Hashes/Objects 
		for(var item in arr) {
			var value = arr[item];
			
			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}

function showLocationForGeolocation(){

	var estado = jQuery('select[name=estado]').val();
	var cidade = jQuery('select[name=cidade]').val();
	var bairro = jQuery('select[name=bairro]').val();

	// location.href = Drupal.settings.basePath+"/busca/lojas/json?estado=" + estado + "&cidade=" + cidade + "&bairro=" + bairro;

	location.href = "http://www.ieintercambio.com.br/busca/lojas/json?estado=" + estado + "&cidade=" + cidade + "&bairro=" + bairro;
}

function recuperaDados()
{
	var tipo = jQuery('input:radio[name=tipo]:checked').val() ;

	// location.href = Drupal.settings.basePath+"/busca/lojas/json?latitude=" + myLati + "&longitude=" + myLongi;

	location.href = "http://www.ieintercambio.com.br/busca/lojas/json?latitude=" + myLati + "&longitude=" + myLongi;
	
	/*
	jQuery.ajax({
		type: "POST",
		url: "/koni/busca/lojas/json",
		data: "latitude=" + myLati + "&longitude=" + myLongi,
		dataType: "json",
		success: function( data ) {
			plotaPontos( data.pontos );
		},
		error: function( xhr, ajaxOptions, thrownError ) {
			alert( thrownError ) ;
		} 
	});	
	*/
}

function plotaPontos( pontos ) 
{
	for ( var i = 0; i < pontos.length ; i++ )
	{
		var ponto = montaPonto( 
			pontos[i].nome,
			pontos[i].endereco,
			pontos[i].bairro,
			pontos[i].telefone,
			pontos[i].email,
			pontos[i].latitude.replace(',', '.'),
			pontos[i].longitude.replace(',', '.')
		) ;
		
		ponto.setMap( map ) ;
		overlays.push( ponto ) ;

	}
}

function montaPonto( nome, endereco, bairro, telefone, email, latitude, longitude )
{
	var corpo = "<strong>" + nome + "</strong><br/>" + endereco + "<br/>" + bairro + "<br/><br/>Tel: " + telefone + "<br/><a href=\"mailto:" + email + "\">" + email + "</a><br/><br/><a href=\"javascript:tracaRota('" + endereco + ", " + bairro + ", BR ')\">Mostrar Rota</a>" ;

	var localidade = new google.maps.LatLng( latitude, longitude ) ;
	
	var infowindow = new google.maps.InfoWindow({
		content: corpo
	});

	var marker = new google.maps.Marker({
		position: localidade, 
		title: nome,
		icon: icone 
	}) ;
	
	google.maps.event.addListener( marker, 'click', function() {
		infowindow.open( map, marker ) ;
	});
	
	return marker ;
}

function setDirections( fromAddress, toAddress, locale )
{
	gdir.load("from: " + fromAddress + " to: " + toAddress, { "locale": locale });
}

function handleErrors()
{
   if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
     alert("No  corresponding geographic location could be found for one of the  specified addresses. This may be due to the fact that the address is  relatively new, or it may be incorrect.\nError code: " + gdir.getStatus().code);
   else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
     alert("A  geocoding or directions request could not be successfully processed,  yet the exact reason for the failure is not known.\n Error code: " + gdir.getStatus().code);
   
   else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
     alert("The  HTTP q parameter was either missing or had no value. For geocoder  requests, this means that an empty address was specified as input. For  directions requests, this means that no query was specified in the  input.\n Error code: " + gdir.getStatus().code);

  //   else if (gdir.getStatus().code == G_UNAVAILABLE_ADDRESS)  <--- Doc bug... this is either not defined, or Doc is wrong
  //      alert("The geocode for the given address or the route for the given  directions query cannot be returned due to legal or contractual  reasons.\n Error code: " + gdir.getStatus().code);
     
   else if (gdir.getStatus().code == G_GEO_BAD_KEY)
     alert("The given key is either invalid or does not match the domain for which it was given. \n Error code: " + gdir.getStatus().code);

   else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
     alert("A directions request could not be successfully parsed.\n Error code: " + gdir.getStatus().code);
    
   else alert("An unknown error occurred.");
}

function tracaRota( toAddress )
{
	var start = jQuery('#busca_endereco').val() + ', BR' ;
    var end = toAddress ;
	
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    } ;
	
    directionsService.route( request, function( response, status ) {
		if (status == google.maps.DirectionsStatus.OK) {
			gdir.setDirections( response ) ;
		}
    });	
}

function getEstados(){

}

function getCidades(select){

	var uf = select[select.selectedIndex].value;

	jQuery.ajax({
		type: "POST",
		// url: Drupal.settings.basePath+"/busca/ajax/getCidades/"+uf,
		url: "http://www.ieintercambio.com.br"+"/busca/ajax/getCidades/"+uf,
		data: "1=1",
		success: function( data ) {
			jQuery('select[name=cidade]').html(data);
		},
		error: function( xhr, ajaxOptions, thrownError ) {
			alert( thrownError ) ;
		} 
	});		
}

function getBairros(select){

	var cidade = select[select.selectedIndex].value;

	jQuery.ajax({
		type: "POST",
		// url: Drupal.settings.basePath+"/busca/ajax/getBairros/"+cidade,
		url: "http://www.ieintercambio.com.br"+"/busca/ajax/getBairros/"+cidade,
		data: "1=1",
		success: function( data ) {
			jQuery('select[name=bairro]').html(data);
		},
		error: function( xhr, ajaxOptions, thrownError ) {
			alert( thrownError ) ;
		} 
	});		

}

function getLojas(select){

	var cidade = select[select.selectedIndex].value;

	jQuery.ajax({
		type: "POST",
		// url: Drupal.settings.basePath+"/busca/ajax/getBairros/"+cidade,
		url: "http://www.ieintercambio.com.br"+"/busca/ajax/getLojas/"+cidade,
		data: "1=1",
		success: function( data ) {
			jQuery('select[name=loja]').html(data);
			//jQuery('.busca #btbusca').hide();
		},
		error: function( xhr, ajaxOptions, thrownError ) {
			alert( thrownError ) ;
		} 
	});		

}

function goLoja()
{
	var loja = jQuery('select[name=loja]').val();

	location.href = 'http://www.ieintercambio.com.br/lojas/detalhes/'+loja;
}