<style type="text/css">

.loja{
	float:none;
	height:130px;
	width:920px; 
	padding:20px;
	font-family: arial;
	border-bottom: 1px solid #d9d9d9;
}

.lojas{border: 3px solid #e9e9e9; margin-bottom:20px;}

.lojas .title{font-size:30px; color:#484848; background-color:#e9e9e9; width:940px; padding:10px;}

.lojas h2{font-size:24px;}

.lojas .endereco{width:200px;}

.lojas .link_loja{font-size:16px; width: 110px; height: 25px;height: 25px;margin-top: 10px;}
.lojas .link_loja a{font-size:16px; color:#484848; text-decoration: underline;}
.lojas .veja_loja{font-size:16px; width: 110px; height: 25px;height: 23px;}
.lojas .veja_loja a{font-size:16px; color:#484848; text-decoration: underline;}

.lojas .loja-1{float:left; width:300px;}
.lojas .loja-2{float:left; width:260px;}
.lojas .loja-3{float:left;}

</style>


<?php foreach($lojas as $key => $_lojas){ ?>
<div class="lojas">

	<div class="title"><?php echo $key; ?></div>

	<?php foreach($_lojas as $loja){ ?>
	<div class="loja">
		<div class="loja-1">
			<h2><?php echo $loja['titulo']; ?></h2>
			<div class="endereco"><?php echo $loja['endereco_real']; ?></div>
			<!-- <div class="cep">CEP: <?php echo $loja['cep']; ?></div> -->
			<div class="estado-cidade"><?php echo $key.' - '.$loja['cidade']; ?></div>
		</div>	
		<div class="loja-2">
			<h2>Telefone</h2>
			<div class="telefone"><?php echo $loja['telefone']; ?></div>
			<div class="link_loja"><?php echo l('Fale Conosco','sobre-a-ie/fale-conosco'); ?></div>
			<div class="veja_loja"><?php echo l('Veja o mapa','lojas/detalhes/'.$loja['oid'].'/?loja='.removeAcentos($loja['titulo'])); ?></div>
		</div>		
		<div class="loja-3">
			<h2>Horário de Funcionamento</h2>
			<div class="horario-de-funcionamento"><?php echo $loja['atendimento']; ?></div>
		</div>	
	</div>
	<?php } ?>

</div>
<div style="clear:both;"></div>
<?php } ?>