<?php 
	drupal_add_css(drupal_get_path('theme', 'iesite') . '/css/busca.css');
?>

<div class="fltlft boxIcon icone boxcolor"></div>
<div class="fltlft boxTitle">Resultados</div>
<div class="clear"></div>

<p>Procurando pelo termo "<span><?php echo $texto; ?></span>"</p>

<?php if(count($resultados) == 0){ ?>

	<h5>Nenhum resultado encontrado.</h5>

<?php }else{ ?>

	<dl>
	<?php foreach($resultados as $resultado){ ?>
	<dd>
		<div class="borda"></div>
		<?php if(strlen($resultado['imagem']) > 0){ ?>
		<div class="image"><a href="<?php echo $resultado['link']; ?>"><?php echo theme('image_style', array('style_name' => 'imagem_busca', 'path' => $resultado['imagem'])); ?></a></div>
		<?php } ?>
		<div class="title"><a href="<?php echo $resultado['link']; ?>"><?php echo $resultado['title']; ?></a></div>
		<div class="body"><?php echo $resultado['body']; ?></div>

		<div class="linha"></div>
	</dd>
	<?php } ?>
	</dl>

<?php } ?>