<div class="content-top">
	<h3>Bem-vindo ao aplicativo da <span>IE Intercâmbio</span></h3>
	<p>Compartilhe sua experiência, peça dicas e troque informações!</p>
</div>
<div class="clear"></div>

<hr class="acima">
<div class="content-abas">
		<ul class="botoes">
			<?php // echo $links; ?>
			<li class="quero transition" rel="quero">
				<p>eu<span>quero</span></p>
				<div id="box" class="quero">
					<div class="infoBox txtQuero">
						<span class="setaVermelha"></span>
						<img src="<?php echo drupal_get_path('module', 'appiefacebook'); ?>/images/home/mapa-euquero.png" alt="">
						<p>Aqui você encontra a galera que quer viajar e curtir novas experiências.</p>
						<?php echo $link_eu_quero; ?>
					</div>
				</div>
			</li>
			<li class="vou transition" rel="vou">
				<p>eu<span>vou</span></p>
				<div id="box" class="vou">
					<div class="infoBox txtVou">
						<span class="setaVerde"></span>
						<img src="<?php echo drupal_get_path('module', 'appiefacebook'); ?>/images/home/mapa-euvou.png" alt="">
						<p>Aqui você troca informações e conhece quem está de viagem marcada.</p>
						<?php echo $link_eu_vou; ?>
					</div>
				</div>
			</li>
			<li class="fui transition" rel="fui">
				<p>eu<span>fui</span></p>
				<div id="box" class="fui">
					<div class="infoBox txtFui">
						<span class="setaAzul"></span>
						<img src="<?php echo drupal_get_path('module', 'appiefacebook'); ?>/images/home/mapa-eufui.png" alt="">
						<p>Aqui você compartilha suas experiências e confere dicas de outros viajantes.</p>
						<?php echo $link_eu_fui; ?>
					</div>
				</div>
			</li>
		</ul>
		<div class="imgBase"><img src="<?php echo drupal_get_path('module', 'appiefacebook'); ?>/images/home/home.png" height="311" width="760" alt=""></div>
		<div class="clear"></div>
</div>
<hr class="abaixo">

<div class="clear"></div>
<div class="content-bottom">
	<p>Explore um</p>
	<span>mundo de intercâmbios.</span>
</div>