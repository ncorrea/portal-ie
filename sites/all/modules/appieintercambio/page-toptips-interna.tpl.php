	<div class="facebook_share">
	<div class="centraliza">
		<div class="fb-like" data-href="https://www.facebook.com/IEintercambio?ref=ts&amp;fref=ts" data-send="true" data-width="450" data-show-faces="false"></div>
	</div>
</div>
<div class="header">
	<div class="header-1">
		<h1>Ie explorer</h1>
		<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
	</div>
	<div class="menuPrinc">
		<div class="centraliza">
			<ul>
				<!-- <li><a href="http://facebook.ieintercambio.com.br/appieintercambio/inicial" class="transition">Home</a></li> -->
				<li>
					<a href="javascript:void()" class="transition">Cadastrar Viagens</a>
					<dl>
						<dt></dt>
						<dd><a href="https://apps.facebook.com/ieintercambio/euquero" target="_top" class="transition">Quero Viajar!</a></dd>
						<dd style="border-top: 1px solid #333333; border-bottom: 1px solid #333333;"><a href="https://apps.facebook.com/ieintercambio/euvou" target="_top" class="transition">Vou VIAJAR para</a></dd>
						<dd><a href="https://apps.facebook.com/ieintercambio/eufui" class="transition" target="_top">Já VIAJEI para</a></dd>
					</dl>
				</li>
				<li><a href="https://apps.facebook.com/ieintercambio/minhasviagens" target="_top" class="transition" target="_top">Minhas Viagens</a></li>
				<li><a href="https://apps.facebook.com/ieintercambio/encontre" class="transition" target="_top">Outros Viajantes<span></span></a></li>
				<li class="last"><a href="https://apps.facebook.com/ieintercambio/toptips" class="transition ativo" target="_top">Top Tips</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="limite">
	<div class="content-top">
		<div class="texto">
			<h3>ESPAÇO PARA COMPARTILHAR</h3>
			<p>Aqui você consegue o máximo de informações úteis para uma viagem IE.</p>
		</div>
	</div>
	<div class="breadcrumb">
		<p> Home > Top Tips > <span>Perguntas</span></p>
	</div>
	<div class="clear"></div>
	<div class="conteudo">
		<h4>DICAS E DÚVIDAS:</h4>
		<div class="clear"></div>
		<div class="busca-pergunta">
			<div class="camposbuscar">
				<form action="<?php echo url('appieintercambio/toptips/busca'); ?>">
					<label for="categoria">Busca:</label>
					<select name="categoria" id="">
						<option value="">Todas as Categorias</option>
						<option value="1">Estudo</option>
						<option value="2">Trabalho</option>
						<option value="3">Explorar Destinos</option>
						<option value="4">Classificados</option>
						<option value="5">Dicas Gerais</option>
					</select>
					<input type="text" name="busca">
					<input type="submit" class="transition" value="buscar">
				</form>
			</div>
		</div>
		<div class="clear"></div>
		<div class="blocoMural">
		<?php foreach($categorias as $categoria){ ?>
<!-- 		<div class="header-mural">
			<h3 class="title"><?php echo $categoria['titulo']; ?></h3>						
		</div> -->
			<h3><?php echo $categoria['titulo']; ?></h3>
			<div class="clear"></div>
			<?php foreach($categoria['topicos'] as $topico){ ?>
			<div class="post">
				<div class="barra"></div>
				<div class="txtpost">
					<div class="fechar"><a href="javascript:void(0)">fechar</a></div>
					<p><span><?php echo $topico['usuario']; ?></span> | <?php echo $topico['created']; ?></p>
					<?php echo l($topico['pergunta'],'appieintercambio/toptips/topico/'.$topico['tid'],array('attributes' => array('class' => 'transition'))); ?>
					<div class="clear"></div>
					<div class="infos">
						<div class="respostas"><span><?php echo $topico['respostas']; ?></span> resposta(s)</div>
						<div class="seguidores"><span><?php echo $topico['seguidores']; ?></span> seguidor(es)</div>
						<div class="visualizacoes"><span><?php echo $topico['visualizacoes']; ?></span> visualização(ões)</div>
					</div>
				</div>
				<div class="resposta">
					<a href="<?php echo url('appieintercambio/toptips/topico/'.$topico['tid']); ?>" class="transition">responder</a>
				</div>
			</div>
            <div class="clear"></div>
			<?php } ?>
		<?php } ?>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="footer">
	<div class="barra">
		<div class="footer-1">
			<p>IE Intercâmbio no Exterior - Todos os direitos reservados.</p>
			<a class="transition" href="http://www.kindle.com.br" target="_blank">Kindle</a>
		</div>
	</div>
</div>
  <script type='text/javascript'>
    window.onload=function() {


      altura = screen.height;

      if(altura > 800){
        FB.Canvas.setSize({width:760,height:altura});
    }else{
        FB.Canvas.setSize({width:760,height:document.body.offsetHeight});
      }
      FB.Canvas.scrollTo(0,0);
      
    }
  </script>
<!-- <p><span><a href="encontre/user/144" style="font-size: 1em">Alessandro Moreira Pinto </a></span> | 15/02/2013</p>
<p class="categoria">Estudo</p>
<a href="/appieintercambio/toptips" class="transition">qualquer coisa</a>	

	 
					 

<div class="clear"></div>							
<div class="respostas">								
	<a class="follow" rel="63" href="#" style="font-size: 0.8em">Seguir esta conversa</a>
	<div class="container-respostas tid-63">
								</div>
    <div class="respostas-2">                                
    	<div class="foto"><img src="https://graph.facebook.com/100001983979428/picture"></div>
        <div class="text"><textarea cols="65" rows="2" class="reply-topic" rel="63" style="width: 6px; height: auto; overflow: hidden;"></textarea></div>
        <div class="clear"></div>
    </div>                            	
</div> -->