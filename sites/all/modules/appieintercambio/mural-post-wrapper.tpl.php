<?php 
    $facebook = facebook_client();
    $fuid = $facebook->getUser();
    $graph = "https://graph.facebook.com/" . $fuid;
    $uid = getUid($fuid);
?>
<?php foreach ($content as $c) : ?>
<div>
  <div class="post">
      <div class="txtpost">
        <!-- <h4><?php print $c['data'] ?></h4> -->
        <?php  $name = explode(':', $c['nome'])?>
        <p><span><?php print $name[0] ?></span> - <?php print $c['data'] ?></p>

<!--         <a href="#" class="excluir" rel="<?php print $c['tid'] ?>"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/minhas-viagens/btn-fechar.png" border="0" /></a>    -->   
        <div class="clear"></div>
        <!-- <div class="titulo"> <?php print $c['topicos'] ?> </div> -->
        <div class="titulo"> <?php print appieintercambio_toptips_getCategorias($c['tipo_id']) ?> </div>
        <!-- <a href="<?php print base_path() . 'appieintercambio/toptips' ?>" class="transition"><?php print $c['pergunta'] ?></a> -->
        <?php echo l($c['pergunta'],'appieintercambio/toptips/topico/'.$c['tid'],array('attributes' => array('class' => 'transition'))); ?>
        <div class="tags"><span><?php print $c['tags'] ?></span></div>
      </div>
      <div class="resposta">
        <p><a rel="<?php print $c['tid'] ?>"><?php print $c['respostas'] . " respostas" ?></a></p>
      </div> 

      <div class="container-respostas tid-<?php print $c['tid'] ?>"></div>
      <div class="clear"></div>
      <div class="respostas-2">
        <div class="foto"><img src="<?php print $graph . '/picture' ?>" /></div>
        <div class="text"><textarea cols="65" rows="2" class="reply-topic" rel="<?php print $c['tid']?>"></textarea></div>
        <div class="clear"></div>
      </div>
   
  </div>
  <div class="clear"></div>
  <div class="hr"></div>
</div>
<?php endforeach; ?> 
 
