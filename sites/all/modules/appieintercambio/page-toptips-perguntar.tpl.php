<div class="facebook_share">
	<div class="centraliza">
		<div class="fb-like" data-href="https://www.facebook.com/IEintercambio?ref=ts&amp;fref=ts" data-send="true" data-width="450" data-show-faces="false"></div>
	</div>
</div>
<div class="header">
	<div class="header-1">
		<h1>Ie explorer</h1>
		<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
	</div>
	<div class="menuPrinc">
		<div class="centraliza">
			<ul>
				<!-- <li><a href="http://facebook.ieintercambio.com.br/appieintercambio/inicial" class="transition">Home</a></li> -->
				<li>
					<a href="javascript:void()" class="transition">Cadastrar Viagens</a>
					<dl>
						<dt></dt>
						<dd><a href="https://apps.facebook.com/ieintercambio/euquero" class="transition" target="_top">Quero Viajar!</a></dd>
						<dd style="border-top: 1px solid #333333; border-bottom: 1px solid #333333;"><a href="https://apps.facebook.com/ieintercambio/euvou" class="transition" target="_top">Vou VIAJAR para</a></dd>
						<dd><a href="https://apps.facebook.com/ieintercambio/eufui" class="transition" target="_top">Já VIAJEI para</a></dd>
					</dl>
				</li>
				<li><a href="https://apps.facebook.com/ieintercambio/minhasviagens" target="_top" class="transition" target="_top">Minhas Viagens</a></li>
				<li><a href="https://apps.facebook.com/ieintercambio/encontre" class="transition" target="_top">Outros Viajantes<span></span></a></li>
				<li class="last"><a href="https://apps.facebook.com/ieintercambio/toptips" class="transition ativo" target="_top">Top Tips</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="limite">
<div class="content-top">
		<div class="texto">
			<h3>DICAS E DÚVIDAS</h3>
			<p>Converse com outros participantes<br>do IE Explorer.</p>
		</div>
	</div>
	<div class="breadcrumb">
		<p> Home > Top Tips > <span>Perguntar</span></p>
	</div>
	<div class="clear"></div>
		<div class="conteudo">
			<h4>ESCREVA SUA MENSAGEM:</h4>
			<div class="clear"></div>
		
			<div class="envia-pergunta">
				<form action="<?php echo url('appieintercambio/toptips/perguntar') ?>" method="POST" />
					<div class="blococategorias">
						<label for="">Categoria:</label>
						<select name="tipo_id">
							<option value="1">Estudo</option>
							<option value="2">Trabalho</option>
							<option value="3">Explorar Destinos</option>
							<option value="4">Classificados</option>
							<option value="5">Dicas Gerais</option>
						</select>
					</div>
		
					<div class="campo">
						<label for="">Pergunte ou dê uma dica:</label>
						<textarea name="pergunta" id=""></textarea>
					</div>
					
					<div class="hr"></div>

					<div class="campo">
						<label for="">Deixe referências: <b>(opcional)</b></label>
						<textarea name="conteudo_adicional" id=""></textarea>
					</div>
					
					<div class="hr"></div>

					<div class="campo">
						<label for="">Adicione alguns tags que facilitam a pesquisa:</label>
						<input type="text" class="inputTags" name="tags">
						<input type="button" class="transition" value="Adicionar Tag">
						<span>Utilize vírgulas para separar as tags. ex.: tag01, tag02</span>
					</div>

					<div class="tags">
						<span>tag</span>
					</div>
		
					<input type="submit" class="transition" value="Publicar mensagem">
				</form>	
			</div>
		</div>
	</div>

<div class="footer">
	<div class="barra">
		<div class="footer-1">
			<p>IE Intercâmbio no Exterior - Todos os direitos reservados.</p>
			<a class="transition" href="https://www.kindle.com.br" target="_blank">Kindle</a>
		</div>
	</div>
</div>
  <script type='text/javascript'>
    window.onload=function() {


      altura = screen.height;

      if(altura > 800){
        FB.Canvas.setSize({width:760,height:altura});
    }else{
        FB.Canvas.setSize({width:760,height:document.body.offsetHeight});
      }
      FB.Canvas.scrollTo(0,0);
      
    }
  </script>