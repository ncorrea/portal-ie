

<div class="facebook_share">
	<div class="centraliza">
		<div class="fb-like" data-href="https://www.facebook.com/IEintercambio?ref=ts&amp;fref=ts" data-send="true" data-width="450" data-show-faces="false"></div>
	</div>
</div>
<div class="header">
	<div class="header-1">
		<h1>Ie explorer</h1>
		<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
	</div>
	<div class="menuPrinc">
		<div class="centraliza">
			<ul>
				<!-- <li><a href="http://facebook.ieintercambio.com.br/appieintercambio/inicial" class="transition">Home</a></li> -->
				<li>
					<a href="javascript:void()" class="transition ativo">Cadastrar Viagens</a>
					<dl>
						<dt></dt>
						<dd><a href="https://apps.facebook.com/ieintercambio/euquero" class="transition" target="_top">Quero Viajar!</a></dd>
						<dd style="border-top: 1px solid #333333; border-bottom: 1px solid #333333;"><a href="https://apps.facebook.com/ieintercambio/euvou" target="_top" class="transition">Vou VIAJAR para</a></dd>
						<dd><a href="https://apps.facebook.com/ieintercambio/eufui" class="transition" target="_top">Já VIAJEI para</a></dd>
					</dl>
				</li>
				<li><a href="https://apps.facebook.com/ieintercambio/minhasviagens" target="_top" class="transition" target="_top">Minhas Viagens</a></li>
				<li><a href="https://apps.facebook.com/ieintercambio/encontre" class="transition" target="_top">Outros Viajantes<span></span></a></li>
				<li class="last"><a href="https://apps.facebook.com/ieintercambio/toptips" class="transition" target="_top">Top Tips</a></li>
			</ul>
		</div>
	</div>
</div>
<div id="referencia"></div>
<div class="limite">
	<div class="content-top">
		<div class="texto">
			<h3>Dados enviados<br>com sucesso.</h3>
			<!-- <p>Em breve vamos inserir no site.</p> -->
		</div>
	</div>
	<div class="breadcrumb">
		<p> Home > Cadastrar Viagens > <span>Finalizado</span></p>
	</div>
	<div class="clear"></div>
	<div class="content-conteudo">
		
		<h4><?php echo $tipo; ?></h4>
		<div class="usuario">
				<img src="<?php echo $avatar; ?>" height="49" width="51" />
			<div class="dados">
				<p><?php echo $nome_usuario; ?>
					<span><?php echo $frase; ?></span>
				</p>
			</div>
	
			 <div class="viagens">
			 	<?php echo l('Ver minhas viagens','appieintercambio/minhasviagens',array('attributes' => array('class' => 'transition'))); ?>
			 </div>
	
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		
		<div class="itens">
			<div class="topTips">
				<h5>Top Tips</h5>
				<p>Dicas, dúvidas e ajuda
				<span>Nosso mural de informações úteis para os viajantes IE</span></p>
				<div class="link">
					<?php echo l('Top Tips','appieintercambio/toptips',array('attributes' => array('class' => 'transition'))); ?>
				</div>
			</div>
			
			<div class="busca">
				<h5>Busca</h5>
				<p>Encontre mais viajantes
				<span>Utilize nossa busca de participantes e conecte-se à eles!</span></p>
				<div class="link">				
					<?php echo l('Busca de Viajantes','appieintercambio/encontre',array('attributes' => array('class' => 'transition'))); ?>
				</div>
			</div>
		</div>
	
		<div class="clear"></div>
	
		<div class="outrosUsuarios">
			<div class="header">
				<p>Conheça a galera!</p>
			</div>
			<div class="quemQuer">
				<h5>Quem também QUER</h5>
				<?php foreach($quequerem as $_quequerem){ ?>
				<div>
					<img src="<?php echo $_quequerem['avatar']; ?>" />
					<span><?php echo $_quequerem['nome']; ?></span>
					<p><?php echo $_quequerem['frase']; ?></p>
				</div>
				<?php } ?>
			</div>
			
			<div class="quemVai">
				<h5>Quem VAI</h5>
				<?php foreach($quevao as $_quevao){ ?>
				<div>
					<img src="<?php echo $_quevao['avatar']; ?>" />
					<span><?php echo $_quevao['nome']; ?></span>
					<p><?php echo $_quevao['frase']; ?></p>
				</div>
				<?php } ?>
			</div>

			<div class="quemFoi">
				<h5>Quem já FOI</h5>
				<?php foreach($queforam as $_queforam){ ?>
				<div>
					<img src="<?php echo $_queforam['avatar']; ?>" />
					<span><?php echo $_queforam['nome']; ?></span>
					<p><?php echo $_queforam['frase']; ?></p>
				</div>
				<?php } ?>
			</div>
		</div>
	
		<div class="busca-viajantes">
			<?php echo l('Procurar mais viajantes IE','appieintercambio/encontre',array('attributes' => array('class' => 'transition'))); ?>
		</div>
	</div>
</div>

<div class="footer">
	<div class="barra">
		<div class="footer-1">
			<p>IE Intercâmbio no Exterior - Todos os direitos reservados.</p>
			<a class="transition" href="https://www.kindle.com.br" target="_blank">Kindle</a>
		</div>
	</div>
</div>

  <script type='text/javascript'>
    window.onload=function() {


      altura = screen.height;

      if(altura > 800){
        FB.Canvas.setSize({width:760,height:altura});
    }else{
        FB.Canvas.setSize({width:760,height:document.body.offsetHeight});
      }
      FB.Canvas.scrollTo(0,0);
      
    }
  </script>