<?php


function appieintercambio_toptips(){



	# Traz os últimos tópicos de cada categoria

	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/toptips.css');
	drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/toptips.js', 'file');
	drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/autogrow.js', 'file');
	drupal_add_js('
	jQuery(document).ready(function($){

		var basepath = Drupal.settings["appieintercambio"]["basepath"];

		loading = $(".mostra-posts").html();
		
		$(".lista-botoes ul li").click(function(){
			$(".mostra-posts").html(loading);
		});

		$("html").ajaxStart(function () {
			$("body").css("cursor", "wait");
			$(".mostra-posts").html(loading);
		});

		$("html").ajaxComplete(function () {
			$("body").css("cursor", "auto");
			$(".loadGif").remove();
		});

		$.post(basepath + "appieintercambio/mostraposts", {"tipo" : 1}, function(data){				
				$(".mostra-posts").html(data);			
				$("textarea").autoGrow(); 	
		});

		$(".follow").live("click", function(){
			var tid = $(this).attr("rel");
			var obj = $(this);

			$("html").unbind("ajaxStart ajaxStop");

			$.post(basepath + "appieintercambio/toptips/seguirconversa", {"tid":tid}, function(data){
				obj.hide();

				$("html").ajaxStart(function () {
					$("body").css("cursor", "wait");
					$(".mostra-posts").html(loading);
				});

				$("html").ajaxComplete(function () {
					$("body").css("cursor", "auto");
					$(".loadGif").remove();
				});
			});
			
			$("html").ajaxStart(function () {
				$("body").css("cursor", "wait");
				$(".mostra-posts").html(loading);
			});

			$("html").ajaxComplete(function () {
				$("body").css("cursor", "auto");
				$(".loadGif").remove();
			});

			return false;
		});

			
		$("textarea").live("keypress",function(event){
			
			var tid = $(this).attr("rel");
			var resposta = $(this).val();
			var container = $(".tid-" + tid);
			var textarea = $(this);
			var containertopicos = $(".mostra-posts");
			var resposta_id = $("#resposta_id").val()

			if ( event.which == 13 ) {								
				
				textarea.attr("disabled", true); 

				$.post(basepath + "appieintercambio/toptips/responder", {"tid":tid, "resposta":resposta, "resposta_id": resposta_id}, function(data){
			        container.html(data);				          
			        textarea.val("");
			        textarea.removeAttr("disabled"); 
			    });       			

				event.preventDefault();
  			}   			
		});

		$(".categoria-button").click(function() {
			
			var tipo = $(this).attr("rel");

			$.post(basepath + "appieintercambio/mostraposts", {"tipo" : tipo}, function(data){				
				$(".mostra-posts").html(data);			
				$("textarea").autoGrow(); 	
			});

		});
		
	})','inline');

	return theme("page-toptips", 
		array(
			'categorias' => appieintercambio_toptips_getTopicos()
		)
	);	
}

function appieintercambio_toptips_interna(){

	# Trazer os últimos tópicos pela busca

	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/toptips-int.css');
	drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/toptips.js', 'file');	

	// ID da Categoria
	$tipo_id = arg(3); 

	return theme("page-toptips-interna", 
		array(
			'categorias' => appieintercambio_toptips_getTopicos($tipo_id)
		)
	);	

}

function appieintercambio_toptips_busca(){

	# Trazer os tópicos pela busca

	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/toptips-busca.css');
	drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/toptips.js', 'file');		

	$tipo_id = $_GET['categoria'];
	$busca = $_GET['busca'];

	if(strlen($tipo_id) == 0){
		$tipo_id = null;
	}

	return theme("page-toptips-interna", 
		array(
			'categorias' => appieintercambio_toptips_getTopicos($tipo_id,$busca)
		)
	);	

}

function appieintercambio_toptips_topico(){

	# Tópico

	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/toptips-interna.css');
	drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/toptips.js', 'file');	
	drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/sprintf-0.7-beta1.js', 'file');	



	// ID do Tópico
	$tid = arg(3);
	upVisualizacoes($tid);

	$result = db_query('SELECT * FROM appieintercambio_topico WHERE tid = :tid',array('tid' => $tid))->fetchObject();

	// Pega o nome do usuário
	$usuario = user_load($result->uid);
	$nome = explode(':',$usuario->name);

	// Usuário do Facebook
	$user_face = db_query('SELECT * FROM appieintercambio_usuariosfacebook WHERE user_id = :uid',array('uid' => $result->uid))->fetchObject();

	// Pega a data
	$data = date('d/m/Y',$result->created);

	// Conta o número de respostas
	$num_respostas = db_query('SELECT * FROM appieintercambio_resposta WHERE tid = :tid',array('tid' => $result->tid))->rowCount();	

	// Conta o número de seguidores
	$seguidores = db_query('SELECT * FROM appieintercambio_seguidores WHERE tid = :tid',array('tid' => $result->tid));

	// Carrega o Usuário
	$facebook = facebook_client();
	$fuid = $facebook->getUser();
	
	$uid = getUid($fuid);

	$temp_usuario = $facebook->api('/'.$fuid);
	$usuario_facebook_nome = $temp_usuario['name'];

	// Verifica se o usuário já é um "seguidor" da conversa
	$bt_seguir_conversa = 1;	
	foreach($seguidores as $seguidor){
		if($seguidor->uid == $uid)
		{
			$bt_seguir_conversa = 0;
		}
	}


	drupal_add_js("
	jQuery(document).ready(function($){

		$('html, body').animate({
			scrollTop:0
		}, 300);

		var basepath = Drupal.settings['appieintercambio']['basepath'];

		$('a.seguir').click(function(){
			var obj = $(this);
			var tid = $(this).attr('rel');
			$.post(basepath + 'appieintercambio/toptips/seguirconversa',{'tid':tid}, function(data){
				obj.hide();
				var respostas = parseInt($('.infos dl dd').first().next().find('span').html()) + 1;
				$('.infos dl dd').first().next().find('span').html(respostas);

			});
			return false;
		});

		$('#botao-resposta').click(function() {
			var tid = $('#tid').val();
			var textarea = $('#resposta');
			var resposta = textarea.val();
			var resposta_id = $('#resposta_id').val();
			var facebook_id = $('#facebook_id').val();
			var usuario_nome = $('#usuario_nome').val();
			
			d = new Date();
		    var curr_date = d.getDate();
		    var curr_month = (d.getMonth()+1).toString();
		    var curr_year = d.getFullYear().toString();

		    var pad = '00'
		    var hoje = pad.substring(0, pad.length - curr_date.length) + curr_date + '/' + pad.substring(0, pad.length - curr_month.length) + curr_month + '/' + curr_year;

		    if (resposta == ''){
		    	alert('É necessário escrever algo para responder.')
		    	return false;
		    }

			$.post(basepath + 'appieintercambio/toptips/responder', {'tid':tid, 'resposta':resposta, 'resposta_id': resposta_id, 'user_fuid' : ".$user_face->facebook_id."}, function(data){

				html_code = vsprintf('<div class=resposta1><div class=user><img src=http://graph.facebook.com/%s/picture/><p><strong>%s</strong><br>%s</p></div><div class=texto><p>%s</p></div></div>', [facebook_id, usuario_nome, hoje, resposta,]);
        
				textarea.val('');
				textarea.removeAttr('disabled'); 
				$('.respostas').prepend(html_code);
				$('.form_responder').hide();

				num_respostas = (parseInt($('#num_respostas').html())+1).toString();
				$('#num_respostas').html(pad.substring(0, pad.length - num_respostas.length) + num_respostas);
			});
		});

		$('.botao-replica').click(function() {
			var form = $(this).parents('form:first');
			var tid = $('#form2_tid', form).val();
			var textarea = $('#replica', form);
			var resposta = textarea.val();
			var resposta_id = $('#form2_resposta_id', form).val();
			var facebook_id = $('#form2_facebook_id', form).val();
			var usuario_nome = $('#form2_usuario_nome', form).val();
			
			d = new Date();
		    var curr_date = d.getDate();
		    var curr_month = (d.getMonth()+1).toString();
		    var curr_year = d.getFullYear().toString();

		    var pad = '00'
		    var hoje = pad.substring(0, pad.length - curr_date.length) + curr_date + '/' + pad.substring(0, pad.length - curr_month.length) + curr_month + '/' + curr_year;

		    if (resposta == ''){
		    	alert('É necessário escrever algo para responder.')
		    	return false;
		    }

			$.post(basepath + 'appieintercambio/toptips/responder', {'tid':tid, 'resposta':resposta, 'resposta_id': resposta_id, 'user_fuid' : ".$user_face->facebook_id."}, function(data){

				alert(data);

				html = vsprintf('<div class=resposta2><a class=fechar href=javascript:void(0) tipo=resposta identificador=%s></a><div class=user><img src=http://graph.facebook.com/%s/picture/><p><strong>%s</strong><br>%s</p></div><div class=texto><p>%s</p></div></div>', [facebook_id, usuario_nome, hoje, resposta, resposta_id]);

				//container.html(data);				          
				textarea.val('');
				textarea.removeAttr('disabled'); 

				// encontrando a div mais proxima de resposta para a replica
				form.parent().parent().parent().append(html);
				// escondendo o form
				form.parent().hide();
				// escondendo o botão resposta
				form.parent().parent().find('#botao-areplicar').hide();

				var respostas = parseInt($('.infos dl dd').first().next().find('span').html()) + 1;
				$('.infos dl dd').first().next().find('span').html(respostas);
			});
		});

	});","inline");	



	$topico['titulo'] = appieintercambio_toptips_getCategorias($result->tipo_id);
	$topico['tipo_id'] = $result->tipo_id;
	$topico['tid'] = $result->tid;
	$topico['pergunta'] = $result->pergunta;
	$topico['conteudo_adicional'] = $result->conteudo_adicional;
	$topico['usuario'] = $nome[0];
	$topico['created'] = $data;
	$topico['tags'] = $result->tags;
	$topico['respostas'] = $num_respostas;
	$topico['seguidores'] = $seguidores->rowCount();
	$topico['bt_seguir_conversa'] = $bt_seguir_conversa;
	$topico['visualizacoes'] = $result->visualizacoes;
	$topico['facebook_id'] = $user_face->facebook_id;

	$topico['usuario_facebook_id'] = $fuid;	
	$topico['usuario_facebook_nome'] = $usuario_facebook_nome;	
	$topico['friends'] = getFriends($user_face->facebook_id);



	$_respostas = db_query('
		SELECT * FROM appieintercambio_resposta as Resposta
		LEFT JOIN appieintercambio_usuariosfacebook as UsuarioFacebook ON Resposta.uid = UsuarioFacebook.user_id
		WHERE tid = :tid and resposta_id = 0',
			array(
				'tid' => $tid
				)
		);

	foreach($_respostas as $_resposta)
	{

		// Seta os dados da resposta
		$respostas[$_resposta->rid]['rid'] = $_resposta->rid;
		$respostas[$_resposta->rid]['resposta'] = $_resposta->resposta;
		$respostas[$_resposta->rid]['facebook_id'] = $_resposta->facebook_id;
		$respostas[$_resposta->rid]['data'] = date('d/m/Y',$_resposta->created);

		// Pega o nome do usuário da resposta
		$usuario_resposta = user_load($_resposta->uid);		
		$usuario_resposta_name = explode(':',$usuario_resposta->name);
		$respostas[$_resposta->rid]['usuario'] = $usuario_resposta_name[0];


		// Pega a replica
		$replica = db_query('
			SELECT * FROM appieintercambio_resposta as Resposta
			INNER JOIN appieintercambio_usuariosfacebook as UsuarioFacebook ON Resposta.uid = UsuarioFacebook.user_id
			WHERE resposta_id = :resposta_id'
				,array(
					'resposta_id' => $_resposta->rid
				))->fetchObject();

		// Pega o nome do usuário da resposta
		$usuario_replica = user_load($replica->uid);		
		$usuario_replica_name = explode(':',$usuario_replica->name);

		// Seta os dados da replica
		$respostas[$_resposta->rid]['usuario_replica'] = $usuario_replica_name[0];	
		$respostas[$_resposta->rid]['replica_data'] = date('d/m/Y',$_resposta->created);
		$respostas[$_resposta->rid]['facebook_id_replica'] = $replica->facebook_id;
		$respostas[$_resposta->rid]['replica'] = $replica->resposta;
		$respostas[$_resposta->rid]['replica_rid'] = $replica->rid;

	}

	return theme("page-toptips-topico", 
		array(
			'topico' => $topico,
			'topico_facebook_id' => $user_face->facebook_id,
			'respostas' => $respostas
		)
	);	

}

function appieintercambio_toptips_perguntar(){

	if(!empty($_POST)){

		$facebook = facebook_client();
		$uid = $facebook->getUser();

		// Usuário do Facebook
		$user_face = db_query('SELECT * FROM appieintercambio_usuariosfacebook WHERE facebook_id = :facebook_id',array('facebook_id' => $uid))->fetchObject();	

		$tid = db_insert('appieintercambio_topico') // Table name no longer needs {}
		->fields(array(
		  'uid' => $user_face->user_id,
		  'pergunta' => $_POST['pergunta'],
		  'conteudo_adicional' => $_POST['conteudo_adicional'],
		  'tags' => $_POST['tags'],
		  'tipo_id' => $_POST['tipo_id'],
		  'created' => time()
		))
		->execute();

		$usuario = user_load($user_face->user_id);
		$nome = explode(':',$usuario->name);			

        $post = $nome[0].' acaba de criar um novo tópico no aplicativo IE Explorers. Cadastre-se e veja se é uma pergunta ou uma dica de viagem.';

        $post_on_wall = $facebook->api('/'.$uid.'/feed', 'POST',
                                          array(
                                            'link' => 'https://apps.facebook.com/ieintercambio',
                                            'message'=> $post,
                                      'description' => 'Um aplicativo para você compartilhar e conversar sobre experiências, ideias e dicas com outros viajantes IE. Faça novos amigos e explore um mundo de intercâmbios.'
                                       ));		

		drupal_goto(url('appieintercambio/toptips/topico/'.$tid));

	}else{

		drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
		drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/toptips-pergunta.css');
		drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/toptips.js', 'file');		

		return theme("page-toptips-perguntar", 
			array()
		);		
	}


}

function appieintercambio_toptips_seguirconversa(){
	$tid = $_POST['tid'];

	$facebook = facebook_client();
	$fuid = $facebook->getUser();

	$uid = getUid($fuid);

	db_query('INSERT INTO appieintercambio_seguidores(uid,tid) VALUES(:uid,:tid)',
		array(
				'uid' => $uid,
				'tid' => $tid
			)
	);		

	print "ok";
}



function upVisualizacoes($tid){
	db_update('appieintercambio_topico')
    ->expression('visualizacoes', 'visualizacoes + :up', array(':up' => 1))
    ->condition('tid', $tid)
    ->execute();
}	

function getFriends($fuid){

  $facebook = facebook_client();

  try {


	$friends = $facebook->api('/'. $fuid .'/friends');    

   } catch (FacebookApiException $e) {

  		error_log($e);

        $user = null;
        $friends = array(array('vazio'=>'vazio'));

   }


  $friendsList = array();
  
  foreach ($friends as $friend){
      foreach ($friend as $f){
        $friendsList[] = $f['id'];
      }
  }


  $query = db_select('appieintercambio_usuariosfacebook','u');
  $query->fields('u',array('facebook_id'));
  $query->condition('u.facebook_id',$friendsList,'IN');
  $result = $query->execute()->rowCount();

  return $result;
}


