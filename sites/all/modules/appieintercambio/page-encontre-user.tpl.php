<div class="facebook_share">
	<div class="centraliza">
		<div class="fb-like" data-href="http://facebook.ieintercambio.com.br/appieintercambio" data-send="true" data-width="450" data-show-faces="false"></div>
	</div>
</div>
<div class="header">
	<div class="header-1">
		<h1>Ie explorer</h1>
		<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
	</div>
	<div class="menuPrinc">
		<div class="centraliza">
			<ul>
				<!-- <li><a href="http://facebook.ieintercambio.com.br/appieintercambio/inicial" class="transition">Home</a></li> -->
				<li>
					<a href="javascript:void()" class="transition">Cadastrar Viagens</a>
					<dl>
						<dt></dt>
						<dd><a href="https://apps.facebook.com/ieintercambio/euquero" class="transition" target="_top">Quero Viajar!</a></dd>
						<dd style="border-top: 1px solid #333333; border-bottom: 1px solid #333333;" target="_top"><a href="https://apps.facebook.com/ieintercambio/euvou" class="transition">Vou VIAJAR para</a></dd>
						<dd><a href="https://apps.facebook.com/ieintercambio/eufui" class="transition" target="_top">Já VIAJEI para</a></dd>
					</dl>
				</li>
				<li><a href="https://apps.facebook.com/ieintercambio/minhasviagens" target="_top" class="transition" target="_top">Minhas Viagens</a></li>
				<li><a href="https://apps.facebook.com/ieintercambio/encontre" class="transition ativo" target="_top">Outros Viajantes<span></span></a></li>
				<li class="last"><a href="https://apps.facebook.com/ieintercambio/toptips" class="transition" target="_top">Top Tips</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="limite">
	<div class="content">
		<input type="hidden" id="uid" value = <?php print $uid ?> />
		<div class="conteudo">
				<div class="content-top">
					<div class="texto">
						<h3>MAIS AMIGOS NA VIAGEM!</h3>
						<p>Conheça o perfil de outras pessoas e seus programas de intercâmbio.</p>
					</div>
				</div>
				<div class="breadcrumb">
					<p> Home > <span>Outros Viajantes</span></p>
				</div>
				<div class="clear"></div>

				<h4>ADICIONE E TROQUE IDEIAS</h4>
				
			<div class="usuario dadosUsuario">
				<?php print $user ?>
			</div>
				
			<div class="clear"></div>
				
			<div class="foi box-jafoi">
            
				<h4>JÁ FOI PARA</h4>
				
				<div class="paises">
					<ul>
						<?php 			
							foreach ($fui['paises'] as $pais){ print "<li class='pais_id".$pais['pid']."'><img src='" . $pais['flag'] . "' /></li>"; }
						?>
					</ul>
                    <div class="clear"></div>
				</div>
				
				<div class="empregadores">
					<h5> <div class="box"></div>Empregadores</h5>
					<ul>
					<?php	
//						foreach($fui['empregadores'] as $empregadores){ print "<li class='pais_id".$empregadores['pid']."'><img src='" .$empregadores['flag'] . "' /></li>"; }
						foreach($fui['empregadores'] as $empregadores){ 
							if ($empregadores['interesse'] != 3 ) {
								print "<li class='pais_id".$empregadores['pid']."'>" .$empregadores['titulo'] . "</li>"; 
							}
						}
					?>
					</ul>

                    <div class="clear"></div>
				
            </div>
				
				<div class="instituicao">
					<h5><div class="box"></div> Instituições de Ensino</h5>
					<ul>
					<?php	
						//foreach($fui['escolas'] as $escolas){ print "<li class='pais_id".$escolas['pid']."'><img src='" .$escolas['flag'] . "' /></li>"; }
						foreach($fui['escolas'] as $escolas){ 
							if ($escolas['interesse'] != 3 ) {
								print "<li class='pais_id".$escolas['pid']."'>" .$escolas['titulo'] . "</li>"; 
							}
						}
					?>
					</ul>
                    <div class="clear"></div>
				</div>
                
                <div class="bottom"></div>
			
			</div>
				
			<div class="quero box-querir">
            	
				<h4> QUER IR PARA </h4>
				
				<div class="paises">
					<ul>
						<?php 
							foreach ($quero['paises'] as $pais){ print "<li class='pais_id".$pais['pid']."'><img src='" . $pais['flag'] . "' /></li>"; }
						?>
					</ul>
                    <div class="clear"></div>
				</div>
				
				<div class="empregadores">
					<h5><div class="box"></div>Empregadores</h5>
					<ul>
						<?php 
						//foreach ($quero['empregadores'] as $empregadores){ print "<li class='pais_id".$empregadores['pid']."'><img src='" .$empregadores['flag'] . "' /></li>"; }
						foreach ($quero['empregadores'] as $empregadores){ 
							if ($empregadores['interesse'] != 3 ) {
								print "<li class='pais_id".$empregadores['pid']."'>" .$empregadores['titulo'] . "</li>"; 
							}
						}
						?>
					</ul>
                    <div class="clear"></div>
				</div>
				
				<div class="instituicao">
					<h5><div class="box"></div>Instituições de Ensino</h5>
					<ul>
						<?php 
//						foreach ($quero['escolas'] as $escolas){ print "<li class='pais_id".$escolas['pid']."'><img src='" .$escolas['flag'] . "' /></li>"; }
						foreach ($quero['escolas'] as $escolas){ 
							if ($escolas['interesse'] != 3 ) {
								print "<li class='pais_id".$escolas['pid']."'>" .$escolas['titulo'] . "</li>"; 
							}
						}
						?>
					</ul>
                    <div class="clear"></div>
				</div>
                
                <div class="bottom"></div>
				
			</div>
				
			<div class="vai box-estaindo">
            
				<h4> ESTÁ INDO PARA </h4>
				
				<div class="paises">	
					<ul>	
						<?php 
							foreach ($vou['paises'] as $pais){ print "<li class='pais_id".$pais['pid']."'><img src='" . $pais['flag'] . "' /></li>"; }
						?>
					</ul>
                    <div class="clear"></div>
				</div>
				
				<div class="empregadores">
					<h5><div class="box"></div>Empregadores</h5>
					<ul>
						<?php 
							//foreach ($vou['empregadores'] as $empregadores){ print "<li class='pais_id".$empregadores['pid']."'><img src='" .$empregadores['flag'] . "' /></li>"; }
							foreach ($vou['empregadores'] as $empregadores){ 
								if ($empregadores['interesse'] != 3 ) {
									print "<li class='pais_id".$empregadores['pid']."'>" .$empregadores['titulo'] . "</li>"; 
								}
							}
						?>
					</ul>
                    <div class="clear"></div>
				</div>
				
				<div class="instituicao">
					<h5><div class="box"></div>Instituições de Ensino</h5>
					<ul>
						<?php 
							//foreach ($vou['escolas'] as $escolas){ print "<li class='pais_id".$escolas['pid']."'><img src='" .$escolas['flag'] . "' /></li>"; }
							foreach ($vou['escolas'] as $escolas){ 
								if ($escolas['interesse'] != 3 ) {
									print "<li class='pais_id".$escolas['pid']."'>" .$escolas['titulo'] . "</li>"; 
								}
							}
						?>
					</ul>
                    <div class="clear"></div>
				</div>
                
                <div class="bottom"></div>
				
			</div>
		</div>
	</div>
</div>

<div class="clear"></div>

<div class="footer">
	<div class="barra">
		<div class="footer-1">
			<p>IE Intercâmbio no Exterior - Todos os direitos reservados.</p>
			<a class="transition" href="https://www.kindle.com.br" target="_blank">Kindle</a>
		</div>
	</div>
</div>

<div class="bg-preto"></div>
<div class="bordaPublicar"></div>
<div class="publicarMural">
    	<h1>Publicar mensagem no mural</h1>
        <textarea id="mensagem"></textarea>
        <div class="bts">
            <a href="#" class="enviar" id="postmural">Enviar</a>
            <a href="#" class="fechar">Fechar</a>
        </div>
</div>

  <script type='text/javascript'>
    window.onload=function() {


      altura = screen.height;

      if(altura > 800){
        FB.Canvas.setSize({width:760,height:altura});
    }else{
        FB.Canvas.setSize({width:760,height:document.body.offsetHeight});
      }
      FB.Canvas.scrollTo(0,0);
      
    }
  </script>