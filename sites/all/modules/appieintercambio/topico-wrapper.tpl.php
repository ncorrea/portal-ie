<?php	
	$facebook = facebook_client();
	$fuid = $facebook->getUser();
	$graph = "https://graph.facebook.com/" . $fuid;
	// $uid = getUid($fuid);

	$uid = $user_uid;

   $a = 0
?>

<?php foreach($categorias as $categoria){ ?>

<?php $a++;?>
	<div id="categoria<?php echo $a; ?>" class="post-categoria ativo">
<!-- <div class="topicHeader">
		<?php echo $categoria['titulo']; ?>
		<h5 class="title"><?php echo $categoria['titulo']; ?></h5>
	</div> -->

			<?php //echo l('Ver todas as '.count($categoria['topicos']).' conversas','appieintercambio/toptips/interna/'.$categoria['tipo_id'],array('attributes' => array('class' => 'transition'))); ?>
		<div class="mural">
			<div class="clear"></div>
			<?php foreach($categoria['topicos'] as $topico){  ?>
			<div class="post" identificador="<?php print $topico['tid'] ?>">
				<div class="txtpost">

					<?php if($topico['uid'] == $uid){ ?>
					<a class="fechar" href="javascript:void(0)" tipo="pergunta" identificador="<?php print $topico['tid'] ?>"></a>
					<?php } ?>

					<p><span><a href="encontre/user/<?php print $topico['uid'] ?>" style="font-size: 1em"><?php echo $topico['usuario']; ?></a></span> | <?php echo $topico['created']; ?></p>
					<p class="categoria"><?php echo $categoria['titulo']; ?></p>
					<a href="https://apps.facebook.com/ieintercambio/toptips/topico/<?php echo $topico['tid']; ?>" target="_top" class="transition"><?php echo $topico['pergunta']; ?></a>

					<?php 
						if (!(is_following($topico['tid'],$uid))){
					?>	 
					<?php } ?>
					 
					
					<div class="clear"></div>							
					<div class="respostas">				

						<?php if($topico['is_seguidor'] == 0){ ?>
                    	<a class="follow" rel="<?php print $topico['tid'] ?>" href="#" style="font-size: 0.8em">Seguir esta conversa</a>
						<?php }else{ ?>
						<a class="follow deixar_de_seguir" rel="<?php print $topico['tid'] ?>" href="#" style="font-size: 0.8em">Deixar de seguir</a>	
						<?php } ?>


						<div class="container-respostas tid-<?php print $topico['tid'] ?>">
							<?php 
								
								$respostas = getRespostas($topico['tid']);
								
								foreach ($respostas as $resposta) {
									print theme('resposta-topico-wrapper',array('content'=>$resposta,'user_uid' => $user_uid));
								}	

							?>
						</div>
                        <div class="respostas-2">                                
                        	<div class="foto"><img src="<?php print $graph . '/picture' ?>" /></div>
                            <div class="text"><textarea cols="65" rows="2" class="reply-topic" rel="<?php print $topico['tid']?>"></textarea></div>
                            <div class="clear"></div>
                        </div>                            	
					</div>
					
				</div>						
			</div>
			<div class="clear"></div>
			<?php } ?>
		</div>
	</div>

<?php } ?>