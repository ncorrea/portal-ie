<div class="facebook_share">
	<div class="centraliza">
		<div class="fb-like" data-href="https://www.facebook.com/IEintercambio?ref=ts&amp;fref=ts" data-send="true" data-width="450" data-show-faces="false"></div>
	</div>
</div>
<div class="header">
	<div class="header-1">
		<h1>Ie explorer</h1>
		<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
	</div>
	<div class="menuPrinc">
		<div class="centraliza">
			<ul>
				<!-- <li><a href="http://facebook.ieintercambio.com.br/appieintercambio/inicial" class="transition">Home</a></li> -->
				<li>
					<a href="javascript:void()" class="transition">Cadastrar Viagens</a>
					<dl>
						<dt></dt>
						<dd><a href="https://apps.facebook.com/ieintercambio/euquero" class="transition" target="_top">Quero Viajar!</a></dd>
						<dd style="border-top: 1px solid #333333; border-bottom: 1px solid #333333;"><a href="https://apps.facebook.com/ieintercambio/euvou" class="transition" target="_top">Vou VIAJAR para</a></dd>
						<dd><a href="https://apps.facebook.com/ieintercambio/eufui" class="transition" target="_top">Já VIAJEI para</a></dd>
					</dl>
				</li>
				<li><a href="https://apps.facebook.com/ieintercambio/minhasviagens" target="_top" class="transition" target="_top">Minhas Viagens</a></li>
				<li><a href="https://apps.facebook.com/ieintercambio/encontre" class="transition ativo" target="_top">Outros Viajantes<span></span></a></li>
				<li class="last"><a href="https://apps.facebook.com/ieintercambio/toptips" class="transition" target="_top">Top Tips</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="content">
	<div class="limite">
			<div class="conteudo">
				<div class="content-top">
					<div class="texto">
						<h3>MAIS AMIGOS NA VIAGEM!</h3>
						<p>Conheça o perfil de outras pessoas e seus programas de intercâmbio.</p>
					</div>
				</div>
				<div class="breadcrumb">
					<p> Home > <span>Encontrar Viajantes</span></p>
				</div>
				<div class="clear"></div>

				<h4>Encontre outros viajantes</h4>

				<div class="content-abas">
					<!-- inicio do form correto -->

					<div class="botoes">
						<p>Selecione um grupo:</p>
						<div class="radio radioquero">
							<input id="euquero" type="radio" name="tipobusca" value="1" checked/>
							<label for="euquero">
								<div class="btli quero transition">
									<p style="">Querem Viajar</p>
								</div>
								<div class="seta"></div>
							</label>
						</div>
						<div class="radio radiovou">
							<input id="euvou" type="radio" name="tipobusca" value="2" />
							<label for="euvou">
								<div class="btli vou transition">
									<p style="">Vão Viajar</p>
								</div>
								<div class="seta"></div>
							</label>
						</div>
						<div class="radio radiofui">
							<input id="eufui" type="radio" name="tipobusca" value="3" />
							<label for="eufui">
								<div class="btli fui transition">
									<p style="">Foram viajar</p>
								</div>
								<div class="seta"></div>
							</label>
						</div>
					</div>
					<div class="dadosbusca">
						<div id="box">
							<?php 	
								print drupal_render($form_busca); 
							?>
						<div>
					</div>
				</div> <!--fim abas-->
				<div class="clear"></div>

				<div class="content-resultados" style="display: none;">
					
					<div class="boxheader">	
						<?php
							print drupal_render($form_filtra_busca);
						?>
					</div>

					<div id="resultado_pesquisa_ajax">
						<!-- RESULTADO DO AJAX -->
						
					</div>

				</div></div></div>
		</div>
	</div>
</div>

<div class="footer">
	<div class="barra">
		<div class="footer-1">
			<p>IE Intercâmbio no Exterior - Todos os direitos reservados.</p>
			<a class="transition" href="https://www.kindle.com.br" target="_blank">Kindle</a>
		</div>
	</div>
</div>
