
<div class="facebook_share">
	<div class="centraliza">
		<div class="fb-like" data-href="https://www.facebook.com/IEintercambio?ref=ts&amp;fref=ts" data-send="true" data-width="450" data-show-faces="false"></div>
	</div>
</div>
<div class="header">
	<div class="header-1">
		<h1>Ie explorer</h1>
		<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
	</div>
	<div class="menuPrinc">
		<div class="centraliza">
			<ul>
				<!-- <li><a href="http://facebook.ieintercambio.com.br/appieintercambio/inicial" class="transition">Home</a></li> -->
				<li>
					<a href="javascript:void()" class="transition ativo">Cadastrar Viagens</a>
					<dl>
						<dt></dt>
						<dd><a href="https://apps.facebook.com/ieintercambio/euquero" class="transition" target="_top">Quero Viajar!</a></dd>
						<dd style="border-top: 1px solid #333333; border-bottom: 1px solid #333333;"><a href="https://apps.facebook.com/ieintercambio/euvou" target="_top" class="transition">Vou VIAJAR para</a></dd>
						<dd><a href="https://apps.facebook.com/ieintercambio/eufui" class="transition" target="_top">Já VIAJEI para</a></dd>
					</dl>
				</li>
				<li><a href="https://apps.facebook.com/ieintercambio/minhasviagens" target="_top" class="transition" target="_top">Minhas Viagens</a></li>
				<li><a href="https://apps.facebook.com/ieintercambio/encontre" class="transition" target="_top">Outros Viajantes<span></span></a></li>
				<li class="last"><a href="https://apps.facebook.com/ieintercambio/toptips" class="transition" target="_top">Top Tips</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="limite">
	<div class="content-top">
		<div class="texto">
			<span>Não Vejo a Hora.</span>
			<h3>Vou conhecer gente nova.</h3>
			<p>Cadastre sua viagem e veja quem está indo para a mesma cidade.</p>
		</div>
	</div>
	<div class="breadCrumb">
		<p> Home > Cadastrar Viagens > <span>Vou VIAJAR para</span></p>
	</div>
	<div class="clear"></div>
	<div class="content-conteudo">
		<h4>Não Vejo a Hora</h4>
		<form class="form-eu-vou" action="<?php echo url('appieintercambio/cadastrar'); ?>" method="POST">
			<div class="objetivo">Seu objetivo é:</div>
			<div class="eu-quero ajax-tipo-motivo">
				<div class="opt est">
					<input type="radio" name="interesse_id" value="2" id="estudar">
					<label for="estudar">
						<div class="bgradio">
							<span class="estudar transition"><p>Estudar<br> no Exterior</p></span>
							<div class="setaAtiva"></div>
						</div>
					</label>
				</div>

				<div class="opt est-trab">
					<input type="radio" name="interesse_id" value="3" id="explorar">
					<label for="explorar">
						<div class="bgradio">
							<span class="est-trab transition"><p>Explorar<br> Destinos</p></span>
							<div class="setaAtiva"></div>
						</div>
					</label>
				</div>

				<div class="opt trab">
					<input type="radio" name="interesse_id" value="1" id="trabalhar">
					<label for="trabalhar">
						<div class="bgradio">
							<span class="trabalhar transition"><p>Trabalhar <br> no exterior</p></span>
							<div class="setaAtiva"></div>
						</div>
					</label>
				</div>
	
			</div>
			<div class="clear"></div>
			<div class="mapa-paises">
				<div class="localViagem">
					<h5 id="aonde">Onde?</h5>
					<div class="btTipo">
						<div class="map">Mapa</div>
						<div class="list">Lista de Países</div>
					</div>
					<div class="leftPaises">
						<div class="icone"></div>
						<div class="seta"></div>
					</div>

					<div class="mapa" style="float: left;">
					
					</div>
						
					<div id="lista-paises" class="lista">
					
					</div>

					<div class="clear"></div>


					<div class="quando">
					<h5 class="quando">Quando?</h5>
					<div class="leftQuando">
						<div class="icone"></div>
					</div>
						<div class="data ida">
							<p>Ida:</p>
							<!-- <label class="ida" for="">dd/mm/aaaa</label> -->
							<input name="data_ida" type="text" class="dataIda" placeholder="dd/mm/aaaa">
						</div>
		
						<div class="data volta">
							<p>Volta:</p>
							<!-- <label for="">dd/mm/aaaa</label> -->
							<input name="data_volta" type="text" class="dataVolta" placeholder="dd/mm/aaaa">
						</div>				
					</div>

				<div class="clear"></div>

				<div class="boxselect">
					<div class="select">
						<select name="cidade_id" class="cidade" id="cidade">
							<option value="0">Cidade</option>
						</select>
	
						<select name="iid" class="instituicao" id="instituicao" style="float: left !important;">
							<option value="0">Instituição/Escola</option>
						</select>
                        
                        <p class="clique-aqui">Não encontrou? <a href="#">Clique aqui</a></p>

						<div id="buscar" class="ui-widget" style="display: none">
							<label for="searchBox">Cidade:</label>
					        <input id="searchBox" />
					        <div id="searchResult" class="ui-widget"></div><!-- style="margin-top: 1em;" -->
					   	</div> 

					   	<input type="hidden" id="latitude_input" name="latitude" value="NULL" />
       					<input type="hidden" id="longitude_input" name="longitude" value="NULL" />
       					<input type="hidden" id="pais_input" name="pais" value="0" />
					   
					</div>
				</div>
				</div>
				
				<div class="clear"></div>
	
				<div class="submit">
					<input type="hidden" name="user_fuid" value="<?php echo $user_fuid; ?>" />
					<input type="hidden" name="tipo_id" value="2" />
					<input class="transition" type="submit" Value="Cadastrar">
				</div>
			</div>
		</form>
	</div>
</div>
<div class="footer">
	<div class="footer-1">
		<p>Ie Intercâmbio no Exterior - Todos os direitos reservados.</p>
		<a class="transition" href="http://www.kindle.com.br" target="_blank">Kindle</a>
	</div>
</div>
<div class="branco"></div>

<div class="bg-preto"></div>
<div class="add-instituicao">
	<div class="alinha">
		<div class="cpoInstituicao">	
			<label class="nome">Nome da Instituição:</label> 
		    <input name="edit-nome" id="edit-nome" type="text" value="" class="iNome" />
	    </div>

	    <div class="cpoPais">	
		    <label class="pais">País:</label> 
		    <input name="edit-cidade" id="edit-cidade" type="text" value="" class="iCidade" />
	    </div>

	    <div class="cpoCidade">	
		    <label class="cidade">Cidade:</label> 
		    <input name="edit-pais" id="edit-pais" type="text" value="" class="iPais" />
	    </div>

	    <div class="clear"></div>
	    
	    <div class="pontilhado"></div>
	    <div class="bt-fechar"></div>
	    
	    <div class="bgradio">
	    	<span><a href="#" class="envia_solicitacao">Enviar</a></span>
	    </div>
	</div>
</div>

  <script type='text/javascript'>
  /*
    window.onload=function() {


      altura = screen.height;

      if(altura > 800){
        FB.Canvas.setSize({width:760,height:altura});
    }else{
        FB.Canvas.setSize({width:760,height:document.body.offsetHeight});
      }
      FB.Canvas.scrollTo(0,0);
      
    }
 */	
  </script>