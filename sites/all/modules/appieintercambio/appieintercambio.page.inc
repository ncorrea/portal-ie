<?php
# Página "Bem-Vindo"
function appieintercambio_inicial(){  

  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/home.css');
  drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/home.js', 'file');

  $facebook = facebook_client();
  $uid = $facebook->getUser();

  $url_retorno = 'https://apps.facebook.com/ieintercambio/';
  $app_id = '105385746283411';

  $html ='';

  if( $uid == 0 ) {

    $link_eu_quero .= 'https://www.facebook.com/dialog/permissions.request?app_id='.$app_id.'&display=page&next='.urlencode( $url_retorno .'euquero' ).'&response_type=code&fbconnect=1&perms=publish_stream,read_friendlists"';

    $link_eu_vou .= 'https://www.facebook.com/dialog/permissions.request?app_id='.$app_id.'&display=page&next='.urlencode( $url_retorno .'euvou' ).'&response_type=code&fbconnect=1&perms=publish_stream,read_friendlists';

    $link_eu_fui .= 'https://www.facebook.com/dialog/permissions.request?app_id='.$app_id.'&display=page&next='.urlencode( $url_retorno .'eufui' ).'&response_type=code&fbconnect=1&perms=publish_stream,read_friendlists';        
 
  } else { 

    $permissions = $facebook->api( $uid . '/permissions' );      

    if ($permissions['data'][0]['publish_stream'] == 1 ) {

      // Verifica se o usuário já está cadastrado
      $result = db_query('SELECT * FROM appieintercambio_usuariosfacebook WHERE facebook_id = :facebook_id',array('facebook_id' => $uid))->fetchAll();

      $usuario_exist = FALSE;

      foreach ( $result as $_usuario )    {
        $usuario_exist = TRUE ;
      }      

      if(!$usuario_exist){

        global $user;

        // Cadastra um usuário anônimo no drupal
        $usuario = $facebook->api('/'.$uid);
        $newuser = drupal_anonymous_user();
        $newuser->name = $usuario[name].' :'.date('His');
        $newuser = user_save($newuser);

        // Loga o usuário no drupal
        $form_state = array();
        $form_state['uid'] = $newuser->uid;      
        user_login_submit(array(), $form_state);

        $user = user_load($newuser->uid);   

        // Cadastra o usuário na tabela appieintercambio_usuariosfacebook
        db_query('INSERT INTO appieintercambio_usuariosfacebook (user_id,facebook_id) VALUES (:user_id,:facebook_id)',array('user_id' => $newuser->uid,'facebook_id' => $uid));        

        // Post no Mural no Facebook

        // Seta o Objeto do Facebook e pega o ID do Usuário corrente
        $facebook = facebook_client();
        $fuid = $facebook->getUser();       

        // Frase antiga
        // $post = $usuario[name].' entrou no aplicativo IE Explorers. Ele vai cadastrar viagens que fez ou pretende fazer e compartilhar experiências com outros viajantes. Conheça o IE e faça parte também!';

        $post = 'Encontrei dicas de viagem feitas por quem já foi para o exterior no app IE Explorer. Compartilhe suas experiências internacionais também!';

        $post_on_wall = $facebook->api('/'.$fuid.'/feed', 'POST',
                                          array(
                                            'link' => 'https://apps.facebook.com/ieintercambio',
                                            'message'=> $post,
                                      'description' => 'Um aplicativo para você compartilhar e conversar sobre experiências, ideias e dicas com outros viajantes IE. Faça novos amigos e explore um mundo de intercâmbios.'
                                       ));

        

      }
      else
      {

        global $user;

        // Loga o usuário no drupal
        $form_state = array();
        $form_state['uid'] = $result[0]->user_id;      
        user_login_submit(array(), $form_state);   

        $user = user_load($result[0]->user_id);   

      }

      if(isset($_GET['r'])){
        drupal_goto(url('appieintercambio/'.$_GET['r']));        
      }
  
      $link_eu_quero .= $url_retorno.'euquero';
      $link_eu_vou .= $url_retorno.'euvou';
      $link_eu_fui .= $url_retorno.'eufui';

    } else {

      $link_eu_quero .= 'https://www.facebook.com/dialog/permissions.request?app_id='.$app_id.'&display=page&next='.urlencode( $url_retorno .'euquero' ).'&response_type=code&fbconnect=1&perms=publish_stream,read_friendlists';

      $link_eu_vou .= 'https://www.facebook.com/dialog/permissions.request?app_id='.$app_id.'&display=page&next='.urlencode( $url_retorno .'euvou' ).'&response_type=code&fbconnect=1&perms=publish_stream,read_friendlists';

      $link_eu_fui .= 'https://www.facebook.com/dialog/permissions.request?app_id='.$app_id.'&display=page&next='.urlencode( $url_retorno .'eufui' ).'&response_type=code&fbconnect=1&perms=publish_stream,read_friendlists';  
    }

    $query = db_select('appieintercambio_viagem','v');
    $query->fields('v', array('facebook_id'));
    $query->condition('facebook_id',$uid);
    $result = $query->execute()->rowCount();   

    if ($result>0){

      drupal_add_js(
      'jQuery(document).ready(function($){

        $("body").hide();

        top.location = "https://apps.facebook.com/ieintercambio/minhasviagens";
      
      });','inline');

      // drupal_goto('appieintercambio/minhasviagens',array('query' => array('fuid' => $uid)));
    }
   
  }


  return theme("page-inicial", 
    array(
    'link_eu_quero' => $link_eu_quero,
    'link_eu_vou' => $link_eu_vou,
    'link_eu_fui' => $link_eu_fui
    )
  );  

}

# Página de Cadastro "Eu Quero"
function appieintercambio_cadastro_euquero(){

  $facebook = facebook_client();
  $fuid = $facebook->getUser();
  // $fuid = $_POST['user_fuid'];   

  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/cad-euquero.css');
  
  drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/cad-euquero.js', 'file');
  drupal_add_js(drupal_get_path('module', 'appieintercambio') .'/js/appieintercambio_cadastro.js', 'file');
  drupal_add_js(drupal_get_path('module', 'appieintercambio') .'/js/appieintercambio_live_cadastro.js', 'file');

  drupal_add_js(drupal_get_path('module', 'appieintercambio') .'/js/jquery-ui.js', 'file');
  drupal_add_css(drupal_get_path('module', 'appieintercambio') . '/css/jquery-ui.css');

  drupal_add_js(
  'jQuery(document).ready(function($){
     $("#searchBox").autocomplete({
          source: function (request, response) {

            country = cc;
            cidade = request.term;

            $.ajax({
                url: "https://dev.virtualearth.net/REST/v1/Locations",
                dataType: "jsonp",
                data: {
                    key: "Aqc_aSDvyUOosa5YAwmnSkgjuIdUkZ9jHJ9F-6RDoubG12YxaSIXAf7XkAAcPczv",
                     countryRegion: country,
                     locality: cidade,
                     maxResults: 1
                },
                jsonp: "jsonp",
                success: function (data) {
                    var result = data.resourceSets[0];
                    if (result) {
                        if (result.estimatedTotal > 0) {
                            response($.map(result.resources, function (item) {
                               

                                FB.Canvas.scrollTo(0,0);
                                console.log("Chegou Na Função.");

                                //
                                return {
                                    data: item,
                                    label: item.name +  "(" + item.address.countryRegion + ")",
                                    value: item.name
                                }
                            }));
                        }






                    }
                }
            });
        },
        minLength: 1,
        change: function (event, ui) {
            if (!ui.item)
                $("#searchBox").val("");
        },
        select: function (event, ui) {
            displaySelectedItem(ui.item.data);
        }
    });

  function displaySelectedItem(item) {      
      $("#latitude_input").val(item.point.coordinates[0]);
      $("#longitude_input").val(item.point.coordinates[1]);
  }

  $(".envia_solicitacao").click(function() {
    var nome = $("#edit-nome").val();
    var cidade = $("#edit-cidade").val();
    var pais = $("#edit-pais").val();

    $.post("solicitacadastro", {"nome": nome, "cidade": cidade, "pais": pais}, function(data) {
        $(".add-instituicao .bgradio").html("Salvo com sucesso!");
    });

  });


  $("html").ajaxStart(function () {
      $("body").css("cursor", "wait");
  });

  $("html").ajaxComplete(function () {
      $("body").css("cursor", "auto");
      jQuery(".bgradio").click(function(){
        jQuery(".mapa-paises").show();
        jQuery("html, body").animate({
            scrollTop: jQuery(".localViagem").offset().top
          }, 1000);
      });
  });

  
  });','inline');


  return theme("page-eu-quero", 
    array(
        'continentes' => appieintercambio_getArrayContinentes(),
        'user_fuid' => $fuid
      )
  ); 
}

# Página de Cadastro "Eu Vou"
function appieintercambio_cadastro_euvou(){

  $facebook = facebook_client();
  $fuid = $facebook->getUser(); 
  // $fuid = $_POST['user_fuid'];

  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/cad-euvou.css');  

  drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/jquery.maskedinput-1.2.2.js', 'file');
  drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/cad-euvou.js', 'file');
  drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/mask.js', 'file');
  drupal_add_js(drupal_get_path('module', 'appieintercambio') .'/js/appieintercambio_cadastro.js', 'file');
  drupal_add_js(drupal_get_path('module', 'appieintercambio') .'/js/appieintercambio_live_cadastro.js', 'file');

  drupal_add_js(drupal_get_path('module', 'appieintercambio') .'/js/jquery-ui.js', 'file');
  drupal_add_css(drupal_get_path('module', 'appieintercambio') . '/css/jquery-ui.css');
 
  drupal_add_js(
  'jQuery(document).ready(function($){
     $("#searchBox").autocomplete({
          source: function (request, response) {

            country = cc;
            cidade = request.term;

            $.ajax({
                url: "https://dev.virtualearth.net/REST/v1/Locations",
                dataType: "jsonp",
                data: {
                    key: "Aqc_aSDvyUOosa5YAwmnSkgjuIdUkZ9jHJ9F-6RDoubG12YxaSIXAf7XkAAcPczv",
                     countryRegion: country,
                     locality: cidade,
                     maxResults: 1
                },
                jsonp: "jsonp",
                success: function (data) {
                    var result = data.resourceSets[0];
                    if (result) {
                        if (result.estimatedTotal > 0) {
                            response($.map(result.resources, function (item) {
                                return {
                                    data: item,
                                    label: item.name +  "(" + item.address.countryRegion + ")",
                                    value: item.name
                                }
                            }));
                        }
                    }
                }
            });
        },
        minLength: 1,
        change: function (event, ui) {
            if (!ui.item)
                $("#searchBox").val("");
        },
        select: function (event, ui) {
            displaySelectedItem(ui.item.data);
        }
    });

  function displaySelectedItem(item) {      
      $("#latitude_input").val(item.point.coordinates[0]);
      $("#longitude_input").val(item.point.coordinates[1]);
  }

  $(".envia_solicitacao").click(function() {
    var nome = $("#edit-nome").val();
    var cidade = $("#edit-cidade").val();
    var pais = $("#edit-pais").val();

    $.post("solicitacadastro", {"nome": nome, "cidade": cidade, "pais": pais}, function(data) {
        $(".add-instituicao .bgradio").html("Salvo com sucesso!");
    });

  });


  $("html").ajaxStart(function () {
      $("body").css("cursor", "wait");
  });

  $("html").ajaxComplete(function () {
      $("body").css("cursor", "auto");
      jQuery(".bgradio").click(function(){
        jQuery(".mapa-paises").show();
        jQuery("html, body").animate({
            scrollTop: jQuery(".localViagem").offset().top
          }, 1000);
      });
  });
  

  
  });','inline');

  return theme("page-eu-vou", 
    array(
        'continentes' => appieintercambio_getArrayContinentes(),
        'user_fuid' => $fuid
      )
  );   

}

# Página de Cadastro "Eu Fui"
function appieintercambio_cadastro_eufui(){

  $facebook = facebook_client();
  $fuid = $facebook->getUser();   

  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/cad-eufui.css');
  
  drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/jquery.maskedinput-1.2.2.js', 'file');
  drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/cad-eufui.js', 'file');
  drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/mask.js', 'file');
  drupal_add_js(drupal_get_path('module', 'appieintercambio') .'/js/appieintercambio_cadastro.js', 'file');
  drupal_add_js(drupal_get_path('module', 'appieintercambio') .'/js/appieintercambio_live_cadastro.js', 'file');

  drupal_add_js(drupal_get_path('module', 'appieintercambio') .'/js/jquery-ui.js', 'file');
  drupal_add_css(drupal_get_path('module', 'appieintercambio') . '/css/jquery-ui.css');

  drupal_add_js(
  'jQuery(document).ready(function($){

    var basepath = Drupal.settings["appieintercambio"]["basepath"];

     $("#searchBox").autocomplete({
          source: function (request, response) {

            country = cc;
            cidade = request.term;

            $.ajax({
                url: "https://dev.virtualearth.net/REST/v1/Locations",
                dataType: "jsonp",
                data: {
                    key: "Aqc_aSDvyUOosa5YAwmnSkgjuIdUkZ9jHJ9F-6RDoubG12YxaSIXAf7XkAAcPczv",
                     countryRegion: country,
                     locality: cidade,
                     maxResults: 1
                },
                jsonp: "jsonp",
                success: function (data) {
                    var result = data.resourceSets[0];
                    if (result) {
                        if (result.estimatedTotal > 0) {
                            response($.map(result.resources, function (item) {
                                return {
                                    data: item,
                                    label: item.name +  "(" + item.address.countryRegion + ")",
                                    value: item.name
                                }
                            }));
                        }
                    }
                }
            });
        },
        minLength: 1,
        change: function (event, ui) {
            if (!ui.item)
                $("#searchBox").val("");
        },
        select: function (event, ui) {
            displaySelectedItem(ui.item.data);
        }
    });

  function displaySelectedItem(item) {      
      $("#latitude_input").val(item.point.coordinates[0]);
      $("#longitude_input").val(item.point.coordinates[1]);
  }

  $(".envia_solicitacao").click(function() {
    var nome = $("#edit-nome").val();
    var cidade = $("#edit-cidade").val();
    var pais = $("#edit-pais").val();

    $.post("solicitacadastro", {"nome": nome, "cidade": cidade, "pais": pais}, function(data) {
        $(".add-instituicao .bgradio").html("Salvo com sucesso!");
    });

  });


  $("html").ajaxStart(function () {
      $("body").css("cursor", "wait");
  });

  $("html").ajaxComplete(function () {
      $("body").css("cursor", "auto");
      jQuery(".bgradio").click(function(){
        jQuery(".mapa-paises").show();
        jQuery("html, body").animate({
            scrollTop: jQuery(".localViagem").offset().top
          }, 1000);
      });
  });
  
  
  });','inline');



  return theme("page-eu-fui", 
    array(
        'continentes' => appieintercambio_getArrayContinentes(),
        'user_fuid' => $fuid
      )
  );  
}

# Método para cadastrar a viagem do usuário
function appieintercambio_cadastrar(){

  $params['uid'] = appieintercambio_get_user_id($_POST['user_fuid']);
  // $params['facebook_id'] = appieintercambio_get_facebook_id();
  $params['facebook_id'] = $_POST['user_fuid'];

  $params['interesse_id'] = $_POST['interesse_id'];

  /**
    incluir iid apenas se o motivo do cadastro for ( Intercambio com Trabalho ou Estudar no Exterior)
  */

  $params['iid'] = $_POST['iid'];

  if ($params['iid'] == 0) {
    $params['iid'] = 1;
  }

  $params['pid'] = $_POST['pais'];


  if(isset($_POST['data_ida']) && isset($_POST['data_volta'])){

    $data_ida = explode('/',$_POST['data_ida']);
    $params['data_ida'] = strtotime($data_ida[2].'-'.$data_ida[1].'-'.$data_ida[0]);

    $data_volta = explode('/',$_POST['data_volta']);
    $params['data_volta'] = strtotime($data_volta[2].'-'.$data_volta[1].'-'.$data_volta[0]);

  }

  if(isset($_POST['depoimento'])){
    $params['depoimento'] = $_POST['depoimento'];
  }
  

  $params['cidade'] = $_POST['cidade_id'];  
  $params['tipo_id'] = $_POST['tipo_id'];
  $params['created'] = time();  

  if(isset($_POST['latitude'])){
    $params['latitude'] = $_POST['latitude'];    
  }

  if(isset($_POST['longitude'])){
    $params['longitude'] = $_POST['longitude'];
  }
 
  /**
    VERIFICAR SE LATITUDE E LONGITUDE FORAM SETADOS E GRAVAR NA TABELA appieintercambio_explorer COM DESCRICAO / LATITUDE / LONGITUDE / UID / FACEBOOK_ID 
    
  */


   $id = db_insert('appieintercambio_viagem')
  ->fields($params)
  ->execute();    



  // Seta o Objeto do Facebook e pega o ID do Usuário corrente
  $facebook = facebook_client();
  // $fuid = $facebook->getUser(); 

  $fuid = $_POST['user_fuid'];


  // Pega o nome do Usuário
  $usuario = user_load($params['uid']);
  $nome = explode(':',$usuario->name);

  // Pega o nome da Cidade e do País

  $cidade = getCidadeName($params['cidade']);
  $pais = getPaisName($params['pid']);  



  // $post = 'Fulano vai para Miami,EUA. Cadastre-se no aplicativo IE Explorers e descubra para onde seus amigos vão.';

  // $post = $nome[0].' '.appieintercambio_getVerboPorTipo($params['tipo_id']).' '.appieintercambio_getNameInteresse($params['interesse_id']); 
  $post = appieintercambio_getNameTipo($params['tipo_id']).' '.appieintercambio_getNameInteresse($params['interesse_id']); 

  $prep = $params['interesse_id'] < 3 ? ' em ' : ' para ';
  
  $post .= $prep . $cidade.', '.$pais.'.';  
  $post .= ' Cadastre-se no aplicativo IE Explorers e descubra para onde seus amigos vão.';



  $post_on_wall = $facebook->api('/'.$fuid.'/feed', 'POST',
                                    array(
                                      'link' => 'https://apps.facebook.com/ieintercambio',
                                      'message'=> $post,
                                      'description' => 'Um aplicativo para você compartilhar e conversar sobre experiências, ideias e dicas com outros viajantes IE. Faça novos amigos e explore um mundo de intercâmbios.'
                                 ));
/*
  print_r($_POST);
  print_r($params);

  die();  

  header('location: https://apps.facebook.com/ieintercambio/confirmacao?vid='.$id.'&iid='.$params['iid'].'&pid='.$params['pid'].'&interesse_id='.$params['interesse_id'].'&tipo_id='.$params['tipo_id'].'&cidade='.$params['cidade']); 
  exit();
  */
  
  drupal_goto(url('appieintercambio/confirmacao'),array(
    'query' => array(
    'vid' => $id,
    'iid' => $params['iid'],
    'pid' => $params['pid'],
    'interesse_id' => $params['interesse_id'],
    'tipo_id' => $params['tipo_id'],
    'cidade'=> $params['cidade'],
    'user_fuid' => $fuid
    )));
    

}

# Página de confirmação após o cadastro
function appieintercambio_confirmacao()
{

  switch($_GET['tipo_id']){
    case '1':
      $css = 'euquero-resposta.css';
      $js = 'cad-euquero.js';
      $path_img_header = 'eu-quero';
      break;
    case '2':
      $css = 'euvou-resposta.css';
      $js = 'cad-euvou.js';
      $path_img_header = 'eu-vou';
      break;
    case '3':
      $css = 'eufui-resposta.css';
      $js = 'cad-eufui.js';
      $path_img_header = 'eu-fui';
      break;            
  }  

  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/'.$css);
  drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/'.$js, 'file');

  $vid = $_GET['vid'];  
  $cid = $_GET['cidade'];
  $pid = $_GET['pid'];

  $cidade = getCidadeName($cid);
  $pais = getPaisName($pid);

  $interesse_id = $_GET['interesse_id'];

  $facebook = facebook_client();
  // $fuid = $facebook->getUser(); 
  $fuid = $_GET['user_fuid'];
  $usuario = $facebook->api('/'.$fuid);

  $tipo = appieintercambio_getNameTipo($_GET['tipo_id']);
  $tipo_id = $_GET['tipo_id'];
  $avatar = 'https://graph.facebook.com/'.$fuid.'/picture';
  $nome_usuario = $usuario['name'];

  $frase = ' '.appieintercambio_getVerboPorTipo($tipo_id).' '.appieintercambio_getNameInteresse($_GET['interesse_id']); 
  $prep = $interesse_id < 3 ? ' em ' : ' para ';
  $frase .= $prep . $cidade.', '.$pais;

  # Trazer listagem de usuários que querem ( estudar, trabalhar, estudar e trabalhar ) na Cidade Escolhida pelo usuário
  $quequerem = db_query('
    SELECT * FROM appieintercambio_viagem as viagem     
    WHERE cidade = :cidade AND interesse_id = :interesse_id AND tipo_id = 1
    ',array('cidade' => $cid,'interesse_id' => $interesse_id))->fetchAll();

  $array_quequerem = array();

  foreach($quequerem as $_quequerem){

    if($_quequerem->facebook_id != 0)
    {
      $usuario = $facebook->api('/'.$_quequerem->facebook_id);
      
      $array_quequerem[] = array(
          'avatar' => 'https://graph.facebook.com/'.$_quequerem->facebook_id.'/picture',
          'nome' => $usuario['name'],
          'frase' => appieintercambio_getVerboPorTipo(1).' '.appieintercambio_getNameInteresse($_GET['interesse_id']).' '.$prep.' '.$cidade
        );      
    }
    
  }  
  
  # Trazer listagem de usuários que vão ( estudar, trabalhar, estudar e trabalhar ) na Cidade Escolhida pelo usuário
  $quevao = db_query('
    SELECT * FROM appieintercambio_viagem as viagem     
    WHERE cidade = :cidade AND interesse_id = :interesse_id AND tipo_id = 2
    ',array('cidade' => $cid,'interesse_id' => $interesse_id))->fetchAll();


  $array_quevao = array();

  foreach($quevao as $_quevao){

    if($_quevao->facebook_id != 0)
    {    

      $usuario = $facebook->api('/'.$_quevao->facebook_id);

      $array_quevao[] = array(
          'avatar' => 'https://graph.facebook.com/'.$_quevao->facebook_id.'/picture',
          'nome' => $usuario['name'],
          'frase' => appieintercambio_getVerboPorTipo(2).' '.appieintercambio_getNameInteresse($_GET['interesse_id']).' '.$prep.' '.$cidade
        );

    }
  }  


  # Trazer listagem de usuários que foram ( estudar, trabalhar, estudar e trabalhar ) na Cidade Escolhida pelo usuário
  $queforam = db_query('
    SELECT * FROM appieintercambio_viagem as viagem     
    WHERE cidade = :cidade AND interesse_id = :interesse_id AND tipo_id = 3
    ',array('cidade' => $cid,'interesse_id' => $interesse_id))->fetchAll();

  $array_queforam = array();

  foreach($queforam as $_queforam){

    if($_queforam->facebook_id != 0)
    {    
      $usuario = $facebook->api('/'.$_queforam->facebook_id);

      $array_queforam[] = array(
          'avatar' => 'https://graph.facebook.com/'.$_queforam->facebook_id.'/picture',
          'nome' => $usuario['name'],
          'frase' => appieintercambio_getVerboPorTipo(3).' '.appieintercambio_getNameInteresse($_GET['interesse_id']).' '.$prep.' '.$cidade
        );
    }
  }  


  return theme("confirmacao", 
    array(
    'tipo' => $tipo,
    'avatar' => $avatar,
    'nome_usuario' => $nome_usuario,
    'frase' => $frase,
    'quequerem' => $array_quequerem,
    'quevao' => $array_quevao,
    'queforam' => $array_queforam,
    'path_img_header' => $path_img_header,
    'user_fuid' => $_GET['user_fuid']
    )
  );

}


# Busca por outros usuários
function appieintercambio_busca(){

}

# Perfil do viajante ( não usuário )
function appieintercambio_perfildoviajante(){

}

# Página com todos os tópicos do fórum
function appieintercambio_forum(){

}

# Página do tópico
function appieintercambio_topico(){

}

# Formulário para criar um tópico
function appieintercambio_criartopico(){

}

/*

function appieintercambio_libraries_info(){
  // Returns an associative array, with information about external library
  $libraries['facebook-php-sdk'] = array(
    'name' => 'Facebook PHP SDK',
    'vendor url' => 'https://github.com/facebook/php-sdk',
    'download url' => 'https://github.com/facebook/php-sdk/tarball/v3.1.1',
    'version arguments' => array(
      'file' => 'readme.md',
      // Best practice: Document the actual version strings for later reference.
      // 3.1.1:  Facebook PHP SDK (v.3.1.1), filters for 3.x.x versions
      'pattern' => '/Facebook PHP SDK \(v\.(3\.\d\.\d)\)/',
      'lines' => 10,
    ),
    // supported library version, including relevant files
    'versions' => array(
      '3.1.1' => array(
        'files' => array(
          'php' => array(
            'src/base_facebook.php',
            'src/facebook.php',
          ),
        ),
      ),
    ),
  );
  return $libraries;
}


function appieintercambio_facebook_client_load_include(){

  if (!class_exists('Facebook')) {
    if (function_exists('libraries_load')) {
      $library = libraries_load('facebook-php-sdk');
    }
    else 
    {
      $sdk_path = DRUPAL_ROOT . '/sites/all/libraries/facebook-php-sdk/src/facebook.php';
      $library = array('loaded' => file_exists($sdk_path));
      if ($library['loaded']) {
        require_once $sdk_path;
      }
    }
    if (!$library['loaded']) {
      watchdog('fbconnect', t('Unable to load the required Facebook library, please check the README.txt for instructions on how to resolve this.'));
    }
  }
  return class_exists('Facebook') && defined('Facebook::VERSION');

}


function appieintercambio_facebook_client(){

  static $fb = NULL;

  appieintercambio_facebook_client_load_include();

  if (is_null($fb)) {

      $initParams = array(
        'appId' => '105385746283411',
        'secret' => '5536f92ea0a6da1f3ce3787ae74d2b67',
      );

      $fb = new Facebook($initParams);

      // hack for #902542
      Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = FALSE;
      Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYHOST] = 2;
    
  }

  return $fb;

}
*/

function appieintercambio_get_user_id($user_fuid = null){

    $facebook = facebook_client();
    $fuid = $facebook->getUser();   

    if($user_fuid != null)
    {
       $fuid = $user_fuid;
    }

    // Verifica se o usuário já está cadastrado
    $result = db_query('SELECT * FROM appieintercambio_usuariosfacebook WHERE facebook_id = :facebook_id',array('facebook_id' => $fuid));

    $usuario_exist = FALSE;

    foreach ( $result as $usuario )
    {
      $usuario_exist = TRUE ;
    }  

    if($usuario_exist){
      return $usuario->user_id;
    }else{
      return 0;
    }

}

function appieintercambio_get_facebook_id(){

    $facebook = facebook_client();
    $fuid = $facebook->getUser();   

    return $fuid;

}

function appieintercambio_getNameInteresse($interesse_id){

  switch($interesse_id){
    case 1:
      return 'trabalhar';
      break;
    case 2:
      return 'estudar';
      break;
    case 3:
      return 'viajar';
      break;
  }

}

function appieintercambio_getNameTipo($tipo_id){

  switch($tipo_id){
    case 1:
      return 'Eu quero';
      break;
    case 2:
      return 'Eu vou';
      break;
    case 3:
      return 'Eu fui';
      break;
  }

}

function appieintercambio_getVerboPorTipo($tipo_id){
  switch($tipo_id){
    case 1:
      return 'quer';
      break;
    case 2:
      return 'vai';
      break;
    case 3:
      return 'foi';
      break;
  }  
}

function appieintercambio_getContinente($continente_id){

  switch($continente_id){
    case 1:
      $continente = 'Américas';
      break;
    case 2:
      $continente = 'Europa';
      break;
    case 3:
      $continente = 'Oceania';
      break;
    case 4:
      $continente = 'África';
      break;
    case 5:
      $continente = 'Ásia';
      break;                        
  }

  return $continente;
}

function appieintercambio_getArrayContinentes(){

    $_continentes = array('1','2','3','4','5');

    foreach($_continentes as $_continente){

      $_paises = db_query('SELECT * FROM appieintercambio_pais WHERE continente_id = :continente_id ORDER BY nome ASC',array('continente_id' => $_continente));

      if($_paises->rowCount() > 0){

         $continentes[$_continente]['nome'] = appieintercambio_getContinente($_continente);
      }

      foreach($_paises as $_pais){

          $continentes[$_continente]['paises'][$_pais->pid]['pid'] = $_pais->pid;
          $continentes[$_continente]['paises'][$_pais->pid]['nome'] = $_pais->nome;
          $continentes[$_continente]['paises'][$_pais->pid]['fid'] = $_pais->fid;      
      }      

    }

    return $continentes;
}

function getCidadeName($cid){
  $query = db_select('appieintercambio_cidades','c');
  $query->fields('c',array('name'));
  $query->condition('id',$cid);
  $result = $query->execute()->fetchCol();

  return $result[0];

}

function getPaisName($pid){
  $query = db_select('appieintercambio_pais','p');
  $query->fields('p',array('nome'));  
  $query->condition('pid',$pid);
  $result = $query->execute()->fetchCol();

  return $result[0];
}