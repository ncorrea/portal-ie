

<script type="text/javascript">

jQuery(document).ready(function($){

	$('.fechar').live('click',function(){

		if($(this).attr('tipo') == 'pergunta')
		{
			var tid = $(this).attr('identificador');

			$.get('https://facebook.ieintercambio.com.br/appieintercambio/deletar/pergunta?tid='+tid, function(data) {
			  $('.post[identificador='+tid+']').fadeOut('fast');
			});
		}
		else if($(this).attr('tipo') == 'resposta')
		{
			var rid = $(this).attr('identificador');

			$.get('https://facebook.ieintercambio.com.br/appieintercambio/deletar/resposta?rid='+rid, function(data) {
			  $('.respostas-1[identificador='+rid+']').fadeOut('fast');
			});			
		}

	});

});

</script>

<div class="facebook_share">
	<div class="centraliza">
		<div class="fb-like" data-href="https://www.facebook.com/IEintercambio?ref=ts&amp;fref=ts" data-send="true" data-width="450" data-show-faces="false"></div>
	</div>
</div>
<div class="header">
	<div class="header-1">
		<h1>Ie explorer</h1>
		<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
	</div>
	<div class="menuPrinc">
		<div class="centraliza">
			<ul>
				<!-- <li><a href="http://facebook.ieintercambio.com.br/appieintercambio/inicial" class="transition">Home</a></li> -->
				<li>
					<a href="javascript:void()" class="transition">Cadastrar Viagens</a>
					<dl>
						<dt></dt>
						<dd><a href="https://apps.facebook.com/ieintercambio/euquero" class="transition" target="_top">Quero Viajar!</a></dd>
						<dd style="border-top: 1px solid #333333; border-bottom: 1px solid #333333;"><a href="https://apps.facebook.com/ieintercambio/euvou" target="_top" class="transition">Vou VIAJAR para</a></dd>
						<dd><a href="https://apps.facebook.com/ieintercambio/eufui" class="transition" target="_top">Já VIAJEI para</a></dd>
					</dl>
				</li>
				<li><a href="https://apps.facebook.com/ieintercambio/minhasviagens" target="_top" class="transition" target="_top">Minhas Viagens</a></li>
				<li><a href="https://apps.facebook.com/ieintercambio/encontre" class="transition" target="_top">Outros Viajantes<span></span></a></li>
				<li class="last"><a href="https://apps.facebook.com/ieintercambio/toptips" class="transition ativo" target="_top">Top Tips</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="limite">
	<div class="content-top">
		<div class="texto">
			<h3>ESPAÇO PARA COMPARTILHAR</h3>
			<p>Aqui você consegue o máximo de informações úteis para uma viagem IE.</p>
		</div>
	</div>
	<div class="breadcrumb">
		<p> Home > <span>Top Tips</span></p>
	</div>
	<div class="clear"></div>
	<div class="conteudo">
		<h4>DICAS E DÚVIDAS</h4>
		<div class="clear"></div>
	
		<div class="busca-pergunta">
			<div class="camposbuscar">
				<form action="<?php echo url('appieintercambio/toptips/busca'); ?>">
					<label for="categoria">Busca:</label>
					<select name="categoria" id="">
						<option value="">Todas as Categorias</option>
						<option value="1">Estudo</option>
						<option value="2">Trabalho</option>
						<option value="3">Explorar Destinos</option>
						<option value="4">Classificados</option>
						<option value="5">Dicas Gerais</option>
					</select>
					<input type="text" name="busca">
					<input type="submit" class="transition" value="buscar">
				</form>
			</div>
		</div>
		<div class="clear"></div>
		<div class="blocoMural">
			<h3></h3>
			<div class="facapergunta">
				<?php echo l('Enviar Mensagem','appieintercambio/toptips/perguntar',array('attributes' => array('class' => 'transition', 'target' => '_self'))); ?>
			</div>
			<div class="clear"></div>
			<div class="lista-botoes">
				<ul>
					<li><a href="javascript:void()" class="ativo categoria-button" rel="1" id="categoria5">Estudo</a></li>
					<li><a href="javascript:void()" class="categoria-button" rel="2" id="categoria1">Trabalho</a></li>
					<li><a href="javascript:void()" class="categoria-button" rel="3" id="categoria2">Explorar<br>Destinos</a></li>
					<li><a href="javascript:void()" class="categoria-button" rel="4" id="categoria4">Classificados</a></li>
					<li><a href="javascript:void()" class="categoria-button" rel="5" id="categoria5">Dicas Gerais</a></li>
				</ul>
			</div>
			<div class="clear"></div>

			<div class="mostra-posts">
				<div class="loadGif"><img src="http://static.tumblr.com/d0qlne1/qVol4tb08/loading.gif" alt=""></div>		
			</div>

		</div>
	</div>
	<div class="clear"></div>
</div>

<div class="footer">
	<div class="barra">
		<div class="footer-1">
			<p>IE Intercâmbio no Exterior - Todos os direitos reservados.</p>
			<a class="transition" href="https://www.kindle.com.br" target="_blank">Kindle</a>
		</div>
	</div>
</div>
  <script type='text/javascript'>
    window.onload=function() {


      altura = screen.height;

      if(altura > 800){
        FB.Canvas.setSize({width:760,height:altura});
    }else{
        FB.Canvas.setSize({width:760,height:document.body.offsetHeight});
      }
      FB.Canvas.scrollTo(0,0);
      
    }
  </script>