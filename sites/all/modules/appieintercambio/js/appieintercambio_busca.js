jQuery(document).ready(function($){	
	var filtro = "0";

	$('.form-item-pais, .form-item-cidade, .form-item-ida, .form-item-volta').append('<div class="cortaClick"></div>');
	$('#edit-busca').append('<div class="cortaClickBt"></div>');

	$('#edit-objetivo').change(function() {
		$('.form-item-pais').animate({
			opacity: 1
		}, 200, function(){
			$('.form-item-pais').css('opacity', '1');
			$('.form-item-pais .cortaClick').remove();
		});
		$('.cortaClickBt').remove();
		$('#edit-enviar').css({'opacity':'1', 'cursor':'pointer'});
		
		$('.form-item-ida').css('opacity', '1').children('.cortaClick').hide();
		$('.form-item-volta').css('opacity', '1').children('.cortaClick').hide();			
	});

	$('#edit-pais').change(function() {
		$('.form-item-cidade').animate({
			opacity: 1
		}, 200, function(){
			$('.form-item-cidade').css('opacity', '1');
			$('.form-item-cidade .cortaClick').remove();
		});
	});	

	$('#edit-cidade').change(function() {
		$('.form-item-ida').animate({
			opacity: 1
		}, 200, function(){
			$('.form-item-ida').css('opacity', '1');
			$('.form-item-ida .cortaClick').remove();
			$('#edit-enviar').css({'opacity':'1', 'cursor':'pointer'});			
		});
	});	


	$('#edit-ida-month, #edit-ida-year').change(function() {
		$('.form-item-volta').animate({
			opacity: 1
		}, 200, function(){
			$('.form-item-volta').css('opacity', '1');
			$('.form-item-volta .cortaClick').remove();
		});
	});	

	$('#edit-volta-month, #edit-volta-year').change(function() {
		$('#edit-enviar').animate({
			opacity: 1
		}, 200, function(){
			$('#edit-enviar').css({'opacity':'1', 'cursor':'pointer'});
			$('.cortaClickBt').remove();
		});
	});	


	var basepath = Drupal.settings.appieintercambio.basepath;
	//var hoje = (Date.today().getTime()/1000);
	var hoje = new Date();
	var paises = $('#edit-pais').html();


	
	$('#edit-pais').html('<option value=\'0\'>Selecionar País</option>' + paises);
	$('#edit-ida div label').hide();
	$('#edit-volta div label').hide();

	
	$('#form-busca').submit(function(){	

		var tipo = $('input[name=tipobusca]:checked').val();
		var objetivo = $('#edit-objetivo').val(); 
		var pais = $('#edit-pais').val();
		var cidade = $('#edit-cidade').val();
		
		var data_ida = $('#edit-ida-year ').val() + '/' + $('#edit-ida-month').val() + '/' + '30';
		var data_volta = $('#edit-volta-year').val() + '/' + $('#edit-volta-month').val() + '/' + '30';

		var data_ida_formatada = new Date(data_ida);
		var data_volta_formatada = new Date(data_volta);

		if (tipo == 1) {
			data_ida = 0;
			data_volta = 0;
		}

		/*
		//Verifica se Data de partida é maior que data de retorno
		if ((Date.parse(data_ida).getTime()/1000) > (Date.parse(data_volta).getTime()/1000)){
			alert('Data de retorno, não pode ser menor que a data de partida! ');
			return false;
		}


		//Se tipobusca = EuVou então, data não pode ser menor que data atual
		if (tipo == 2) { 
			var dataida = ((Date.parse(data_ida).getTime())/1000);
			if(dataida < hoje){
				alert('Data de Partida não pode ser menor que a data atual! ');
			}
			//return false;
		}
		*/

		//Verifica se Data de partida é maior que data de retorno
		if (data_ida_formatada > data_volta_formatada){
			alert('Data de retorno, não pode ser menor que a data de partida! ');
			return false;
		}


		//Se tipobusca = EuVou então, data não pode ser menor que data atual
		if (tipo == 2) { 
			if(data_ida_formatada < hoje){
				alert('Data de Partida não pode ser menor que a data atual! ');
				return false;
			}
			//return false;
		}

		/*
		if (cidade == 0) {
			alert('Informe a cidade antes de procurar.');
			return false;
		}
		*/


		$.post(basepath + 'appieintercambio/encontre/getUsuarios', {'tipo':tipo, 'objetivo':objetivo, 'pais':pais, 'cidade':cidade, 'data_ida':data_ida, 'data_volta':data_volta, 'filtro': filtro}, function(data){
			
			$('.content-resultados').hide().fadeIn();
			$('#resultado_pesquisa_ajax').html(data);
	
			$('.userlist ul li:nth-child(1n)').css('margin-left', '14px');
			$('.userlist ul li:nth-child(3n)').css('margin', '15px 0 0 0');

			if (filtro == "0") {
				$('#edit-criterio-0').attr('checked', true);
			} else {
				$('#edit-criterio-1').attr('checked', true);
			}

		});		
		return false;

	});	

	$('.form-radio').click(function() {
		filtro = $(this).val();
		$('#form-busca').submit();
	});
	
	$('#edit-pais').change(function() {
		
		var pid = $(this).val();

		$.post(basepath + 'appieintercambio/encontre/getCidades',{'pid':pid},function(data){
			
			$("#edit-cidade").html('<option value="0">Selecionar Cidade</option>');
			
			for(i=0;i<data.length;i++){
				$("#edit-cidade").append('<option value="'+data[i].id+ '">'+data[i].cidade+'</option>');
			}

			
		});

	});	


	/**
		FRONT
	*/

	// $('.radio').click(function(){

	// });
	$('.radioquero').click(function(){
		$('#edit-camposdata').css('display', 'none');
	});

	$('.radiovou, .radiofui').click(function(){
		$('#edit-camposdata').css('display', 'block');
	});


	$('.radio:eq(0) label').find('.seta').addClass('ativo').show();
   	
	$('#edit-camposdata').hide();

    $('.radio label').click(function(){
       $('.radio label .seta').removeClass('ativo').hide();
       $(this).find('.seta').addClass('ativo').show();
       $('.form-item-objetivo').css('opacity', '1').show();
	   $('.form-item-pais, .form-item-cidade, .form-item-ida, .form-item-volta').css('opacity', '1').css('opacity', '0.2').append('<div class="cortaClick"></div>');
	   $('#edit-enviar').css('opacity', '1').css('opacity', '0.2');
	   $('#edit-busca').append('<div class="cortaClickBt"></div>');

	   $('#edit-objetivo').val('Fazer o que?');
	   $('#edit-pais').val('Selecionar País');
	   $('#edit-cidade').val('Selecionar Cidade');
    });

});