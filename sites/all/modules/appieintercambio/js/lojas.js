var map;
var gdir;
var geocoder = null;
var addressMarker;
var icone ;
var overlays = new Array() ;
var esperando = false ;

var myLati ;
var myLongi ;

jQuery(document).ready(function()
{
	jQuery('#edit-endereco').keyup(function() { plotaPonto() ; });	
	jQuery('#edit-bairro').keyup(function() { plotaPonto() ; });
	jQuery('#edit-cidade').keyup(function() { plotaPonto() ; });
	jQuery('#edit-uf').change(function() { plotaPonto() ; });

	var latlng = new google.maps.LatLng(-15, -55) ;
    var myOptions = {
      zoom: 4,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map( document.getElementById("map_canvas"), myOptions ) ;

	geocoder = new google.maps.Geocoder() ;

	gdir = new google.maps.DirectionsRenderer() ;
	gdir.setMap( map ) ;

});

function plotaPonto()
{
	var endereco = jQuery('#edit-endereco').val() ;
	
	if ( jQuery('#edit-bairro').val() ) endereco += ', ' + jQuery('#edit-bairro').val() ;
	if ( jQuery('#edit-cidade').val() ) endereco += ', ' + jQuery('#edit-cidade').val() ;
	if ( jQuery('#edit-uf').val() ) endereco += ', ' + jQuery('#edit-uf').val() ;
	if ( jQuery('#edit-cep').val() ) endereco += ', ' + jQuery('#edit-cep').val() ;
		
	if ( endereco.length > 10 && !esperando )
	{
		esperando = true ;
		
		geocoderequest = {
			address: endereco,
			latLng: new google.maps.LatLng(-15, -55),
			region: 'BR'
		}
	
		geocoder.geocode( geocoderequest, showAddress );
	}
}

function showAddress( response, status )
{
	esperando = false ;
		
    if ( !response || status != google.maps.GeocoderStatus.OK ) 
	{
    }
    else 
	{
		while( overlays[0] )
			overlays.pop().setMap( null ) ;

        point = response[0].geometry.location ;
		
		jQuery('input[name=latitude]').val( point.lat() ) ;
		jQuery('input[name=longitude]').val( point.lng() ) ;
		
		var marker = new google.maps.Marker({
			position: point,
			title: "Sua localidade"
		});
  
		// To add the marker to the map, call setMap();
		marker.setMap( map ) ;
		
		map.setCenter( point ) ;
		map.setZoom( 13 ) ;
		
		overlays.push( marker ) ;
    }
}
