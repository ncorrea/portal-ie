jQuery(document).ready(function($){
    
    basepath = Drupal.settings.appieintercambio.basepath;
    var pindescription = '';
    var itempages = 40;                           // quantidade de itens por página

   
    $("#pager .position").html(1);

    page = $("#pagercontroller").val();       // controle de página
    
    url = 'https://graph.facebook.com/';     

    $("#ajaxControl").ajaxStart(function(){
       // $(this).show();
     });

    $("#ajaxControl").ajaxStop(function(){
       // $("#ajaxControl").hide();
    });

    $("#pager a.left").live("click", function(){
      var total = parseInt($("#pagercount").val());
      var valor = parseInt($("#pagercontroller").val()) - 1;
      
      if (total > itempages){

        $("#pagercontroller").val(valor);
        $("#pager .position").html(valor);
        page = $("#pagercontroller").val();
        
        $.post('minhasviagens/alluserscountry',{'pin':pindescription, 'page':page}, function(data){
          $("#mostraUsers").html('');
           
           for(var i=0;i<data.length;i++){                                           
               $("#mostraUsers").append('<a href="#" class="user_profile" rel="'+data[i].facebook_id+'/'+data[i].tipo+'/'+data[i].uid+'"><img src="' + url + data[i].facebook_id + '/picture?width=40&height=40" /></a>');                  
           }

        });

      }

      return false;
    });

    $("#pager a.right").live("click", function(){
      var total = parseInt($("#pagercount").val());
      var valor = parseInt($("#pagercontroller").val()) + 1;

      if (total > itempages){
        var pos = (valor * itempages);
        if (pos < total){
            $("#pagercontroller").val(valor);
            $("#pager .position").html(valor);
            page = $("#pagercontroller").val();

            $.post('minhasviagens/alluserscountry',{'pin':pindescription, 'page':page}, function(data){
              $("#mostraUsers").html('');
               
               for(var i=0;i<data.length;i++){                                           
                   $("#mostraUsers").append('<a href="#" class="user_profile" rel="'+data[i].facebook_id+'/'+data[i].tipo+'/'+data[i].uid+'"><img src="' + url + data[i].facebook_id + '/picture?width=40&height=40" /></a>');                  
               }

            });
        }  else {
          $("#pager a.right").hide();
        }  
      } else {
        $("#pager a.right").hide();
      }
      
      return false;
    });

    var key = Drupal.settings.appieintercambio.apikey;
    var key = 'Aqc_aSDvyUOosa5YAwmnSkgjuIdUkZ9jHJ9F-6RDoubG12YxaSIXAf7XkAAcPczv';
    
    var tipomapa = [];
    var map = null;
    var pinLayer, pinInfobox, infoboxLayer;

    map = new Microsoft.Maps.Map(document.getElementById('myMap'), {credentials: key, mapTypeId: Microsoft.Maps.MapTypeId.road,  zoom: 1, showScalebar: true});  
    
    pinLayer = new Microsoft.Maps.EntityCollection();
    map.entities.push(pinLayer);

    geraMapa(map);

    $("#pagercontroller").val('1');   
    $("#pager .position").html('1');

    $('input[type=checkbox]').click(function(){      
      geraMapa(map);              
      
      $("#pagercontroller").val('1');   
      $("#pager .position").html('1');  

      $('#detalhaUsers').html('').hide();       
    });

    $('#escopo').change(function(){
      geraMapa(map);            
     
      $("#pagercontroller").val('1');   
      $("#pager .position").html('1');

      $('#detalhaUsers').html('').hide();
    });

    function limpa(map){
      for(var i=map.entities.getLength()-1;i>=0;i--) {
        var pushpin= map.entities.get(i); 
        if (pushpin instanceof Microsoft.Maps.Pushpin) { 
          map.entities.removeAt(i);  
        }
      } 
    }

  function geraMapa(map){
      tipomapa = [];
      limpa(map); 
      pinLayer.clear();    

      var scope = $('#escopo').val();   

       $('input[type=checkbox]:checked').each(function(){
              var valor = $(this).val();
              tipomapa.push(valor);        
          }
        );

        $.post('minhasviagens/getMap', {'tipomapa':tipomapa, 'escopo':scope}, function(data){   

            
              for(var i=0;i<data.length;i++){ 

                  var latitude = data[i].latitude;    
                  var longitude = data[i].longitude;
                  var elat = data[i].elat;
                  var elong = data[i].elong;
    
                  switch(data[i].tipo){                  
                    case '1':                    
                      var pinOptions =  {width: null, height: null, htmlContent: "<div class='pin_quero'></div>", anchor:new Microsoft.Maps.Point(12, 31)};                    
                      nlat = latitude;
                      nlong = longitude;    

                      if (elat != 'NULL'){
                         nlat = elat;
                         nlong = elong;   
                      }
                     
                      break;
                    case '2':
                      var pinOptions =  {width: null, height: null, htmlContent: "<div class='pin_vou'></div>", anchor:new Microsoft.Maps.Point(12, 31)};
                      nlat = latitude;
                      nlong = longitude;

                      if (elat != 'NULL'){
                         nlat = elat;
                         nlong = elong;   
                      }                        
                                          
                      break;
                    case '3':
                      var pinOptions =  {width: null, height: null, htmlContent: "<div class='pin_fui'></div>", anchor:new Microsoft.Maps.Point(12, 31)};
                      nlat = latitude;
                      nlong = longitude;

                      if (elat != 'NULL'){
                         nlat = elat;
                         nlong = elong;   
                      }
                      
                      break;                              
                  }           
      
                  var loc = new Microsoft.Maps.Location(nlat,nlong);   
                  var pin = new Microsoft.Maps.Pushpin(loc, pinOptions); 
                  
                  // pin.title = data[i].tipo + '/' + data[i].pais_nome;
                  pin.title = data[i].tipo + '/' + data[i].pais_nome + '/' + data[i].cidade_nome;

                  // pin.description = data[i].pais_id + '/' + data[i].tipo + '/' + data[i].iid; 
                  pin.description = data[i].pais_id + '/' + data[i].tipo + '/' + data[i].iid + '/' + data[i].cidade_nome; 
                      
                  map.entities.push(pin);

                  Microsoft.Maps.Events.addHandler(pin, 'click', evento);


              }
       });       
  }

  function evento(e){    

    pin = e.target;    
    var tipo = pin.title.split("/");
    pindescription = pin.description;    
    loc = pin.getLocation();

    //alert(pindescription);

    $("#pagercontroller").val('1');
    $("#pager .position").html('1');
    page = 1;
    
    switch(tipo[0]){                  
      case '1':
        // var pinOptions =  {width: null, height: null, htmlContent: "<div class='active_quero'>"+tipo[1]+"</div>"};                   
        var pinOptions =  {width: null, height: null, htmlContent: "<div class='active_quero'>"+tipo[2]+"</div>", anchor:new Microsoft.Maps.Point(12, 31)};                   
        break;
      case '2':
         // var pinOptions =  {width: null, height: null, htmlContent: "<div class='active_vou'>"+tipo[1]+"</div>"};
         var pinOptions =  {width: null, height: null, htmlContent: "<div class='active_vou'>"+tipo[2]+"</div>", anchor:new Microsoft.Maps.Point(12, 31)};
        break;
      case '3':
        // var pinOptions =  {width: null, height: null, htmlContent: "<div class='active_fui'>"+tipo[1]+"</div>"};
        var pinOptions =  {width: null, height: null, htmlContent: "<div class='active_fui'>"+tipo[2]+"</div>", anchor:new Microsoft.Maps.Point(12, 31)};
      break;                              
    }           

    pinLayer.clear();        
    pinLayer.push(new Microsoft.Maps.Pushpin(loc, pinOptions));

    $("#pager").show();
    $("#mostraUsers").html('');
    $('#detalhaUsers').html('').hide();  

    $.post('minhasviagens/alluserscountry',{'pin':pindescription, 'page':page}, function(data){

      for(var i=0;i<=data.length;i++){                                 
          // $("#mostraUsers").append('<a href="#" class="user_profile" rel="'+data[i].facebook_id+'/'+data[i].tipo+'/'+data[i].uid+'"><img src="' + url + data[i].facebook_id + '/picture?width=40&height=40" /></a>');           
          $("#mostraUsers").append('<a href="#" class="user_profile" rel="'+data[i].facebook_id+'/'+data[i].tipo+'/'+data[i].uid+'/'+data[i].interesse_id+'/'+data[i].descricao_tipo+'/'+data[i].descricao_interesse+'/'+tipo[2]+'"><img src="' + url + data[i].facebook_id + '/picture?width=40&height=40" /></a>');
          $("#pagercount").val(data[i].items);
      }        

    });

  }
 

  $(".user_profile").live("click", function(){

    $(".user_profile").find('.hover').remove();
    $(this).append("<div class='hover'></div>");

    conteudo = $(this).attr('rel').split("/"); 
    facebook_id = conteudo[0];
    tipo = conteudo[1];    
    uid = conteudo[2];

    descricao_tipo = conteudo[4];
    descricao_interesse = conteudo[5];
    cidade_nome = conteudo[6]

    var url = 'https://graph.facebook.com/' + facebook_id; 
  
    
    $.getJSON(url, function(data) {
    
      nome = data.name;      

      // html = "<span>" + nome + "</span><img src='"+url+"/picture?width=120&height=120' />" + "<a href='encontre/user/" +uid+ "' class='tipo_"+tipo+"'>Visualizar Perfil</a>";
      html = "<span>" + nome + "</span><span class='local'><strong>" + descricao_tipo+" "+descricao_interesse+"</strong> "+cidade_nome+"</span><img src='"+url+"/picture?width=120&height=120' /><a href='encontre/user/" +uid+ "' class='tipo_"+tipo+"'>Visualizar Perfil</a>";

      $('#detalhaUsers').html(html).show();

    });
    
    return false;
 
  }); 

  Microsoft.Maps.Events.addHandler(map, "mousemove", function (e) {
    // get the HTML DOM Element that represents the Map
    var mapElem = map.getRootElement();
    if (e.targetType === "map") {
      // Mouse is over Map
      mapElem.style.cursor = "crosshair";
    } else {
      // Mouse is over Pushpin, Polyline, Polygon
      mapElem.style.cursor = "pointer";
    }
  });

  function getRandomPins(pinCount, layer, pinIcon) {
    var view = map.getBounds();
    var topLeft = view.getNorthwest();
    var bottomRight = view.getSoutheast();
    var latSpan = bottomRight.latitude - topLeft.latitude;
    var longSpan = bottomRight.longitude - topLeft.longitude;
    for (i = 1; i <= pinCount; i++) {
        var pinLatLong = new Microsoft.Maps.Location(topLeft.latitude + latSpan * Math.random(), topLeft.longitude + longSpan * Math.random());
        var randomPin = new Microsoft.Maps.Pushpin(pinLatLong, { text: i.toString(), icon: 1 });
        layer.push(randomPin);
    }
  }

  // "map" is our Bing Maps object, overload the built-in getZoomRange function
  // to set our own min/max zoom
  map.getZoomRange = function () {
    return {
      max:      7,
      min:      1
    };
  };

  // Attach a handler to the event that gets fired whenever the map's view is about to change
  //Microsoft.Maps.Events.addHandler(map,'viewchangestart',restrictZoom);

  Microsoft.Maps.Events.addHandler(map,'viewchangeend',function(){
    geraMapa(map);  
        
    //document.getElementByClass('position').html('1');  

  });


  // Forcibly set the zoom to our min/max whenever the view starts to change beyond them 
  var restrictZoom = function ()  {
    if (map.getZoom() <= map.getZoomRange().min) 
    {
      map.setView({
        'zoom':       map.getZoomRange().min,
        'animate':    false
      });
    }
    else if (map.getZoom() >= map.getZoomRange().max) 
    {
      map.setView({
        'zoom':       map.getZoomRange().max,
        'animate':    false
      });
    }
  };

  function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places
      var newnumber = Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);
      return newnumber;
  }

});