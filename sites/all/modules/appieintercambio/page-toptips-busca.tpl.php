	<div class="facebook_share">
	<div class="centraliza">
		<div class="fb-like" data-href="https://www.facebook.com/IEintercambio?ref=ts&amp;fref=ts" data-send="true" data-width="450" data-show-faces="false"></div>
	</div>
</div>
<div class="header">
	<div class="header-1">
		<h1>Ie explorer</h1>
		<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
	</div>
	<div class="menuPrinc">
		<div class="centraliza">
			<ul>
				<!-- <li><a href="http://facebook.ieintercambio.com.br/appieintercambio/inicial" class="transition">Home</a></li> -->
				<li>
					<a href="javascript:void()" class="transition">Cadastrar Viagens</a>
					<dl>
						<dt></dt>
						<dd><a href="https://apps.facebook.com/ieintercambio/euquero" target="_top" class="transition">Quero Viajar!</a></dd>
						<dd style="border-top: 1px solid #333333; border-bottom: 1px solid #333333;"><a href="https://apps.facebook.com/ieintercambio/euvou" target="_top" class="transition">Vou VIAJAR para</a></dd>
						<dd><a href="https://apps.facebook.com/ieintercambio/eufui" class="transition" target="_top">Já VIAJEI para</a></dd>
					</dl>
				</li>
				<li><a href="https://apps.facebook.com/ieintercambio/minhasviagens" target="_top" class="transition">Minhas Viagens</a></li>
				<li><a href="https://apps.facebook.com/ieintercambio/encontre" class="transition" target="_top">Outros Viajantes<span></span></a></li>
				<li class="last"><a href="https://apps.facebook.com/ieintercambio/toptips" class="transition ativo" target="_top">Top Tips</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="limite">
	<div class="content-top">
		<div class="texto">
			<h3>DICAS E DÚVIDAS</h3>
			<p>Converse com outros participantes<br>do IE Explorer.</p>
		</div>
	</div>
	<div class="breadcrumb">
		<p> Home > Top Tips > <span>Perguntas</span></p>
	</div>
	<div class="clear"></div>
	<div class="conteudo">
		<h4>ESCREVA SUA MENSAGEM:</h4>
		<div class="clear"></div>
		<div class="busca-pergunta">
			<div class="camposbuscar">
				<form action="<?php echo url('appieintercambio/toptips/busca'); ?>">
					<label for="categoria">Busca:</label>
					<select name="categoria" id="">
						<option value="">Todas as Categorias</option>
						<option value="1">Estudo</option>
						<option value="2">Trabalho</option>
						<option value="3">Explorar Destinos</option>
						<option value="4">Classificados</option>
						<option value="5">Dicas Gerais</option>
					</select>
					<input type="text" name="busca">
					<input type="submit" class="transition" value="buscar">
				</form>
			</div>
		</div>
		<div class="clear"></div>
	
		<div class="lista-botoes">
			<h3>Troque<span>experiências</span></h3>
			<ul>
				<li><a href="#" class="transition">estudo</a></li>
				<li><a href="#" class="transition">trabalho</a></li>
				<li><a href="#" class="transition"><p>explorar<br>destinos</p></a></li>
				<li><a href="#" class="transition">classificados</a></li>
				<li><a href="#" class="transition"><p>dicas<br>gerais</p></a></li>
			</ul>
		</div>
		<div class="sombra"></div>
	
		<div class="clear"></div>
	
		<?php foreach($categorias as $categoria){ ?>
		<div class="mural">
			<div class="header-mural">
				<h3 class="title"><?php echo $categoria['titulo']; ?></h3>
				<?php echo l('Ver todas as '.count($categoria['topicos']).' conversas','appieintercambio/toptips/interna/'.$categoria['tipo_id'],array('attributes' => array('class' => 'transition'))); ?>
			</div>
			<div class="clear"></div>
			<?php foreach($categoria['topicos'] as $topico){ ?>
			<div class="post">
				<div class="txtpost">
					<a href="top-tips-interna.html" class="transition">Disney ou Aspen? Estou na dúvida, me ajudem!</a>
					<?php echo l($topico['pergunta'],'appieintercambio/toptips/topico/'.$topico['tid'],array('attributes' => array('class' => 'transition'))); ?>
					<p><span><?php echo $topico['usuario']; ?></span> | <?php echo $topico['created']; ?></p>
					<div class="clear"></div>
					<div class="infos">
						<div class="respostas"><span><?php echo $topico['respostas']; ?></span> resposta(s)</div>
						<div class="seguidores"><span><?php echo $topico['seguidores']; ?></span> seguidor(es)</div>
						<div class="visualizacoes"><span><?php echo $topico['visualizacoes']; ?></span> visualização(ões)</div>
					</div>
				</div>
				<div class="resposta">
					<?php echo l('responder','appieintercambio/toptips/topico/'.$topico['tid'],array('attributes' => array('class' => 'transition'))); ?>
				</div>
            <div class="clear"></div>
			</div>            
			<div class="hr"></div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
	<div class="clear"></div>
</div>

<div class="footer">
	<div class="barra">
		<div class="footer-1">
			<p>IE Intercâmbio no Exterior - Todos os direitos reservados.</p>
			<a class="transition" href="http://www.kindle.com.br" target="_blank">Kindle</a>
		</div>
	</div>
</div>

  <script type='text/javascript'>
    window.onload=function() {


      altura = screen.height;

      if(altura > 800){
        FB.Canvas.setSize({width:760,height:altura});
    }else{
        FB.Canvas.setSize({width:760,height:document.body.offsetHeight});
      }
      FB.Canvas.scrollTo(0,0);
      
    }
  </script>