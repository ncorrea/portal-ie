<div class="facebook_share">
	<div class="centraliza">
		<div class="fb-like" data-href="https://www.facebook.com/IEintercambio?ref=ts&amp;fref=ts" data-send="true" data-width="450" data-show-faces="false"></div>
	</div>
</div>
<div class="header">
	<div class="header-1">
		<h1>Ie explorer</h1>
		<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
	</div>
	<div class="menuPrinc">
		<div class="centraliza">
			<ul>
				<!-- <li><a href="http://facebook.ieintercambio.com.br/appieintercambio/inicial" class="transition">Home</a></li> -->
				<li>
					<a href="javascript:void()" class="transition">Cadastrar Viagens</a>
					<dl>
						<dt></dt>
						<dd><a href="https://apps.facebook.com/ieintercambio/euquero" class="transition" target="_top">Quero Viajar!</a></dd>
						<dd style="border-top: 1px solid #333333; border-bottom: 1px solid #333333;"><a href="https://apps.facebook.com/ieintercambio/euvou" target="_top" class="transition">Vou VIAJAR para</a></dd>
						<dd><a href="https://apps.facebook.com/ieintercambio/eufui" class="transition" target="_top">Já VIAJEI para</a></dd>
					</dl>
				</li>
				<li><a href="https://apps.facebook.com/ieintercambio/minhasviagens" target="_top" class="transition ativo" target="_top">Minhas Viagens</a></li>
				<li><a href="https://apps.facebook.com/ieintercambio/encontre" class="transition" target="_top">Outros Viajantes<span></span></a></li>
				<li class="last"><a href="https://apps.facebook.com/ieintercambio/toptips" class="transition" target="_top">Top Tips</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="limite">
	<div class="content-top">
		<div class="texto">
			<h3>Sua lista de destinos</h3>
			<p>Marque os lugares que você já <br>passou e que ainda quer ir.</p>
		</div>
	</div>
	<div class="breadCrumb">
		<p> Home > <span>Minhas Viagens</span></p>
	</div>
	<div class="clear"></div>
	
	<div class="content">
	
		<h4>Minhas Viagens</h4>

		<div class="usuario">
			<?php print $content['user'] ?>
		</div>
	
		<div class="minhasviagens">
			<h5>Viagens</h5>
			<div class="leftPaises">
				<div class="icone"></div>
			</div>
			<div class="clear"></div>
	
			<div class="filtros" style="">
				<ul>
					<li style="margin-right: 5px;"><p>Escolha um mapa</p></li>
					<li style="margin-right: 10px;">
						<select id='escopo'>
						<option value="0">Eu</option>
						<option value="1">Meus Amigos</option>
						<option value="3" selected="selected">Todos</option>
					</select>
					</li>
					<li>
						<label for="euquero" class="vermelho">
							<span class="vermelho ativo">Querem</span>
						</label>
								<input type="checkbox" name="tipomapa" value="1" id="euquero" checked="true"/></li>
					</li>
					<li>
						<label for="euvou" class="verde">
							<span class="verde ativo">Irão</span>
						</label>
								<input type="checkbox" name="tipomapa" value="2" id="euvou" checked="true"/></li>
					<li>
						<label for="eufui" class="azul">
							<span class="azul ativo">Foram</span>
						</label>
								<input type="checkbox" name="tipomapa" value="3" id="eufui" checked="true"/></li>
				</ul>
			</div>
			<div id='myMap'>
				<div id="ajaxControl">CARREGANDO</div>
			</div>						
			<div class="clear"></div>
		</div>
	
		<div class="clear"></div>

		<div class="box-users">
			<div class="users-block">
				
				<h2>IE Explorers</h2>		
				<div id="mostraUsers">
					<p>Selecione um local no <span>mapa acima</span> e descubra quem passou ou ainda vai passar por lá.</p>
				</div>				

				<div class="clear"></div>

			</div>

			<div id="detalhaUsers"></div>	

			<div id="pager" style="display: none;">
				<div class="page" style="display: none;">
					<a href="#" class="left"></a> 	
					<span class="position"><b>1 /</b> 4</span>
					<a href="#" class="right"></a>
				</div> 
			</div>

			<input type="hidden" id="pagercontroller" value="1" />
			<input type="hidden" id="pagercount" value="1" />

			<div class="clear"></div>
		</div>
		
		<div class="clear"></div>

		<div class="maisviagens">
			<h5>Cadastrar mais viagens</h5>
			<div class="leftPaises">
				<div class="icone"></div>
			</div>
			<div class="botoes-cad">
				<div class="bg-bt p01">
					<a href="https://apps.facebook.com/ieintercambio/euquero#top" target="_top" class="vermelho transition"><span>Quero</span> <br> VIAJAR para</a>
				</div>
				<div class="bg-bt p03">
					<a href="https://apps.facebook.com/ieintercambio/eufui#top" target="_top" class="azul transition"><span>Já</span> <br> VIAJEI para</a>
				</div>
				<div class="bg-bt p02">
					<a href="https://apps.facebook.com/ieintercambio/euvou#top" target="_top" class="verde transition"><span>Vou</span> <br> VIAJAR para</a>
				</div>
			</div>
		</div>
	
		<div class="clear"></div>
	
		<div class="titlemural">
			<h5>Meu Mural</h5>
		</div>
		<div class="mural">
			<div class="content">
			<?php print $content['mural'] ?>	
			</div>
		</div>
	</div>
</div>

<div class="footer">
	<div class="barra">
		<div class="footer-1">
			<p>IE Intercâmbio no Exterior - Todos os direitos reservados.</p>
			<a class="transition" href="https://www.kindle.com.br" target="_blank">Kindle</a>
		</div>
	</div>
</div>

<script type='text/javascript'>
window.onload=function() {


  altura = screen.height;

  if(altura > 800){
    FB.Canvas.setSize({width:760,height:altura});
}else{
    FB.Canvas.setSize({width:760,height:document.body.offsetHeight});
  }
  FB.Canvas.scrollTo(0,0);
  
}
</script>