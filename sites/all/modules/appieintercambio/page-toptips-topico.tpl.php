<?php 
$facebook = facebook_client();
$fuid = $facebook->getUser();
$uid = getUid($fuid);
?>

<script type="text/javascript">

jQuery(document).ready(function($){

	$('.fechar').live('click',function(){

		if($(this).attr('tipo') == 'pergunta')
		{
			var tid = $(this).attr('identificador');

			$.get('https://facebook.ieintercambio.com.br/appieintercambio/deletar/pergunta?tid='+tid, function(data) {
			  $('.post[identificador='+tid+']').fadeOut('fast');
			});
		}
		else if($(this).attr('tipo') == 'resposta')
		{
			var rid = $(this).attr('identificador');

			$.get('https://facebook.ieintercambio.com.br/appieintercambio/deletar/resposta?rid='+rid, function(data) {
			  $('.resposta2[identificador='+rid+']').fadeOut('fast');
			});			
		}

	});

});

</script>

<div class="facebook_share">
	<div class="centraliza">
		<div class="fb-like" data-href="https://www.facebook.com/IEintercambio?ref=ts&amp;fref=ts" data-send="true" data-width="450" data-show-faces="false"></div>
	</div>
</div>
<div class="header">
	<div class="header-1">
		<h1>Ie explorer</h1>
		<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
	</div>
	<div class="menuPrinc">
		<div class="centraliza">
			<ul>
				<!-- <li><a href="http://facebook.ieintercambio.com.br/appieintercambio/inicial" class="transition">Home</a></li> -->
				<li>
					<a href="javascript:void()" class="transition">Cadastrar Viagens</a>
					<dl>
						<dt></dt>
						<dd><a href="https://apps.facebook.com/ieintercambio/euquero" class="transition" target="_top">Quero Viajar!</a></dd>
						<dd style="border-top: 1px solid #333333; border-bottom: 1px solid #333333;"><a href="https://apps.facebook.com/ieintercambio/euvou" target="_top" class="transition">Vou VIAJAR para</a></dd>
						<dd><a href="https://apps.facebook.com/ieintercambio/eufui" class="transition" target="_top">Já VIAJEI para</a></dd>
					</dl>
				</li>
				<li><a href="https://apps.facebook.com/ieintercambio/minhasviagens" target="_top" class="transition" target="_top">Minhas Viagens</a></li>
				<li><a href="https://apps.facebook.com/ieintercambio/encontre" class="transition" target="_top">Outros Viajantes<span></span></a></li>
				<li class="last"><a href="https://apps.facebook.com/ieintercambio/toptips" class="transition ativo" target="_top">Top Tips</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="limite">
<div class="content-top">
	<div class="texto">
		<h3>DICAS E DÚVIDAS</h3>
		<p>Converse com outros participantes<br>do IE Explorer.</p>
	</div>
</div>
<div class="breadcrumb">
	<p> Home > Top Tips > <span>Perguntas</span></p>
</div>
	<div class="clear"></div>
		<div class="conteudo">
			<h4>ESCREVA SUA MENSAGEM:</h4>
			<div class="clear"></div>
	
		<div class="busca-pergunta">
			<div class="camposbuscar">
				<form action="<?php echo url('appieintercambio/toptips/busca'); ?>" method="GET">
					<select name="categoria">
						<option value="">Todas as Categorias</option>
						<option value="1">Estudo</option>
						<option value="2">Trabalho</option>
						<option value="3">Explorar Destinos</option>
						<option value="4">Classificados</option>
						<option value="5">Dicas Gerais</option>
					</select>
					<input type="text" name="busca" />
					<input type="submit" class="transition" value="Pesquisar" />
				</form>
			</div>
			<div class="facapergunta">
				<a href="<?php echo url('appieintercambio/toptips/perguntar'); ?>" class="transition">Troque ideias</a>
			</div>
		</div>
	
		<div class="clear"></div>
	
		<div class="question">
			<div class="post">
				<div class="border"></div>
				<div class="txtpost">
					<p class="categoria"><?php echo $topico['titulo']; ?></p>
					<p class="topic transition"><?php echo $topico['pergunta']; ?></p>
					<p class="data"><span><?php echo $topico['usuario']; ?></span> | <?php echo $topico['created']; ?></p>
				</div>
				<div class="tags">
					<ul>
						<li><?php echo $topico['tags']; ?></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="infos">
			<dl>
				<dd><span><?php echo $topico['respostas']; ?></span> respostas |</dd>
				<dd><span><?php echo $topico['seguidores']; ?></span> seguidores |</dd>
				<dd><span><?php echo $topico['visualizacoes']; ?></span> visualizações</dd>
			</dl>
		</div>
	
		<div class="question-principal">
			<div class="user-left">
				<span>Quem pergunta?</span>
				<img src="https://graph.facebook.com/<?php echo $topico['facebook_id']; ?>/picture?type=normal" />
				<p><?php echo $topico['usuario']; ?></p>
				<p class="amigos"><?php echo $topico['friends']; ?> amigos em comum</p>
			</div>
			<div class="dados-right">
				<p><?php echo $topico['conteudo_adicional']; ?></p>
	
				<?php if($topico['bt_seguir_conversa'] == 1){ ?>
				<a href="#" rel = "<?php print $topico['tid'] ?>" class="seguir transition">seguir conversa</a>
				<?php } ?>
				<a href="javascript: void(0)" class="responder transition">Responder</a>
				<div class="clear"></div>
			</div>
			<div class="form_responder">
				
				<span>Escreva sua resposta:</span>
				<!-- <form action="<?php echo url('appieintercambio/toptips/responder/'.$topico['tid']); ?>" method="POST" > -->
				<form action="" method="POST">
					<input type="hidden" id="tid" name="tid" value="<?php print $topico['tid'] ?>" />
					<input type="hidden" id="facebook_id" name="facebook_id" value="<?php print $topico['usuario_facebook_id'] ?>" />
					<input type="hidden" id="usuario_nome" name="usuario_nome" value="<?php print $topico['usuario_facebook_nome']; ?>" />
					<input type="hidden" id="user_uid" name="user_uid" value="<?php print $user_uid; ?>" />
					<input type="hidden" id="user_fuid" name="user_fuid" value="<?php print $user_fuid; ?>" />
					<textarea name="resposta" id="resposta"></textarea>
					<input type="button" value="Responder" id="botao-resposta" />
					<input type="hidden" name="resposta_id" id="resposta_id" value="0" />
				</form>

				<div class="loadGif">
					<img src="http://static.tumblr.com/d0qlne1/qVol4tb08/loading.gif" alt="">
				</div>

			</div>			
			<div class="clear"></div>
			

			<div class="qtd-resposta"><div id="num_respostas"><?php echo $topico['respostas']; ?></div> resposta(s)</div>
			<div class="respostas">
				<?php
				foreach($respostas as $resposta){ ?>
				<div class="resposta">
					<div class="resposta1" identificador="<?php echo $resposta['rid']; ?>">

				    	<?php if($fuid == $resposta['facebook_id']){ ?>  	
				    	<a class="fechar" href="javascript:void(0)" tipo="resposta" identificador="<?php echo $resposta['rid']; ?>"></a>
				    	<?php } ?>

						<div class="user">
							<img src="http://graph.facebook.com/<?php echo $resposta['facebook_id']; ?>/picture" />
							<p><strong><?php echo $resposta['usuario']; ?></strong><br>
							<?php echo $resposta['data']; ?></p>
						</div>
				
						<div class="texto">
							<p><?php echo $resposta['resposta']; ?></p>
							<?php if(strlen($resposta['replica']) == 0){ ?>
								<a class="transition" id="botao-areplicar" href="#">Resposta</a>
							<?php } ?>	
						</div>

						<div class="envia_replica">
							<span>Escreva sua resposta:</span>
							<form action="" method="POST" id="form-replica">
								<textarea name="replica" id="replica"></textarea>
								<input type="hidden" id="form2_tid" name="form2_tid" value="<?php print $topico['tid'] ?>" />
								<input type="hidden" id="form2_facebook_id" name="form2_facebook_id" value="<?php print $topico['usuario_facebook_id'] ?>" />
								<input type="hidden" id="form2_usuario_nome" name="form2_usuario_nome" value="<?php print $topico['usuario_facebook_nome'] ?>" />
								<input type="hidden" id="user_uid" name="user_uid" value="<?php print $user_uid; ?>" />
								<input type="hidden" id="user_fuid" name="user_fuid" value="<?php print $user_fuid; ?>" />
								<input type="hidden" name="form2_resposta_id" id="form2_resposta_id" value="<?php echo $resposta['rid']; ?>" />
								<input type="button" value="Responder" id="botao-replica" class="botao-replica" class="transition" />
							</form>
							<div class="loadGif">
								<img src="http://static.tumblr.com/d0qlne1/qVol4tb08/loading.gif" width="32" alt="">
							</div>
						</div>
					
					</div>

					<div class="clear"></div>
				
						<?php if(strlen($resposta['replica']) > 0){ ?>
				
							<div class="resposta2" identificador="<?php echo $resposta['replica_rid']; ?>">

						    	<?php if($fuid == $resposta['facebook_id_replica']){ ?>  	
						    	<a class="fechar" href="javascript:void(0)" tipo="resposta" identificador="<?php echo $resposta['replica_rid']; ?>"></a>
						    	<?php } ?>
								
								<div class="user">
									<img src="http://graph.facebook.com/<?php echo $resposta['facebook_id_replica']; ?>/picture" />
									<p><strong><?php echo $resposta['usuario_replica']; ?></strong><br>
									<?php echo $resposta['replica_data']; ?></p>
								</div>
					
								<div class="texto">
									<p><?php echo $resposta['replica']; ?></p>
								</div>
							</div>

						<?php } ?>

					<div class="clear"></div>
				</div>
				<?php } ?>
			</div>
			</div>
	
		<div class="clear"></div>
		
		

	
	</div>
	<div class="clear"></div>
</div>

<div class="footer">
	<div class="barra">
		<div class="footer-1">
			<p>IE Intercâmbio no Exterior - Todos os direitos reservados.</p>
			<a class="transition" href="https://www.kindle.com.br" target="_blank">Kindle</a>
		</div>
	</div>
</div>
  <script type='text/javascript'>
    window.onload=function() {


      altura = screen.height;

      if(altura > 800){
        FB.Canvas.setSize({width:760,height:altura});
    }else{
        FB.Canvas.setSize({width:760,height:document.body.offsetHeight});
      }
      FB.Canvas.scrollTo(0,0);
      
    }
  </script>