	<div class="facebook_share">
		<div class="centraliza">
			<div class="fb-like" data-href="https://www.facebook.com/IEintercambio?ref=ts&amp;fref=ts" data-send="true" data-width="450" data-show-faces="false"></div>
		</div>
	</div>
	<div class="header">
		<div class="header-1">
			<h1>Ie explorer</h1>
			<h2>Um mundo de intercâmbios.</h2>
			<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
		</div>
	</div>
	
	<div class="clear"></div>
	
<div class="limite">
	<div class="content-abas">
			<ul class="botoes">
				<?php // echo $links; ?>
				<li class="quero" rel="quero">
					<a class="quero" href="<?php echo $link_eu_quero; ?>" target="_top"><p>quero<span><b>VIAJAR</b> para</span></p></a>
					<div id="box" class="quero">
						<div class="infoBox txtQuero">
							<span class="setaVermelha"></span>
							<img src="/<?php echo drupal_get_path('theme', 'appiefacebook'); ?>/images/home/mapa-euquero.png" alt="">
							<p>Aqui está a galera que<br /> pretende conhecer novos<br /> lugares em breve.</p>
							<!-- <a class="transition" href="<?php echo $link_eu_quero; ?>" target="_top">saiba mais</a> -->
						</div>
					</div>
				</li>
				<li class="vou" rel="vou">
					<a class="vou" href="<?php echo $link_eu_vou; ?>" target="_top"><p>vou<span> <b>VIAJAR</b> para</span></p></a>
					<div id="box" class="vou">
						<div class="infoBox txtVou">
							<span class="setaVerde"></span>
							<img src="/<?php echo drupal_get_path('theme', 'appiefacebook'); ?>/images/home/mapa-euvou.png" alt="">
							<p>Aqui é o pessoal que <br /> já está com a<br /> passagem na mão.</p>
							<!-- <a class="transition" href="<?php echo $link_eu_vou; ?>" target="_top">saiba mais</a> -->
						</div>
					</div>
				</li>
				<li class="fui" rel="fui">
					<a class="fui" href="<?php echo $link_eu_fui; ?>" target="_top"><p class="">j&aacute;<span><b>VIAJEI</b> para</span></p></a>
					<div id="box" class="fui">
						<div class="infoBox txtFui">
							<span class="setaAzul"></span>
							<img src="/<?php echo drupal_get_path('theme', 'appiefacebook'); ?>/images/home/mapa-eufui.png" alt="">
							<p>Aqui se encontra quem <br> viajou e está cheio <br> de histórias pra contar.</p>
							<!-- <a class="transition" href="<?php echo $link_eu_fui; ?>" target="_top">saiba mais</a> -->
						</div>
					</div>
				</li>
			</ul>
			<div class="imgBase"><img src="/<?php echo drupal_get_path('theme', 'appiefacebook'); ?>/images/home/home.png" width="762" height="410" alt=""></div>
			<div class="clear"></div>
	</div>
	<hr class="abaixo">
	
	<div class="clear"></div>
	<div class="content-bottom">
		<p>Compartilhe
		<span> experiências, <br />
		troque dicas e faça amigos!</span></p>
	</div>
</div>

<div class="footer">
		<div class="footer-1">
			<p>IE Intercâmbio no Exterior - Todos os direitos reservados.</p>
			<a class="transition" href="http://www.kindle.com.br" target="_blank">Kindle</a>
		</div>
</div>

  <script type='text/javascript'>
    window.onload=function() {


      altura = screen.height;

      if(altura > 800){
        FB.Canvas.setSize({width:760,height:altura});
    }else{
        FB.Canvas.setSize({width:760,height:document.body.offsetHeight});
      }
      FB.Canvas.scrollTo(0,0);
      
    }
  </script>