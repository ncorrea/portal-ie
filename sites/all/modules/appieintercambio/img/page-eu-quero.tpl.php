<script type="text/javascript">
jQuery(document).ready(function($){

	$('html, body').animate({
	   		scrollTop: $('.limite').offset().top
    }, 1000);

	$('.opt.trab').click(function(){
		$('select.instituicao').children('option').html('Selecionar empregador');
	});
	$('span.estudar').click(function(){
		$('select.instituicao').children('option').html('Selecione a Instituição / Escola');
	});	
});
</script>

<div class="facebook_share">
	<div class="centraliza">
		<div class="fb-like" data-href="http://facebook.ieintercambio.com.br/appieintercambio" data-send="true" data-width="450" data-show-faces="false"></div>
	</div>
</div>
<div class="header">
	<div class="header-1">
		<h1>Ie explorer</h1>
		<h3><a class="linkTop" href="http://ieintercambio.com.br" target="_blank"><img src="<?php print base_path() . drupal_get_path('theme','appiefacebook') ?>/images/home/logo-ie-home.png" height="84" width="78" alt="IE Intercâmbio no Exterior" title="IE Intercâmbio no Exterior"></a></h3>
	</div>
	<div class="menuPrinc">
		asdasdsa
	<!-- 		<a href="http://facebook.ieintercambio.com.br/appieintercambio" class="transition">Home</a>
			<a href="http://facebook.ieintercambio.com.br/appieintercambio" class="transition">Cadastrar Viagens</a>
			<a href="<?php echo url('appieintercambio/minhasviagens'); ?>" class="transition">Minhas Viagens</a>
			<a href="<?php echo url('appieintercambio/encontre'); ?>" class="transition">Outros Viajantes</a>
			<a class="last" href="<?php echo url('appieintercambio/toptips'); ?>" class="transition">Top Tips</a> -->
	</div>
</div>
<div class="limite">
	<div class="content-top">
		<img src="/<?php echo drupal_get_path('theme', 'appiefacebook'); ?>/images/cadastro/eu-quero/webdoor.png" height="214" width="483" alt="">
		<span class="legend">Vancouver</span>
		<div class="texto">
			<h3>Bem-vindo ao aplicativo da <span>IE Intercâmbio</span></h3>
			<p>Compartilhe sua experiência, peça dicas e troque informações!</p>
		</div>
	</div>
	<div class="clear"></div>
	<div class="content-conteudo">
		<h4>Eu quero</h4>
		<form action="<?php echo url('appieintercambio/cadastrar'); ?>" method="POST">
			<div class="eu-quero ajax-tipo-motivo">
				<div class="opt trab">
					<input type="radio" name="interesse_id" value="1" id="trabalhar">
					<label for="trabalhar">
						<div class="bgradio">
							<span class="trabalhar"><p>Intercambio<br> com Trabalho</p></span>
						</div>
					</label>
				</div>
				
				<div class="opt est">
					<input type="radio" name="interesse_id" value="2" id="estudar">
					<label for="estudar">
						<div class="bgradio">
							<span class="estudar"><p>Estudar<br> no Exterior</p></span>
						</div>
					</label>
				</div>
				
				<div class="opt est-trab">
					<input type="radio" name="interesse_id" value="3" id="explorar">
					<label for="explorar">
						<div class="bgradio">
							<span class="est-trab"><p>Explorar<br> Destinos</p></span>
						</div>
					</label>
				</div>
	
			</div>
			<div class="clear"></div>
			<div class="mapa-paises">
				<div class="localViagem">
					<h5>Aonde?</h5>
					<div class="btTipo">
						<div class="map">Mapa</div>
						<div class="list">Lista de Países</div>
					</div>
					<div class="mapa">
					
					</div>
						
					<div id="lista-paises" class="lista">
					
					</div>
	
				<div class="clear"></div>
				<div class="boxselect">
					<div class="select">
						<select name="cidade_id" class="cidade" id="cidade">
							<option value="0">Cidade</option>
						</select>
	
						<select name="iid" class="instituicao" id="instituicao">
							<option value="0">Instituição/Escola ...</option>
						</select> 
                        
                        <p class="clique-aqui">Não encontrou? <a href="#">Clique aqui</a></p>

						<div id="buscar" class="ui-widget" style="display: none">
							<label for="searchBox">Cidade:</label>
					        <input id="searchBox" />
					        <div id="searchResult" class="ui-widget"></div><!-- style="margin-top: 1em;" -->
					   	</div> 

					   	<input type="hidden" id="latitude_input" name="latitude" value="NULL" />
       					<input type="hidden" id="longitude_input" name="longitude" value="NULL" />
       					<input type="hidden" id="pais_input" name="pais" value="0" />

					</div>
				</div>
				</div>
				<div class="clear"></div>
				<div class="submit">
					<input type="hidden" name="tipo_id" value="1" />
					<input class="transition" type="submit" Value="Participar">
				</div>
			</div>
		</form>
	</div>
</div>
<div class="footer">
	<div class="barra">
		<div class="footer-1">
			<p>Ie Intercâmbio no Exterior - Todos os direitos reservados.</p>
			<a class="transition" href="http://www.kindle.com.br" target="_blank">Kindle</a>
		</div>
	</div>
</div>
<div class="branco"></div>

<div class="bg-preto"></div>
<div class="add-instituicao">
	<label class="nome">Nome da Instituição:</label> 
    <label class="pais">País:</label> 
    <label class="cidade">Cidade:</label> 
    <input name="edit-nome" id="edit-nome" type="text" value="" class="iNome" />
    <input name="edit-cidade" id="edit-cidade" type="text" value="" class="iCidade" />
    <input name="edit-pais" id="edit-pais" type="text" value="" class="iPais" />
    
    <div class="pontilhado"></div>
    <div class="bt-fechar"></div>
    
    <div class="bgradio">
    	<span><a href="#" class="envia_solicitacao">Enviar</a></span>
    </div>
</div>