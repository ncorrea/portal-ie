<?php

# Perfil do Usuário ( Minhas Viagens )
function appieintercambio_minhasviagens(){

  $facebook = facebook_client();
  $fuid = $facebook->getUser();
  $uid = getUid($fuid);

  $key = variable_get('bing_api_key');

  drupal_add_js("https://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&s=1&mkt=pt-BR","external");  

  drupal_add_js(drupal_get_path('module','appieintercambio') . '/js/bingmapscontrol.js');
  drupal_add_js(drupal_get_path('theme', 'appiefacebook') . '/js/minhasviagens.js');
  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
  drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/viagens.css');
  drupal_add_js(drupal_get_path('theme', 'appiefacebook') .'/js/autogrow.js', 'file');
  drupal_add_js('
  jQuery(document).ready(function($){

    var basepath = Drupal.settings["appieintercambio"]["basepath"];

    $("html").ajaxStart(function () {
        $("body").css("cursor", "wait");
    });

    $("html").ajaxComplete(function () {
        $("body").css("cursor", "auto");
    });
  
    $("textarea").autoGrow(); 

    $(".excluir").click(function(){           
      var obj = $(this);
      var tid = $(this).attr("rel");      
      $.post(basepath + "appieintercambio/unfollow", {"tid":tid}, function(data){      
          obj.parent().parent().parent().fadeOut();
      });   
      
    return false;
    });

    $(".container-respostas").hide();
  
    $(".resposta p a").click(function(){
       var container = $(this).parent().parent().next();
       var tid = $(this).attr("rel");       
       
      $.get(basepath + "appieintercambio/respostas", {"tid":tid,"user_uid" : '.$uid.'}, function(data){
          container.html("<h2>Respostas:</h2>" + data);
          container.slideToggle("fast");    
      });   

    });

    $("textarea").keypress(function(event){
      
      var tid = $(this).attr("rel");
      var resposta = $(this).val();
      var container = $(".tid-" + tid);
      var textarea = $(this);

      if ( event.which == 13 ) {        
        
        
        textarea.attr("disabled", true); 

        $.post(basepath + "appieintercambio/toptips/responder", {"tid":tid, "resposta":resposta}, function(data){
              container.html(data);                 
              container.prepend("<h2>Respostas:</h2>");
              textarea.val("");
              textarea.removeAttr("disabled"); 
              container.slideDown("fast"); 
          });             

        event.preventDefault();
        }         
    });

 })','inline');

  drupal_set_title(t("Minhas Viagens"));

  $facebook = facebook_client();
  $fuid = $facebook->getUser(); 

  $uid = getUid($fuid);

  $mural = getMural($uid); 
  $usuariofb = getUserData($fuid);

  $conteudo['user'] = theme('user-header-profile', array('user' => $usuariofb));
  $conteudo['mural'] = theme('mural-post-wrapper', array('content' => $mural));

  /*
  if ($fuid == 0){
    // drupal_goto('appieintercambio');
  }
  else
  {
    return theme('page-minhas-viagens',array('content' => $conteudo));       
  }
  */

  return theme('page-minhas-viagens',array('content' => $conteudo));       
  
    
}


function appieintercambio_minhasviagens_getMap(){   

    $tipomapa =  $_POST['tipomapa'];
    $escopo = $_POST['escopo'];

    $facebook = facebook_client();
    $fuid = $facebook->getUser();

    $trips = getViagens($tipomapa,$escopo,$fuid);  

    while ($rows = $trips->fetchAssoc()){
      $meujson[] = array(
        'latitude' => $rows['latitude'],
        'longitude' => $rows['longitude'],
        'tipo' => $rows['tipo_id'],
        'pais_id' => $rows['pid'],
        'pais_nome' => $rows['nome'], 

        'cidade_nome' => $rows['name'], 

        'iid' => $rows['iid'],
        'elat' => $rows['elat'],
        'elong' =>$rows['elong']
      ); 
    };

    print drupal_json_output($meujson);

}

/** 
    Retorna latitude e longitude das viagens do usuario corrente
    ou dos amigos do usuário
 */
function getViagens($tipo,$escopo,$fuid){
    
    $query = db_select('appieintercambio_viagem', 'v');
    $query->leftjoin('appieintercambio_cidades','c','v.cidade = c.id');
    $query->leftjoin('appieintercambio_pais','p','v.pid = p.pid');
        
    $query->fields('v', array('iid','uid','facebook_id','tipo_id','cidade','pid'));

    // $query->fields('c', array('latitude','longitude'));
    $query->fields('c', array('name', 'latitude','longitude'));
    $query->addField('v','latitude','elat');
    $query->addField('v','longitude','elong');
    
    $query->fields('p',array('nome'));
    
    $query->groupBy('tipo_id');
    $query->groupBy('cidade');   
      
    switch ($escopo) {
      case '1':
        $friends = getFriends($fuid);
        $query->condition('v.facebook_id',$friends,'IN');
      break;
      
      case '0':
        $query->condition("v.facebook_id",$fuid);
      break;

      case '3':
        //$query->condition("v.facebook_id",$user);
      break;
    }
    
    $query->condition('v.tipo_id',$tipo,'IN');
    $result = $query->execute();

    return $result;

}

function appieintercambio_all_users_country(){
  $pin = $_POST['pin'];  
  $page = $_POST['page'];

  $result = getUsersCountry($pin,$page);

  $items = $result->rowCount();

  foreach($result as $row){

    if($row->facebook_id != 0)
    {
      $meujson[] = array(
        'uid' => $row->uid,
        'facebook_id' => $row->facebook_id, 
        'tipo' => $row->tipo_id,
        'items' => $items,

        'interesse_id' => $row->interesse_id,
        'descricao_tipo' => appieintercambio_getVerboPorTipo($row->tipo_id),
        'descricao_interesse' => appieintercambio_getNameInteresse($row->interesse_id),

      );    
    }

  }

  print drupal_json_output($meujson);

}

/**
  Retorna usuarios que viajaram para um determinado país 
**/
function getUsersCountry($pin,$page = 1){

  $range = 4;  
  $start = ((($page) * ($range)) - $range);
  
  $config = explode('/', $pin);
  
  $pais_id = $config[0];
  $tipo_id = $config[1];
  $iid = $config[2];  

  $cidade = $config[3];  

  $query = db_select('appieintercambio_viagem', 'v');
  //$query->leftjoin('appieintercambio_instituicao','i','v.iid = i.iid');
  //$query->join('appieintercambio_cidades','c','v.cidade = c.id');
  

  // $query->fields('v', array('uid','facebook_id','tipo_id'));    
  $query->fields('v', array('uid','facebook_id','tipo_id','interesse_id'));    
  
  $query->condition('v.tipo_id',$tipo_id);  
  //$query->condition('v.iid',$iid);
  $query->condition('v.pid',$pais_id);
  $query->groupBy('facebook_id');  

  $query->range($start,$range);

  $rows = $query->execute();

  return $rows;
}

function appieintercambio_getNameInteresse($interesse_id){

  switch($interesse_id){
    case 1:
      return 'trabalhar em';
      break;
    case 2:
      return 'estudar em';
      break;
    case 3:
      return 'viajar para';
      break;
  }

}

function appieintercambio_getNameTipo($tipo_id){

  switch($tipo_id){
    case 1:
      return 'Eu quero';
      break;
    case 2:
      return 'Eu vou';
      break;
    case 3:
      return 'Eu fui';
      break;
  }

}

function appieintercambio_getVerboPorTipo($tipo_id){
  switch($tipo_id){
    case 1:
      return 'quer';
      break;
    case 2:
      return 'vai';
      break;
    case 3:
      return 'foi';
      break;
  }  
}

function getMural($uid){
  
  $mural = array();


  $query = db_select('appieintercambio_seguidores','s');
  $query->leftjoin('appieintercambio_topico','t','s.tid = t.tid');
  $query->join('users','u','t.uid = u.uid');
  // $query->fields('t',array('tid','pergunta','tags','created')); 
  $query->fields('t',array('tid','pergunta','tags','created', 'tipo_id')); 

  $query->fields('u',array('name'));
  $query->condition('s.uid',$uid);
  //$query->groupBy('tid','uid');
  $result = $query->execute();

  /**
    ao criar um topico eu automaticamento insiro uma linha em appieintercambio_seguidores
    ao postar no mural de um user , eu crio um topico ,, e associo uid do user no qual estou postando e o id do topico que eu criei
  **/
  
  while ($row = $result->fetchAssoc()) {
     // $mural[] = array('data'=>date('d/m',$row['created']),'nome' => $row['name'], 'tid' => $row['tid'], 'pergunta' => $row['pergunta'], 'tags' =>  $row['tags'], 'respostas' => getRespostasCount($row['tid']));
    $mural[] = array('data'=>date('d/m',$row['created']),'nome' => $row['name'], 'tipo_id' => $row['tipo_id'], 'tid' => $row['tid'], 'pergunta' => $row['pergunta'], 'tags' =>  $row['tags'], 'respostas' => getRespostasCount($row['tid']));
  }

  return $mural;

}

/** 
  Retorna lista de amigos do usuario corrente que também curtem o app 
*/
function getFriends($fuid){

  $facebook = facebook_client();
  $friends = $facebook->api('/'. $fuid .'/friends');    

  $friendsList = array();
  
  foreach ($friends as $friend){
      foreach ($friend as $f){
        $friendsList[] = $f['id'];
      }
  }


  $query = db_select('appieintercambio_usuariosfacebook','u');
  $query->fields('u',array('facebook_id'));
  $query->condition('u.facebook_id',$friendsList,'IN');
  $result = $query->execute()->fetchCol();

  return $result;

}
