<?php

function appieintercambio_encontre(){
	
	drupal_set_title('Encontre');

	drupal_add_js(drupal_get_path("module","appieintercambio")."/js/date.js");		
	drupal_add_js(drupal_get_path("module","appieintercambio")."/js/appieintercambio_busca.js","file");

	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/busca-viajantes.css');

	$formulario_busca = drupal_get_form('form_busca');
	$formulario_filtro_busca = drupal_get_form('form_filtra_busca');

	return theme('page-encontre', array('content' => NULL, 'form_busca' => $formulario_busca, 'form_filtra_busca' => $formulario_filtro_busca ));
}

function appieintercambio_encontre_user($uid){
	
	drupal_add_js(drupal_get_path("module","appieintercambio")."/js/encontre_user.js","file");
	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/global.css');
	drupal_add_css(drupal_get_path('theme', 'appiefacebook') . '/css/busca-interna.css');
	drupal_add_js(
	'jQuery(document).ready(function($) {
		
		var basepath = Drupal.settings["appieintercambio"]["basepath"];   

		$("#postmural").click(function(){
			
			var uid = $("#uid").val();
			var mensagem = $("#mensagem").val();

			$.post(basepath + "appieintercambio/postmural", {"uid":uid, "mensagem":mensagem }, function(data) {				
			    $(".bg-preto").hide(450);
        	    $(".bordaPublicar").hide(400);
            	$(".publicarMural").hide(300);
            	$(".bts textarea").empty();		 		
				
				return false;
			});
		});

	})','inline');

	
	/**
		getPaisesVisitados recebe um $result com FetchAll()
	*/
	$paisesVisitados = getPaisesVisitados($uid);

	$quero = array();
	$vou = array();
	$fui = array();

	foreach ($paisesVisitados as $p) {
		switch ($p->tipo_id) {
			case 1:
				
				$pais = file_load($p->fid);
				//$inst = file_load($p->ifid);

				$quero['paises'][] = array('flag' => file_create_url($pais->uri), 'pid' => $p->pid);

				switch ($p->funcao) {
						case '2':			
							//$quero['escolas'][] = array('flag' => file_create_url($inst->uri),'pid' => $p->pais_id,'titulo' => $p->titulo);
							$quero['escolas'][] = array('interesse' => $p->interesse_id, 'pid' => $p->pid,'titulo' => $p->titulo);
							break;
						
						case '1':
							//$quero['empregadores'][] = array('flag' => file_create_url($inst->uri),'pid' => $p->pais_id,'titulo' => $p->titulo);
							$quero['empregadores'][] = array('interesse' => $p->interesse_id, 'pid' => $p->pid,'titulo' => $p->titulo);
							break;
					}
				break;
			
			case 2:
				$pais = file_load($p->fid);
				//$inst = file_load($p->ifid);				
				$vou['paises'][] =  array('flag' => file_create_url($pais->uri), 'pid' => $p->pid);

				switch ($p->funcao) {
						case '2':			
							//$vou['escolas'][] = array('flag' => file_create_url($inst->uri),'pid' => $p->pais_id,'titulo' => $p->titulo);
							$vou['escolas'][] = array('interesse' => $p->interesse_id, 'pid' => $p->pid,'titulo' => $p->titulo);
							break;
						
						case '1':
							//$vou['empregadores'][] = array('flag' => file_create_url($inst->uri),'pid' => $p->pais_id,'titulo' => $p->titulo);
							$vou['empregadores'][] = array('interesse' => $p->interesse_id, 'pid' => $p->pid,'titulo' => $p->titulo);
							break;
					}
				break;

			case 3:
				$pais = file_load($p->fid);
				//$inst = file_load($p->ifid);
				$fui['paises'][] =  array('flag' => file_create_url($pais->uri), 'pid' => $p->pid);

				switch ($p->funcao) {
						case '2':			
							//$fui['escolas'][] = array('flag' => file_create_url($inst->uri),'pid' => $p->pais_id,'titulo' => $p->titulo);
							$fui['escolas'][] = array('interesse' => $p->interesse_id, 'pid' => $p->pid,'titulo' => $p->titulo);
							break;
						
						case '1':
							//$fui['empregadores'][] = array('flag' => file_create_url($inst->uri),'pid' => $p->pais_id,'titulo' => $p->titulo);
							$fui['empregadores'][] = array('interesse' => $p->interesse_id, 'pid' => $p->pid,'titulo' => $p->titulo);
							break;
					}
				break;
		}

	}

  	$fuid = getFacebookId($uid);  
 
	$user = getUserData($fuid);	

  	$friends = getFriends($fuid);

	$usuario = theme('user-header-profile-friend', array('user' => $user, 'friends' => $friends));

	return theme('page-encontre-user', array('user' => $usuario, 'quero' => $quero, 'vou' => $vou, 'fui' => $fui, 'uid' => $uid));
}


function appieintercambio_encontre_getUsuarios(){

	$tipo = $_POST['tipo'];
	$objetivo = $_POST['objetivo'];
	$pais = $_POST['pais'];
	$cidade = $_POST['cidade'];

	$data_ida = null;
	$data_volta = null;

	$filtro = $_POST['filtro'];

	if(strlen($_POST['data_ida']) > 0)
	{
		$data_ida = strtotime(str_replace('/','-',$_POST['data_ida'])); //YearMonthDay
	}

	if(strlen($_POST['data_volta']) > 0)
	{
		$data_volta = strtotime(str_replace('/','-',$_POST['data_volta'])); //YearMonthDay
	}	
	
	$result = _getUsuarios($tipo, $objetivo, $pais, $cidade, $data_ida, $data_volta, $filtro);
	
 	print theme('facebook-profile-wrapper', array('user' => $result));
}


function _getUsuarios($tipo, $objetivo, $pais, $cidade, $data_ida, $data_volta, $filtro){

	$query = db_select('appieintercambio_viagem','v');		


	if($pais != '0')
	{
		$query->innerjoin('appieintercambio_pais','p','v.pid = p.pid');
		$query->condition('v.pid',$pais);	
	}

	if($cidade != '0')
	{
		$query->innerjoin('appieintercambio_cidades','c','v.cidade = c.id');
		$query->condition('c.id',$cidade);	
	}

	$query->innerjoin('users','u','v.uid = u.uid');
	
	$query->fields('v',array('iid','data_ida','data_volta','facebook_id'));
	$query->fields('u',array('name','uid'));

	$query->condition('v.tipo_id',$tipo);
	$query->condition('v.interesse_id',$objetivo);
		
	$query->groupBy('facebook_id');

	if($data_ida != '0' && $data_volta != '0')
	{
		$query->condition('data_ida',$data_ida,'>=');
		$query->condition('data_volta',$data_volta,'<=');
	}	

	$result = $query->execute();	

	$resultado = array();
	
	foreach ($result as $friend) {

		if($friend->facebook_id != 0 && $friend->uid != 0)
		{
			if ($filtro == "1") {
				$eh_amigo = is_friend($friend->facebook_id);
				if ($eh_amigo) {
					$resultado[] = array('iid' => $friend->iid, 'data_ida' => $friend->data_ida, 'data_volta' => $friend->data_volta, 'facebook_id' => $friend->facebook_id, 'name' => $friend->name, 'uid' => $friend->uid );
				}
			} else {
				$resultado[] = array('iid' => $friend->iid, 'data_ida' => $friend->data_ida, 'data_volta' => $friend->data_volta, 'facebook_id' => $friend->facebook_id, 'name' => $friend->name, 'uid' => $friend->uid );
			}			
		}

	}
	

	return $resultado;
}

function form_busca($form, &$form_state){
	$objetivos = array(
		0 => t('Fazer o que?'),
		1 => t('Trabalhar'),
		2 => t('Estudar'),
		3 => t('Explorar'),
	);

	$paises = getPaises();
	$cidades = array(0 => 'Selecionar Cidade');
	
	$form['busca'] = array(
		'#type' => 'fieldset',

	);

	$form['busca']['camposlocal'] = array(
		'#type' => 'fieldset',
		'#attributes' => array(
			'class' => array('camposlocal'),
		),
	);

	$form['busca']['camposlocal']['objetivo'] = array(
		'#title' => t('Pra quê?'),
		'#type' => 'select',
		'#options' => $objetivos,
	);

	$form['busca']['camposlocal']['pais'] = array(
		'#title' => t('Qual lugar?'),
		'#type' => 'select',
		'#options' => $paises,
	);

	$form['busca']['camposlocal']['cidade'] = array(
		'#type' => 'select',
		'#options' => $cidades,		
	);

	$form['busca']['camposdata'] = array(
		'#type' => 'fieldset',
		'#attributes' => array(
			'class' => array('camposdata'),
		),
	);

	$form['busca']['camposdata']['ida'] = array(
		// '#prefix' => '<h3>Quando?</h3>',
		'#title' => 'Ida',
		'#type' => 'date_select',
		'#date_format' => 'm-Y',
		'#date_year_range' => '-10:+10',
		'#default_value' => '0',
		// '#default_value' => date('m-Y',time()),
	);

	$form['busca']['camposdata']['volta'] = array(
		'#title' => 'Volta',
		'#type' => 'date_select',
		'#date_format' => 'm-Y',
		'#date_year_range' => '-10:+10',
		'#default_value' => '0',
		// '#default_value' => date('m-Y',time()),
	);

	$form['busca']['enviar'] = array(
		'#type' => 'submit',
		'#value' => t('Procurar!'),
	);

	return $form;
}


function form_filtra_busca($form,&$form_state){
	$estados = array();
	$criterios = array(0 => 'Todos', 1 => 'Meus Amigos');	

	$form['filtro'] = array(
		'#title' => 'Filtrar',
		'#type' => 'fieldset',
	);

	$form['filtro']['criterio'] = array(
		'#type' => 'radios',
		'#options' => $criterios,
	);

	return $form;
}

/** 
  Retorna a contagem de amigos do usuario corrente que também curtem o app 
  para ser usada em user-header-profile-friend
*/
function getFriends($fuid){

  $facebook = facebook_client();

  try {


	$friends = $facebook->api('/'. $fuid .'/friends');    

   } catch (FacebookApiException $e) {

  		error_log($e);

        $user = null;
        $friends = array(array('vazio'=>'vazio'));

   }


  $friendsList = array();
  
  foreach ($friends as $friend){
      foreach ($friend as $f){
        $friendsList[] = $f['id'];
      }
  }


  $query = db_select('appieintercambio_usuariosfacebook','u');
  $query->fields('u',array('facebook_id'));
  $query->condition('u.facebook_id',$friendsList,'IN');
  $result = $query->execute()->rowCount();

  return $result;
}

/**
	Verifica se o usuário corrente ja é amigo do usuário sendo visualizado 
	na página encontre/user/[uid]
*/

function is_friend($fuid){
	$facebook = facebook_client();

  	try {


	$friends = $facebook->api('/me/friends');    

   } catch (FacebookApiException $e) {

  		error_log($e);

        $user = null;
        $friends = array(array('vazio'=>'vazio'));

   }


  $friendsList = array();
  
  foreach ($friends as $friend){
      foreach ($friend as $f){
        $friendsList[] = $f['id'];
      }
  }

  if (in_array($fuid, $friendsList)){
  	return true;
  } else {
  	return false;
  }

}
