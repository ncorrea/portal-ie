﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

// Register a templates definition set named "default".
CKEDITOR.addTemplates( 'default',
{
	// The name of sub folder which hold the shortcut preview images of the
	// templates.
	imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

	// The templates definitions.
	templates :
		[
		{
				title: 'Box 1 coluna com barra',
				image: 'images_templates_02.png',
				description: 'Box 1 coluna com barra',
				html:
					'<div id="box1col">'+
							'<div class="fltlft boxIcon icone boxcolor"></div>'+
							'<div class="fltlft boxTitle">Título</div>'+
							'<div class="clear"></div>'+
							'<div class="fltlft boxBar"></div>'+
							'<div class="fltlft box">'+
								'<div class="detail"></div>'+
								'<h4>Lorem ipsum</h4>'+
								'<p class="subtitle">Lorem ipsum</p>'+
								'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
								'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
								'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
								'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse'+
								'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non'+
								'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'+
							'</div>'+
					'</div>'
			},
		{
				title: 'Box 1 coluna sem barra',
				image: 'images_templates_04.png',
				description: 'Box 1 coluna sem barra',
				html:
					'<div id="box1colnobar">'+
							'<div class="fltlft boxIcon icone boxcolor"></div>'+
							'<div class="fltlft boxTitle">Título</div>'+
							'<div class="clear"></div>'+
							'<div class="fltlft boxBar"></div>'+
							'<div class="fltlft box">'+
								'<div class="detail"></div>'+
								'<h4>Lorem ipsum</h4>'+
								'<p class="subtitle">Lorem ipsum</p>'+
								'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
								'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
								'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
								'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse'+
								'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non'+
								'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'+
							'</div>'+
					'</div>'
			},
		{
				title: 'Box 1 coluna grande',
				image: 'images_templates_22.png',
				description: 'Box 1 coluna grande',
				html:
					'<div id="box1colbig">'+
						'<div class="fltlft boxIcon icone boxcolor"></div>'+
						'<div class="fltlft boxTitle">Título</div>'+
						'<div class="clear"></div>'+
						'<div class="fltlft boxBar"></div>'+
						'<div class="box">'+
							'<div class="detail"></div>'+
							'<h4>Lorem ipsum</h4>'+
							'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
							'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
							'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
							'consequat. </p>'+
							'<ul>'+
								'<li>lorem ipsum dolor sit amet</li>'+
								'<li>lorem ipsum dolor sit amet</li>'+
								'<li>lorem ipsum dolor sit amet</li>'+
							'</ul>'+
						'</div>'+
					'</div>'
			},

					{
				title: 'Bloco 2 Colunas Pequeno',
				image: 'images_templates_14.png',
				description: 'Bloco 2 Colunas Pequeno',
				html:
				'<div id="box2colsmall">'+
					'<div class="col01">'+
						'<span class="detailBGt">&nbsp;</span>'+
						'<span class="detailBGb">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<ul>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
						'</ul>'+

						'<h5>Lorem ipsum</h5>'+
						'<ul>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
						'</ul>'+
					'</div>'+
					'<div class="col02">'+
						'<span class="detailBGt">&nbsp;</span>'+
						'<span class="detailBGb">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<ul>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
						'</ul>'+
					'</div>'+
				'</div>'
			},
			{
				title: 'Bloco modelo 02 - 2 Colunas Pequeno',
				image: 'images_templates_14.png',
				description: 'Bloco 2 Colunas modelo 02 Pequeno',
				html:
				'<div id="box2colsmall02">'+
					'<div class="fltlft boxIcon icone boxcolor"></div>'+
					'<div class="fltlft boxTitle">Título</div>'+
					'<div class="clear"></div>'+
					'<div class="col01">'+
						'<span class="detailBGt">&nbsp;</span>'+
						'<span class="detailBGb">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<ul>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
						'</ul>'+

						'<h5>Lorem ipsum</h5>'+
						'<ul>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
						'</ul>'+
					'</div>'+
					'<div class="col02">'+
						'<span class="detailBGt">&nbsp;</span>'+
						'<span class="detailBGb">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<ul>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
							'<li>lorem</li>'+
						'</ul>'+
					'</div>'+
				'</div>'
			},
					{
				title: 'Bloco Full 3 Colunas',
				image: 'images_templates_30.png',
				description: 'Bloco Full 3 colunas',
				html:
				'<div id="boxfull2colbig">'+
					'<div class="col01">'+
						'<h5> Lorem ipsum</h5>'+
						'<ul>'+
							'<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</li>'+
							'<li>Lorem ipsum dolor sit amet, consectetuer adipiscing e sit amet,</li>'+
							'<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</li>'+
							'<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</li>'+
							'<li>Lorem ipsum</li>'+
						'</ul>'+

						'<ul>'+
							'<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</li>'+
							'<li>LLorem ipsum dolor sit amet, consectetuer adipiscing ;</li>'+
							'<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</li>'+
							'<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</li>'+
							'<li>Lorem ipsum</li>'+
						'</ul>'+

					'</div>'+
					'<div class="col02">'+
						'<h5> Lorem ipsum</h5>'+
						'<div class="primeiralista">'+
							'<ul>'+
								'<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</li>'+
								'<li>Lorem ipsum dolor sit amet, consectetuer adipiscing e sit amet,</li>'+
								'<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</li>'+
								'<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</li>'+
								'<li>Lorem ipsum</li>'+
							'</ul>'+
						'</div>'+

						'<ul>'+
							'<li>Lorem ipsum dolor</li>'+
							'<li>LLorem ipsum dolor</li>'+
							'<li>Lorem ipsum dolor</li>'+
							'<li>Lorem ipsum dolor</li>'+
							'<li>Lorem ipsum</li>'+
						'</ul>'+

						'<div class="boxlink">'+
							'<a href="javascript:void(0)" class="link">link</a>'+
						'</div>'+

					'</div>'+
				'</div>'
			},
		{
				title: 'Bloco 2 Colunas',
				image: 'images_templates_12.png',
				description: 'Bloco 2 colunas',
				html:
				'<div id="box2col">'+
					'<div class="col01 fltlft">'+
						'<span class="detailBGr">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
							'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
							'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
							'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse'+
							'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non'+
							'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'+
					'</div>'+
					'<div class="col02 fltlft">'+
						'<span class="detailBGl">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<ul>'+
							'<li>lorem ipsum</li>'+
							'<li>lorem ipsum</li>'+
							'<li>lorem ipsum</li>'+
							'<li>lorem ipsum</li>'+
						'</ul>'+
					'</div>'+
					'</div>'+
				'</div>'
			},
		{
				title: 'Bloco 3 Colunas',
				image: 'images_templates_20.png',
				description: 'Bloco 3 colunas',
				html:
				'<div id="box3col">'+
					'<div class="col01 fltlft">'+
						'<span class="detailBGr">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
							'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
							'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
							'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse'+
							'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non'+
							'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'+
					'</div>'+
					'<div class="col02 fltlft">'+
						'<span class="detailBGl">&nbsp;</span>'+
						'<span class="detailBGr">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
							'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
							'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
							'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse'+
							'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non'+
							'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'+
					'</div>'+
					'<div class="col03 fltlft">'+
						'<span class="detailBGl">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
							'tempor incididunt ut labore et dolore magna aliqua.</p>'+
						'<ul>'+
							'<li>lorem ipsum</li>'+
							'<li>lorem ipsum</li>'+
							'<li>lorem ipsum</li>'+
							'<li>lorem ipsum</li>'+
						'</ul>'+
					'</div>'+
				'</div>'
			},
			{
				title: 'Bloco 3 Colunas Com Título',
				image: 'images_templates_25.png',
				description: 'Bloco 3 colunas com título',
				html:
				'<div id="box3colbig">'+
						'<div class="fltlft boxIcon icone boxcolor"></div>'+
						'<div class="fltlft boxTitle">Título</div>'+
						'<div class="clear"></div>'+
						'<div class="fltlft boxBar"></div>'+
						'<div class="fltlft col01">'+
							'<div class="detail"></div>'+
							'<div clas="img">'+
								'<span class="detailBGt">&nbsp;</span>'+
								'<img src="img/img01.jpg" height="171" width="291" alt="">'+
							'</div>'+
							'<h5>Lorem ipsum</h5>'+
							'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
							'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Ut enim ad minim veniam.</p>'+
							'<div class="boxlink">'+
								'<a href="javascript:void(0)" class="link">link</a>'+
							'</div>'+
						'</div>'+
						'<div class="fltlft col02">'+
							'<div class="detail"></div>'+
							'<div clas="img">'+
								'<span class="detailBGt"></span>'+
								'<img src="img/img02.jpg" height="171" width="291" alt="">'+
							'</div>'+
							'<h5>Lorem ipsum</h5>'+
							'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
							'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Ut enim ad minim veniam.</p>'+
							'<div class="boxlink">'+
								'<a href="javascript:void(0)" class="link">link</a>'+
							'</div>'+
						'</div>'+
						'<div class="fltlft col03">'+
							'<div class="detail"></div>'+
							'<div clas="img">'+
								'<span class="detailBGt">&nbsp;</span>'+
								'<img src="img/img03.jpg" height="171" width="291" alt="">'+
							'</div>'+
							'<h5>Lorem ipsum</h5>'+
							'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
							'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Ut enim ad minim veniam.</p>'+
							'<div class="boxlink">'+
								'<a href="javascript:void(0)" class="link">link</a>'+
							'</div>'+
						'</div>'+
				'</div>'
			},
			{
				title: 'Bloco 3 Colunas Com  imagem e Título',
				image: 'images_templates_07.png',
				description: 'Bloco 3 colunas com imagem e título',
				html:
				'<div id="boxfull3col">'+
					'<div class="col01">'+
						'<img src="img/Visa-Travel-Money.jpg" height="347" width="315" alt="">'+
					'</div>'+
					'<div class="col02">'+
						'<h5>Lorem ipsum</h5>'+
						'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
							'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
							'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
							'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse'+
							'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non'+
							'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'+
					'</div>'+
					'<div class="col03">'+
						'<h5>Lorem ipsum</h5>'+
						'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
							'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
							'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
							'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse'+
							'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non'+
							'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'+
					'</div>'+
				'</div>	'
			},
			{
				title: 'Bloco 4 Colunas',
				image: 'images_templates_18.png',
				description: 'Bloco 4 colunas',
				html:
				'<div id="box4col">'+
					'<div class="col01">'+
						'<span class="detailBGr-2">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
						'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
						'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
						'consequat.</p>'+
					'</div>'+
					'<div class="col02">'+
						'<span class="detailBGl-2">&nbsp;</span>'+
						'<span class="detailBGr-2">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
						'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
						'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
						'consequat.</p>'+
					'</div>'+
					'<div class="col03">'+
						'<span class="detailBGl-2">&nbsp;</span>'+
						'<span class="detailBGr-2">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
						'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
						'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
						'consequat.</p>'+
					'</div>'+
					'<div class="col04">'+
						'<span class="detailBGl-2">&nbsp;</span>'+
						'<h5>Lorem ipsum</h5>'+
						'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'+
						'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'+
						'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'+
						'consequat.</p>'+
					'</div>'+
				'</div>'
			},
			{
				title: 'Galeria 01',
				image: 'galeria01.jpg',
				description: 'Galeria 01',
				html:
				'<div class="galeria01">'+
				'</div>'
			},
			{
				title: 'Galeria 02',
				image: 'galeria02.jpg',
				description: 'Galeria 02',
				html:
				'<div class="galeria02">'+
				'</div>'
			},
			{
				title: 'Galeria 03',
				image: 'galeria03.jpg',
				description: 'Galeria 03',
				html:
				'<div class="galeria03">'+
				'</div>'
			},
			{
				title: 'Galeria 04',
				image: 'galeria04.jpg',
				description: 'Galeria 04',
				html:
				'<div class="galeria04">'+
				'</div>'
			},
			{
				title: 'Módulo de datas',
				image: 'modulo_datas.jpg',
				description: 'Módulo de datas',
				html:
				'<div class="modulo_datas">'+
				'</div>'
			}
		]
});