﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	config.filebrowserBrowseUrl = '/sites/all/libraries/ckeditor/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = '/sites/all/libraries/ckeditor/ckfinder/ckfinder.html?Type=Images';
    config.filebrowserFlashBrowseUrl = '/sites/all/libraries/ckeditor/ckfinder/ckfinder.html?Type=Flash';
    config.filebrowserUploadUrl = '/sites/all/libraries/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type;=Files';
    config.filebrowserImageUploadUrl = '/sites/all/libraries/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type;=Images';
    config.filebrowserFlashUploadUrl = '/sites/all/libraries/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type;=Flash';

};
