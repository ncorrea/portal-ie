<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>

<?php 

$tax_escola = taxonomy_term_load($row->_field_data['nid']['entity']->field_promocoes_escola['und'][0]['tid']);

$tax_escola_logo = image_style_url('imagem_empresa_logo',$tax_escola->field_escolas_logo['und'][0]['uri']);

?>

<?php // print $output; ?>

<div class="taxonomy_name">
  <?php echo $tax_escola->name; ?>
</div>
<div class="taxonomy_empresa_logo">
  <?php echo '<img src="'.$tax_escola_logo.'" />'; ?>
</div>