<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>
<head profile="<?php print $grddl_profile; ?>">
  <?php print $head; ?>
  <link rel="shortcut icon" href="<?php print drupal_get_path('theme','iesite'); ?>/img/favicon.ico" />
  <title><?php print $head_title; ?></title>
  <meta name="robots" content="all" />
  <meta name="copyright" content="2013 IE Agencia de Viagens e Turismo Ltda." />
  <meta name="keywords" content="ie, intercambio, viajar, estudar exterior, trabalhar exterior" />
  <meta name="description" content="A IE é a rede de agências de intercâmbio e turismo que mais cresce no país e oferece as melhores opções para você viajar, estudar e trabalhar no exterior." />
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!-- Start Alexa Certify Javascript -->
  <script type="text/javascript">
  _atrk_opts = { atrk_acct:"qNcph1aMQV00Ov", domain:"ieintercambio.com.br",dynamic: true};
  (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
  </script>
  <!--<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=qNcph1aMQV00Ov" style="display:none" height="1" width="1" alt="" /></noscript>-->
  <!-- Start Google Analytics Javascript -->
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-3881937-1'],['b._setAccount', 'UA-24421697-1']);
    _gaq.push(['_setDomainName', 'ieintercambio.com.br']);
    _gaq.push(['_trackPageview'],['b._trackPageview']);
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  </script>
    
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
