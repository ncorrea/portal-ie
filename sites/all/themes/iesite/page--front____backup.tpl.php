<?php
 drupal_add_css(drupal_get_path('theme', 'iesite') .'/css/home.css');
 drupal_add_js(drupal_get_path('theme', 'iesite') .'/js/jquery.easing.1.3.js');
?>
  <?php print render($page['header']); ?>
  <div id="master">      
      <div id="header">
        <div class="header-1">
          <div class="header-2">
            <a href="<?php print base_path() ?>" class="logo"><img src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/logo.png" height="96" width="101" alt="IE intercâmbio"></a>
            <div class="busca">
                <div class="form-busca">
                  <form action="javascript;void(0)">
                  <label class="option" for="input-busca" style="display: block;"> Digite o que procura que a gente ajuda </label>
                  <input title="Digite os termos que você deseja procurar." type="text" name="search_block_form" value="" size="15" id="input-busca" class="input-busca">
                </div>
                <div class="bt_submit">
                  <input type="submit" name="op" value="Buscar" class="form-submit">
                  </form>
                </div>
             <?php //print render($page['busca']); ?>
            </div>
            <div class="menu-principal">
              <ul class="primeiro-nivel">
                <li class="expanded l01" style="height: 45px;">
                  <span title="" class="nolink">Estudar</span>
                  <dl class="segundo-nivel" style="display: none;">
                    <div class="col01">
                      <dd class="collapsed"> 
                        <a href="<?php print base_path() ?>estudar/curso-de-idiomas/programa">Curso de Idiomas</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php print base_path() ?>estudar/teen-experience/programa">Teen Experience</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php print base_path() ?>estudar/high-school/programa">High School</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php print base_path() ?>estudar/study-and-work/programa">Study And Work - Estudo e Trabalho</a> 
                      </dd>
                    </div>
                    <div class="col02">
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print base_path() ?>estudar/seguro-iac">Seguro IAC</a> 
                      </dd>
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print base_path() ?>estudar/visa-travel-money">Visa Travel Money</a> 
                      </dd>
                    </div>
                  </dl>
                </li>
                <li class="expanded l02">
                  <span title="" class="nolink">Trabalhar</span>
                  <dl class="segundo-nivel" style="display: none;">
                    <div class="col01">
                      <dd class="collapsed"> 
                        <a href="<?php print base_path() ?>trabalhar/work-experience/programa">Work Experience</a>
                      </dd>

                      <div class="paises">
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 01</a>
                        </dd>
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 02</a>
                        </dd>
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 03</a>
                        </dd>
                      </div>

                    </div>
                    <div class="col02">
                      <dd class="collapsed"> 
                        <a href="<?php print base_path() ?>trabalhar/au-pair-experience/programa">Au Pair Experience IE</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php print base_path() ?>trabalhar/trainee-ie-estados-unidos/programa">Trainee IE Estados Unidos</a> 
                      </dd>
                       <dd class="collapsed"> 
                        <a href="<?php print base_path() ?>trabalhar/estagio-ie/programa">Estágio IE</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php print base_path() ?>trabalhar/study-and-work/programa">Study and Work</a> 
                      </dd>
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print base_path() ?>trabalhar/seguro-iac">Seguro IAC</a> 
                      </dd>
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print base_path() ?>trabalhar/visa-travel-money">Visa Travel Money</a> 
                      </dd>
                    </div>
                  </dl>
                </li>
                <li class="expanded l03">
                  <span title="" class="nolink">Conhecer</span>
                  <dl class="segundo-nivel" style="display:none;">
                    <div class="col01">
                      <dd class="collapsed">
                        América
                      </dd>
                      <div class="paises">
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 01</a>
                        </dd>
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 02</a>
                        </dd>
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 03</a>
                        </dd>
                      </div>
                      </dd>
                    </div>
                    <div class="col02">
                      <dd class="collapsed">
                        Europa
                      </dd>
                      <div class="paises">
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 01</a>
                        </dd>
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 02</a>
                        </dd>
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 03</a>
                        </dd>
                      </div>
                    </div>
                    <div class="col03">
                      <dd class="collapsed">
                        Oceania
                      </dd>
                      <div class="paises">
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 01</a>
                        </dd>
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 02</a>
                        </dd>
                        <dd>
                          <a href="javascript:void(0)"><img class="flag" src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/flag.jpg" alt="">País 03</a>
                        </dd>
                      </div>
                    </div>
                    <div class="col04">
                      <dd class="collapsed"> 
                        <a href="<?php print base_path() ?>conhecer/mochilao/programa">Mochilão</a> 
                      </dd>
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print base_path() ?>conhecer/seguro-iac">Seguro IAC</a> 
                      </dd>
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print base_path() ?>conhecer/visa-travel-money">Visa Travel Money</a> 
                      </dd>
                    </div>
                  </dl>
                </li>
                <li class="expanded l04">
                  <span title="" class="nolink">IExplorer</span>
                   <dl class="segundo-nivel" style="display:none;">
                      <dd class="collapsed"> 
                        <a href="<?php print base_path() ?>iexplorer/ie-explorer">IE Explorer</a>
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php echo url('iexplorer/destinos/america'); ?>">América</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php echo url('iexplorer/destinos/europa'); ?>">Europa</a> 
                      </dd>
                       <dd class="collapsed"> 
                        <a href="<?php echo url('iexplorer/destinos/africa'); ?>">África</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php echo url('iexplorer/destinos/oceania'); ?>">Oceania</a> 
                      </dd>
                  </dl>
                </li>
                <li class="expanded l05">
                  <span><a href="<?php echo url('promocoes'); ?>" title="">Promoções</a></span>
                </li>
              </ul>
            </div>
            <div class="menu-secundario">
              <ul><li class="first leaf"><a href="<?php print base_path() ?>sobre-a-ie" title="">Sobre a IE</a></li>
                <li class="leaf"><a href="<?php print base_path() ?>sobre-a-ie/trabalhe-conosco" title="">Trabalhe Conosco</a></li>
                <li class="last leaf"><a href="<?php print base_path() ?>sobre-a-ie/fale-conosco" title="">Contato</a></li>
              </ul>
              <?php //print render($page['secondary_menu']); ?>
            </div>
          </div>
        </div>
      </div>

      <div class="webdoor">
        <div class="contentWebdoor">
          <?php print render($page['webdoor']); ?>
          <div class="thumbs">
            <div class="thumWebdoor" id="thumbPrev">thumb Prev</div>
            <div class="thumWebdoor" id="thumbNext">thumb Next</div>
          </div>
          <div class="bullets">
            <ul class="navegacao">
            </ul>
          </div>
        </div>
        <div class="detalheBase">
          <div class="esquerda"></div>
          <div class="direita"></div>
          <div class="detalhe"></div>
        </div>
      </div>

      <div id="content">   
           <!-- <?php print $messages; ?>  -->
           <?php print render($page['content']); ?>
      </div>

      <div id="iexplorer">
        <div class="titulo">
          <div class="fltlft boxIcon icone boxcolor"></div>
          <div class="fltlft boxTitle">iexplorer</div>
        </div>
        <div class="clear"></div>
        <div class="boxes">
           <?php print render($page['iexplorer']); ?>
        </div>
      </div>

      <br class="clear" />

       <div id="footer">
          <?php print render($page['footer_menu']); ?>        
      </div>  
      <div class="footer-2">
          <div class="footer-center">
            <ul class="footerLeft">
              <li><a href="javascript:void(0)">Fale Conosco</a></li>
              <li>
                <a href="javascript:void(0)">Localize sua agência</a>
                <div class="boxForm">
                  <div class="boxInterno">
                    <div class="form-busca">
                      <form action="javascript:void(0)" name="busca_agencias">
                        <div class="select">
                          <select name="estado" id="estado">
                            <option value="" selected>Estado</option>
                            <option value="estado01">Estado 01</option>
                            <option value="estado02">Estado 02</option>
                            <option value="estado03">Estado 03</option>
                          </select>
                          <select name="cidade" id="cidade">
                            <option value="" selected>Cidade</option>
                            <option value="cidade01">Cidade 01</option>
                            <option value="cidade02">Cidade 02</option>
                            <option value="cidade03">Cidade 03</option>
                          </select>
                          <select name="bairro" id="bairro">
                            <option value="" selected>Bairro</option>
                            <option value="bairro01">bairro 01</option>
                            <option value="bairro02">bairro 02</option>
                            <option value="bairro03">bairro 03</option>
                          </select>
                        </div>
                        <input type="submit" value="Procurar">
                      </form>
                    </div>
                    <div class="clear"></div>
                    <div class="endereco">
                      <p class="agencia">IE São Paulo - Paraíso</p>
                      <p>Rua, Vergueiro, 1353 / sala 1004 - Torre Norte <br> São Paulo, SP</p>
                      <p class="contato">Contato:</p>
                      <p>(11) 2935-7160 <br>
                      paraiso@ieintercambio.com.br</p>
                      <div class="boxlink">
                        <a href="javascrip:void(0)" class="link">Ver no mapa</a>
                      </div>
                    </div>

                  </div>
                </div>
              </li>
            </ul>
            <ul class="footerRight">
              <li class="telefone">0800.000000</li>
              <li class="atendimento">Atendimento online</li>
            </ul>
          </div>
      </div>
    </div>  

  