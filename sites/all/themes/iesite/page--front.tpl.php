<?php
 drupal_add_css(drupal_get_path('theme', 'iesite') .'/css/home.css');
 drupal_add_js(drupal_get_path('theme', 'iesite') .'/js/jquery.easing.1.3.js');
?>
  <?php print render($page['header']); ?>
  <div id="master">      
      <div id="header">
        <div class="header-1">
          <div class="header-2">
            <a href="<?php print base_path() ?>" class="logo"><img src="<?php print base_path() ?><?php print drupal_get_path('theme','iesite'); ?>/img/logo.png" height="96" width="101" alt="IE intercâmbio" /></a>
            <div class="busca">
              <form action="<?php echo url('busca/resultado'); ?>" method="get">
                <div class="form-busca">                  
                  <label class="option" for="input-busca" style="display: block;"> Digite o que procura que a gente ajuda </label>
                  <input title="Digite os termos que você deseja procurar." type="text" name="texto" value="" size="15" id="input-busca" class="input-busca" />
                </div>
                <div class="bt_submit">
                  <input type="submit" value="Buscar" class="form-submit" />                  
                </div>
              </form>
				<div class="socials">
					<div class="facebook">
					  <div id="fb-root"></div>
					  <script type="text/javascript">(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=105385746283411";
						fjs.parentNode.insertBefore(js, fjs);
					  }(document, 'script', 'facebook-jssdk'));</script>
					  <div class="fb-like" data-href="https://www.facebook.com/IEintercambio?fref=ts"  data-send="false" data-layout="button_count" data-width="60" data-show-faces="false"></div>
					</div>
					<div class="linkedin">
						<script src="//platform.linkedin.com/in.js" type="text/javascript">
						 lang: en_US
						</script>
						<script type="IN/Share"></script>
					</div>
					<div class="pinterest">
					  <script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
					  <a href="http://pinterest.com/pin/create/button/?url=http://www.ieintercambio.com.br&media=http://www.ieintercambio.com.br&description=IE%20Interc%C3%A2mbio" class="pin-it-button" count-layout="none" target="_blank" ><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
					</div>	
					<div class="gmais">
					  <!-- Place this tag where you want the +1 button to render. -->
					  <div class="g-plusone" data-annotation="inline"></div>
					  <!-- Place this tag after the last +1 button tag. -->
					  <script type="text/javascript">
						window.___gcfg = {lang: 'pt-BR'};

						(function() {
						  var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
						  po.src = 'https://apis.google.com/js/plusone.js';
						  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
						})();
					  </script>
					</div>									
              </div>
             <?php //print render($page['busca']); ?>
            </div>

            <div class="menu-principal">
              <ul class="primeiro-nivel">
                <li class="expanded l01" style="height: 45px;">

                  <span title="" class="nolink">Estudar</span>

                  <div class="segundo-nivel" style="display: none;">
                    <dl class="col01">
                      <dd class="collapsed"> 
                        <a href="<?php print url('estudar/curso-de-idiomas/programa'); ?>">Curso de Idiomas</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php print url('estudar/teen-experience/programa'); ?>">Intercâmbio de Férias Teen</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php print url('estudar/high-school/programa'); ?>">High School</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php print url('estudar/study-and-work/programa'); ?>">Estudo e Trabalho</a> 
                      </dd>
                    </dl>
                    <dl class="col02">
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print url('estudar/seguro-iac'); ?>">Seguro IAC</a> 
                      </dd>
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print url('estudar/visa-travel-money'); ?>">Visa Travel Money</a> 
                      </dd>
                    </dl>                    
                  </div>

                </li>

                <li class="expanded l02">
                  <span title="" class="nolink">Trabalhar</span>
                  <div class="segundo-nivel" style="display: none;">
                    <dl class="col01">
                      <dd class="collapsed"> 
                        <a href="javascript:void(0);">Work Experience</a>
                      </dd>
                      <dd class="paises">
                        <dl>
                          <?php       
                          $query = new EntityFieldQuery();
                          $query->entityCondition('entity_type', 'node')
                            ->entityCondition('bundle', 'work_experience')
                            ->range(0, 6);
                          $results = $query->execute();
                          $work_experiences = node_load_multiple(array_keys($results['node']));       
                          foreach($work_experiences as $we){
                            $tax_pais = taxonomy_term_load($we->field_pais['und'][0]['tid']);
                            $tax_bandeira = image_style_url('bandeira_workexperience',$tax_pais->field_paises_bandeira['und'][0]['uri']);
                          ?>                        
                          <dd>
                          <a href="<?php echo url(drupal_lookup_path('alias',"node/".$we->nid)); ?>">
                            <img class="flag" src="<?php echo $tax_bandeira;   ?>" alt="<?php echo $tax_pais->name; ?>" />
                            <?php echo $tax_pais->name; ?>
                          </a>
                          </dd>                        
                          <?php } ?>
                        </dl>
                      </dd>
                    </dl>
                    <dl class="col02">
                      <dd class="collapsed"> 
                        <a href="<?php print url('trabalhar/au-pair-experience/programa'); ?>">Au Pair Experience IE</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php print url('trabalhar/trainee/75/estados-unidos'); ?>">Trainee IE Estados Unidos</a> 
                      </dd>
                       <dd class="collapsed"> 
                        <a href="<?php print url('trabalhar/estagio/76/estados-unidos'); ?>">Estágio IE</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php print url('estudar/study-and-work/programa'); ?>">Estudo e Trabalho</a>
                      </dd>
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print url('trabalhar/seguro-iac'); ?>">Seguro IAC</a> 
                      </dd>
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print url('trabalhar/visa-travel-money'); ?>">Visa Travel Money</a> 
                      </dd>
                    </dl>
                  </div>
                </li>
                <li class="expanded l03">
                  <span title="" class="nolink">Viajar</span>
                  <div class="segundo-nivel" style="display:none;">
                    <?php
                    $taxonomyQuery = new EntityFieldQuery();
                    $taxonomyTerms = $taxonomyQuery->entityCondition('entity_type', 'taxonomy_term')
                      ->propertyCondition('vid', 4)
                      ->propertyOrderBy('weight')
                      ->range(0, 5)
                      ->execute();
                    foreach($taxonomyTerms['taxonomy_term'] as $term) {
                      $continente = taxonomy_term_load($term->tid);     
                    ?>

                    <dl class="col01">
                      <dd class="collapsed">
                        <?php echo $continente->name; ?>
                      </dd>
                      <dd class="paises">
                        <dl>
                        <?php       
                        $query = new EntityFieldQuery();
                        $query->entityCondition('entity_type', 'node')
                          ->entityCondition('bundle', 'paises')
                          ->fieldCondition('field_paises_continente', 'tid', $term->tid)
                          ->range(0, 6);
                        $results = $query->execute();
                        $paises = node_load_multiple(array_keys($results['node']));       
                        foreach($paises as $pais){
                          $tax_pais = taxonomy_term_load($pais->field_paises_pais['und'][0]['tid']);
                          $tax_bandeira = image_style_url('bandeira_workexperience',$tax_pais->field_paises_bandeira['und'][0]['uri']);
                        ?>
                        <dd>
                          <a href="<?php echo url(drupal_lookup_path('alias',"node/".$pais->nid)); ?>">
                            <img class="flag" src="<?php echo $tax_bandeira;   ?>" alt="<?php echo $pais->title; ?>" />
                            <?php echo $pais->title; ?>
                          </a>
                        </dd>
                        <?php } ?>
                        </dl>
                      </dd>
                    </dl>
                    <?php } ?>

                    <dl class="col04">
                      <dd class="collapsed"> 
                        <a href="<?php print url('conhecer/mochilao/programa'); ?>">Trip Experience</a> 
                      </dd>
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print url('conhecer/seguro-iac'); ?>">Seguro IAC</a> 
                      </dd>
                      <dd class="collapsed orangeitem"> 
                        <a href="<?php print url('conhecer/visa-travel-money'); ?>">Visa Travel Money</a> 
                      </dd>
                    </dl>

                  </div>
                </li>
                <li class="expanded l04">
                  <span title="" class="nolink">IE Explorer</span>
                   <dl class="segundo-nivel" style="display:none;">
                      <dd class="collapsed"> 
                        <a href="<?php print url('iexplorer/ie-explorer'); ?>">IE Explorer</a>
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php echo url('iexplorer/destinos/america'); ?>">América</a> 
                      </dd>
                      <dd class="collapsed"> 
                        <a href="<?php echo url('iexplorer/destinos/europa'); ?>">Europa</a> 
                      </dd>
                       <dd class="collapsed"> 
                        <!-- <a href="<?php echo url('iexplorer/destinos/africa'); ?>">África</a>  -->
                      </dd>
                      <dd class="collapsed"> 
                        <!-- <a href="<?php echo url('iexplorer/destinos/oceania'); ?>">Oceania</a>  -->
                      </dd>
                  </dl>
                </li>
                <li class="expanded l05">
                  <span><a href="<?php echo url('promocoes'); ?>" title="">Promoções</a></span>
                </li>
              </ul>
            </div>
            <div class="menu-secundario">
              <ul><li class="first leaf"><a href="<?php print base_path() ?>sobre-a-ie" title="">Sobre a IE</a></li>
                <li class="leaf"><a href="<?php print base_path() ?>sobre-a-ie/trabalhe-conosco" title="">Trabalhe Conosco</a></li>
                <li class="last leaf"><a href="<?php print base_path() ?>lojas" title="">Nossas Lojas</a></li>
              </ul>
              <?php //print render($page['secondary_menu']); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="webdoor">
        <div class="contentWebdoor">
          <?php print render($page['webdoor']); ?>
          <div class="thumbs">
            <div class="thumWebdoor" id="thumbPrev">thumb Prev</div>
            <div class="thumWebdoor" id="thumbNext">thumb Next</div>
          </div>
          <div class="bullets">
            <ul class="navegacao">
            </ul>
          </div>
        </div>
        <div class="detalheBase">
          <div class="esquerda"></div>
          <div class="esquerdaBase"></div>
          <div class="direita"></div>
          <div class="detalhe"></div>
        </div>
        <div class="auxiliary-items">
          <div class="adjust">
            <div class="item01">
              <span class="bdrlft"></span>
              <!--<a href="<?php echo url('sobre-ie/receba-um-intercambista'); ?>">Receba um intercambista</a>-->
              <span class="bdrrgt"></span>
            </div>
            <div class="item02">
              <span class="bdrlft"></span>
              <!--<a href="http://www.brazilexperienceie.com" target="_blank">International visitors</a>-->
              <span class="bdrrgt"></span>
            </div>
          </div>
        </div>
      </div>
      <div id="content">   
           <!-- <?php print $messages; ?>  -->
           <?php print render($page['content']); ?>
      </div>
      <div id="iexplorer">
        <div class="titulo">
          <div class="fltlft boxIcon icone boxcolor"></div>
          <div class="fltlft boxTitle">IE Explorer</div>
        </div>
        <div class="clear"></div>
        <div class="boxes">
           <?php print render($page['iexplorer']); ?>
        </div>
      </div>
      <br class="clear" />
       <div id="footer">
          <?php print render($page['footer_menu']); ?>        
      </div>  
      <div class="footer-2">
          <div class="footer-center">
            <ul class="footerLeft">
              <li><a href="<?php print base_path() ?>sobre-a-ie/fale-conosco">Fale Conosco</a></li>
              <li>                
                <a href="javascript:void(0)">Localize sua agência</a>
                <div class="boxForm">
					<span class="bt_fechar">X</span>
                  <div class="boxInterno">
                    <div class="form-busca">							
                      <?php
                        $raw_block = block_load('lojas', 'lojas_buscaloja');
                        $rendered_block = drupal_render(_block_get_renderable_array(_block_render_blocks(array($raw_block))));
                        print $rendered_block;
                      ?>
                    </div>
                    <div class="clear"></div>
                  </div>
                </div>
              </li>
            </ul>
            <ul class="footerRight">
              <li class="telefone">0800 605 3900</li>
              <li class="atendimento">Atendimento online</li>
            </ul>
          </div>
      </div>
    </div>
    <script type="text/javascript">
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=105385746283411";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- Place this tag after the last +1 button tag. -->
    <script type="text/javascript">
      window.___gcfg = {lang: 'pt-BR'};

      (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
      })();
    </script>
    <script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>