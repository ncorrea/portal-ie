jQuery(document).ready(function($) {
	//adiciona classe no conteudo
	$('#block-system-main').addClass('blocoVideos');

	var getUrl = window.location.href;
	var splitUrl = getUrl.split('video=');

	
	if( splitUrl[1] == null ){ //se for nulo exibe o primeiro video.

		exibeVideo(0); 

	}else{ //identifica o iD do video e imprime
		
		var totalVideos = $('.blocoVideos .views-row').size();

		for(i=0; i<totalVideos; i++){
			var getIdVideo = $('.blocoVideos .views-row:eq('+ i +') .views-field-nid span').text();
			if( getIdVideo == splitUrl[1] ){
				exibeVideo(i);
				break;
			}
		}

	}

	$('.views-row').click(function(){
		var ref = $(this).index();
		$('html, body').animate({
          scrollTop: $("#box1col").offset().top
      }, 600, function () {
          exibeVideo(ref);
      });
		
	});

	function exibeVideo(ref){
		$('.view-tv-ie-view .fltlft.box').contents().remove();

		var getVideo = $('.blocoVideos .views-row:eq('+ ref +') .views-field-field-video .field-content').text();	 
		var getVideoId = getVideo.split('=');
		
		var getVideoTitulo = $('.blocoVideos .views-row:eq('+ ref +') .views-field-title').text();
		var getVideoDescription = $('.blocoVideos .views-row:eq('+ ref +') .views-field-body .field-content').text();
		
		$('.view-tv-ie-view .fltlft.box').append('<div class="field-title"><h3>'+getVideoTitulo+'</h3></div><div class="field-description"><p>'+getVideoDescription+'</p></div><iframe width="666" height="389" src="http://www.youtube.com/embed/'+getVideoId[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');
	}
	
});