jQuery(document).ready(function($) {

	$('#block-views-clippings-block').appendTo('.bloco-clippings');
	$('#block-views-parcerias-bloco-block').appendTo('.bloco-parcerias');
	
	$('#block-views-clippings-block .views-row').after('<div class="clear"></div>');
	$('.bloco-parcerias').after('<div class="clear"></div>');
	
	$('.bloco-clippings').append('<div class="img-rodape"></div>');
	
	
	// Monta datas
	
	var arrayMes = ['','jan','fev','mar','abr','mai','jun','jul','ago','set','out','nov','dez'];
	
	$('.view-clippings .date-display-single').each(function(){
		
		var getMes = $(this).text();
		var numMes = getMes.split("/");
		var diaAtual = numMes[1].toString();
		var mesAtual = numMes[0];
		
		if(mesAtual < 10){
			mesAtual.toString();
			mesAtual = mesAtual.substring(1,2);
		}
		
		$(this).empty();
		$(this).prepend(diaAtual+'<strong>'+arrayMes[mesAtual]+'</strong>');				
		
	});
	
	
	$('.view-parcerias-bloco .views-row').each(function(){
		var getTitle = $(this).find('.views-field-title').html();
		var getSubtitle = $(this).find('.views-field-field-subtitulo-parcerias').html();
		
		/*
		$(this).find('.views-field-title').remove();
		$(this).find('.views-field-field-subtitulo-parcerias').remove();
		$(this).find('.views-field-title').body();
		*/
		
		$(this).prepend('<div class="conteudo">'+getTitle+getSubtitle+'</div>');
	});
	
		
	$('#edit-submitted-estado').change(function(){
		$('#webform-component-cidade').remove();
		$(this).parent().css({'height':'74px'});				
	});
	
	
	
});