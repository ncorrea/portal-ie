jQuery(document).ready(function($) {

	$('.clipping').after('<div class="bloco-clippings"></div>');
	$('#block-views-clippings-interno-block').appendTo('.bloco-clippings');
	$('.bloco-clippings').after('<div class="clear"></div>');
	$('.bloco-clippings').prepend('<div class="fltlft boxIcon icone boxcolor">&nbsp;</div><div class="fltlft boxTitle">Clippings</div><br />');
	$('.bloco-clippings').append('<img src="http://site.ieintercambio.com.br/sites/all/themes/iesite/img/sobre/bottom-clippings.png">');
	$('.field-name-field-clipping-arquivo').after('<a class="voltar_clippings" title="Voltar" href="http://site.ieintercambio.com.br/sobre-a-ie"><span>voltar</span>Voltar para todos os clippings</a>');
	
	
	// Monta url dos arquivos
	$('.field-name-field-clipping-arquivo').each(function(){
		var getUrl = $(this).find('.field-item').text();	
		$(this).find('.field-item').empty();
		var extensao_arquivo = getUrl.split(".").pop();
		console.log(extensao_arquivo);
		
		var getUrl = $(this).find('.field-item').prepend('<a href="'+getUrl+'" class="baixar" target="_blank">Baixar .'+extensao_arquivo+'</a>');
		
	});
	
	
	// Monta datas	
	var arrayMes = ['','jan','fev','mar','abr','mai','jun','jul','ago','set','out','nov','dez'];
	
	$('.view-clippings-interno .date-display-single').each(function(){
		
		var getMes = $(this).text();
		var numMes = getMes.split("/");
		var diaAtual = numMes[1].toString();
		var mesAtual = numMes[0];
		
		if(mesAtual < 10){
			mesAtual.toString();
			mesAtual = mesAtual.substring(1,2);
		}
		
		$(this).empty();
		$(this).prepend(diaAtual+'<strong>'+arrayMes[mesAtual]+'</strong>');				
		
	});
	
	
	$('.view-parcerias-bloco .views-row').each(function(){
		var getTitle = $(this).find('.views-field-title').html();
		var getSubtitle = $(this).find('.views-field-field-subtitulo-parcerias').html();
		
		$(this).prepend('<div class="conteudo">'+getTitle+getSubtitle+'</div>');
	});
	
	
	
});