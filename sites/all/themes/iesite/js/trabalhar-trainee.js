jQuery(document).ready(function($) {
	$('.page-title .adjust').append('<ul class="menu-dif"><li class="first active"><a href="javascript:void(0);" class="active">O Programa</a><div class="ativo"></div></li><li class="last"><a href="javascript:void(0);">Você nos EUA</a></li></ul>');

	$('.field-name-field-trainee-voceno').prependTo('#content').hide();
	$('.field-name-field-trainee-voceno #box1colbig').prepend('<div class="fltlft boxIcon icone boxcolor">&nbsp;</div>');
	
	$('.page-title .adjust ul li a').click(function(){
		$(this).parent().removeClass('active');
		$(this).removeClass('active');
		$('.page-title .adjust ul li .ativo').remove();
		$(this).parent().append('<div class="ativo"></div>').addClass('active');
		$(this).addClass('active');
		
		if($(this).parent().hasClass('first')){
			$('.field-name-field-trainee-voceno').hide();	
			$('.clipping').show();	
		}else{
			$('.field-name-field-trainee-voceno').show();	
			$('.clipping').hide();	
		}
		
	if( !$('#block-block-19').length[0] ){
		$('#block-block-16').css({'top':'360px !important'});		
	}	
		
	});	
});