/* Promo��es */

jQuery(document).ready(function($) {

	var url = window.location.href;	
	var getUrl = url.toString();	
	var urlArray = getUrl.split("/");
	var urlFinal = urlArray[4];	
	
	if(urlFinal != null){

	$('#block-system-main').append('<div id="promo-dez"><h4>Investimento</h4><p class="x10">INTERC&Aring;MBIO EM AT&Eacute; <span style="font-size:41px;">24x</span></p><p class="cidade">Kgic - Toronto</p><ul class="info"><li>4 semanas de cursos de ingl&ecirc;s geral(28h/semana).</li><li>4 semanas de acomoda&ccedil;&atilde;o em dormit&oacute;rio com quarto compartilhado, sem refei&ccedil;&otilde;es.</li><li>Inclui taxas da escola.</li></ul><p class="investimento">Investimento:</p>	<div class="p1"><div class="border-yellow"></div><div class="text"><span>CAD</span> 1.640,00 (R$ 3.509,60)</div></div>	<div class="p2"><div class="ou">ou</div><div class="border-yellow"></div><div class="text"><span>10x</span> de R$ 385,71*</div></div>	<div class="p3"><div class="ou">ou</div><div class="border-yellow"></div><div class="text"><span>10x</span> de R$ 385,71*</div></div><p class="aviso dois">*Valores e condi&ccedil;&otilde;es sujeitos a mudan&ccedil;a sem pr&eacute;vio aviso.</p><div class="euquero"><a href="javascript:void(0)">Eu quero</a></div></div><div class="clear"></div>');
		
	$('.view-promoces .views-row .views-field-field-promocoes-valor').each(function(){
		// $(this).append('<a href="javascript:void(0)">eu quero</a>');
	});
	
	
	trocaValores(0);
	$('.view-promoces .views-row:first').addClass('ativo');	
	
	$('.view-promoces .views-row').click(function(){	
		var index = $(this).index();
		trocaValores(index);
		$('html, body').animate({
         scrollTop: $("#promo-dez").offset().top
     }, 1000);
	});
		
	function trocaValores(indice){
	
		$('.view-promoces .views-row').removeClass("ativo");
		$('.view-promoces .views-row:eq('+indice+')').addClass("ativo");
		
		var empresa = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-empresa div').text();
		var body_texto = $('.view-promoces .views-row:eq('+indice+') .views-field-body div p').text();
		var legenda = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-legenda div').text();
		var valorCad = $('.view-promoces .views-row:eq('+indice+') .views-field-field-valor-cad div').text();
		var promocaoValor = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-valor div').text();
		var link_item = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-valor a').attr('href');		
		var cidade = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-cidade div').text();
		var preco = $('.view-promoces .views-row:eq('+indice+') .views-field-field-promocoes-valor div').text();
		var parcela_um = $('.view-promoces .views-row:eq('+indice+') .views-field-field-parcelamento-1 div').text();
		var parcela_dois = $('.view-promoces .views-row:eq('+indice+') .views-field-field-parcelamento-2 div').text();
		

		if(parcela_um == 0 || parcela_um == ''){
			$('#promo-dez .p2').hide();			
		}else{
			array_parcUm = parcela_um.split("x");	
			$('#promo-dez .p2 .text').empty().append('<span>'+array_parcUm[0]+'x</span>'+array_parcUm[1]);
			$('#promo-dez .p2').show();
		}
		if(parcela_dois == 0 || parcela_dois == ''){
			$('#promo-dez .p3').hide();			
		}else{
			array_parcDois = parcela_dois.split("x");
			$('#promo-dez .p3 .text').empty().append('<span>'+array_parcDois[0]+'x</span>'+array_parcDois[1]);
			$('.aviso').removeClass('dois');
			$('#promo-dez .p3').show();
		}		
		
		valorCad = valorCad.replace(".",",");		
		valorCad = valorCad.replace(" ",".");		
		
		var corpo = body_texto.split(">");
		
		$('#promo-dez .cidade').empty().append(empresa+' - '+cidade);	
		$('#promo-dez .p1 .text').empty().append('<span>'+legenda+'</span> '+valorCad+' ('+promocaoValor+')');
		$('#promo-dez .euquero a').attr('href',link_item);		
		
		$('#promo-dez .info').empty();
		for(x=0; x<corpo.length; x++){
			$('#promo-dez .info').append('<li>'+corpo[x]+'</li>');
		}
		
	}
	
	}else{		
	
		$('.view-promooes-destaque-pagina .views-row').each(function(){
			var link = $(this).find('.views-field-field-promopagina-link div a').attr('href');	
			var conteudo_imagem = $(this).find('.views-field-field-promopagina-imagem div').html();
			$(this).find('.views-field-field-promopagina-imagem div').empty().append('<a href="'+link+'">'+conteudo_imagem+'</a>');			
		});		
	}
	
});