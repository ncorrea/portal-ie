jQuery(document).ready(function($) {

	$('#webform-component-nome-completo, #webform-component-data-de-nascimento, #webform-component-endereco, #webform-component-endereco-fieldset, #webform-component-bairro, #webform-component-cidade, #webform-component-telefone-fieldset, #webform-component-e-mail, #webform-component-confirma-e-mail, #webform-component-sexo, #webform-component-estado-civil').addClass('formulario-1');
	
	$('#webform-component-tipo-de-acomodacao-filedset, #webform-component-o-estudante-tera-acesso-fieldset, #webform-component-o-quarto-oferecido, #webform-component-quer-receber-em-sua-casa-alguem-do-sexo, #webform-component-por-quanto-tempo-gostaria-de-hospedar-o-estudante, #webform-component-possui-animais-de-estimacao-fieldset, #webform-component-voce-aceita-fumantes, #webform-component-o-banheiro-e, #webform-component-numero-de-pessoas-que-moram-na-casa-fieldset, #webform-component-liste-o-sexo-dos-moradores, #webform-component-o-que-inclui-fieldset, #webform-component-comentarios').addClass('formulario-2');

	$('#edit-submit').remove();	
	$('#edit-actions').after('<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Enviar" class="form-submit"></div>');	
	$('#edit-submit').parent().addClass('enviarOculto');	
	$('.enviarOculto').hide();	
	$('#webform-client-form-369').prepend('<h2 class="first">1. Dados Pessoais</h2>');
	$('#edit-actions').prepend('<input type="button" id="edit-continuar" name="op" value="Continuar" class="form-submit">');
	
	$('#edit-continuar').live("click",function(){
	
		$('.formulario-1').hide();
		$('.formulario-2').show();
		$('#webform-client-form-369 h2').addClass('last');
		$(this).hide();
		$(this).parent().prepend('<input type="button" id="edit-voltar" name="op" value="Voltar" class="form-submit">');
		
		$('#edit-voltar').parent().css({'margin-right':'190px'});
		$('.enviarOculto').show();
		$('#webform-client-form-369 h2').empty();
		$('#webform-client-form-369 h2').prepend('2. Dados de Acomoda&ccedil;&atilde;o');
		$('#node-369').css({'height':'1640px'});
	
	});
	
	$('#edit-voltar').live("click",function(){
	
		$('.formulario-1').show();
		$('.formulario-2').hide();
		$('#webform-client-form-369 h2').removeClass('last');
		$(this).hide();
		$(this).parent().prepend('<input type="button" id="edit-continuar" name="op" value="Continuar" class="form-submit">');
		
		$('#edit-voltar').parent().css({'margin-right':'250px'});
		$('.enviarOculto').hide();
		$('#webform-client-form-369 h2').empty();
		$('#webform-client-form-369 h2').prepend('1. Dados Pessoais');
		$('#node-369').css({'height':'1040px'});
	
	});
	
	/*
	if($('#edit-next').length){
		$('#webform-client-form-369').prepend('<h2 class="first">1. Dados Pessoais</h2>');
		$('#node-369').css({'height':'1040px'});
	}else{
		$('#webform-client-form-369').prepend('<h2 class="last">2. Dados de Acomoda&ccedil;&atilde;o</h2>');
		$('#node-369').css({'height':'1640px'});
	}
	*/
	// Edita o value dos botoes
	$('#edit-next').attr('value','Continuar');
	$('#edit-previous').attr('value','< voltar');
	$('#edit-submit').attr('value','Enviar');
	
	$('#webform-component-data-de-nascimento--dia').append('<label for="edit-submitted-data-de-nascimento-dia">Data de Nascimento</label>');
	
	// Faz o valor default sumir e aparecer
	$('.form-item input').focus(function(){
	
		var valor_texto = $(this).attr('value');
		if( valor_texto == ''){
			$(this).parent().find('.field-prefix').hide();
			$(this).parent().find('.field-suffix').hide();
			$('#webform-component-nome-completo .field-suffix').show();
		}
		
	});
	
	// Faz o valor default sumir e aparecer
	$('.form-item input').focusout(function(){	
	var valor_texto = $(this).attr('value');
	if( valor_texto == ''){	
		$(this).parent().find('.field-suffix').show();
		$(this).parent().find('.field-prefix').show();
	}
	});
	
		
});