jQuery(document).ready(function($) {
	$('.page-title .adjust').append('<ul class="menu-dif"><li class="first"><a href="javascript:void(0);">O Programa</a></li><li><a href="javascript:void(0);">Você nos EUA</a></li><li class="last"><a href="javascript:void(0);">Modalidades</a></li></ul>');
	
	var disp = $('#block-block-19').css('display');
	if( disp != 'none'){
		$('#block-block-16').addClass('feira');
	}

	var url = window.location.href;
	var urlString = url.toString();
	var getUrl = urlString.split("/");
	var nodeAtual = getUrl[5];
	
	$('#node-'+nodeAtual+' .field').hide();	

	for(i=0;i<($('#node-'+nodeAtual+' .field').size());i++){
		var classe = $('#node-'+nodeAtual+' .field:eq('+i+')').attr('class');
		var classeFinal = classe.split(' ');

		$('.menu-dif li:eq('+i+')').attr('rel', classeFinal[1]);
	}

	$('#node-'+nodeAtual+' .field:eq(0)').show();
	$('.page-title .adjust ul li:eq(0)').append('<div class="ativo"></div>').find('a').addClass('active');

	$('.page-title .adjust ul li a').click(function(){
		$(this).parent().removeClass('active');
		$(this).removeClass('active');
		$('.page-title .adjust ul li .ativo').remove();
		$(this).parent().append('<div class="ativo"></div>').addClass('active');
		$(this).addClass('active');

		var pegaRel = $(this).parent().attr('rel');
		$('#node-'+nodeAtual+' .field').hide();
		$('#node-'+nodeAtual+' .'+pegaRel+'').show();
		
		if(pegaRel == "field-name-body"){
			$('.view-mochiloes, #block-block-16').show();
			$('#block-block-19').show();
		}else{
			$('.view-mochiloes, #block-block-16').hide();
			$('#block-block-19').hide();
		}
		
	});	
	
	//alert(getUrl[6]);
	if(getUrl[6] != 'estados-unidos'){
		$('.menu-dif li:eq(1) a').empty().append('Você no Exterior');
	}
	
	if( $('.field-name-field-opcoes').length > 0 ) {
		return false;
	}else{
		$('.menu-dif li:eq(2)').hide();
	}
			
	var browser = navigator.appName;
	// alert(browser);
	
});