jQuery(document).ready(function($) {

	  $('.flexslider').flexslider({
	    animation: "fade",
	    pauseOnHover: true,
	    directionNav: false,
	    slideshow: false,
	    animationSpeed: 600,
	  });

	//Campo de busca
	jQuery('.region-busca #edit-search-block-form--2').focus(function(){
		jQuery('.region-busca label').hide();
	});
	
	jQuery('.region-busca #edit-search-block-form--2').blur(function(){
		if(jQuery(this).val() == ''){
			jQuery(this).prev('label').show();
		}
	});
	
	// Menu
	$('#block-system-main-menu ul.menu:eq(0)').children('li:eq(0)').addClass('l01');
	$('#block-system-main-menu ul.menu:eq(0)').children('li:eq(1)').addClass('l02');
	$('#block-system-main-menu ul.menu:eq(0)').children('li:eq(2)').addClass('l03');
	$('#block-system-main-menu ul.menu:eq(0)').children('li:eq(3)').addClass('l04');
	$('#block-system-main-menu ul.menu:eq(0)').children('li:eq(4)').addClass('l05');


	$('#block-system-main-menu ul.menu li').mouseenter(function(){
		$(this).css('height', '53');
		$(this).find('ul').show();
		// $(this).find('span').stop().animate({marginTop: '5'}, 300);
	}).mouseleave(function(){
		$(this).find('ul').hide();
		$(this).css('height', '45');
		// $(this).find('span').stop().animate({marginTop: '0'}, 300);
	});




	//menu em cima do footer
	$('#block-menu-menu-menu-footer-1 ul.menu:eq(0)').css('margin', '0px 0 0 -40px')
	$('#block-menu-menu-menu-footer-1 ul.menu:eq(0)').children('li:eq(0)').addClass('estudar');
	$('#block-menu-menu-menu-footer-1 ul.menu:eq(0)').children('li:eq(1)').addClass('trabalhar');
	$('#block-menu-menu-menu-footer-1 ul.menu:eq(0)').children('li:eq(2)').addClass('destinos');
	$('#block-menu-menu-menu-footer-1 ul.menu:eq(0)').children('li:eq(3)').addClass('outros');

	$('li.outros span.nolink:eq(0)').css('display', 'none');

	//links do footer
	$('.footer-2 .footerRight li').mouseenter(function(){
		$(this).css('background-color', '#000000');
	}).mouseleave(function(){
		$(this).css('background-color', '#1C1C1C');
	});

	//localiza sua agência 
	$('.footer-2 .footerLeft li:eq(1)').mouseenter(function(){
		$(this).children('.boxForm').stop().fadeTo(300, 1, function() {
      		$(this).children('.boxForm').show();
    	});
	}).mouseleave(function(){
		$(this).children('.boxForm').stop().fadeTo(300, 0, function() {
      		$('.boxForm').css('display', 'none');
    	});
	});


	//Itens da HOME

	$('#block-main-site-quero ul.iexplorer-bloco li:even').css('background-color', '#ebebeb')
	$('#block-main-site-vou ul.iexplorer-bloco li:even').css('background-color', '#ebebeb')
	$('#block-main-site-fui ul.iexplorer-bloco li:even').css('background-color', '#ebebeb')

	//fim itens da HOME

	$('.galeria01').html('Galeria de imagens via Jquery');

});
