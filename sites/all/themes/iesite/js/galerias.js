jQuery(document).ready(function($) {

		$(".scrool").mCustomScrollbar({
			horizontalScroll:true,
			scrollButtons:{
				enable:true
			}
		});
	
	
	jQuery('<div>').addClass('pelicula').appendTo('li.views-row-1');

	var qtd = jQuery('.block-views .item-list ul li.item-curso').size();

	for(i=0; i<=qtd; i++){

		var contaAltura = jQuery('.block-views .item-list ul li:eq('+i+') .title_curso a').height();

		if(contaAltura == 12){
			jQuery('.block-views .item-list ul li:eq('+i+') .title_curso a').css('padding-top', '16px');
		}else if(contaAltura == 24) {
			jQuery('.block-views .item-list ul li:eq('+i+') .title_curso a').css('padding-top', '11px');
		}else{
			jQuery('.block-views .item-list ul li:eq('+i+') .title_curso a').css('padding-top', '4px');
		}
	}

	//Monta a galeria das internas de cursos
	function galeriaInterna(viewClass, blockId){

		//adiciona o rel para tratamento
		$('.'+viewClass+' .views-row').each(function(){
			var titulo = $(this).find('.views-field-title span').text();
			var qtdTitulo = titulo.length;
			
			// corrigindo e passando param para contato
			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI(titulo);
		    // $(this).find('.link').attr('href',url_contato);
		    // $(this).find('.link').html('Quero ir para <span>"'+titulo+'"</span>');
			
		
			for(i=0; i<qtdTitulo; i++){
				if(titulo.indexOf(" ") != -1){
				 titulo = titulo.replace(" ", "");
				}
			}
			
			$(this).attr('id', titulo);

		});

		//monta a galeria
		var qtdCursos = $('.'+viewClass+' .views-row').size();
		for(i=0; i<qtdCursos; i++){
			var pegaId = $('.'+viewClass+' .views-row:eq('+i+')').attr('id');
			var pegaThumb = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-field-cursos-imagem img').attr('src');
			var pegaTitle = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-title span').text();
			var pegaNid = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-nid span').text();
			$('.'+viewClass+' .views-row:eq('+i+')').addClass(''+pegaNid+'');

			//Adiciona as li's a ul da galeria
			$('#'+blockId+' .galeria03 ul').append('<li id="'+pegaNid+'" rel="'+pegaId+'"><img src="'+pegaThumb+'" alt=""><div class="title_curso">'+pegaTitle+'</div></li>');
		}

		//mostra sempre o primeiro registro | Adiciona a classe ativa ao primeiro registro da galeria
		$('.galeria03 ul li:eq(0)').addClass('ativo');
		$('.'+viewClass+' .views-row:eq(0)').show();

		//clique das thumbs
		$('.galeria03 ul li').live('click', function(){
			var pegaRel = $(this).attr('rel');

			$('.galeria03 ul li').removeClass('ativo');
			$('.'+viewClass+' .views-row').hide();
			
			$(this).addClass('ativo');
			$('.'+viewClass+' #'+pegaRel+'').show();
		});


		//Coloca a Imagem e o nome da escola no lugar correto e depois remove da posição original
		$('.'+viewClass+' .views-row').each(function(){
			var pegaImg = $(this).find('.views-field-field-cursos-imagem').html();
			var pegaNome = $(this).find('.views-field-field-cursos-escola').html();
			var local = $(this).find('#box3col .col01');
			var localNome = $(this).find('#box3col .col03');

			local.append(pegaImg);
			localNome.append(pegaNome);
			$(this).find('.views-field-field-cursos-imagem').remove();
			$(this).find('.views-field-field-cursos-escola').remove();
		});

		
		
		//Pega a URL da página
		var url = window.location.href;
		var seParametro = url.split("?");
		
		//Verifica se a URL está com parâmetro | Se estiver com parâmetro,
		//pega o nid do li e exibe o view row com a classe igual ao nid.
		//Se não tiver parâmetro exibe o primeiro registro e ativa a primeira li da galeria.

		

		
		if(seParametro.length > 1){

			var nid = seParametro[1].split("=");
			$('.galeria03 ul li').removeClass('ativo');
			$('.'+viewClass+' .views-row').hide();

			$('.galeria03 ul li#'+nid[1]+'').addClass('ativo');
			$('.'+viewClass+' .'+nid[1]+'').show();
		}else{
			$('.'+viewClass+' .views-row:eq(0)').show();
			$('.galeria03 ul li:eq(0)').addClass('ativo');
		}
		
		
	}

	//Monta a galeria das internas de destinos
	function galeriaDestinos(viewClass, blockId){

		//adiciona o rel para tratamento
		$('.field-name-body .'+viewClass+' .views-row').each(function(){
			var titulo = $(this).find('.views-field-title span').text();
			var qtdTitulo = titulo.length;
			
			// corrigindo e passando param para contato
			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI(titulo);
		    // $(this).find('.link').attr('href',url_contato);
		    // $(this).find('.link').html('Quero ir para <span>"'+titulo+'"</span>');
		    $(this).find('.link').html('Quero ir');
			
		
			for(i=0; i<qtdTitulo; i++){
				if(titulo.indexOf(" ") != -1){
				 titulo = titulo.replace(" ", "");
				}
			}
			
			$(this).attr('id', titulo);

		});

		//monta a galeria
		var qtdCursos = $('.field-name-body .'+viewClass+' .views-row').size();
		for(i=0; i<qtdCursos; i++){
			var pegaId = $('.field-name-body .'+viewClass+' .views-row:eq('+i+')').attr('id');
			var pegaThumb = $('.field-name-body .'+viewClass+' .views-row:eq('+i+')').find('.views-field-field-destinos-imagem img').attr('src');
			var pegaTitle = $('.field-name-body .'+viewClass+' .views-row:eq('+i+')').find('.views-field-title span').text();
			var pegaNid = $('.field-name-body .'+viewClass+' .views-row:eq('+i+')').find('.views-field-nid span').text();
			$('.field-name-body .'+viewClass+' .views-row:eq('+i+')').addClass('nid-'+pegaNid+'');
			$('.field-name-body .'+viewClass+' .views-row:eq('+i+')').attr('nid',pegaNid);

			//Adiciona as li's a ul da galeria
			$('#'+blockId+' .galeria03 ul').append('<li id="'+pegaNid+'" rel="'+pegaId+'"><img src="'+pegaThumb+'" alt=""><div class="title_curso">'+pegaTitle+'</div></li>');
		}

		//mostra sempre o primeiro registro | Adiciona a classe ativa ao primeiro registro da galeria
		$('.galeria03 ul li:eq(0)').addClass('ativo');
		$('.field-name-body .'+viewClass+' .views-row:eq(0)').show();

		//clique das thumbs
		$('.galeria03 ul li').live('click', function(){

			var pegaRel = $(this).attr('rel');
			var pegaNid = $(this).attr('id');

			$('.galeria03 ul li').removeClass('ativo');
			$('.field-name-body .'+viewClass+' .views-row').hide();
			
			$(this).addClass('ativo');
			// $('.'+viewClass+' #'+pegaRel+'').show();
			$('div[nid='+pegaNid+']').show();

			$('#block-block-13').hide();
			// alert('.'+viewClass+' #'+pegaRel+'');
		});


		//Coloca a Imagem no lugar correto e depois remove da posição original
		$('.'+viewClass+' .views-row').each(function(){
			var pegaImg = $(this).find('.views-field-field-destinos-imagem').html();
			var local = $(this).find('#box3col .col01');

			local.append(pegaImg);
			$(this).find('.views-field-field-destinos-imagem').remove();
		});

		//Pega a URL da página
		var url = window.location.href;
		var seParametro = url.split("?");
		
		//Verifica se a URL está com parâmetro | Se estiver com parâmetro,
		//pega o nid do li e exibe o view row com a classe igual ao nid.
		//Se não tiver parâmetro exibe o primeiro registro e ativa a primeira li da galeria.
		if(seParametro.length > 1){

			var nid = seParametro[1].split("=");
			$('.galeria03 ul li').removeClass('ativo');
			$('.'+viewClass+' .views-row').hide();

			$('.galeria03 ul li#'+nid[1]+'').addClass('ativo');
			$('.'+viewClass+' .'+nid[1]+'').show();
			
		}else{
			$('.'+viewClass+' .views-row:eq(0)').show();
			$('.galeria03 ul li:eq(0)').addClass('ativo');
		}
		
	}
	
 

		//Monta a galeria dos mochilões
	function galeriaMochiloes(viewClass, blockId){

		//adiciona o rel para tratamento
		$('.'+viewClass+' .views-row').each(function(){
			var titulo = $(this).find('.views-field-title span').text();
			var qtdTitulo = titulo.length;
			
			// corrigindo e passando param para contato
			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI(titulo);
		    // $(this).find('.link').attr('href',url_contato);
			// $(this).find('.link').html('Quero ir para <span>"'+titulo+'"</span>');
			$(this).find('.link').html('Quero ir');
			
			
			for(i=0; i<qtdTitulo; i++){
				if(titulo.indexOf(" ") != -1){
				 titulo = titulo.replace(" ", "");
				}
			}
			
			$(this).attr('id', titulo);

		});

		//monta a galeria
		var qtdCursos = $('.'+viewClass+' .views-row').size();
		for(i=0; i<qtdCursos; i++){
			var pegaId = $('.'+viewClass+' .views-row:eq('+i+')').attr('id');
			var pegaThumb = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-field-mochiloes-imagem img').attr('src');
			var pegaTitle = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-title span').text();
			var pegaNid = $('.'+viewClass+' .views-row:eq('+i+')').find('.views-field-nid span').text();
			$('.'+viewClass+' .views-row:eq('+i+')').addClass(''+pegaNid+'');

			//Adiciona as li's a ul da galeria
			$('#'+blockId+' .galeria03 ul').append('<li id="'+pegaNid+'" rel="'+pegaId+'"><img src="'+pegaThumb+'" alt=""><div class="title_curso">'+pegaTitle+'</div></li>');
		}

		//mostra sempre o primeiro registro | Adiciona a classe ativa ao primeiro registro da galeria
		$('.galeria03 ul li:eq(0)').addClass('ativo');
		$('.'+viewClass+' .views-row:eq(0)').show();
		
		/* Insere o primeiro vídeo */
			if($('#block-block-9').length > 0){
							
				var urlVideo = $('.view-mochiloes-destaques-pagina .views-row:eq(0) .views-field-field-mochiloes-video div').text();
				var getId = urlVideo.split("?v=");
				var idVideo = getId[1];			
							
				$('#box2col .col01').prepend('<iframe width="580" height="376" src="http://www.youtube.com/embed/'+idVideo+'?rel=0" frameborder="0" allowfullscreen></iframe>');

			}
		
		//clique das thumbs
		$('.galeria03 ul li').live('click', function(){
			var pegaRel = $(this).attr('rel');

			$('.galeria03 ul li').removeClass('ativo');
			$('.'+viewClass+' .views-row').hide();
			
			$(this).addClass('ativo');
			$('.'+viewClass+' #'+pegaRel+'').show();
			
			
			/* Carrega o video no box2 abaixo */
			if($('#block-block-9').length > 0){
	
			var ref = $(this).index();
						
			var urlVideo = $('.view-mochiloes-destaques-pagina .views-row:eq('+ref+') .views-field-field-mochiloes-video div').text();
			var getId = urlVideo.split("?v=");
			var idVideo = getId[1];			
			
			$('#box2col .col01').find('iframe').remove();			
			$('#box2col .col01').prepend('<iframe width="580" height="376" src="http://www.youtube.com/embed/'+idVideo+'?rel=0" frameborder="0" allowfullscreen></iframe>');			
		
			}
		});

		//Pega a URL da página
		var url = window.location.href;
		var seParametro = url.split("?");
		
		//Verifica se a URL está com parâmetro | Se estiver com parâmetro,
		//pega o nid do li e exibe o view row com a classe igual ao nid.
		//Se não tiver parâmetro exibe o primeiro registro e ativa a primeira li da galeria.
		if(seParametro.length > 1){
			var nid = seParametro[1].split("=");
			$('.galeria03 ul li').removeClass('ativo');
			$('.'+viewClass+' .views-row').hide();

			$('.galeria03 ul li#'+nid[1]+'').addClass('ativo');
			$('.'+viewClass+' .'+nid[1]+'').show();
		}else{
			$('.'+viewClass+' .views-row:eq(0)').show();
			$('.galeria03 ul li:eq(0)').addClass('ativo');
		}
	}

	//Curso de idiomas - destaques
	if($('.view-cursos-idiomas-destaques-pagina')[0]){
		var minhaViewClass = "view-cursos-idiomas-destaques-pagina";
		var meuBlockId = "block-block-3";
		galeriaInterna(minhaViewClass, meuBlockId);
	}
	
	//Curso de idiomas - Profissionalizantes
	if($('.view-cursos-idiomas-profissionalizantes-pagina')[0]){
		var minhaViewClass = "view-cursos-idiomas-profissionalizantes-pagina";
		var meuBlockId = "block-block-4";
		galeriaInterna(minhaViewClass, meuBlockId);
	}

	//Teen Experience - Destaques
	if($('.view-teen-experience-destaques-pagina')[0]){
		var minhaViewClass = "view-teen-experience-destaques-pagina";
		var meuBlockId = "block-block-5";
		galeriaInterna(minhaViewClass, meuBlockId);
	}

	//High School - Destaques
	if($('.view-high-school-destaques-pagina')[0]){
		var minhaViewClass = "view-high-school-destaques-pagina";
		var meuBlockId = "block-block-6";
		galeriaInterna(minhaViewClass, meuBlockId);
	}

	//Study And Work | Estudar - Destaques
	if($('.view-study-and-work-destaques-pagina')[0]){
		var minhaViewClass = "view-study-and-work-destaques-pagina";
		var meuBlockId = "block-block-7";
		galeriaInterna(minhaViewClass, meuBlockId);
	}

	//Study And Work | Trabalhar - Destaques
	if($('.view-study-and-work-destaques-pagina-trabalhar')[0]){
		var minhaViewClass = "view-study-and-work-destaques-pagina-trabalhar";
		var meuBlockId = "block-block-8";
		galeriaInterna(minhaViewClass, meuBlockId);
	}

	//
	if($('.view-destinos-por-continente')[0]){
		var minhaViewClass = "view-destinos-por-continente";
		// var meuBlockId = "block-block-7";
		var meuBlockId = "block-block-20";
		galeriaDestinos(minhaViewClass, meuBlockId);
		
		$('.galeria03').append('<a href="javascript:void(0)" class="btLeft"></a><a href="javascript:void(0)" class="btRight"></a>');

		// monta o vídeo

		/*
			var getUrlVideo = $('.view-destinos-por-continente .views-field-field-destinos-video .field-content').html();
			var idVIdeo = getUrlVideo.split('=');
			$('.view-destinos-por-continente .views-field-body .col01').append('<iframe width="580px" height="380px" src="http://www.youtube.com/embed/'+idVIdeo[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');						
		*/

		$('.view-destinos-por-continente .view-content .views-row').each(function(){

			var getUrlVideo = $(this).children('.views-field-field-destinos-video').children('.field-content').html();
			var idVIdeo = getUrlVideo.split('=');

			$(this).children('.views-field-body').children('.field-content').children('#box2col').children('.col01').append('<iframe width="580px" height="380px" src="http://www.youtube.com/embed/'+idVIdeo[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');			

		});


	}	

	//Mochilões
	if($('.view-id-mochiloes_destaques_pagina')[0]){
		// monta a visualização
	 	var minhaViewClass = "view-id-mochiloes_destaques_pagina";
	 	var meuBlockId = "block-block-9";
		galeriaMochiloes(minhaViewClass, meuBlockId);
		
		// monta o vídeo
		var getUrlVideo = $('.view-id-mochiloes_destaques_pagina .views-field-field-mochiloes-video .field-content').html();
		var idVIdeo = getUrlVideo.split('=');
		$('.view-id-mochiloes_destaques_pagina .views-field-body .col01').append('<iframe width="580px" height="380px" src="http://www.youtube.com/embed/'+idVIdeo[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');

		//monta o mapa
		var getMapUrl = $('.view-id-mochiloes_destaques_pagina .views-field-field-embed-mapa .field-content').html();
		$(this).find('.views-field-field-embed-mapa').html('<div id="mapviewer"><iframe id="map" Name="mapFrame" scrolling="no" width="910" height="442" frameborder="0" src="http://br.bing.com/maps/embed/?'+getMapUrl+'w=910&amp;h=442"></iframe></div>');
	}

	
	if($('.galeria03').length){		
		//Scroll das galerias internas quando necessário
		// verifica a qtd para ver se ativa o scroll
		
		var qtdCompara = $('.galeria03 .scroll li').size();

		if( qtdCompara > 5){
			// Função que faz o slide funcionar
			var largura_item = 122;
			var qtd_items = $('.galeria03 .scroll li').size();
			var limite = qtd_items - 5;
			var posicao_scroll = parseInt($('.galeria03 .container').css('marginLeft'));
			
			var contador = 0;
			/*$('.galeria03 a.btRight').click(function(){
				posicao_scroll = parseInt($('.galeria03 .container').css('marginLeft'));
				if(contador == limite){
					return false;		
				}else{								
					contador++;
					$(this).hide();
					$('.galeria03 .container').stop().animate({
						marginLeft : posicao_scroll-largura_item
					}, 300, function(){
						if(contador != limite){
							$(this).show();
						}
					});				
				}	
			});
			$('.galeria03 a.btLeft').click(function(){				
				posicao_scroll = parseInt($('.galeria03 .container').css('marginLeft'));
				if(contador == 0){
					return false;
				}else{
					contador--;
					$(this).hide();
					$('.galeria03 .container').stop().animate({
						marginLeft : posicao_scroll+largura_item
					}, 300, function(){
						if(contador != 0){
							$(this).show();
						}						
					});				
				}		
			});*/
			$('.galeria03 a.btRight').click(function(){
				posicao_scroll = parseInt($('.galeria03 .container').css('marginLeft'));				
				if(contador == limite){
					$(this).hide();
					return false;		
				}else{			
					contador++;
					$('.btLeft').fadeTo(10,1);										
					$(this).hide();
					$('.galeria03 .container').stop().animate({
						marginLeft : posicao_scroll-largura_item
					}, 400, function(){												
						if(contador == limite && qtd_items > 5){
							//$('.btRight').fadeTo(10,0.5);							
							$('.btLeft').fadeTo(10,1);
						}else if(contador == limite){
							//$('.btRight').fadeTo(10,0.5);							
						}else{
							$('.btRight').show();
						}						
					});				
				}	
			});
			$('.galeria03 a.btLeft').click(function(){
				posicao_scroll = parseInt($('.galeria03 .container').css('marginLeft'));
				if(contador == 0){
					$(this).hide();
					return false;
				}else{			
					contador--;					
					$('.btRight').fadeTo(10,1);
					$(this).hide();	
					$('.galeria03 .container').stop().animate({
						marginLeft : posicao_scroll+largura_item
					}, 400, function(){							
						if(contador == 0 && qtd_items > 5){
							//$('.btLeft').fadeTo(10,0.5);							
							$('.btRight').fadeTo(10,1);
						}else if(contador == 0){
							//$('.btLeft').fadeTo(10,0.5);							
						}else{
							$('.btLeft').show();
						}				
					});				
				}		
			});
		}else{
			$('.galeria03 a.btRight').css('display', 'none');
			$('.galeria03 a.btLeft').css('display', 'none');
		}	
	}


	if($('.galeria20').length > 0){
		//Scroll das galerias internas quando necessário
		// verifica a qtd para ver se ativa o scrool
		var qtdCompara = $('.galeria20 .scroll .views-row').size();
		if( qtdCompara > 4){
			// Função que faz o slide funcionar
			var largura_item = 215;
			var qtd_items = $('.galeria20 .scroll .views-row').size();
			var limite = qtd_items - 4;
			var posicao_scroll = parseInt($('.galeria20 .container').css('marginLeft'));
			
			var contador = 0;
			$('.galeria20 a.btRight').click(function(){
				posicao_scroll = parseInt($('.galeria20 .container').css('marginLeft'));
				if(contador == limite){
					return false;		
				}else{			
					contador++;
					$('.galeria20 .container').animate({
						marginLeft : posicao_scroll-largura_item
					}, 600, function(){
						if(contador == limite && qtd_items > 4){
							$('.btRight').fadeTo(10,0.1);
							$('.btLeft').fadeTo(10,1);
						}else if(contador == limite){
							$('.btRight').fadeTo(10,0.1);
						}				
					});				
				}	
			});
			$('.galeria20 a.btLeft').click(function(){
				posicao_scroll = parseInt($('.galeria20 .container').css('marginLeft'));
				if(contador == 0){
					return false;
				}else{			
					contador--;
					$('.galeria20 .container').animate({
						marginLeft : posicao_scroll+largura_item
					}, 600, function(){
						if(contador == 0 && qtd_items > 4){
							$('.btLeft').fadeTo(10,0.1);
							$('.btRight').fadeTo(10,1);
						}else if(contador == 0){
							$('.btLeft').fadeTo(10,0.1);
						}				
					});				
				}		
			});
		}else{
			$('.galeria20 a.btRight').css('display', 'none');
			$('.galeria20 a.btLeft').css('display', 'none');
		}	
	}

// GALERIA da Pagina Feira de Contratação
	if($('.view-feira-de-contratacao')[0]){
		$('#block-views-empresas-block').remove();
		$('#block-block-19').css({'background':' url(http://www.ieintercambio.com.br/sites/all/themes/iesite/img/bg_setas_feiras.png) 0 268px no-repeat'});
		$('.view-feira-de-contratacao').append('<a class="btLeft"></a><a class="btRight"></a>');
		
		var status = $('.views-field-status span').text();
		var url = window.location.href;
		var splitUrl = url.split("/");
		
		
		if(status == 'On'){
			if(splitUrl[4] == 'trainee'){
				$('#block-block-19').show();
				$('#block-block-16').css({'top':'750px'});
			}	
		}else{
			if(splitUrl[4] == 'trainee'){
				$('#block-block-19').hide();
				$('#block-block-16').css({'top':'350px'});
			}
		}
		
		if(status == 'On'){
			if(splitUrl[4] == 'workexperience'){		
				$('#block-block-19').show();
				$('#block-block-16').css({'top':'395px'});
			}	
		}else{
			if(splitUrl[4] == 'workexperience'){
				$('#block-block-19').hide();
				$('#block-block-16').css({'top':'0'});
			}	
		}

		
		//Scroll das galerias internas quando necessário
		// verifica a qtd para ver se ativa o scroll
		var qtdCompara = $('.views-field-field-feiras-empresas .views-row').size();
		if( qtdCompara > 5){
			// Função que faz o slide funcionar
			var largura_item = $('.views-field-field-feiras-empresas .views-row').width();
			var larguraFinal = largura_item + 10;
			var qtd_items = $('.views-field-field-feiras-empresas .views-row').size();
			var limite = qtd_items - 5;
			$('.views-field-field-feiras-empresas').children().addClass('conteudo');
			var posicao_scroll = parseInt($('.views-field-field-feiras-empresas .conteudo').css('marginLeft'));
			
			
			var contador = 0;
			$('.view-feira-de-contratacao a.btRight').click(function(){
				posicao_scroll = parseInt($('.views-field-field-feiras-empresas .conteudo').css('marginLeft'));				
				if(contador == limite){
					$(this).fadeTo(10,0.5);
					return false;		
				}else{			
					contador++;
					$('.btLeft').fadeTo(10,1);										
					$(this).hide();										
					$('.views-field-field-feiras-empresas .conteudo').stop().animate({
						marginLeft : posicao_scroll-larguraFinal
					}, 400, function(){												
						if(contador == limite && qtd_items > 5){
							//$('.btRight').fadeTo(10,0.5);							
							$('.btLeft').fadeTo(10,1);
						}else if(contador == limite){
							//$('.btRight').fadeTo(10,0.5);							
						}else{
							$('.btRight').show();
						}						
					});				
				}	
			});
			$('.view-feira-de-contratacao a.btLeft').click(function(){
				posicao_scroll = parseInt($('.views-field-field-feiras-empresas .conteudo').css('marginLeft'));
				if(contador == 0){
					$(this).fadeTo(10,0.5);	
					return false;
				}else{			
					contador--;					
					$('.btRight').fadeTo(10,1);
					$(this).hide();	
					$('.views-field-field-feiras-empresas .conteudo').stop().animate({
						marginLeft : posicao_scroll+larguraFinal
					}, 400, function(){							
						if(contador == 0 && qtd_items > 5){
							//$('.btLeft').fadeTo(10,0.5);							
							$('.btRight').fadeTo(10,1);
						}else if(contador == 0){
							//$('.btLeft').fadeTo(10,0.5);							
						}else{
							$('.btLeft').show();
						}				
					});				
				}		
			});
		}else{
			$('.view-feira-de-contratacao a.btRight').css('opacity', '.3');
			$('.view-feira-de-contratacao a.btLeft').css('opacity', '.3');
		}

		$('.view-feira-de-contratacao .views-field-nothing').hide();
		$('.view-feira-de-contratacao .views-field-field-feiras-empresas .views-row').mouseenter(function(){
				$(this).find('.views-field-nothing').show().append('<div class="seta"></div>');
				//$('.view-feira-de-contratacao .views-field-field-feiras-empresas .views-row:eq(5)').find('.views-field-nothing').css('left', '-103px');
				//$('.view-feira-de-contratacao .views-field-field-feiras-empresas .views-row:eq(5)').find('.views-field-nothing .seta').css('left', '145px');
			}).mouseleave(function(){
				$(this).find('.views-field-nothing').hide();
				$('.views-field-nothing .seta').remove();
			});
	}


	//galeria sem filtros
	if($('#block-block-15').length > 0){	

		var ativo = true;
		var indice = 1;
		var qtdFotos = $('#block-block-15 .view-fotos-sem-filtro-por-pais .views-row').size();			
		var qtdVideos = $('#block-block-15 .view-videos-sem-filtro-por-pais .views-row').size();
		qtdFotos--;
		qtdVideos--;
		
		$('#block-block-15 .view-fotos-sem-filtro-por-pais').mouseenter(function(){ativo = false;});
		$('#block-block-15 .view-fotos-sem-filtro-por-pais').mouseleave(function(){ativo = true;});
		
		$('#block-block-15 .view-videos-sem-filtro-por-pais').mouseenter(function(){ativo = false;});
		$('#block-block-15 .view-videos-sem-filtro-por-pais').mouseleave(function(){ativo = true;});
		
		var interval = window.setInterval(function(){			
			if(ativo){
				if(indice != qtdFotos ){	
					$('#block-block-15 .view-fotos-sem-filtro-por-pais .mCSB_container img:eq('+indice+')').click();
					$('#block-block-15 .view-videos-sem-filtro-por-pais .mCSB_container img:eq('+indice+')').click();
					indice++;
				}else{
					indice = 0;
				}			
			}
			//alert(contaClique);
		},5000);			
		
		
		//Bullets de navegação 		
			var qtdItensExperiencia = $('.view-experiencias-sem-filtro-por-pais .views-row').size();
			for(k=0;k<qtdItensExperiencia;k++){
				$('.view-experiencias-sem-filtro-por-pais .view-footer .navegacao').append('<div class="bullet">'+k+'</div>');
			}

			$('.view-experiencias-sem-filtro-por-pais .view-footer .navegacao .bullet:eq(0)').addClass('ativo');

			$('.navegacao .bullet').click(function(){
			
				if( $(this).parent().parent().hasClass('barraExperiencias') ){
							
					var pegaIndiceBullet = $(this).index();
					$('.barraExperiencias .navegacao .bullet').removeClass('ativo');
					$(this).addClass('ativo');

					var tamPadrao = $('.view-experiencias-sem-filtro-por-pais .views-row').width();
					var vaiPos = tamPadrao*pegaIndiceBullet;
					$('.view-experiencias-sem-filtro-por-pais .view-content').animate({left: -vaiPos}, 300);
			
				}else{
				
					var pegaIndiceBullet = $(this).index();
					$(' .navegacao .bullet').removeClass('ativo');
					$(this).addClass('ativo');

					var tamPadrao = $('.view-mochiloes .grupo').width();
					var vaiPos = tamPadrao*pegaIndiceBullet;
					$('.view-mochiloes .conteudo').animate({left: -vaiPos}, 300);
					
				}
			
			});
				

		// monta o vídeo e a thumb
			
			//adiciona a div que faz o scroll das fotos
				$('.view-fotos-sem-filtro-por-pais').append('<div class="thumbImage"></div>');

			//verifica a quantidade de itens
				var qtdItensFotos = $('.view-fotos-sem-filtro-por-pais .views-row').size();

			//monta as thumbs das fotos
				for(f=0;f<qtdItensFotos;f++){
					var fazThumb = $('.view-fotos-sem-filtro-por-pais .views-row:eq('+f+') .views-field-field-fotos-imagem .field-content').html();
					$('.view-fotos-sem-filtro-por-pais .thumbImage').append(fazThumb);
				}

			// clique das thumbs
				$('.view-fotos-sem-filtro-por-pais .thumbImage img').click(function(){
					var indice = $(this).index();
					$('.view-fotos-sem-filtro-por-pais .views-row').fadeTo(300, 0);	
					$('.view-fotos-sem-filtro-por-pais .views-row:eq('+indice+')').fadeTo(300, 1);
				});

			//verifica a quantidade de itens
				var qtdItensVideos = $('.view-videos-sem-filtro-por-pais .views-row').size();

			//adiciona a div que faz o scroll dos vídeos
				$('.view-videos-sem-filtro-por-pais').append('<div class="thumbVideos"></div>');

			//adiciona a div com o player
				var getUrlVideoInicial = $('.view-videos-sem-filtro-por-pais .views-row:eq(0) .views-field-field-url-do-video .field-content').html();
				if(getUrlVideoInicial != null){
					var idVIdeoInicial = getUrlVideoInicial.split('&amp;list');
					if(idVIdeoInicial[0] != null){						
						var idVideoLista = idVIdeoInicial[0].split('?v=');						
						$('.view-videos-sem-filtro-por-pais').append('<div class="playerVideo"><iframe width="283px" height="204px" src="http://www.youtube.com/embed/'+idVideoLista[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');
					}else{						
						idVIdeoInicial = getUrlVideoInicial.split('?v=');
						$('.view-videos-sem-filtro-por-pais').append('<div class="playerVideo"><iframe width="283px" height="204px" src="http://www.youtube.com/embed/'+idVIdeoInicial[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');
					}
					
				}

			//puxa a thumb do video no youtube
				for(v=0;v<qtdItensVideos;v++){
				 	var getUrlVideo = $('.view-videos-sem-filtro-por-pais .views-row:eq('+v+') .views-field-field-url-do-video .field-content').html();	
				 	var idVIdeo = getUrlVideo.split('&amp;list');
					if(idVIdeo[0] != null){
						var idVideoL = idVIdeo[0].split('?v=');
						$('.view-videos-sem-filtro-por-pais .thumbVideos').append('<img src="http://img.youtube.com/vi/'+idVideoL[1]+'/0.jpg">');						
						$('.view-videos-sem-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+idVideoL[1]+'?rel=0');
					}else{
						idVIdeo = getUrlVideo.split('?v=');
						$('.view-videos-sem-filtro-por-pais .thumbVideos').append('<img src="http://img.youtube.com/vi/'+idVideoL[1]+'/0.jpg">');						
						$('.view-videos-sem-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+idVideoL[1]+'?rel=0');
					}
				}

			// Clique das thumbs dos vídeos
				$('.view-videos-sem-filtro-por-pais .thumbVideos img').click(function(){
					// alert('clique do thumb');
					var pegaSrc = $(this).attr('src');
					var geraId = pegaSrc.split('/');

					$('.view-videos-sem-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+geraId[4]+'?rel=0');
					
				});

		//inicia a visualização da galeria
			$('.view-fotos-sem-filtro-por-pais .views-row:eq(0)').show();
			
			$('.galeriaPrincipal .link a:eq(0)').addClass('ativo');
			$('.view-fotos-sem-filtro-por-pais').show();
			$('.view-videos-sem-filtro-por-pais').hide();

		//troca a visualização de fotos para vídeos
			$('.galeriaPrincipal .link a').click(function(){

				var pegaId = $(this).attr('id');
				
				$('.galeriaPrincipal .link a').removeClass('ativo');
				$(this).addClass('ativo');

				if(pegaId == "fotos"){
					$('.galeriaPrincipal .foto').show();
					$('.galeriaPrincipal .video').hide();

					$('.view-fotos-sem-filtro-por-pais').show();
					$('.view-videos-sem-filtro-por-pais').hide();
				}else{
					$('.galeriaPrincipal .foto').hide();
					$('.galeriaPrincipal .video').show();
					
					$('.view-videos-sem-filtro-por-pais').show();
					$('.view-fotos-sem-filtro-por-pais').hide();				
				}
			});

		//inicia o scroll das fotos e dos vídeos
		$(window).load(function(){
			$(".thumbImage, .thumbVideos").mCustomScrollbar({
				horizontalScroll:true,
				set_width:false,
                set_height:false,
				advanced:{
					updateOnBrowserResize:false, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                    autoExpandHorizontalScroll:true /*auto expand width for horizontal scrolling: boolean*/
				}
			});

		});
	}

	//galeria com filtros
	if($('#block-block-16').length > 0){
	
		var ativo = true;
		var indice = 1;
		var qtdFotos = $('#block-block-16 .view-fotos-com-filtro-por-pais .views-row').size();			
		var qtdVideos = $('#block-block-16 .view-videos-com-filtro-por-pais .views-row').size();
		qtdFotos--;
		qtdVideos--;
		
		$('#block-block-16 .view-fotos-com-filtro-por-pais').mouseenter(function(){ativo = false;});
		$('#block-block-16 .view-fotos-com-filtro-por-pais').mouseleave(function(){ativo = true;});
		
		$('#block-block-16 .view-videos-com-filtro-por-pais').mouseenter(function(){ativo = false;});
		$('#block-block-16 .view-videos-com-filtro-por-pais').mouseleave(function(){ativo = true;});
		
		var interval = window.setInterval(function(){			
			if(ativo){
				if(indice != qtdFotos ){	
					$('#block-block-16 .view-fotos-com-filtro-por-pais .mCSB_container img:eq('+indice+')').click();
					$('#block-block-16 .view-videos-com-filtro-por-pais .mCSB_container img:eq('+indice+')').click();
					indice++;
				}else{
					indice = 0;
				}			
			}
			//alert(contaClique);
		},5000);
		
		//Bullets de navegação 		
			var qtdItensExperiencia = $('.view-experiencias-com-filtro-por-pais .views-row').size();
			for(k=0;k<qtdItensExperiencia;k++){
				$('.view-experiencias-com-filtro-por-pais .view-footer .navegacao').append('<div class="bullet">'+k+'</div>');
			}

			$('.view-experiencias-com-filtro-por-pais .view-footer .navegacao .bullet:eq(0)').addClass('ativo');

			$('.view-experiencias-com-filtro-por-pais .view-content').addClass('conteudo2');

			$('.navegacao .bullet').click(function(){
			
				if( $(this).parent().parent().hasClass('barraExperiencias') ){

					var pegaIndiceBullet = $(this).index();
					$('.barraExperiencias .navegacao .bullet').removeClass('ativo');
					$(this).addClass('ativo');

					var tamPadrao = $('.view-experiencias-com-filtro-por-pais .views-row').width();
					var vaiPos = tamPadrao*pegaIndiceBullet;
					$('.view-experiencias-com-filtro-por-pais .conteudo2').animate({left: -vaiPos}, 300);
				
				}else{
				
					var pegaIndiceBullet = $(this).index();
					$('.view-mochiloes .navegacao .bullet').removeClass('ativo');
					$(this).addClass('ativo');

					var tamPadrao = $('.view-mochiloes .grupo').width();
					var vaiPos = tamPadrao*pegaIndiceBullet;
					$('.view-mochiloes .conteudo').animate({left: -vaiPos}, 300);
				
				}
			
			});
					
		
		// monta o video e a thumb
			
			//adiciona a div que faz o scroll das fotos
				$('.view-fotos-com-filtro-por-pais').append('<div class="thumbImage"></div>');

			//verifica a quantidade de itens
				var qtdItensFotos = $('.view-fotos-com-filtro-por-pais .views-row').size();

			//monta as thumbs das fotos
				for(f=0;f<qtdItensFotos;f++){
					var fazThumb = $('.view-fotos-com-filtro-por-pais .views-row:eq('+f+') .views-field-field-fotos-imagem .field-content').html();
					$('.view-fotos-com-filtro-por-pais .thumbImage').append(fazThumb);
				}

			// clique das thumbs
				$('.view-fotos-com-filtro-por-pais .thumbImage img').click(function(){
					var indice = $(this).index();
					$('.view-fotos-com-filtro-por-pais .views-row').fadeTo(300, 0);	
					$('.view-fotos-com-filtro-por-pais .views-row:eq('+indice+')').fadeTo(300, 1);
				});

			//verifica a quantidade de itens
				var qtdItensVideos = $('.view-videos-com-filtro-por-pais .views-row').size();

			//adiciona a div que faz o scroll dos vídeos
				$('.view-videos-com-filtro-por-pais').append('<div class="thumbVideos"></div>');

			//adiciona a div com o player
				var getUrlVideoInicial = $('.view-videos-com-filtro-por-pais .views-row:eq(0) .views-field-field-url-do-video .field-content').html();
				if(getUrlVideoInicial != null){
					var idVIdeoInicial = getUrlVideoInicial.split('&amp;list');
					if(idVIdeoInicial[0] != null){						
						var idVideoLista = idVIdeoInicial[0].split('?v=');						
						$('.view-videos-com-filtro-por-pais').append('<div class="playerVideo"><iframe width="283px" height="204px" src="http://www.youtube.com/embed/'+idVideoLista[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');
					}else{						
						idVIdeoInicial = getUrlVideoInicial.split('?v=');
						$('.view-videos-com-filtro-por-pais').append('<div class="playerVideo"><iframe width="283px" height="204px" src="http://www.youtube.com/embed/'+idVIdeoInicial[1]+'?rel=0" frameborder="0" allowfullscreen></iframe></div>');
					}
					
				}

			//puxa a thumb do video no youtube
				for(v=0;v<qtdItensVideos;v++){
				 	var getUrlVideo = $('.view-videos-com-filtro-por-pais .views-row:eq('+v+') .views-field-field-url-do-video .field-content').html();	
				 	var idVIdeo = getUrlVideo.split('&amp;list');
					if(idVIdeo[0] != null){
						var idVideoL = idVIdeo[0].split('?v=');
						$('.view-videos-com-filtro-por-pais .thumbVideos').append('<img src="http://img.youtube.com/vi/'+idVideoL[1]+'/0.jpg">');						
						$('.view-videos-com-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+idVideoL[1]+'?rel=0');
					}else{
						idVIdeo = getUrlVideo.split('?v=');
						$('.view-videos-com-filtro-por-pais .thumbVideos').append('<img src="http://img.youtube.com/vi/'+idVideoL[1]+'/0.jpg">');						
						$('.view-videos-com-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+idVideoL[1]+'?rel=0');
					}
				}

			// Clique das thumbs dos videos
				$('.view-videos-com-filtro-por-pais .thumbVideos img').click(function(){
					// alert('clique do thumb');
					var pegaSrc = $(this).attr('src');
					var geraId = pegaSrc.split('/');

					$('.view-videos-com-filtro-por-pais .playerVideo iframe').attr('src', 'http://www.youtube.com/embed/'+geraId[4]+'?rel=0');
					
				});

		//inicia a visualização da galeria
			$('.view-fotos-com-filtro-por-pais .views-row:eq(0)').show();
			
			$('.galeriaPrincipal .link a:eq(0)').addClass('ativo');
			$('.view-fotos-com-filtro-por-pais').show();
			$('.view-videos-com-filtro-por-pais').hide();

		//troca a visualização de fotos para vídeos
			$('.galeriaPrincipal .link a').click(function(){

				var pegaId = $(this).attr('id');
				
				$('.galeriaPrincipal .link a').removeClass('ativo');
				$(this).addClass('ativo');

				if(pegaId == "fotos"){
					$('.galeriaPrincipal .foto').show();
					$('.galeriaPrincipal .video').hide();

					$('.view-fotos-com-filtro-por-pais').show();
					$('.view-videos-com-filtro-por-pais').hide();
				}else{
					$('.galeriaPrincipal .foto').hide();
					$('.galeriaPrincipal .video').show();
					
					$('.view-videos-com-filtro-por-pais').show();
					$('.view-fotos-com-filtro-por-pais').hide();				
				}
			});

		//inicia o scroll das fotos e dos vídeos
		$(window).load(function(){
			$(".thumbImage, .thumbVideos").mCustomScrollbar({
				horizontalScroll:true,
				set_width:false,
                set_height:false,
				advanced:{
					updateOnBrowserResize:false, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
                    updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
                    autoExpandHorizontalScroll:true /*auto expand width for horizontal scrolling: boolean*/
				}
			});

		});
	}
	
	if( $('.barraExperiencias')[0] ){
		var indExp = 1;
		var qtdBullets = $('.barraExperiencias .navegacao .bullet').size();		
		var intExp = window.setInterval(function(){				
			if(qtdBullets > indExp){	
				$('.barraExperiencias .navegacao .bullet:eq('+indExp+')').click();						
				indExp++;
			}else{ indExp = 0; }								
		},4000);
	}

	if($('.view-galeria-seja-um-franqueado').length > 0){

		var qtdItens = $('.view-galeria-seja-um-franqueado .views-row').size();
		var tamPadraoInicial = $('.view-galeria-seja-um-franqueado .views-row').width();
		var vaiPosInicial = (tamPadraoInicial*qtdItens)+1;
		$('.view-galeria-seja-um-franqueado .view-content').width(vaiPosInicial);

		//Bullets de navegação 		
			for(s=0;s<qtdItens;s++){
				$('.view-galeria-seja-um-franqueado .view-footer .barraItens').append('<div class="marcador">'+s+'</div>');
			}

			$('.view-galeria-seja-um-franqueado .view-footer .barraItens .marcador:eq(0)').addClass('ativo');

			$('.barraItens .marcador').click(function(){

				var pegaIndiceBullet = $(this).index();
				$('.barraItens .marcador').removeClass('ativo');
				$(this).addClass('ativo');

				var tamPadrao = $('.view-galeria-seja-um-franqueado .views-row').width();
				var vaiPos = tamPadrao*pegaIndiceBullet;
				$('.view-galeria-seja-um-franqueado .view-content').animate({left: -(vaiPos)}, 300);

			});
	}	
	
	
});