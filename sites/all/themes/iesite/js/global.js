jQuery(document).ready(function($) {

	
	
// --------------- ## HEADER ## ------------------

	
	//Mostra e esconde a label do campo de busca
	$('.form-busca .input-busca').focus(function(){
		$('.form-busca label').stop().fadeOut(400);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('label').stop().fadeIn(400);
		}
	});

	//Hover do menu principal	
	$('.menu-principal ul.primeiro-nivel li').mouseenter(function(){		
		$(this).css('height', '53').find('.segundo-nivel').show();
	}).mouseleave(function(){
		$(this).css('height', '45').find('.segundo-nivel').hide();
		$('.menu-principal ul.primeiro-nivel li.l03 .segundo-nivel').css({'top':'0'}).removeClass('segundo-nivel-footer');				
	});
	
	$('.menu-principal ul.primeiro-nivel li.l03 .segundo-nivel').prepend('<span class="close">X</span>');
	$('.menu-principal ul.primeiro-nivel li:eq(1)').addClass('li-trabalhar');	

	//Divide o menu de 3º nível	
	
	var getUrl = window.location.href;
	var arrayUrl = getUrl.split("/");
	var indiceUrl = arrayUrl[3].toString();
		
	
	if(indiceUrl == 'iexplorer'){
		var listaTotal = $('.menu-level-3 ul.menu li').size();	
		var listaFinal = ''	;
		listaTotal--;

		for (i=1; i<=listaTotal; i++){		
			var pegaLi = $('.menu-level-3 ul.menu li:eq('+i+')').html();
			listaFinal += '<li>'+pegaLi+'</li>';

			$('.menu-level-3 ul.menu li:eq('+i+')').hide();
		}

		var listaFinal = '<li><a href="http://site.ieintercambio.com.br/iexplorer/destinos/america">América</a></li><li><a href="http://site.ieintercambio.com.br/iexplorer/destinos/europa">Europa</a></li>';

		$('.menu-level-3 .menu').after('<div class="menu_33"><span>NAVEGAR POR CONTINENTE</span><ul class="menu_sel">'+listaFinal+'</ul></div>');
		$('.menu_sel').prependTo('#content');
	}
	
	
	$('.menu_33 span').click(function(){
		$('.menu_sel').slideToggle('fast');
	});

	//$('#content .menu_sel li a').removeClass('active');
	/*$('#content .menu_sel li a').click(function(){
		$('#content .menu_sel li a').removeClass('active');
		$(this).addClass('active');
	});*/

	
	//adiciona o ativo ao submenu de 3º nível
	$('.menu-level-3 ul.menu li.active').append('<div class="ativo">botão ativo</div>');

	//Centraliza o submenu de 3º nível
	var tamTotal = 450;
	var tamAtual = $('.page-title .fltrgt').width();
	var pxMargin = parseInt(tamTotal-tamAtual)/2;

	$('.page-title .fltrgt').css('margin-right', pxMargin);


	//WEBDOOR
	// Trata os títulos do webdoor
	$('.webdoor .views-row .textos div').each(function(){
		var pegaTexto = $(this).find('p.titulo').text();
		var quebraTexto = pegaTexto.split(' ');

		var qtd_itens = quebraTexto.length;
	//	console.log(qtd_itens);
		var frase_final = '';

		for(i=0; i<qtd_itens; i++){

			if(quebraTexto[i] == 'intercâmbios'){
				frase_final += '<strong>intercâmbios</strong> ';
			}else{
				frase_final += quebraTexto[i]+' ';  
			}
		}	
	//	console.log(frase_final);
		$(this).find('p.titulo').empty();
		$(this).find('p.titulo').html(frase_final);
	});


	//Ativa o primeiro item do webdoor.
	$('.webdoor .item-webdoor:eq(0)').addClass('wAtivo').find('.textos div:eq(0)').addClass('dados-ativo');

	//adiciona ID para identificar a imagem, o texto e o bullet com a div principal
	var qtd = $('.webdoor .item-webdoor').size();
	for(i=0; i<qtd; i++){		
		$('.bullets .navegacao').append('<li class="item'+i+'" rel="item'+i+'">item'+i+'</li>');
		$('.webdoor .item-webdoor:eq('+i+')').addClass('item'+i+'').find('.imagem img').attr('id', 'item'+i+'');		
	}

	//Ativa a primeira bullet
	$('.navegacao li:eq(0)').addClass('ativo');

	//executa a função que monta as thumbs
	montaThumbs();

	//Navegação por bullet
	$('.navegacao li').click(function(){			

		if(!$(this).hasClass('ativo')){

			var pegaRel = $(this).attr('class');

			$('.view-id-webdoors .view-content div').removeClass('wAtivo').find('.textos div:eq(0)').removeClass('dados-ativo');
			$('.view-id-webdoors').find('.'+pegaRel+'').addClass('wAtivo').find('.textos div:eq(0)').addClass('dados-ativo');
			montaThumbs();
			$('.navegacao li').removeClass('ativo');
			$(this).addClass('ativo');

		}

	});

	
	//navegação por thumbs
	function montaThumbs() {
		//Identifica o primeiro e o último item.
		var qtdWebdoor = ($('.webdoor .item-webdoor').size())-1;
		$('.webdoor .item-webdoor:eq(0)').attr('id', 'primeiro');
		$('.webdoor .item-webdoor:eq('+qtdWebdoor+')').attr('id', 'ultimo');

		//Verifica se é o primeiro ou o último item.
		var identificaId = $('.webdoor .wAtivo').attr('id');

		//Gera e aplica a thumb para navegação do anterior e do próximo.
		if(identificaId == "primeiro"){
			var pegaNextThumb = $('.webdoor .wAtivo').next('.item-webdoor').find('.imagem img').clone();
			var pegaPrevThumb = $('.webdoor').find('#ultimo').find('.imagem img').clone();
		}else if(identificaId == "ultimo"){
			var pegaNextThumb = $('.webdoor').find('#primeiro').find('.imagem img').clone();
			var pegaPrevThumb = $('.webdoor .wAtivo').prev('.item-webdoor').find('.imagem img').clone();
		}else{
			var pegaNextThumb = $('.webdoor .wAtivo').next('.item-webdoor').find('.imagem img').clone();
			var pegaPrevThumb = $('.webdoor .wAtivo').prev('.item-webdoor').find('.imagem img').clone();
		}
		
		//Aplica a thumb
		$('#thumbPrev').html(pegaPrevThumb);
		$('#thumbNext').html(pegaNextThumb);
	}
	
	//hover das thumbs
	$('#thumbPrev').hover(function(){
		$(this).append('<span class="itemPrev"></span>');
		$(this).children('span').stop().animate({right: '0'}, 300);
	}).mouseleave(function(){
		$(this).children('span').stop().animate({right: '-135px'}, 300, function(){
			$('#thumbPrev').find('span').remove();
		});
	});

	$('#thumbNext').hover(function(){
		$(this).append('<span class="itemNext"></span>');
		$(this).children('span').stop().animate({left: '0'}, 300);
	}).mouseleave(function(){
		$(this).children('span').stop().animate({left: '-135px'}, 300, function(){
			$('#thumbNext').find('span').remove();
		});
	});

	//Navegação por thumbnail
	$('.thumbs .thumWebdoor').click(function(){
		var pegaRel = $(this).children('img').attr('id');
		
		$('.view-id-webdoors .view-content div').removeClass('wAtivo').find('.textos div:eq(0)').removeClass('dados-ativo');
		$('.view-id-webdoors').find('.'+pegaRel+'').addClass('wAtivo').find('.textos div:eq(0)').addClass('dados-ativo');
		
		$('.navegacao li').removeClass('ativo');
		$('.navegacao li.'+pegaRel+'').addClass('ativo');
		montaThumbs()
	});

	//Identifica o primeiro e o último item.
		var qtdWebdoor = ($('.webdoor .item-webdoor').size())-1;
		$('.navegacao li:eq(0)').attr('id', 'primeiro');
		$('.navegacao li:eq('+qtdWebdoor+')').attr('id', 'ultimo');
	
	//aqui vem a troca de webdoor com timeout
	function trocaWebdoor(){
		var identificaId = $('.navegacao li.ativo').attr('id');
		var pegaRel = $('.navegacao li.ativo').next('li').attr('rel');

		if(identificaId == 'ultimo'){
			$('.webdoor div.wAtivo').removeClass('wAtivo').find('.textos div:eq(0)').removeClass('dados-ativo');
			$('.webdoor .item-webdoor:eq(0)').addClass('wAtivo').find('.textos div:eq(0)').addClass('dados-ativo');
			$('.navegacao li').removeClass('ativo');
			$('.navegacao li:eq(0)').addClass('ativo');
		}else{
			$('.webdoor div.wAtivo').removeClass('wAtivo').find('.textos div:eq(0)').removeClass('dados-ativo');
			$('.view-id-webdoors').find('.'+pegaRel+'').addClass('wAtivo').find('.textos div:eq(0)').addClass('dados-ativo');
			$('.navegacao li.ativo').next('li').addClass('ativo').prev('li').removeClass('ativo');
		}
	}

	// troca automática do webdoor
	setInterval(function() {
      trocaWebdoor()
      montaThumbs()
	}, 10000);



// --------------- ## CONTENT ## ------------------

	//Detalhes dos Boxes
	$('#box3col .col01').prepend('<span class="bicot"></span><span class="bicob"></span>');

	// PÁGINA CIDADES INTERNA
	if($('.node-cidades')[0]){
		$('.field-name-field-cidades-cidade .field-items').prepend('<div class="boxIcon">&nbsp;</div>');
		$('.field-name-field-cidades-imagem-1').append('<div class="espaco2">&nbsp;</div>');
	}

	// PÁGINA PROMOÇÕES
	if($('.view-promooes-destaque-pagina')[0]){
		$('.view-promooes-destaque-pagina .views-row').append('<div class="bread">&nbsp;</div>');
		$('.view-promooes-destaque-pagina .views-row:nth-child(2n)').css('margin-right', '0');
	}

	if($('.view-id-promoces')[0]){
		$('.view-id-promoces .views-row').append('<div class="bread">&nbsp;</div>');
		$('.view-id-promoces .views-row .views-field-field-promocoes-imagem .field-content').append('<div class="dat">&nbsp;</div>');

		var pegaPais = $('.view-id-promoces .views-row:eq(0) .views-field-field-promocoes-pais .field-content');
		$('.galeria20').prepend('<h3>'+pegaPais.html()+'</h3>');
		$('.view-id-promoces .views-row').each(function(){
			$(this).find('.views-field-field-promocoes-pais .field-content').remove();
		});
	}

	// PÁGINA FEIRA DE CONTRATAÇÃO
	if($('.view-feira-de-contratacao')[0]){
		$('.views-field-field-feiras-datas').prepend('<span>Datas e <strong>Locais</strong></span>');

		$('.views-field-field-feiras-datas li').each(function(){
			var pegaTexto = $(this).find('p').text();
			var quebraTexto = pegaTexto.split('-');
			$(this).find('p').remove();
			
			$(this).prepend('<div class="datas"><strong>'+quebraTexto[0]+'</strong>'+quebraTexto[1]+'</div>');
		});
	}

	// Promoções da Home
	$('#block-views-promocoes-destaque-block .view-promocoes-destaque').before('<div id="box1col"><div class="fltlft boxIcon icone boxcolor"></div><div class="fltlft boxTitle">PROMOÇÕES  IE</div><div class="clear"></div><div class="fltlft boxBar"></div><div class="fltlft box"></div></div>');

	// Monta o player do bloco TV IE
	if($('.view-tv-ie')[0]){
		
		var getUrlVideo = $('.view-tv-ie .views-row .views-field-field-video .field-content').text();
		var indexItem = getUrlVideo.indexOf('&list');
		//alert(indexItem);			
		
		if(indexItem < 0){
			var idVIdeo = getUrlVideo.split('=');
			$('.view-tv-ie .views-row .views-field-field-video .field-content').html('<iframe id="video-home" width="287px" height="195px" src="http://www.youtube.com/embed/'+idVIdeo[1]+'?wmode=transparent" frameborder="0" allowfullscreen></iframe></div>');
		}else{			
			var testeUrl = getUrlVideo.split('&list');			
			var idLista = testeUrl[0].split('?v=');			
			$('.view-tv-ie .views-row .views-field-field-video .field-content').html('<iframe id="video-home" width="287px" height="195px" src="http://www.youtube.com/embed/'+idLista[1]+'?wmode=transparent" frameborder="0" allowfullscreen></iframe></div>');
		}		
	}
	
	// Move o conteúdo do Vídeo pra dentro do box	
	$('.view-tv-ie .view-content').prependTo('.view-tv-ie #box1col .fltlft.box');

	// Galeria dos mochilões
	if($('.view-mochiloes')[0]){
		var qtdItens = $('.view-mochiloes .views-row').size();
		var qtdDivs = qtdItens/4; 

		if(qtdDivs > 1 && qtdDivs < 2){
			qtdDivs = 2;
		}else if(qtdDivs > 2 && qtdDivs < 3){
			qtdDivs = 3;
		}else if(qtdDivs > 3 && qtdDivs < 4){
			qtdDivs = 4;
		}else if(qtdDivs > 4 && qtdDivs < 5){
			qtdDivs = 5;
		}else if(qtdDivs > 5 && qtdDivs < 6){
			qtdDivs = 6;
		}else if(qtdDivs > 6 && qtdDivs < 7){
			qtdDivs = 7;
		}

		for(i=0;i<qtdDivs;i++){
			$('.view-mochiloes .view-content').append('<div class="grupo grupo'+i+'"></div>');
		}

		if($('.grupo0')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(0)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(1)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(2)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(3)').clone();

			$('.view-mochiloes .grupo0').append(linha0);
			$('.view-mochiloes .grupo0').append(linha1);
			$('.view-mochiloes .grupo0').append(linha2);
			$('.view-mochiloes .grupo0').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}

		if($('.grupo1')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(4)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(5)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(6)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(7)').clone();

			$('.view-mochiloes .grupo1').append(linha0);
			$('.view-mochiloes .grupo1').append(linha1);
			$('.view-mochiloes .grupo1').append(linha2);
			$('.view-mochiloes .grupo1').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}
				
		if($('.grupo2')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(8)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(9)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(10)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(11)').clone();

			$('.view-mochiloes .grupo2').append(linha0);
			$('.view-mochiloes .grupo2').append(linha1);
			$('.view-mochiloes .grupo2').append(linha2);
			$('.view-mochiloes .grupo2').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}

		if($('.grupo3')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(12)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(13)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(14)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(15)').clone();

			$('.view-mochiloes .grupo3').append(linha0);
			$('.view-mochiloes .grupo3').append(linha1);
			$('.view-mochiloes .grupo3').append(linha2);
			$('.view-mochiloes .grupo3').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}

		if($('.grupo4')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(16)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(17)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(18)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(19)').clone();

			$('.view-mochiloes .grupo4').append(linha0);
			$('.view-mochiloes .grupo4').append(linha1);
			$('.view-mochiloes .grupo4').append(linha2);
			$('.view-mochiloes .grupo4').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}

		if($('.grupo5')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(20)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(21)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(22)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(23)').clone();

			$('.view-mochiloes .grupo5').append(linha0);
			$('.view-mochiloes .grupo5').append(linha1);
			$('.view-mochiloes .grupo5').append(linha2);
			$('.view-mochiloes .grupo5').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');
		}

		if($('.grupo6')[0]){
			var linha0 = $('.view-mochiloes .views-row:eq(24)').clone();
			var linha1 = $('.view-mochiloes .views-row:eq(25)').clone();
			var linha2 = $('.view-mochiloes .views-row:eq(26)').clone();
			var linha3 = $('.view-mochiloes .views-row:eq(27)').clone();

			$('.view-mochiloes .grupo6').append(linha0);
			$('.view-mochiloes .grupo6').append(linha1);
			$('.view-mochiloes .grupo6').append(linha2);
			$('.view-mochiloes .grupo6').append(linha3);

			$('.view-mochiloes .grupo .views-row').css('display', 'block');

			//cria o slide dos mochilões
			$('.view-mochiloes .view-content').before('<div class="navegacao"></div>');
		}

		$('.view-mochiloes').append('<div class="navegacao"></div>');
		var qtdGrupos = $('.view-mochiloes .view-content .grupo').size();
		
		$('.view-mochiloes .view-content .grupo:eq(0)').addClass('grupoAtivo');

		for(k=0;k<qtdGrupos;k++){
			$('.view-mochiloes .navegacao').append('<div class="bullet">'+k+'</div>');
		}

		$('.view-mochiloes .navegacao .bullet:eq(0)').addClass('ativo');

		$('.view-mochiloes .view-content').addClass('conteudo');

		$('.view-mochiloes .navegacao .bullet').click(function(){

			var pegaIndiceBullet = $(this).index();

			$('.view-mochiloes .navegacao .bullet').removeClass('ativo');
			$('.view-mochiloes .view-content .grupo').removeClass('grupoAtivo');

			$(this).addClass('ativo');
			$('.view-mochiloes .view-content .grupo'+pegaIndiceBullet+'').addClass('grupoAtivo');

			var tamPadrao = 261;
			var vaiPos = tamPadrao*pegaIndiceBullet;
			$('.view-mochiloes .conteudo').animate({left: -vaiPos}, 300);

		});
	}


	
	// Destinos
	
	var getUrl = window.location.href;
	var newUrl = getUrl.toString();
	var arrayUrl = newUrl.split("/");	
	
	if(arrayUrl[5] == 'destinos'){
		$('#box2col .boxlink .link').addClass('destinos');
	}
	
	// Viajar
	if(arrayUrl[4] == 'cidade'){
		$('.page-title .adjust .fltrgt').append('<span class="voltar-cidades"><a href="javascript:history.go(-1)"><< Voltar</a></span>');
	}
	
	

// --------------- ## FORMULÁRIOS ## ------------------

	// FORM FALE CONOSCO
	$('#webform-client-form-55 input[type=text], #webform-client-form-55 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

	$('#webform-client-form-55 #edit-submitted-telefone-box-ddd-telefone').mask('99', {placeholder: ""});
	$('#webform-client-form-55 #edit-submitted-telefone-box-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 


	// FORM RECLAMAÇÕES
	$('#webform-client-form-61 input[type=text], #webform-client-form-61 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

    $('#webform-client-form-61 #edit-submitted-telefone-ddd').mask('99', {placeholder: ""});
	$('#webform-client-form-61 #edit-submitted-telefone-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: " "});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: " "});
        }  
    }); 


    // FORM DÚVIDAS E SUGESTÕES
    $('#webform-client-form-60 input[type=text], #webform-client-form-60 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

    $('#webform-client-form-60 #edit-submitted-telefone-fieldset-ddd').mask('99', {placeholder: ""});
	$('#webform-client-form-60 #edit-submitted-telefone-fieldset-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 
    

    // FORM SEJA UM FRANQUEADO
    $('#webform-client-form-57 input[type=text], #webform-client-form-57 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

    $('#webform-client-form-57 #edit-submitted-dados-pessoais-cpf').mask('999.999.999-99', {placeholder: ""});
    $('#webform-client-form-57 #edit-submitted-dados-pessoais-rg').mask('99.999.999-9', {placeholder: ""});
    $('#webform-client-form-57 #edit-submitted-dados-pessoais-rg').mask('99.999.999-9', {placeholder: ""});

    $('#webform-client-form-57 #edit-submitted-dados-para-contato-telefone-fixo-ddd-fixo').mask('99', {placeholder: ""});
	$('#webform-client-form-57 #edit-submitted-dados-para-contato-telefone-fixo-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 

    $('#webform-client-form-57 #edit-submitted-dados-para-contato-telefone-celular-ddd-fixo').mask('99', {placeholder: ""});
	$('#webform-client-form-57 #edit-submitted-dados-para-contato-telefone-celular-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 


    // FORM FALE COM O PRESIDENTE
    $('#webform-client-form-62 input[type=text], #webform-client-form-62 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

    $('#webform-client-form-62 #edit-submitted-telefone-ddd').mask('99', {placeholder: ""});
	$('#webform-client-form-62 #edit-submitted-telefone-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 

    // FORM TRABALHE CONOSCO
    $('#webform-client-form-59 input[type=text], #webform-client-form-59 input[type=email]').focus(function(){
		$(this).prev('.field-prefix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).prev('.field-prefix').stop().fadeIn(400);
		}
	});

	$('#webform-client-form-59 #webform-component-data-de-nascimento input[type=text]').focus(function(){
		$(this).next('.field-suffix').fadeOut(300);
	}).blur(function(){
		if($(this).val() == ''){
			$(this).next('.field-suffix').stop().fadeIn(400);
		}
	});


    $('#webform-client-form-59 #edit-submitted-data-de-nascimento-dia').mask('99', {placeholder: ""});
    $('#webform-client-form-59 #edit-submitted-data-de-nascimento-mes').mask('99', {placeholder: ""});
    $('#webform-client-form-59 #edit-submitted-data-de-nascimento-ano').mask('9999', {placeholder: ""});

    $('#webform-client-form-59 #edit-submitted-telefone-fieldset-ddd').mask('99', {placeholder: ""});
	$('#webform-client-form-59 #edit-submitted-telefone-fieldset-telefone').mask("99999999?9", {placeholder: ""}).live('focusout', function () {  
        var _this = $(this);
        if(_this.length > 8) {  
        	_this.unmask();
            _this.mask("99999-999?9", {placeholder: ""});  
        } else {  
        	_this.unmask();
            _this.mask("9999-9999?9", {placeholder: ""});
        }  
    }); 

    if($('#webform-client-form-369')[0])
    {
    	if($('#edit-previous')[0])
    	{
    		
    	}
    	else
    	{
    		$('#node-369').css('height','940px');
    	}
    }

	//
	if($('#block-block-12')[0]){

		//Coloca a galeria de cidades dentro do body
		// var i = 0;
		var pegaLista = $('.view-id-cidades').clone();
		$('.view-id-cidades').remove();
		$('.field-name-field-paises-saiba-mais .field-item #box2colsmall02 .col01').append(pegaLista);
		$('.col01 .view-id-cidades .views-row:nth-child(2n)').after('<div class="clear"></div>');

		$('.col01 .view-id-cidades').flexslider({
			selector: ".view-content > .views-row",
			animation: "slide",
			direction: "vertical",
			itemWidth: 155,
			itemMargin: 7,
			minItems: 4,
			maxItems: 4,
			slideshow: false,
			move: 2
		});


		// var itens = pegaLista.find('.views-row').html();
		// var qtdItensGaleria = pegaLista.find('.views-row').size();

		// while(i<qtdItensGaleria){
		//  $('.field-name-field-paises-saiba-mais .field-item #box2colsmall02 .col01').append('<div class="grupo"></div>');
		//  for(e=0; e<3; e++){
		//   $('.field-name-field-paises-saiba-mais .field-item #box2colsmall02 .col01 .grupo:eq('+e+')').append(''+itens+':eq('+e+')');
		//  }
		//  i++;
		// }

		// $('.field-name-field-paises-saiba-mais .field-item #box2colsmall02 .col01').append(pegaLista);
		// $('#block-block-12').remove();
	}
	

	if($('#block-lojas-lojas-buscaloja').length){
		$('#block-lojas-lojas-buscaloja').prepend('<div class="fltlft boxIcon icone boxcolor"></div><div class="fltlft boxTitle">Agências IE</div><div class="clear"></div>');
	}



	
	//pegando o titulo do Pais e montando o botão de Contato enviado o param do pais
	if($('#box1colbig')){
			// corrigindo e passando param para contato
			var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI($('#box1colbig h4').text());
		    // $(this).find('.col02 .link').attr('href',url_contato);
			// $(this).find('.col02 .link').html('Quero ir para <br>'+$('#box1colbig h4').text());
	
	}	
	

	// pegando o titulo de IE Explorer e montando o link para contato
	if($('.node .node-page .clearfix')){		


			if($('.page-node a.active-trail.active').html() == 'Modalidades')
			{
				
				$('.fltlft h5').each(function(index){

					var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI($(this).text());

					// $(this).parents('.fltlft').children('.boxlink').children('a').html('Eu quero');
					// $(this).parents('.fltlft').children('.boxlink').children('a').attr('href',url_contato);

				});

			}
			else if($('.page-node .menu-dif li[rel=field-name-field-opcoes] a').html() == 'Tipos de Programa')
			{
				
				$('.fltlft h5').each(function(index){

					var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI($(this).text());

					// $(this).parents('.fltlft').children('.boxlink').children('a').html('Eu quero');
					// $(this).parents('.fltlft').children('.boxlink').children('a').attr('href',url_contato);

				});

			}
			else
			{
				// corrigindo e passando param para contato
				var url_contato = Drupal.settings.basePath + 'sobre-a-ie/fale-conosco?produto='+ encodeURI($('.fltlft h3').text());
			    
				// $(this).find('.fltlft .link').attr('href',url_contato);		
						
				$(this).find('.fltlft .link').html('Eu quero');
			}

			

	}


	// Para a página Work Experience Tipos de Programa
	$('.page-node .menu-dif li.active a.active')



// --------------- ## FOOTER ## ------------------

	//adiciona classes para tratamento de estilos no menu do footer
	var qtdItemFooter = $('#block-menu-menu-footer-menu .menu:eq(0) .expanded').size();
	for(i=0; i<=qtdItemFooter;i++){
		//adiciona classe para identificar cada item principal do menu
		$('#block-menu-menu-footer-menu .menu:eq(0) .expanded:eq('+i+')').addClass('item'+i+'');
		// Remove o link do item principal do menu do footer
		$('#block-menu-menu-footer-menu ul.menu:eq(0) li.expanded:eq('+i+') a:eq(0)').addClass('noclick').click(function(){ return false; });
	}

	//Adiciona classe de tratamento de estilo no footer
	$('.footer-2 ul.footerLeft li:eq(1)').addClass('link02');

	//localiza sua agência
	var browser = navigator.appName;	
	//alert(browser);
	if(browser != 'Microsoft Internet Explorer'){
		$('.footer-2 .link02').hover(
			function(){
				$(this).children('a').addClass('ativo');
				$(this).children('.boxForm').stop().fadeTo(700, 1, function() {
					$(this).children('.boxForm').show();
				});		
		},
			function(){
				$(this).children('a').removeClass('ativo');
				$(this).children('.boxForm').stop().fadeTo(700, 0, function() {
					$('.boxForm').hide();
				});
			}
		);
	}else{
		$('.boxForm .bt_fechar').show();			
		
		$('.footer-2 .link02').hover(
			function(){
				$(this).children('a').addClass('ativo');
				$(this).children('.boxForm').stop().fadeTo(700, 1, function() {
					$(this).children('.boxForm').show();
				});		
		},
			function(){
			}
		);
		
		$('.boxForm .bt_fechar').click(function(){ $('.boxForm').hide(); $('.footer-2 .link02 a').removeClass('ativo'); });			
	}
	
	$('#block-menu-menu-footer-menu .menu .menu .last a').attr('target','_blank');
	
	
	// ---
	// Menu aparecendo no Footer
	// ---
	
	var bodyHeight = $('body').height();
	var topMenu = bodyHeight - 510;
		
	$('#footer .item2 .menu .first a').attr('href','javascript:void(0)');
	
	$('#footer .item2 .menu .first a').hover(
		  function () {
			$('#header .menu-principal ul li.l03 dl').addClass('segundo-nivel-footer');			
			$('.segundo-nivel-footer').css({'top':topMenu});			
			$('.segundo-nivel-footer').show();			
		  }, 
		  function () { return false; }
	);
	
	$('#header .menu-principal ul li.l03 dl.segundo-nivel-footer .close').live('click',function(){
		$(this).parent().removeClass('segundo-nivel-footer').hide();		
	});
	
	
});