<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>

<?php 
/*
$tax_empresa = taxonomy_term_load($row->_field_data['nid']['entity']->field_promocoes_empresa['und'][0]['tid']);

$tax_empresa_logo = image_style_url('imagem_empresa_logo',$tax_empresa->field_empresas_logo['und'][0]['uri']);
*/

foreach($row->_field_data['nid']['entity']->field_feiras_empresas['und'] as $empresa_tid)
{
  $tax_empresa[] = taxonomy_term_load($empresa_tid['tid']);
}

?>

<?php // print $output; ?>

<?php foreach($tax_empresa as $empresa){ 

$tax_empresa_logo = image_style_url('imagem_empresa_feira',$empresa->field_empresas_logo['und'][0]['uri']);

?>
<div class="views-row">
      
  <div class="views-field views-field-field-empresas-logo">        
    <div class="field-content">
      <img typeof="foaf:Image" src="<?php echo $tax_empresa_logo; ?>" width="110" height="78" alt="">
    </div>  
  </div>  
  <div class="views-field views-field-nothing" style="display: none;">        
    <span class="field-content">
      <div class="desc_empresa">
        <div class="nome"><?php echo $empresa->name; ?></div>
        <div class="cidade"><?php echo $empresa->field_empresas_cidade['und'][0]['value']; ?></div>
        <div class="descricao">
          <?php echo $empresa->description; ?>
        </div>
      </div>
    </span>  
<div class="seta"></div>
</div>  
</div>
<?php } ?>