<?php 

function hyperlink($texto) {
	
	#Procura pelo protocolo http://...
	$texto = preg_replace("#[a-zA-Z]+:\/\/([.]?[a-zA-Z0-9_\/-])*#", "<a href=\"\\0\">\\0</a>", $texto);

	#Procura pelo dominio.extensao...
	$texto = preg_replace("#(^| )(www([.]?[a-zA-Z0-9_\/-])*)#", "\\1<a href=\"http://\\2\">\\2</a>", $texto);

	#Retorno
	return "$texto";
}

?>