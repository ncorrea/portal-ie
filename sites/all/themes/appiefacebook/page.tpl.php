<div id="master" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <header>
    <div id="header">
      <?php if ($page['header']): ?>
        <div id="header-region">
          <?php print render($page['header']); ?>
        </div>
      <?php endif; ?>
    </div>
  </header>
  <hr />
  <div id="content"> 
    <div id="content-1">
      <div id="content-1-1">
        <?php print render($page['content']); ?>
      </div>  
    </div>
  </div>
  <footer>
    <div id="footer">
      <?php if ($page['footer']): ?>
          <?php print render($page['footer']); ?>
      <?php endif; ?>
    </div>
  </footer>
</div>