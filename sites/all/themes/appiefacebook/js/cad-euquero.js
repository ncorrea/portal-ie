jQuery(document).ready(function() {

	// Adiciona botão de voltar
	$('.add-instituicao .alinha').append('<span class="bt-voltar">Voltar</span>');	
	
	// $('.ajax-tipo-motivo .est-trab').click(function(){
	//   $('p.clique-aqui').css({'font-size':'13px', 'top':'17px'});
	// });

	// $('.ajax-tipo-motivo .est').click(function(){
	//   $('p.clique-aqui').css({'font-size':'10px', 'top':'41px'});
	// });

	// $('.ajax-tipo-motivo .trab').click(function(){
	//   $('p.clique-aqui').css({'font-size':'10px', 'top':'41px'});
	// });

	jQuery('.mapa .checkbox').live('mouseenter', function(){
		// alert('achei você');
		jQuery(this).animate({ opacity: 1 }, 300, function() {
		    jQuery(this).css('opacity', 'filter: alpha(opacity=100);');
		});
	});

	jQuery('.mapa .checkbox').live('mouseleave', function(){
		// alert('achei você');
		jQuery(this).animate({ opacity: 0 }, 300, function() {
		    jQuery(this).css('opacity', 'filter: alpha(opacity=0);');
		});
	});

	$('.quando').hide();	
	$('.mapa-paises').hide();
	$('.mapa-paises .mapa').hide();

	jQuery('.opt label').click(function(){
		jQuery('.opt label .bgradio .setaAtiva').hide();
		jQuery('.opt label .bgradio span').removeClass('ativo');
		jQuery(this).find('.bgradio span').addClass('ativo');
		jQuery(this).find('.bgradio .setaAtiva').show();
	});


	jQuery('.btTipo .map').click(function(){
		jQuery('.mapa-paises .lista').hide();
		jQuery('.mapa-paises .mapa').show();
	});

	jQuery('.btTipo .list').click(function(){
		jQuery('.mapa-paises .mapa').hide();
		jQuery('.mapa-paises .lista').show();
	});

	jQuery('.checkbox label').click(function(){
		if(jQuery(this).hasClass('ativo')){
			jQuery(this).removeClass('ativo');
		}else{
			jQuery(this).addClass('ativo');
		}
	});

	
	$('.opt').click(function(){
		$('.mapa-paises').show();
		var alturaTela = $('body').height();
		console.log(alturaTela);
		scrollTo(alturaTela);
	});
	
	//oculta lytebox e campo de adicione ints.
	jQuery('.bg-preto').hide();
	jQuery('.add-instituicao').hide();
	jQuery('.clique-aqui').hide();
	
	
	//lytebox abre
	jQuery('.clique-aqui a').click(function(){
		jQuery('.bg-preto').show(300);
		jQuery('.add-instituicao').show(400);			
	});
	
	// Lytebox fecha
	$('.bt-fechar, .bt-voltar').click(function(){
		$('.bg-preto').hide(400);
		$('.add-instituicao').hide(300);
		$('.bt-voltar').hide();	
	});
	
	// Mostra o botão de voltar
	$('.envia_solicitacao').click(function(){
		$('.bt-voltar').show();
	});
	
	$('.opt.trab').click(function(){
		$('select.instituicao').children('option').html('Selecionar empregador');
		$('select.instituicao').show();
		$('.nome').show();
		$('.iNome').show();
	});
	$('.opt.est-trab').click(function(){
		$('select.instituicao').hide();
		$('.nome').hide();
		$('.iNome').hide();
	});
	$('span.estudar').click(function(){
		$('select.instituicao').children('option').html('Selecione a Instituição / Escola');
		$('select.instituicao').show();
		$('.nome').show();
		$('.iNome').show();
	});
	/*
	function rolaTela(){
		window.setTimeout(function(){			
			var altura_tela = $(window).height();
			var cont = $('.content-conteudo').offset().top;			
			var i = window.setInterval(function(){
				FB.Canvas.scrollTo(0,cont);
				cont += 10;								
				if(cont >= altura_tela){
					clearInterval(i);
				}
			}, 10);
		}, 500);
	}
	*/
	//adiciona o efeito de scroll a tela,setar parâmetro pelo eixo 'y'
		function scrollTo(y){
			FB.Canvas.getPageInfo(function(pageInfo){
					$({y: pageInfo.scrollTop}).animate(
						{y: y},
						{duration: 1000, step: function(offset){
							FB.Canvas.scrollTo(0, offset);
						}
					});
			});
		}
});