jQuery(document).ready(function($) {

	// Adiciona botão de voltar
	$('.add-instituicao .alinha').append('<span class="bt-voltar">Voltar</span>');	
	
	$('.mapa .checkbox').live('mouseenter', function(){
		// alert('achei você');
		$(this).animate({ opacity: 1 }, 300, function() {
		    $(this).css('opacity', 'filter: alpha(opacity=100);');
		});
	});

	$('.mapa .checkbox').live('mouseleave', function(){
		// alert('achei você');
		$(this).animate({ opacity: 0 }, 300, function() {
		    $(this).css('opacity', 'filter: alpha(opacity=0);');
		});
	});
	
	

	$('.mapa-paises').hide();
	$('.quando').hide();
	$('.comofoi').hide();

	jQuery('.opt label').click(function(){
		jQuery('.opt label .bgradio .setaAtiva').hide();
		jQuery('.opt label .bgradio span').removeClass('ativo');
		jQuery(this).find('.bgradio span').addClass('ativo');
		jQuery(this).find('.bgradio .setaAtiva').show();
	});

	
	$('.mapa-paises .mapa').hide();

	$('.btTipo .map').click(function(){
		$('.mapa-paises .lista').hide();
		$('.mapa-paises .mapa').show();
	});

	$('.btTipo .list').click(function(){
		$('.mapa-paises .mapa').hide();
		$('.mapa-paises .lista').show();
	});

	$('.checkbox label').click(function(){
		$('.checkbox').find('label').removeClass('ativo');
		$(this).addClass('ativo');
	});
	

	$(".experiencias").keyup(function(event){

		var target	= $("#content-countdown");
		var max		= target.attr('title');
		var len 	= $(this).val().length;
		var remain	= max - len;

		if(len > max)
		{
			var val = $(this).val();
			$(this).val(val.substr(0, max));
			
			remain = 0;
		}

		target.html(remain);
		
	});
	
	
		
	// Oculta lytebox e campo de adicione ints.
	$('.bg-preto').hide();
	$('.add-instituicao').hide();
	$('.clique-aqui').hide();
		
	
	// Lytebox abre
	$('.clique-aqui a').click(function(){
		$('.bg-preto').show(300);
		$('.add-instituicao').show(400);			
	});
	
	// Lytebox fecha
	$('.bt-fechar, .bt-voltar').click(function(){
		$('.bg-preto').hide(400);
		$('.add-instituicao').hide(300);
		$('.bt-voltar').hide();	
	});
	
	// Mostra o botão de voltar
	$('.envia_solicitacao').click(function(){
		$('.bt-voltar').show();
	});
	
	
	$('.opt').click(function(){	
		$('.mapa-paises').show();
		//rolaTela(10,1);
		var alturaTela = $('body').height();
		scrollTo(alturaTela);
	});
	
		
	$('.opt.trab').click(function(){
		$('select.instituicao').children('option').html('Selecionar empregador');
		$('select.instituicao').show();
		$('.nome').show();
		$('.iNome').show();
	});
	
	$('.opt.est-trab').click(function(){
		$('select.instituicao').hide();
		$('.nome').hide();
		$('.iNome').hide();
	});
	$('span.estudar').click(function(){
		$('select.instituicao').children('option').html('Selecione a Instituição / Escola');
		$('select.instituicao').show();
		$('.nome').show();
		$('.iNome').show();
	});	
		
	function rolaTela(time, model){
		window.setTimeout(function(){			
			var altura_tela = $('body').height();
			if(model == 1){
				var cont = $('.content-conteudo').offset().top;			
			}else{
				var cont = $('.content-conteudo').offset().top;
				cont += 1109;
				console.log(cont);
			}
			var i = window.setInterval(function(){
				FB.Canvas.scrollTo(0,cont);
				cont += 10;								
				if(cont >= altura_tela){
					clearInterval(i);
				}
			}, time);
		}, 600);
	}
	
});