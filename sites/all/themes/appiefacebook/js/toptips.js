jQuery(document).ready(function() {

	// exibe e esconde os forms de resposta
	// jQuery('.dados-right .responder').click(function(){
	jQuery('.dados-right .responder').on('click', function(){
		jQuery('.form_responder').show();
	});


	//jQuery('.respostas .resposta1 a').click(function(){
	jQuery('.respostas').on('click', '.resposta1 a', function(){
		jQuery(this).parent().parent().find('.envia_replica').show();
	});
	
	// centraliza os botões
	// var tamPadrao = jQuery('.lista-botoes ul li').width();
	// var qtd = jQuery('.lista-botoes ul li').size();
	// var tamUl = (tamPadrao*qtd)+10;
	// jQuery('.lista-botoes ul').css('width', tamUl);

	// exibe a div
	jQuery('.lista-botoes ul li:eq(0)').addClass('ativo');

	jQuery('.lista-botoes ul li').click(function(){

		var pegaId = jQuery(this).find('a').attr('id');
		var pegaClass = jQuery(this).find('a').hasClass('ativo');

		jQuery('.lista-botoes ul li').removeClass('ativo');
		jQuery('.lista-botoes ul li a').removeClass('ativo');
		jQuery(this).addClass('ativo');
		jQuery(this).find('a').addClass('ativo');

		jQuery('.mostra-posts').children('div').hide();		
		jQuery('.mostra-posts').children('div').removeClass('ativo');		
		jQuery('.mostra-posts').find('#'+pegaId+'').addClass('ativo');		
		jQuery('.mostra-posts').find('#'+pegaId+'').show();	

	});

	//tags
	jQuery('.envia-pergunta input[type=button]').click(function(){
		jQuery('.tags').find('span').remove();
		var pegaValor = jQuery('.envia-pergunta .inputTags').val().split(",");	
		a=0;
		for(i=0; i<pegaValor.length; i++){
			jQuery('.tags').append('<span>'+pegaValor[a]+'</span>');
			a++;	
		}
	});
});
