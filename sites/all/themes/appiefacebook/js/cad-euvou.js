jQuery(document).ready(function($) {

	// Adiciona botão de voltar
	$('.add-instituicao .alinha').append('<span class="bt-voltar">Voltar</span>');

	$('.mapa .checkbox').live('mouseenter', function(){
		
		$(this).animate({ opacity: 1 }, 300, function() {
		    $(this).css('opacity', 'filter: alpha(opacity=100);');
		});
	});

	$('.mapa .checkbox').live('mouseleave', function(){
		
		$(this).animate({ opacity: 0 }, 300, function() {
		    $(this).css('opacity', 'filter: alpha(opacity=0);');
		});
	});

	$('.quando').hide();
	$('.mapa-paises').hide();

	jQuery('.opt label').click(function(){
		jQuery('.opt label .bgradio .setaAtiva').hide();
		jQuery('.opt label .bgradio span').removeClass('ativo');
		jQuery(this).find('.bgradio span').addClass('ativo');
		jQuery(this).find('.bgradio .setaAtiva').show();
	});

	$('.dataIda').focus(function(){
		$('.ida label').hide();
	});

	$('.dataVolta').focus(function(){
		$('.volta label').hide();
	});

	$('.mapa-paises .mapa').hide();

	$('.btTipo .map').click(function(){
		$('.mapa-paises .lista').hide();
		$('.mapa-paises .mapa').show();
	});

	$('.btTipo .list').click(function(){
		$('.mapa-paises .mapa').hide();
		$('.mapa-paises .lista').show();
	});
	
	$('.checkbox label').click(function(){
		$('.checkbox').find('label').removeClass('ativo');
		$(this).addClass('ativo');
		rolaTela();		
	});

	$('.opt').click(function(){
		$('.mapa-paises').show();
		var alturaTela = $('body').height();
		scrollTo(alturaTela);
	});
	
	//oculta lytebox e campo de adicione ints.
	$('.bg-preto').hide();
	$('.add-instituicao').hide();
	$('.clique-aqui').hide();
	
	
	//lytebox abre
	$('.clique-aqui a').click(function(){
		$('.bg-preto').show(300);
		$('.add-instituicao').show(400);			
	});
	
	// Lytebox fecha
	$('.bt-fechar, .bt-voltar').click(function(){
		$('.bg-preto').hide(400);
		$('.add-instituicao').hide(300);
		$('.bt-voltar').hide();	
	});
	
	// Mostra o botão de voltar
	$('.envia_solicitacao').click(function(){
		$('.bt-voltar').show();
	})
	
	$('.opt.trab').click(function(){
		$('select.instituicao').children('option').html('Selecionar empregador');
		$('select.instituicao').show();
		$('.nome').show();
		$('.iNome').show();
	});
	$('.opt.est-trab').click(function(){
		$('select.instituicao').hide();
		$('.nome').hide();
		$('.iNome').hide();
	});
	$('span.estudar').click(function(){
		$('select.instituicao').children('option').html('Selecione a Instituição / Escola');
		$('select.instituicao').show();
		$('.nome').show();
		$('.iNome').show();
	});
	/*
	function rolaTela(){
		window.setTimeout(function(){			
			var altura_tela = $(window).height();
			var cont = $('.content-conteudo').offset().top;			
			var i = window.setInterval(function(){
				FB.Canvas.scrollTo(0,cont);
				cont += 10;								
				if(cont >= altura_tela){
					clearInterval(i);
				}
			}, 10);
		}, 500);
	}*/
	//adiciona o efeito de scroll a tela,setar parâmetro pelo eixo 'y'
		function scrollTo(y){
			FB.Canvas.getPageInfo(function(pageInfo){
					$({y: pageInfo.scrollTop}).animate(
						{y: y},
						{duration: 1000, step: function(offset){
							FB.Canvas.scrollTo(0, offset);
						}
					});
			});
		}

});