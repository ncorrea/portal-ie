jQuery(document).ready(function($) {
	
	$('.filtros label').each(function(){
		$(this).append('<div class="icone ativo"></div>');
	});
	
	$('.filtros label').click(function(){
		if($(this).find('.icone').hasClass('ativo')){
			$(this).find('.icone').removeClass('ativo');
		}else{
			$(this).find('.icone').addClass('ativo');
		}
	});


	$('.filtros #escopo').change(function(){

		var valorDoCampo = $('.filtros #escopo').val();
		// console.log(valorDoCampo);

		if(valorDoCampo == '0'){
			$('.filtros label span.vermelho').html('Quero');
			$('.filtros label span.verde').html('Vou');
			$('.filtros label span.azul').html('Fui');
		}
		else{
			$('.filtros label span.vermelho').html('Querem');
			$('.filtros label span.verde').html('Vão');
			$('.filtros label span.azul').html('Foram');
		}

	});

	
	

});
