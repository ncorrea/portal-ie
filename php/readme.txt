INSTALA��O

1. Copie os arquivos para o servidor.
2. Altere o arquivo de conex�o localizado em _gravar/mysql.php com os dados de conex�o a sua base de dados MySQL.
3. De permiss�o de grava��o na pasta _gravar.
4. Acesse a p�gina inicial de se site.

O acesso ao administrador pode ser realizado em:

http://seudominio/admin
usuario: user
senha: pass

LICEN�A

Este sistema pode ser utilizado em seu site e para estudos de PHP.
As fun��es do sistema podem ser copiadas mas pedimos gentilmente que cite a fonte de origem do c�digo especificando a p�gina do projeto.
Este sistema n�o deve ser comercializado sem a permiss�o por escrito do desenvolvedor.

_______________________________________________________________________________

INSTALLATION

1. Copy the files to the server.
2. Change the connection file located in _gravar/mysql.php with the data connection to your MySQL database
3. Write permission on the folder _gravar.
4. Go to the homepage of the site.

The access manager can be performed in:

http://yourdomain/admin
User: user
Password: pass

LICENSE

This system can be used on your site and for studies of PHP.
System functions can be copied but we kindly ask you cite the original source code specifying the project page.
This system should not be sold without the written permission of the developer.

Adicionado documento de instala��o e licen�a do produto