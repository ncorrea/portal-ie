<?
include("dados.php");
error_reporting(E_ALL);
if ($l>0&&$per[19]==1){
	//desenha gr�fico de visitas semanais
	function grafico($titulo,$cor,$v0,$v1,$v2,$v3,$v4,$v5,$v6) {		
		$valor=explode(',',"$v0,$v1,$v2,$v3,$v4,$v5,$v6");
		$max=max($valor);	
		$max==0 ? $max=1 : null;
		header("Content-type: image/png");
		$im = imagecreate(100, 80);
		imagecolorallocate($im, 254, 254, 254);
		$cores[0]=imagecolorallocate($im, 254, 0, 0);   
		$cores[1]=imagecolorallocate($im, 0, 0, 254);
		$cores[2]=imagecolorallocate($im, 0, 128, 0);
		$cores[3]=imagecolorallocate($im, 138, 43, 226);
		$cores[4]=imagecolorallocate($im, 254, 140, 0);
		$preto=imagecolorallocate($im, 0, 0, 0);
		imagefilledrectangle ($im, 8, (65-intval(50*($valor[0]/$max))), 15, 65, $cores[$cor]);
		imagefilledrectangle ($im, 20, (65-intval(50*($valor[1]/$max))), 27, 65, $cores[$cor]);
		imagefilledrectangle ($im, 32, (65-intval(50*($valor[2]/$max))), 39, 65, $cores[$cor]);
		imagefilledrectangle ($im, 44, (65-intval(50*($valor[3]/$max))), 51, 65, $cores[$cor]);
		imagefilledrectangle ($im, 56, (65-intval(50*($valor[4]/$max))), 63, 65, $cores[$cor]);
		imagefilledrectangle ($im, 68, (65-intval(50*($valor[5]/$max))), 75, 65, $cores[$cor]);
		imagefilledrectangle ($im, 80, (65-intval(50*($valor[6]/$max))), 87, 65, $cores[$cor]);
		imagestring($im, 3, 10, 0, $titulo, $preto);
		imagestring($im, 2, 10, 65, 'D S T Q Q S S', $preto);
		imagepng($im);
		imagedestroy($im);
		die();
	}
	isset($_GET['grafico'])?grafico($_GET['grafico'],$_GET['cor'],$_GET['v0'],$_GET['v1'],$_GET['v2'],$_GET['v3'],$_GET['v4'],$_GET['v5'],$_GET['v6']):null;
	//desenha gr�fico por periodo
	function grafico_p($titulo,$cor,$v0,$v1,$v2,$v3) {
		$valor=explode(',',"$v0,$v1,$v2,$v3");
		$max=max($valor);	
		$max==0?$max=1:null;
		header("Content-type: image/png");
		$im=imagecreate(60,80);
		imagecolorallocate($im, 255, 255, 255);
		$cores[0]=imagecolorallocate($im, 139, 0, 139);   
		$cores[1]=imagecolorallocate($im, 106, 90, 205);
		$cores[2]=imagecolorallocate($im, 95, 158, 160);
		$cores[3]=imagecolorallocate($im, 0, 139, 139);
		$cores[4]=imagecolorallocate($im, 46, 139, 87);
		$cores[5]=imagecolorallocate($im, 128, 128, 0);
		$cores[6]=imagecolorallocate($im, 205, 133, 63);
		$preto=imagecolorallocate($im, 0, 0, 0);
		imagefilledrectangle ($im, 8, (65-intval(50*($valor[0]/$max))), 15, 65, $cores[$cor]);
		imagefilledrectangle ($im, 20, (65-intval(50*($valor[1]/$max))), 27, 65, $cores[$cor]);
		imagefilledrectangle ($im, 32, (65-intval(50*($valor[2]/$max))), 39, 65, $cores[$cor]);
		imagefilledrectangle ($im, 44, (65-intval(50*($valor[3]/$max))), 51, 65, $cores[$cor]);
		imagestring($im, 3, 5, 0, $titulo, $preto);
		imagestring($im, 2, 10, 65, 'M M T N', $preto);
		imagepng($im);
		imagedestroy($im);
		die();
	}
	isset($_GET['grafico_p'])?grafico_p($_GET['grafico_p'],$_GET['cor'],$_GET['v0'],$_GET['v1'],$_GET['v2'],$_GET['v3']):null;
	//desenha grafico de linha
	function linhas($titulo,$valores) {
		error_reporting(0);
		$largura=800;	
		header("Content-type: image/png");
		$im=imagecreate(800,200);
		imagecolorallocate($im, 255, 255, 255);
		$verme=imagecolorallocate($im, 255, 0, 0);
		$preto=imagecolorallocate($im, 100, 100, 100);
		$cinza=imagecolorallocate($im, 200, 200, 200);
		$azul=imagecolorallocate($im, 0, 150, 100);
		$dados=explode('|',$valores);
		$max=0;
		$const=intval($largura/13);
		foreach($dados as $chave => $dado){
			$dad=explode(':',$dado);
			if ($dad[1]>$max) $max=$dad[1];
			$valor[$chave]['valor']=$dad[1];
			$valor[$chave]['x']=$const*($chave+1);
			imagestring($im, 1, $const*($chave+1), 185 , $dad[0], $preto);
		}
		$fm='';
		for($ii=0;$ii<strlen($max)-1;$ii++)$fm.='0';
		$max=(substr($max,0,1)+1).$fm;
		for ($ii=30;$ii<=180;$ii=$ii+15){
			imagestring($im, 2, 5, $ii-7, ((180-$ii)*$max)/150, $preto);
			imageline($im, 40, $ii, $largura, $ii, $cinza);
		}
		foreach ($valor as $chavo => $rolav){
			imagestring($im, 1, $rolav['x'], 170-($rolav['valor']*150/$max) , $rolav['valor'] , $azul);
			if($chavo<11){
				imageline($im, $rolav['x']+12, 180-($rolav['valor']*150/$max) , $valor[$chavo+1]['x']+12, 180-($valor[$chavo+1]['valor']*150/$max), $verme);
			}
		}	
		imagestring($im, 3, 5, 0, $titulo, $preto);
		imagepng($im);
		imagedestroy($im);	
		die();	
	}
	isset($_GET['linhas'])?linhas($_GET['linhas'],$_GET['valores']):null;
	
	$colspan="";
	if ($z==1) {//Visualiza��es em categorias
		$categorias='';
		$query=mysql_query("select view,nome,id from arquivos where habilitado=1 order by view desc limit 0,100");
		while($a=mysql_fetch_row($query))$categorias.="<tr class=".($k=$k=='cor2'?'cor1':'cor2')." id=flip name=flip ><td><a href=../index.php?id=$a[2] target=_blank>$a[1]</a></td><td>$a[0]</td></tr>";
		$colspan="colspan=2";
		$meio_pagina="<tr align=center class=cor3><td><b>Categoria</b></td><td width=100><b>Visitas</b></td></tr>$categorias<tr><td $colspan align=center class=cor3><a href=?z=0><b>&nbsp;</b></a></td></tr>";
	} else if ($z==2) {//grafico semana
		$semanal='';	
		$b=mysql_query("select date_format(semana_data,'%d/%m/%y'),semana from estatistica where id!=1 order by id desc limit 12");
		$c=mysql_fetch_row(mysql_query("select max(semana) from estatistica"));
		$fm='';
		for($ii=0;$ii<strlen($c[0])-1;$ii++)$fm.='0';
		$c[0]= (substr($c[0],0,1)+1).$fm;
		while($a=mysql_fetch_row($b)) $semanal="$a[0]:$a[1]|".$semanal;	
		$mensal='';	
		$b=mysql_query("select sum(semana),date_format(dia_data,'%m/%y') from estatistica group by date_format(dia_data,'%m/%y')  order by id desc limit 12");
		$d=mysql_fetch_row(mysql_query("select max(mes) from estatistica"));
		$fm='';
		for($ii=0;$ii<strlen($d[0])-1;$ii++)$fm.='0';
		$d[0]= (substr($d[0],0,1)+1).$fm;
		while($a=mysql_fetch_row($b)) $mensal="$a[1]:$a[0]|".$mensal;	
		$meio_pagina="<tr>
			<td><img title='Dados n�o dispon�veis' src='estatisticas.php?linhas=Visitas Semanais - �ltimo trimestre&valores=".substr($semanal,0,-1)."' border=0></td>
		</tr><tr>
			<td><br><img title='Dados n�o dispon�veis' src='estatisticas.php?linhas=Visitas Mensais - �ltimos 12 meses&valores=".substr($mensal,0,-1)."' border=0><br><br></td>
		</tr>";	
	}else {
		$ds=isset($_GET['ds'])?$_GET['ds']:1;
		$c=mysql_fetch_row(mysql_query("select visitas,view from dados")); //Visitas e views
		$b=mysql_fetch_row(mysql_query("select view,nome from arquivos where view>0 order by view desc limit 1")); //P�gina
		$a=mysql_fetch_row(mysql_query("select dia, semana, mes, ano, domingo_madrugada, domingo_manha, domingo_tarde, domingo_noite , segunda_madrugada, segunda_manha, segunda_tarde, segunda_noite, terca_madrugada, terca_manha, terca_tarde, terca_noite, quarta_madrugada, quarta_manha, quarta_tarde, quarta_noite, quinta_madrugada, quinta_manha, quinta_tarde, quinta_noite, sexta_madrugada, sexta_manha, sexta_tarde, sexta_noite, sabado_madrugada, sabado_manha, sabado_tarde, sabado_noite, dia_temp, semana_temp, mes_temp, ano_temp, domingo, segunda, terca, quarta, quinta, sexta, sabado, date_format(semana_data,'%d/%m/%Y'),date_format(dia_data,'%d/%m/%Y') from estatistica where id=$ds")); //Visitas Diarias, Semanais, Mensais e Anuais		
		if ($a[36]==0) {
			$a[36]=$a[4]+$a[5]+$a[6]+$a[7];
			mysql_query("update estatistica set domingo=$a[36] where id=1");
		}
		if ($a[37]==0) {
			$a[37]=$a[8]+$a[9]+$a[10]+$a[11];
			mysql_query("update estatistica set domingo=$a[37] where id=1");
		}
		if ($a[38]==0) {
			$a[38]=$a[12]+$a[13]+$a[14]+$a[15];
			mysql_query("update estatistica set domingo=$a[38] where id=1");
		}
		if ($a[39]==0) {
			$a[39]=$a[16]+$a[17]+$a[18]+$a[19];
			mysql_query("update estatistica set domingo=$a[39] where id=1");
		}
		if ($a[40]==0) {
			$a[40]=$a[20]+$a[21]+$a[22]+$a[23];
			mysql_query("update estatistica set domingo=$a[40] where id=1");
		}
		if ($a[41]==0) {
			$a[41]=$a[24]+$a[25]+$a[26]+$a[27];
			mysql_query("update estatistica set domingo=$a[41] where id=1");
		}
		if ($a[42]==0) {
			$a[42]=$a[28]+$a[29]+$a[30]+$a[31];
			mysql_query("update estatistica set domingo=$a[42] where id=1");
		}
		$d=mysql_fetch_row(mysql_query("select max(id) from estatistica"));
		$eq=$ds==1?"select id from estatistica order by id desc":"select id from estatistica where id<$ds order by id desc";
		$e=mysql_fetch_row(mysql_query($eq));
		$meio_pagina="<tr><td>
<table border=0 cellspacing=0 cellpadding=2 align=center width=100%>
	<tr>
		<td colspan=2 class=cor3 align=center><b>".($ds==1?'Semana atual':"Dados coletados de $a[43] a $a[44]")."</b></td>
	</tr><tr>	
		<td>O n�mero atual � de visitas � <b>$c[0]</b>.
		<br>O n�mero atual de visualiza��es � <b>$c[1]</b>.
		<br>A <a href=?z=1>p�gina</a> <b>$b[1]</b> teve <b>$b[0]</b> visitas.</td>
		<td align=right>".(empty($e[0])||$ds==2?'':"<img src=i/back.gif align=absmiddle style=cursor:hand title='Ver semana anterior' onclick=\"window.location='?ds=$e[0]'\">").($ds==1?'':"<img src=i/go.gif align=absmiddle style=cursor:hand title='Ver pr�xima semana'  onclick=\"window.location='?ds=".($d[0]<($e[0]+2)?1:($e[0]+2))."'\">")."</td>
	</tr>
</table>
<br><br>
<table border=1 bordercolor=silver cellpadding=0 cellspacing=0 align=center>
	<tr align=center class=cor4>
		<td class=cor4 width=150>&nbsp;</td>
		<td width=100><b>�ltimo(a)</b></td>
		<td width=100><b>Atual</b></td>
	</tr><tr align=center>
		<td class=cor4><b>Dia</b></td>
		<td>$a[0]</td>
		<td>$a[32]</td>
	</tr><tr align=center>
		<td class=cor4><b>Semana</b></td>
		<td>$a[1]</td>
		<td>$a[33]</td>
	</tr><tr align=center>
		<td class=cor4><b>Mes</b></td>
		<td>$a[2]</td>
		<td>$a[34]</td>
	</tr><tr align=center>
		<td class=cor4><b>Ano</b></td>
		<td>$a[3]</td>
		<td>$a[35]</td>
	</tr>
</table><br><br>
<table border=1 bordercolor=silver cellpadding=0 cellspacing=0 align=center>
	<tr class=cor4 align=center>
		<td width=80 class=cor4>&nbsp;</td>
		<td width=100><b>Domingo</b></td>
		<td width=100><b>Segunda</b></td>
		<td width=100><b>Ter�a</b></td>
		<td width=100><b>Quarta</b></td>
		<td width=100><b>Quinta</b></td>
		<td width=100><b>Sexta</b></td>
		<td width=100><b>S�bado</b></td>
	</tr><tr align=center>
		<td class=cor4><b>Madrugada</b></td>
		<td>$a[4]</td>
		<td>$a[8]</td>
		<td>$a[12]</td>
		<td>$a[16]</td>
		<td>$a[20]</td>
		<td>$a[24]</td>
		<td>$a[28]</td>
	</tr><tr align=center>
		<td class=cor4><b>Manh�</b></td>
		<td>$a[5]</td>
		<td>$a[9]</td>
		<td>$a[13]</td>
		<td>$a[17]</td>
		<td>$a[21]</td>
		<td>$a[25]</td>
		<td>$a[29]</td>
	</tr><tr align=center>
		<td class=cor4><b>Tarde</b></td>
		<td>$a[6]</td>
		<td>$a[10]</td>
		<td>$a[14]</td>
		<td>$a[18]</td>
		<td>$a[22]</td>
		<td>$a[26]</td>
		<td>$a[30]</td>
	</tr><tr align=center>
		<td class=cor4><b>Noite</b></td>
		<td>$a[7]</td>
		<td>$a[11]</td>
		<td>$a[15]</td>
		<td>$a[19]</td>
		<td>$a[23]</td>
		<td>$a[27]</td>
		<td>$a[31]</td>
	</tr><tr align=center>
		<td class=cor4><b>Total</b></td>
		<td>$a[36]</td>
		<td>$a[37]</td>
		<td>$a[38]</td>
		<td>$a[39]</td>
		<td>$a[40]</td>
		<td>$a[41]</td>
		<td>$a[42]</td>
	</tr>
</table><br>
<table border=0 align=center>
	<tr>
		<td colspan=5 align=center><b>Gr�ficos de visita��es semanais</b></td>
	</tr><tr>
		<td><img src='estatisticas.php?grafico=Total&cor=0&v0=$a[36]&v1=$a[37]&v2=$a[38]&v3=$a[39]&v4=$a[40]&v5=$a[41]&v6=$a[42]'></td>
		<td><img src='estatisticas.php?grafico=Madrugada&cor=1&v0=$a[4]&v1=$a[8]&v2=$a[12]&v3=$a[16]&v4=$a[20]&v5=$a[24]&v6=$a[28]'></td>
		<td><img src='estatisticas.php?grafico=Manh�&cor=2&v0=$a[5]&v1=$a[9]&v2=$a[13]&v3=$a[17]&v4=$a[21]&v5=$a[25]&v6=$a[28]'></td>
		<td><img src='estatisticas.php?grafico=Tarde&cor=3&v0=$a[6]&v1=$a[10]&v2=$a[14]&v3=$a[18]&v4=$a[22]&v5=$a[26]&v6=$a[30]'></td>
		<td><img src='estatisticas.php?grafico=Noite&cor=4&v0=$a[7]&v1=$a[11]&v2=$a[15]&v3=$a[19]&v4=$a[23]&v5=$a[27]&v6=$a[31]'></td>
	</tr>
</table><table border=0 align=center>
	<tr>
		<td colspan=7 align=center><b>Visita��es dia-madrugada/manha/tarde/noite</b></td>
	</tr><tr>
		<td><img src='estatisticas.php?grafico_p=Domingo&cor=0&v0=$a[4]&v1=$a[5]&v2=$a[6]&v3=$a[7]'></td>
		<td><img src='estatisticas.php?grafico_p=Segunda&cor=1&v0=$a[8]&v1=$a[9]&v2=$a[10]&v3=$a[11]'></td>
		<td><img src='estatisticas.php?grafico_p=Ter�a&cor=2&v0=$a[12]&v1=$a[13]&v2=$a[14]&v3=$a[15]'></td>
		<td><img src='estatisticas.php?grafico_p=Quarta&cor=3&v0=$a[16]&v1=$a[17]&v2=$a[18]&v3=$a[19]'></td>
		<td><img src='estatisticas.php?grafico_p=Quinta&cor=4&v0=$a[20]&v1=$a[21]&v2=$a[22]&v3=$a[23]'></td>
		<td><img src='estatisticas.php?grafico_p=Sexta&cor=5&v0=$a[24]&v1=$a[25]&v2=$a[26]&v3=$a[27]'></td>
		<td><img src='estatisticas.php?grafico_p=S�bado&cor=6&v0=$a[28]&v1=$a[29]&v2=$a[30]&v3=$a[31]'></td>
	</tr>
</table><br>
</td></tr>";
	}
	echo "$inicio_pagina
	<table border=0 cellspacing=0 cellpadding=0 width=100%>
		<tr class=cor4>
			<td $colspan>&nbsp;<img src=i/est.gif border=0 align=absmiddle title='Hist�rico de visitas' onclick=\"window.location='?z=".($z==2?0:2)."'\" style=cursor:hand>&nbsp;<b>Estat�sticas</b></td>
		</tr>$meio_pagina<tr class=cor4>
			<td align=center $colspan>".($z==0?'<br>':'<a href=?z=0><b>Voltar</b></a>')."</td>
		</tr></table>
	$final_pagina";
}else{
	header("Location : index.php");
}

?> 

