var valosg=''
var tipe
var nomes=''
var ColorScheme = new Array('#FF7777','#FFFF88','#aaaaff','#aaffaa','#ffcc88','#99ccFF','#bbeeFF','#FFdddd','#FFFFbb','#ffddbb','#ccccff','#6633FF','#FFFF00','#FF6600','#FF0000','#99CC66','#66CC00','#6600FF','#FFCCCC','#CCFF66','#FFFF99','#66CCFF','#CC0000','#669900','#FF9900','#333399','#7189a7','#ec8693','#86ecd5','#b7c865','#ffc5ad','#ff32ad','#8b32ad','#ff9e49','#49ffa7','#e2d341','#76b4b7','#f9cc78','#ad9ecd','#6633FF','#FFFF00','#FF6600','#FF0000','#99CC66','#66CC00','#6600FF','#FFCCCC','#CCFF66','#FFFF99','#66CCFF','#CC0000','#669900','#FF9900','#333399','#7189a7','#ec8693','#86ecd5','#b7c865','#ffc5ad','#ff32ad','#8b32ad','#ff9e49','#49ffa7','#e2d341','#76b4b7','#f9cc78','#ad9ecd','#6633FF','#FFFF00','#FF6600','#FF0000','#99CC66','#66CC00','#6600FF','#FFCCCC','#CCFF66','#FFFF99','#66CCFF','#CC0000','#669900','#FF9900','#333399','#7189a7','#ec8693','#86ecd5','#b7c865','#ffc5ad','#ff32ad','#8b32ad','#ff9e49','#49ffa7','#e2d341','#76b4b7','#f9cc78','#ad9ecd','#6633FF','#FFFF00','#FF6600','#FF0000','#99CC66','#66CC00','#6600FF','#FFCCCC','#CCFF66','#FFFF99','#66CCFF','#CC0000','#669900','#FF9900','#333399','#7189a7','#ec8693','#86ecd5','#b7c865','#ffc5ad','#ff32ad','#8b32ad','#ff9e49','#49ffa7','#e2d341','#76b4b7','#f9cc78','#ad9ecd');
function pizza(TargetID,SIZE,DATA,titulog){
//pizza('nome do gr�fico',tamanho,"Valor 1:20,Valor 2:30,valor 3:55,valor 4:20,valor 5:40")
	//cria ponteiro para objeto
	var TargetObj = document.getElementById(TargetID);
	//cria array com informa��es
	DATA = DATA.split(",");
	var SortData = new Array();
	//pega o valor total
	var SUM = 0;
	for(var i=0;i<DATA.length;i++){
		DATA[i] = DATA[i].split(":");
		SUM += parseInt(DATA[i][1]);   
	}
	//Organiza dados
	function sortNumbers(a, b){
		return b[1] - a[1]
	}
	DATA = DATA.sort(sortNumbers);
	//Chama fun��o de desenho
	yhWritePieChart();
	//Desenha o gr�fico
	function yhWritePieChart(){
		//document.write('<?xml:namespace Prefix="v" \/>');
		var zIndex= 0
		function getSlice(ColumnName,ColumnData,size,start,end,fillcolor){
			size = size - 40;
			zIndex++;		  
			var r = size/2;
			var ang1 = (start/360)*(Math.PI*2);
			var ang2 = (end / 360)*(Math.PI*2);
			var x1 = r + Math.sin( ang1 )*r +"px";
			var y1 = r - Math.cos( ang1)*r+"px";
			var xy = "";
			var gef =start;
			var deg = Math.ceil((end - start)/5);
			while( gef < end){
				gef += (end - start)/deg;
				var ang_1 = (  gef / 360)*(Math.PI*2);
				var ang_2 = (  gef / 360)*(Math.PI*2);
				xy += r + Math.sin( (ang_1 + ang_2 )/2 )*(r)+"px";
				xy += r - Math.cos( (ang_1 + ang_2 )/2)*(r)+"px"; 
			}
			var x2 = r + Math.sin( ang2 )*r+"px";
			var y2 = r - Math.cos( ang2 )*r+"px";
			var MI = (zIndex%2)?(12):(0);
			var behavior = 'style="behavior:url(#default#VML);"';
			var style = 'behavior:url(#default#VML);width:'+size+';height:'+size+';position:absolute;left:0px;top:0px;';
			var angle = Math.abs(end - start);
			var legenx = r + Math.sin(  (ang1 + ang2 )/2 )*(r + MI+40)+"px";
			var legeny = r - Math.cos( (ang1 + ang2 )/2 )*(r + MI+20 )+"px";
			var info =  '<span style=\'width:50;border-right:solid 1px '+fillcolor+';text-align:right\'>'+Math.ceil((ColumnData/SUM)*100)+'% </span> <span style=\'width:60;border-right:solid 1px '+fillcolor+';text-align:right\'>'+ColumnData +' </span> <b style=\'width:260;text-align:left;\'>'+ColumnName.substring(0,35)+ '</b>'  ;
			//desenha peda�os
			var poly ='<v:polyline style="'+style+';z-index:'+(zIndex+1)+'"  points="'+r+'px,'+r+'px,'+x1 +','+y1+','+xy+','+x2+','+y2+'" stroked="f" fillcolor="'+fillcolor+'"">';
			poly +='<v:fill  '+behavior+' type = "gradient" color2="fill lighten(100)" angle = "'+angle+'" focus = "0%"></v:fill>';
			poly +='</v:polyline>'; 
			poly +='<span style="position:absolute;left:'+(size+10)+'px;padding-left:12px;background:'+fillcolor+';filter:Alpha(opacity=80);z-index:'+(zIndex + 2 + DATA.length)+';"></span> <span style="position:absolute;left:'+(size)+';font:10px verdana">  '+info+'</span><br>';
			return poly ;
		}
		var K=0;
		function getColor(){ 
			if(ColorScheme.length>0){
				return ColorScheme.shift();
			} else {
				K+=30;
				var R = Math.ceil(K * Math.random());
				var G = Math.ceil(K * Math.random());
				var B = Math.ceil(K * Math.random());
				var C = "rgb("+R+","+G+","+B+")";
				return C;
			}
		}
		var VML = "";
		var START = 0;
		var END =0;
		while(DATA.length >0){
			var INC =Math.ceil(( DATA[0][1] / SUM )*360 );
			END =  START + INC;
			VML +=  getSlice(DATA[0][0], DATA[0][1], SIZE ,START, END,  getColor() );
			START = END;
			DATA.shift();
		}
		VML = (titulog?'<b>'+titulog+'</b><br>':'')+'<DIV style="height:'+SIZE+';width:500;position:relative">' + VML + '</DIV>';
		var DD = document.createElement("div");
		document.all[TargetID].innerHTML = VML;		
		TargetObj.insertAdjacentElement("BeforeBegin",DD);
	}
}
function barro(camada,titulo,limite,label,dados,tipo,texto){
//camada onde ser� desenhado o gr�fico, t�tulo do gr�fico, n�mero m�ximo dos dados, label dos dados(x:y1;y2), dados inseridos formato (x:y1;y2|x:y1;y2),wxibe texto
	grafico=''
	desenho=''
	dados=dados.substring(0,1)=='|'?dados.substring(1):dados
	graph=dados.split('|')
	for(ii=0;ii<graph.length;ii++){
		sobra=0
		grap=graph[ii].split(':');
		dese=grap[1].split(';') 
		des=dese.length
		if (tipo==1)desenho=''
		for(jj=0;jj<des;jj++){
			tama=dese[jj]*100/limite
			tama=tama<1?1:tama
			desenho=tipo==0?(desenho+'<td><table height=100%><tr><td height='+(100-tama)+' valign=bottom></td></tr><tr valign=top align=center><td bgcolor='+ColorScheme[jj]+' height='+tama+'% width=45 title='+dese[jj]+' style="font:8px verdana">'+(texto==0?dese[jj]:"")+'</td></tr></table></td>'):(desenho+'<tr align=center><td bgcolor='+ColorScheme[jj]+' height='+tama+'% width=45 title='+dese[jj]+'>'+(texto==0?dese[jj]:"")+'</td></tr>')
			sobra+=parseInt(dese[jj])			
		}
		if (tipo!=0)desenho='<table border=0 cellpadding=0 cellspacing=0 height=100%><tr><td valign=bottom></td></tr>'+desenho+'</table>'
		grafico=tipo==0?(grafico+'<td colspan='+des+' style="font:bold 8px verdana">'+grap[0]+'</td>'):(grafico=grafico+'<td><table border=0 cellpadding=0 cellspacing=0 bordercolor=green height=100%><tbody  align=center valign=bottom><tr><td height=100%>'+desenho+'</td></tr><tr><td style="font:bold 8px verdana">'+grap[0]+'</td></tr></tbody></table></td>')
	}
	grafico=tipo==0?('<table border=0 cellpadding=0 cellspacing=0 width=100% height=100%><tr height=100% align=center valign=bottom>'+desenho+'</tr><tr align=center>'+grafico+'</tr></table>'):(grafico='<table border=0  width=100% height=100% bordercolor=red><tr height=100% align=center valign=bottom>'+grafico+'</tr></table>')
	//label x
	label=label.split(':')
	labex=label[1].split(';')
	labels='';
	for(ii=0;ii<labex.length;ii++)labels=labels+'<span style=height:10;width:10;background:'+ColorScheme[ii]+'></span> '+labex[ii]+' '
	//linhas de fundo
	css="style='behavior:url(#default#VML);position:absolute;left:0px;top:0px;z-index:-2'"	
	g=parseInt(document.all[camada].style.width)
	h=152
	te=limite/10
	it=h/10
	VML='';
	for(ii=it;ii<=h+it;ii=ii+it){
		VML+="<v:line "+css+" from='0px,"+ii+"px' to='"+g+"px,"+ii+"px' style='font:10px verdana'><v:stroke "+css+" Color='silver'/>"+limite+"</v:line>"
		limite=limite-te
	}
	//label y
	divisao=Math.round(limite/10)
	altura='<table border=0 cellpadding=0 cellspacing=0 width=100% height=180><td align=left valign=bottom style=position:relative;padding-left:60>'+VML+''+grafico+'</td></tr></table>'	
	document.all[camada].innerHTML='<table border=0 cellpadding=0 cellspacing=0><tr><td align=center style="font:10px verdana"><b>'+titulo+'</b></td></tr><tr><td align=center style="font:10px verdana"><b>'+altura+'</b><br>'+labels+'</td></tr></table>';
}
function linhas(camada,limite,nome,dados,titulo,texto){ //layer,limite,noes das linhas, eixoY, eixo x, titulo
	css="style='behavior:url(#default#VML);position:absolute;left:0px;top:0px'"	
	po=document.all[camada].style	
	po.position='relative'
	h=parseInt(po.height)-50
	g=parseInt(po.width)
	//desmenbrar para x e y
	valo=new Array()
	valox=''
	valoy=''
	dada=dados.split('|')
	for (xis in dada){
		dad=dada[xis].split(':')
		valoy+=dad[0]+';'
		da=dad[1].split(';')
		for (six in da){
			!valo[six]?valo[six]=da[six]+';':valo[six]+=da[six]+';'
		}
	}		
	label=''
	vlt=nome.split(';')
	for(ii=0;ii<vlt.length;ii++){
		valox+=valo[ii].substr(0,valo[ii].length-1)+':'
		label+="<span style='height:10;width:10;background:"+ColorScheme[ii]+"'></span> "+vlt[ii]+"   "
	}
	label='<b style="font:bold 10px verdana">'+titulo+'</b><span style="position:absolute;top:'+(h+50)+';left:2;font:10px verdana">'+label+'</span>'
	valoy=valoy.substr(0,valoy.length-1)
	valox=valox.substr(0,valox.length-1)
	te=limite/10
	it=h/10
	VML='';
	for(ii=it;ii<=h+it;ii=ii+it){
		VML+="<v:line "+css+" from='0px,"+ii+"px' to='"+(g-3)+"px,"+ii+"px' style='font:10px verdana'><v:stroke "+css+" Color='silver'/>"+limite+"</v:line>"
		limite=limite-te
	}
	ix=50
	vl=it*10
	vlt=valox.split(':')
	lbl=''
	for(ii=0;ii<vlt.length;ii++){
		dot=''
		vli=vlt[ii].split(';')
		iti=vli.length
		es=(g-76)/(iti-1)
		yy=60
		for(jj=0;jj<iti;jj++){
		xx=Math.round((vl-Math.abs((vli[jj]*it)/limite)+it))
		yy=Math.round(yy)
			lbl+=texto==0?('<span style="position:absolute;font:8px verdana;top:'+(xx-10)+';left:'+(yy-10)+'">'+vli[jj]+'</span>'):''
			dot+=yy+'px,'+xx+'px,'
			yy=yy+es
		}
		VML+="<v:polyline "+css+" points='"+dot.substr(0,dot.length-1)+"'><v:fill "+css+" Opacity='0.0' /><v:stroke "+css+" on='true' weight='2px' color='"+ColorScheme[ii]+"' StartArrow='Diamond' EndArrow='Diamond'/></v:polyline>"
	}
	dy=''
	vlt=valoy.split(';')
	yy=30
	es=(g-60)/(vlt.length-1)
	for(ii=0;ii<vlt.length;ii++){
		dy+='<span style="position:absolute;top:'+(h+it+5)+';left:'+yy+';font:bold 8px verdana">'+vlt[ii]+'</span>'
		yy=yy+es
	}	
	document.all[camada].innerHTML = VML+''+dy+''+lbl+''+label;
}
function grafico_t(faz){
	gg=document.graficos
	if(faz==0) {
		gg.limiar.disabled=false
		gg.texto.disabled=false
		eixoy.innerHTML='<b>Eixo X</b><br><table border=0><tr align=center><td><b>Nome</b></td><td><B>Valor</b></td></tr><tr><td><input type=text name=nx class=text size=20 maxlength=50></td><td><input type=text name=vx class=text size=20 maxlength=50></td></tr></table>'
	}
	if(faz==1){
		gg.limiar.disabled=true		
		gg.texto.disabled=true
		eixoy.innerHTML=''		
	}
	if(faz==2) {
		gg.limiar.disabled=false
		gg.texto.disabled=false
		eixoy.innerHTML='<b>Eixo X</b><br><b>Nome: </b><input type=hidden name=nx value=0><input type=text name=vx class=text>'
	}
}
function grafico_c(cap){
	if (!numero(cap)){
		grx=''
		for(ii=0;ii<cap;ii++)grx=grx+'<tr><td><input type=text name=ny['+ii+'] class=text size=20 maxlength=50></td><td><input type=text name=vy['+ii+'] class=text size=20 maxlength=50></td></tr>'
		eixox.innerHTML='<b>Eixo Y</b><table border=0><tr align=center><td><b>Nome</b></td><td><B>Valor</b></td></tr>'+grx+'</table>'
	} else {
		alert('O valor deve ser num�rico')
	}
}
function grafico_v(){
	gg=document.graficos
	if(!gg.titulo.value){
		alert('O t�tulo � de prenchimento obrigat�rio')
		return false
	}
	for(ii=0;ii<gg.tipo.length;ii++){
		if(gg.tipo[ii].checked==true)tipe=ii
		gg.tipo[ii].disabled=true
	}
	if (tipe<2||tipe==3){
		if (!gg.limiar.value||numero(gg.limiar.value)){
			alert('O valor do limite � de preenchimeto obrigat�rio e deve conter n�meros')
			return false
		}
		if (!gg.nx.value||!gg.vx.value){
			alert('Os dados do eixo X s�o de preenchimento Obrigat�rio')
			return false
		}
		nomes=tipe<2?gg.nx.value+':':''
		valores=gg.vx.value+':'
		branco=false
		for(ii=0;ii<gg.itens.value;ii++){
			nomv=document.graficos['ny['+ii+']'].value.replace(',','.')
			valv=document.graficos['vy['+ii+']'].value.replace(',','.')
			nomes=nomes+nomv+';'
			valores=valores+valv+';'
			if (valv==''||nomv==''||numero(valv)) {
				alert('Todos os dados devem ser preenchidos')
				return false
			}
			document.graficos['ny['+ii+']'].disabled=true
			document.graficos['vy['+ii+']'].value=''
		}
		nomes=nomes.substring(0,nomes.length-1)
		valores=valores.substring(0,valores.length-1)
		texty=gg.texto.checked==true?0:1
		gg.nx.disabled=true
		gg.vx.value=''
		valosg=valosg+'|'+valores		
		valores=valosg.substring(0,1)=='|'?valosg.substring(1):valosg
		grafico_d()
	}
	if (tipe==2) {
		valores=''
		for(ii=0;ii<gg.itens.value;ii++){
			nomv=document.graficos['ny['+ii+']'].value.replace(',','.')
			valv=document.graficos['vy['+ii+']'].value.replace(',','.')
			valores=valores+nomv+':'+valv+','
			if (valv==''||nomv==''||numero(valv)) {
				alert('Todos os dados devem ser preenchidos, os valores devem ser num�ricos')
				return false
			}
			document.graficos['ny['+ii+']'].disabled=true
			document.graficos['vy['+ii+']'].disabled=true
		}		
		valores=valores.substring(0,valores.length-1)
		valosg=(valosg+'|'+valores).substr(1)
		valores=valosg
		gg.adic.disabled=true
		pizza('visugraf',150,valores,gg.titulo.value)		
	}
	gg.titulo.disabled=true
	gg.itens.disabled=true
	gg.texto.disabled=true
}
function grafico_r(tii){
	gg=document.graficos
	if (tii==2){		
		valosg=''
		visugraf.innerHTML=''
		gg.itens.disabled=false
		grafico_c(gg.itens.value)
		gg.adic.disabled=false
	}
}
function grafico_d(vre){
	//reescrever fun��o com explode dos dados e remover itens n�o exibidos no for ... atualizar camada com dados
	valosg=valosg.substring(0,1)=='|'?valosg.substring(1):valosg
	a_valor=valosg.split('|')
	ret=''
	valosg=''
	kk='cor2'
	for(xis in a_valor){
		l_c=a_valor[xis].split(':')
		if(a_valor[xis]==vre)continue
		vari=a_valor[xis].replace(':','</a></td><td>')
		while(vari.match(';')) vari=vari.replace(';','</td><td>')
		vari="<tr class="+(kk=kk=='cor1'?'cor2':'cor1')+"><td><a href=\"javascript:grafico_d('"+a_valor[xis]+"')\">"+vari+"</td></tr>"
		ret=ret+vari
		valosg=valosg+a_valor[xis]+'|'
	}
	remos.innerHTML='<b>Remover Itens</b><br><table border=1 cellspacing=0 bordercolor=whitesmoke>'+ret+'</table>'
	valosg=valosg.substring(0,valosg.length-1)
	gg=document.graficos
	if (valosg){
		if (tipe<2)	barro('visugraf',gg.titulo.value,gg.limiar.value,nomes,valosg,tipe,(gg.texto.checked==true?0:1))
		if (tipe==3) {
			linhas('visugraf',gg.limiar.value,nomes,valosg,gg.titulo.value,(gg.texto.checked==true?0:1))
		}
	} else {
		visugraf.innerHTML=''
	}
	//fazer fun��o de atualiza��o dos itens removidos
}
function salver(){
	gg=document.graficos
	if (!valosg) {
		alert('Voc� deve adicionar ao menos um item para salvar as altera��es')
		return false
	}
	if (tipe=='pizza'){
		gg.vals.value=gg.gp.value+'�2�0�'+gg.itens.value+'�0�'+gg.titulo.value+'��'+valores
	} else {
		gg.vals.value=gg.gp.value+'�'+tipe+'�'+(gg.texto.checked==true?'0':'1')+'�'+gg.itens.value+'�'+gg.limiar.value+'�'+gg.titulo.value+'�'+nomes+'�'+ valosg
	}
	gg.submit()
}