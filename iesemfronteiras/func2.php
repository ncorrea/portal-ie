<?php
error_reporting(E_ALL ^ E_NOTICE);
require_once("phpmailer/class.phpmailer.php");

$cidadeEstado = array("1" => "1", "2" => "2",
"3" => "123",
"4" => "20",
"5" => "6",
"6" => "4",
"11" => "15",
"12" => "121",
"13" => "83",
"15" => "124",
"17" => "11",
"18" => "22",
"19" => "120",
"20" => "80",
"22" => "10",
"26" => "9",
"30" => "19",
"31" => "81",
"27" => "1", "16" => "1", "24" => "1", "14" => "1", "23" => "1", "18" => "1", "21" => "1", "28" => "1", "25" => "1", "29" => "1");

$franquia = array("fortaleza@ieintercambio.com.br" => "10",
"brasilia@ieintercambio.com.br" => "11",
"manaus@ieintercambio.com.br" => "8",
"goiania@ieintercambio.com.br" => "12",
"matriz@ieintercambio.com.br" => "5",
"bh@ieintercambio.com.br" => "14",
"juizdefora@ieintercambio.com.br" => "15",
"uberlandia@ieintercambio.com.br" => "16",
"varginha@ieintercambio.com.br" => "17",
"campogrande@ieintercambio.com.br" => "18",
"joaopessoa@ieintercambio.com.br" => "19",
"recife@ieintercambio.com.br" => "20",
"curitiba@ieintercambio.com.br" => "21",
"cabofrio@ieintercambio.com.br" => "23",
"barra@ieintercambio.com.br" => "22",
"rjcentro@ieintercambio.com.br" => "25",
"macae@ieintercambio.com.br" => "27",
"niteroi@ieintercambio.com.br" => "28",
"petropolis@ieintercambio.com.br" => "29",
"natal@ieintercambio.com.br" => "30",
"caxiasdosul@ieintercambio.com.br" => "31",
"passofundo@ieintercambio.com.br" => "32",
"portoalegre@ieintercambio.com.br" => "33",
"florianopolis@ieintercambio.com.br" => "34",
"aracaju@ieintercambio.com.br" => "35",
"bauru@ieintercambio.com.br" => "36",
"brooklin@ieintercambio.com.br" => "37",
"campinas@ieintercambio.com.br" => "38",
"higienopolis@ieintercambio.com.br" => "39",
"morumbi@ieintercambio.com.br" => "40",
"paraiso@ieintercambio.com.br" => "41",
"piracicaba@ieintercambio.com.br" => "42",
"santana@ieintercambio.com.br" => "43",
"sbc@ieintercambio.com.br" => "44",
"saocarlos@ieintercambio.com.br" => "45",
"sjc@ieintercambio.com.br" => "46",
"sorocaba@ieintercambio.com.br" => "47",
"vix@ieintercambio.com.br" => "3",
"salvador@ieintercambio.com.br" => "9",
"campos@ieintercambio.com.br" => "24",
"treinamento@ieintercambio.com.br" => "49",
"ipanema@ieintercambio.com.br" => "26",
"fabiane.brandao@afs.org" => "50",
"saoluis@ieintercambio.com.br" => "13",
"taubate@ieintercambio.com.br" => "48");

$stateFranchise = array("fortaleza@ieintercambio.com.br" => "20",
"brasilia@ieintercambio.com.br" => "31",
"manaus@ieintercambio.com.br" => "26",
"goiania@ieintercambio.com.br" => "30",
"matriz@ieintercambio.com.br" => "5",
"bh@ieintercambio.com.br" => "4",
"juizdefora@ieintercambio.com.br" => "4",
"uberlandia@ieintercambio.com.br" => "4",
"varginha@ieintercambio.com.br" => "4",
"campogrande@ieintercambio.com.br" => "13",
"joaopessoa@ieintercambio.com.br" => "18",
"recife@ieintercambio.com.br" => "17",
"curitiba@ieintercambio.com.br" => "6",
"cabofrio@ieintercambio.com.br" => "1",
"barra@ieintercambio.com.br" => "1",
"rjcentro@ieintercambio.com.br" => "1",
"macae@ieintercambio.com.br" => "1",
"niteroi@ieintercambio.com.br" => "1",
"petropolis@ieintercambio.com.br" => "1",
"natal@ieintercambio.com.br" => "19",
"caxiasdosul@ieintercambio.com.br" => "12",
"passofundo@ieintercambio.com.br" => "12",
"portoalegre@ieintercambio.com.br" => "12",
"florianopolis@ieintercambio.com.br" => "3",
"aracaju@ieintercambio.com.br" => "15",
"bauru@ieintercambio.com.br" => "2",
"brooklin@ieintercambio.com.br" => "2",
"campinas@ieintercambio.com.br" => "2",
"higienopolis@ieintercambio.com.br" => "2",
"morumbi@ieintercambio.com.br" => "2",
"paraiso@ieintercambio.com.br" => "2",
"piracicaba@ieintercambio.com.br" => "2",
"santana@ieintercambio.com.br" => "2",
"sbc@ieintercambio.com.br" => "2",
"saocarlos@ieintercambio.com.br" => "2",
"sjc@ieintercambio.com.br" => "2",
"sorocaba@ieintercambio.com.br" => "2",
"vix@ieintercambio.com.br" => "5",
"salvador@ieintercambio.com.br" => "11",
"campos@ieintercambio.com.br" => "1",
"treinamento@ieintercambio.com.br" => "5",
"ipanema@ieintercambio.com.br" => "1",
"fabiane.brandao@afs.org" => "1",
"saoluis@ieintercambio.com.br" => "22",
"taubate@ieintercambio.com.br" => "2");
      
function enviaPost($url, $fields) {
	//echo "Os parametros sao : " . $fields . "<br>";
	
	$fields_string = http_build_query($fields);

    //open connection
    $ch = curl_init();
	
    //set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    //execute post
    $result = curl_exec($ch);
    
    //close connection
    curl_close($ch);

    return $result;
}


function enviaMail($data){

	$mail = new PHPMailer();

	$mail->IsSMTP(); 
	$mail->Host = "smtp.gmail.com"; 
	$mail->SMTPAuth = true;
	$mail->SMTPSecure = "ssl";
	$mail->Port = 465;
	$mail->Username = "no-reply@ieintercambio.com.br";
	$mail->Password = "ie@VFnh2012"; 
	
	$mail->From = "no-reply@ieintercambio.com.br"; 
	$mail->FromName = "IE Intercambio";
	$mail->AddReplyTo( $data['email'] ,  $data['nome'] );
	
	//$mail->AddAddress('helpdesk@ieintercambio.com.br', 'Help');
	$mail->AddAddress($data['emailfranquia']);
	//$mail->AddBCC('eboni@ieintercambio.com.br', 'Copia Oculta');
	
	$mail->IsHTML(true); 
	$mail->CharSet = 'utf-8';
	
	$mail->Subject  = "Prospect from Ciência sem Fronteiras IE"; 
	
	$produtoNome = array("2" => "Curso de Idiomas", "4" => "Trainee", "6" => "Trip Experience", "10" => "IE Tax Service", "5" => "High School", "3" => "Work Experience IE", 
"11" => "Bus Pass", "1" => "Au Pair", "9" => "Seguro IAC", "12" => "Visa Travel Money");
	$estado = array("27" => "Acre",
"16" => "Alagoas",
"24" => "Amapá",
"26" => "Amazonas",
"11" => "Bahia",
"20" => "Ceará",
"31" => "Distrito Federal",
"5" => "Espírito Santo",
"30" => "Goiás",
"22" => "Maranhão",
"14" => "Mato Grosso",
"13" => "Mato Grosso do Sul",
"4" => "Minas Gerais",
"23" => "Pará",
"18" => "Paraíba",
"6" => "Paraná",
"17" => "Pernambuco",
"21" => "Piauí",
"1" => "Rio de Janeiro",
"19" => "Rio Grande do Norte",
"12" => "Rio Grande do Sul",
"28" => "Rondônia",
"25" => "Roraima",
"3" => "Santa Catarina",
"2" => "São Paulo",
"15" => "Sergipe",
"29" => "Tocantins");

	$conteudo = "Prezado(a), segue um pedido de cotação enviado através do Ciência sem Fronteiras IE: <br/><br/>";
	$conteudo.= 'Nome:  ' . $data['nome'] . "<br/>E-mail: " . $data['email'] . "<br/>Telefone: (" . $data['ddd'] .") " . $data['telefone'] ;
	$conteudo.= "<br/>Origem: ". $data['origem'] . "<br/>Destino: " .$data['destino'];
	$conteudo.= "<br/>Data Saída: ". $data['data_saida'] . "<br/>Data Chegada: " .$data['data_chegada'];
	$conteudo.= "<br/>Agência: " . $data['emailfranquia'];
	$conteudo.= "<br/>Estado: " . $estado[$data['estado']];
	$conteudo.= "<br/><br/>Comentários: <br/>" . $data['comentario'];
	
	$mail->Body = $conteudo;
	
	$enviado = $mail->Send();
	
	$mail->ClearAllRecipients();

	/*if ($enviado) {
		echo "<span class=\"yes\">E-mail enviado com sucesso!</span>";
	} else {
		echo "<span class=\"no\">Não foi possível enviar o e-mail. </span>";
		//echo "Informações do erro: " . $mail->ErrorInfo;
	}*/

}     

?>