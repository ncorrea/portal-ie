<?php require_once("func.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ciência Sem Fronteiras IE</title>


<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<!--[if IE 8]>
			<link rel="stylesheet" type="text/css" media="screen" href="css/ie8-hacks.css" />
		<![endif]-->
		<!-- ENDS CSS -->	
		
		<!-- GOOGLE FONTS 
		<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>-->
		
		<!-- JS -->
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!--[if IE]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<!--[if IE 6]>
			<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
			<script>
	      		/* EXAMPLE */
	      		//DD_belatedPNG.fix('*');
	    	</script>
		<![endif]-->
		<!-- ENDS JS -->
		
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
        
        
        
        <script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<link href="errors.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

$().ready(function() {
	// validate signup form on keyup and submit
	$("#signupForm").validate({
		rules: {
			nome: { required: true, minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			comments:{  required: true, minlength: 4
			}
		},
		messages: {
			nome: "Por favor, insira seu nome",
			email: "Por favor, insira um e-mail válido",
			ddd: "Por favor, insira o DDD",
			telefone: "Por favor, insira seu telefone",
			comments: "Por favor, insira seu comentário"
		}
	});
});
</script>

</head>

<link rel="stylesheet" media="all" href="css/estilo.css"/>
    <body>
    
	<div id="tudo">
    
        <div id="topo">
            <a href="index.php"><img src="images/topo.jpg"></a>
        </div>
        
        <div id="webdoor">
        	<img src="images/webdoor02.jpg">
        </div>
        
        <a href="passagem.php"><div id="menu01"><span class="menu">Passagem Aérea</span></div></a>
        <a href="seguro.php"><div id="menu02"><span class="menu">Assistência de viagem IAC</span></div></a>
        <a href="cartao.php"><div id="menu03"><span class="menu">Cartão VTM</span></div></a>
        <a href="http://site.ieintercambio.com.br/conhecer/mochilao/programa" target="_blank"><div id="menu04"><span class="menu">Trip Experience</span></div></a>
        <a href="http://www.ieintercambio.com.br/promocoes" target="_blank"><div id="menu05"><span class="menu">Curso de idioma</span></div></a>
        <a href="bolsa.php"><div id="menu06"><span class="menu">Bolsa</span></div></a>
        
        <div id="conteudo">
        	<br /><span class="titulo2">Cartão Visa Travel Money</span><br /><br />
            <span class="texto2">
            A IE oferece gratuitamente para você, bolsista do Programa Ciência sem Fronteiras, o Cartão Visa Travel Money IE. Aceito em mais de 30 milhões de estabelecimentos e 1.8 milhões de Caixa Automáticos distribuídos por mais de 200 países, o Cartão Visa Travel Money IE é um cartão pré-pago internacional indispensável para quem viaja ao exterior.<br />
Com o Cartão Visa Travel Money IE você realiza saques e compras sempre na moeda local do país que estiver e com o câmbio do momento da compra -  garantindo conveniência, segurança e maior controle das suas despesas. Além da conveniência, você economiza 6% em todas as compras que realizar no exterior, já que o imposto cobrado sobre esta modalidade de cartão é 6% menor do que um cartão de crédito convencional.<br /><br />

Moedas Disponíveis: Dólar, Euro, Libra, Dólar Australiano e Peso Argentino
            <br /><br />
            </span>
        </div>
        
        <div id="formulario2">
        <!-- column (left)-->
						
							<!-- form -->
							<h2>Contato</h2>
							<script type="text/javascript" src="js/form-validation.js"></script>
							<form action="http://www.ieintercambio.com.br/iesemfronteiras/cartao.php" name="formulario" id="contactForm" method="post" enctype="multipart/form-data">
                            <?php
								if (isset($_POST['enviar']) && $_POST['enviar'] == 'send') {
									$agencia = $_POST['agencia'];
									$estado = $stateFranchise[$agencia];
									
									$data['nome'] = $_POST["nome"];
									$data['sobrenome'] = '';
									$data['data_nascimento'] = '';
									$data['email'] = $_POST["email"];
									$data['ddd'] = $_POST["ddd"];
									$data['telefone'] = $_POST["telefone"];
									$data['ddd2'] = '';
									$data['telefone2'] = '';		
									$data['cidade'] = $cidadeEstado[$estado];
									$data['estado'] = $estado;		
									$data['agencia'] = $franquia[$agencia];
									$data['interesse'] =  '6'; //Facebook
									$data['produto'] = array('2');
									$data['travel_quantity'] = '0';
									$data['sexo'] = '1';
									$data['comentario'] = $_POST["comments"];
									$data['emailfranquia'] = $_POST["agencia"];
								
									 if(empty($data['nome'])) {
										 $retorno = '<span style="color:#f00000"><br>Informe seu nome</span>';
									 }elseif (empty($data['email'])) {
										 $retorno = '<span style="color:#f00000"><br>Informe seu e-mail</span>';
									 }elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
										 $retorno = '<span style="color:#f00000"><br>Informe um e-mail válido</span>';
									 }elseif (empty($data['ddd'])) {
										 $retorno = '<span style="color:#f00000"><br>Digite o ddd</span>';
									 }elseif (empty($data['telefone'])) {
										 $retorno = '<span style="color:#f00000"><br>Digite o telefone</span>';
									 }elseif (empty($data['comentario'])) {
										 $retorno = '<span style="color:#f00000"><br>Digite o comentário</span>';
									 }if (empty($retorno)) {
											//Faz o envio do e-mail e insere no webservice
											$data['comentario'] ="Produto de interesse: Intercambio de ferias Teen<br/> " . $_POST["comments"];
											enviaMail($data);
											$saida = WebServiceSpecta($data);
											//Faz o unset para limpar o formulário
											unset($data);
											echo "<span class=\"textoform\">Formulário enviado com sucesso!</span>";
									 } else {
										 echo "<span class=\"textoform\">$retorno</span>";
									 }
								}
								?>
								<fieldset>
									<div>
										<label>Nome</label>
										<input name="nome" id="nome" type="text" class="form-poshytip" title="Entre com seu nome" />
									</div>
									<div>
										<label>E-mail</label>
										<input name="email" id="email" type="text" class="form-poshytip" title="Entre com seu e-mail" />
									</div>
                                    <div>
										<label>Telefone</label>
                                        <input name="ddd" id="ddd" type="text" class="form-poshytip" title="Entre com seu DDD" style="width:30px;" />
										<input name="telefone" id="telefone" type="text" class="form-poshytip" title="Entre com seu telefone" style="width:216px;" />
                                        
									</div>
                                    <div>
                                    	<label>Agencia de preferência</label>
                                        <select id="agencia" name="agencia" class="form-poshytip">
                                        <option value="" selected="selected">- Selecione -</option>
                                        <option value="aracaju@ieintercambio.com.br">IE SE Aracaju</option>
                                        <option value="barra@ieintercambio.com.br">IE RJ Barra</option>
                                        <option value="bauru@ieintercambio.com.br">IE SP Bauru</option>
                                        <option value="bh@ieintercambio.com.br">IE MG Belo Horizonte</option>
                                        <option value="brasilia@ieintercambio.com.br">IE DF Brasília</option>
                                        <option value="cabofrio@ieintercambio.com.br">IE RJ Cabo Frio</option>
                                        <option value="campinas@ieintercambio.com.br">IE SP Campinas</option>
                                        <option value="campogrande@ieintercambio.com.br">IE MS Campo Grande</option>
                                        <option value="campos@ieintercambio.com.br">IE RJ Campos</option>
                                        <option value="caxiasdosul@ieintercambio.com.br">IE RS Caxias do Sul</option>
                                        <option value="florianopolis@ieintercambio.com.br">IE SC Florianópolis</option>
                                        <option value="fortaleza@ieintercambio.com.br">IE CE Fortaleza</option>
                                        <option value="goiania@ieintercambio.com.br">IE GO Goiânia</option>
                                        <option value="higienopolis@ieintercambio.com.br">IE SP Higienópolis</option>
                                        <option value="ipanema@ieintercambio.com.br">IE RJ Ipanema</option>
                                        <option value="joaopessoa@ieintercambio.com.br">IE PB João Pessoa</option>
                                        <option value="juizdefora@ieintercambio.com.br">IE MG Juiz de Fora</option>
                                        <option value="macae@ieintercambio.com.br">IE RJ Macaé</option>
                                        <option value="manaus@ieintercambio.com.br">IE AM Manaus</option>
                                        <option value="morumbi@ieintercambio.com.br">IE SP Morumbi</option>
                                        <option value="natal@ieintercambio.com.br">IE Natal</option>
                                        <option value="niteroi@ieintercambio.com.br">IE RJ Niterói</option>
                                        <option value="paraiso@ieintercambio.com.br">IE SP Paraíso</option>
                                        <option value="petropolis@ieintercambio.com.br">IE RJ Petrópolis</option>
                                        <option value="piracicaba@ieintercambio.com.br">IE SP Piracicaba</option>
                                        <option value="portoalegre@ieintercambio.com.br">IE RS Porto Alegre</option>
                                        <option value="recife@ieintercambio.com.br">IE PE Recife</option>
                                        <option value="rjcentro@ieintercambio.com.br">IE RJ Centro</option>
                                        <option value="salvador@ieintercambio.com.br">IE BA Salvador</option>
                                        <option value="santana@ieintercambio.com.br">IE SP Santana</option>
                                        <option value="saocarlos@ieintercambio.com.br">IE SP São Carlos</option>
                                        <option value="saoluis@ieintercambio.com.br">IE MA São Luís</option>
                                        <option value="sbc@ieintercambio.com.br">IE SP São Bernardo do Campo</option>
                                        <option value="sjc@ieintercambio.com.br">IE SP São José dos Campos</option>
                                        <option value="sorocaba@ieintercambio.com.br">IE SP Sorocaba</option>
                                        <option value="taubate@ieintercambio.com.br">IE SP Taubaté</option>
                                        <option value="uberlandia@ieintercambio.com.br">IE MG Uberlândia</option>
                                        <option value="varginha@ieintercambio.com.br">IE MG Varginha</option>
                                        <option value="vix@ieintercambio.com.br">IE ES Vitória</option>
                                        </select>
                                    </div>

									
									<div>
										<label>Mensagem</label>
										<textarea  name="comments"  id="comments" rows="5" cols="20" class="form-poshytip" title="Entre com sua mensagem"></textarea>
									</div>
									<p>                                    
                                    <input type="hidden" name="enviar" value="send" />
    								<input type="submit" name="Enviar" value="Enviar" id="submit"/>
                                    </p>
								</fieldset>
								
							</form>
							
						
						<!-- ENDS column -->
        </div>
        
        <div id="base"><img src="images/base.jpg" usemap="#Map" border="0" />
          <map name="Map" id="Map">
            <area shape="rect" coords="660,14,773,118" href="http://www.ieintercambio.com.br" target="_blank" />
            <area shape="rect" coords="13,91,230,114" href="http://www.ieintercambio.com.br" target="_blank" />
            <area shape="rect" coords="301,15,332,46" href="https://market.android.com/details?id=com.magtab.IEIntrcambio" target="_blank" />
            <area shape="rect" coords="263,16,294,47" href="https://itunes.apple.com/br/app/revista-ie-intercambio/id662152267?mt=8" target="_blank" />
            <area shape="rect" coords="225,17,256,48" href="https://www.facebook.com/IEintercambio" target="_blank" />
            <area shape="rect" coords="189,16,220,47" href="https://plus.google.com/116944547482760833313/posts" target="_blank" />
            <area shape="rect" coords="153,16,184,47" href="http://www.youtube.com/ieintercambio" target="_blank" />
            <area shape="rect" coords="118,15,149,46" href="https://www.facebook.com/IEintercambio/app_168188869963563" target="_blank" />
            <area shape="rect" coords="83,15,114,46" href="http://pinterest.com/ieintercambio/" target="_blank" />
            <area shape="rect" coords="49,17,80,48" href="https://twitter.com/#!/ieintercambio" target="_blank" />
            <area shape="rect" coords="13,16,44,47" href="http://blog.ieintercambio.com.br" target="_blank" />
          </map>
        </div>
              
    </div>
    
    </body>
    
    
</html>