<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ciência Sem Fronteiras IE</title>


<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<!--[if IE 8]>
			<link rel="stylesheet" type="text/css" media="screen" href="css/ie8-hacks.css" />
		<![endif]-->
		<!-- ENDS CSS -->	
		
		<!-- GOOGLE FONTS 
		<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>-->
		
		<!-- JS -->
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!--[if IE]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<!--[if IE 6]>
			<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
			<script>
	      		/* EXAMPLE */
	      		//DD_belatedPNG.fix('*');
	    	</script>
		<![endif]-->
		<!-- ENDS JS -->
		
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
        
        <script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-3881937-1']);
		  _gaq.push(['_setDomainName', 'ieintercambio.com.br']);
		  _gaq.push(['_setCampNameKey', 'IE Sem Fronteiras']);
		  _gaq.push(['_setCampSourceKey', 'Landing Page']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>

</head>

<link rel="stylesheet" media="all" href="css/estilo.css"/>
    <body>
    
	<div id="tudo">
    
        <div id="topo">
            <a href="index.php"><img src="images/topo.jpg"></a>
        </div>
        
        <div id="webdoor">
        	<img src="images/webdoor.jpg">
        </div>
        
       
		<a href="passagem.php">
        <div id="caixa01">
            <span class="titulo">Passagem Aérea</span><br /><br />
            <span class="texto">Confira nossas tarifas para estudantes bolsistas.</span>
            <div id="saiba01"><img src="images/saiba01.png" /> </div>
        </div>
        </a>
       	
        <a href="seguro.php">
        <div id="caixa02">
        	<span class="titulo">Assistência de viagem IAC</span><br />
			<span class="texto">20% de desconto para bolsistas</span>
            <div id="saiba02"><img src="images/saiba02.png" /> </div>
        </div>
        </a>
        
        <a href="cartao.php">
        <div id="caixa03">
        	<span class="titulo">Cartão VTM</span><br /><br />
			<span class="texto">Você viaja mais tranquilo em posse do seu VTM!</span>
            <div id="saiba03"><img src="images/saiba03.png" /> </div>
        </div>
        </a>
        
        <a href="http://site.ieintercambio.com.br/conhecer/mochilao/programa" target="_blank">
        <div id="caixa04">
        	<span class="titulo">Trip Experience</span><br />
			<span class="texto">Porque não tornar a viagem ainda mais completa, aproveitando o máximo sua estadia no exterior ?</span>
            <div id="saiba04"><img src="images/saiba04.png" /> </div>
        </div>
        </a>
        
        <a href="http://www.ieintercambio.com.br/promocoes" target="_blank">
        <div id="webdoor2">
        	<img src="images/webdoor2.jpg">
        </div>
        </a>
        
        <a href="http://www.ieintercambio.com.br/promocoes" target="_blank">
        <div id="caixa05">
            <img src="images/caixa05.jpg" />
        </div>
        </a>
        
        <a href="bolsa.php">
        <div id="caixa06">
        	<span class="titulo">Bolsa 50%</span><br />
			<span class="texto"><br /><br />Conheça as opções de bolsa de estudo que a IE traz para você</span>
            <div id="saiba06"><img src="images/saiba06.png" /> </div>
        </div>
        </a>
        
        <div id="base">
        	<img src="images/base.jpg" usemap="#Map" border="0" />
            <map name="Map" id="Map"><area shape="rect" coords="301,17,331,48" href="https://market.android.com/details?id=com.magtab.IEIntrcambio" target="_blank" /><area shape="rect" coords="14,90,231,113" href="http://www.ieintercambio.com.br" target="_blank" /><area shape="rect" coords="657,8,772,113" href="http://www.ieintercambio.com.br" target="_blank" /><area shape="rect" coords="263,17,293,48" href="https://itunes.apple.com/br/app/revista-ie-intercambio/id662152267?mt=8" target="_blank" /><area shape="rect" coords="225,17,255,48" href="https://www.facebook.com/IEintercambio" target="_blank" /><area shape="rect" coords="189,16,219,47" href="https://plus.google.com/116944547482760833313/posts" target="_blank" /><area shape="rect" coords="154,16,184,47" href="http://www.youtube.com/ieintercambio" target="_blank" /><area shape="rect" coords="118,16,148,47" href="https://www.facebook.com/IEintercambio/app_168188869963563" target="_blank" /><area shape="rect" coords="84,15,114,46" href="http://pinterest.com/ieintercambio/" target="_blank" /><area shape="rect" coords="48,17,78,48" href="https://twitter.com/#!/ieintercambio" target="_blank" />
              <area shape="rect" coords="13,16,43,47" href="http://blog.ieintercambio.com.br" target="_blank" />
            </map>
        </div>
              
    </div>
    
    </body>
    
    
</html>