<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ciência Sem Fronteiras IE</title>


<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<!--[if IE 8]>
			<link rel="stylesheet" type="text/css" media="screen" href="css/ie8-hacks.css" />
		<![endif]-->
		<!-- ENDS CSS -->	
		
		<!-- GOOGLE FONTS 
		<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>-->
		
		<!-- JS -->
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!--[if IE]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<!--[if IE 6]>
			<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
			<script>
	      		/* EXAMPLE */
	      		//DD_belatedPNG.fix('*');
	    	</script>
		<![endif]-->
		<!-- ENDS JS -->
		
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
        
        <link href="shadowbox/shadowbox.css" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript" src="shadowbox/shadowbox.js"></script>
        
        <script type="text/javascript">
        
         Shadowbox.init({
        
         language: 'pt',
        
         player: ['img', 'html', 'swf']
        
         });
        
        </script>

</head>

<link rel="stylesheet" media="all" href="css/estilo.css"/>
    <body>
    
	<div id="tudo">
    
        <div id="topo">
            <a href="index.php"><img src="images/topo.jpg"></a>
        </div>
        
        <div id="webdoor">
        	<img src="images/webdoor03.jpg">
        </div>
        
        <a href="passagem.php"><div id="menu01"><span class="menu">Passagem Aérea</span></div></a>
        <a href="seguro.php"><div id="menu02"><span class="menu">Assistência de viagem IAC</span></div></a>
        <a href="cartao.php"><div id="menu03"><span class="menu">Cartão VTM</span></div></a>
        <a href="http://site.ieintercambio.com.br/conhecer/mochilao/programa" target="_blank"><div id="menu04"><span class="menu">Trip Experience</span></div></a>
        <a href="http://www.ieintercambio.com.br/promocoes" target="_blank"><div id="menu05"><span class="menu">Curso de idioma</span></div></a>
        <a href="bolsa.php"><div id="menu06"><span class="menu">Bolsa</span></div></a>
        
        <div id="conteudo_passagem">
        	<br /><span class="titulo2">Passagem Aérea</span><br /><br />
            
            <span class="texto2"> </span><br />
            
            <div id="content">
            
            <ul id="portfolio-filter" class="filter">
							<li>Filtro</li>
							<li><a href="#" data-filter="*" > Todos </a></li>
				    		<li><a href="#" data-filter=".europa" > Europa </a></li>
				    		<li><a href="#" data-filter=".america" > América </a></li>
				    		<li><a href="#" data-filter=".oceania" > Oceania / Ásia </a></li>
				    	</ul>
						<!-- ENDS filter -->
						<br /><br />
		
						<!-- gallery-->
						<ul id="portfolio-list" class="gallery">
							<li class="europa"><a href="form.php" rel="shadowbox" title="Inglaterra"><img src="images/pass01.jpg" alt="Pic"></a></li>
							<li class="europa"><a href="form.php" rel="shadowbox" title="Alemanha"><img src="images/pass02.jpg" alt="Pic"></a></li>
							<li class="europa"><a href="form.php" rel="shadowbox" title="França"><img src="images/pass03.jpg" alt="Pic"></a></li>
							<li class="europa"><a href="form.php" rel="shadowbox"  title="Espanha"><img src="images/pass04.jpg" alt="Pic"></a></li>
							<li class="america"><a href="form.php" rel="shadowbox" title="Estados Unidos"><img src="images/pass05.jpg" alt="Pic"></a></li>
							<li class="america"><a href="form.php" rel="shadowbox" title="Estados Unidos"><img src="images/pass06.jpg" alt="Pic"></a></li>
							<li class="america"><a href="form.php" rel="shadowbox" title="Estados Unidos"><img src="images/pass07.jpg" alt="Pic"></a></li>
							<li class="america"><a href="form.php" rel="shadowbox" title="Canadá"><img src="images/pass08.jpg" alt="Pic"></a></li>
							<li class="america"><a href="form.php" rel="shadowbox" title="Canadá"><img src="images/pass09.jpg" alt="Pic"></a></li>
							<li class="oceania"><a href="form.php" rel="shadowbox" title="Austrália"><img src="images/pass10.jpg" alt="Pic"></a></li>
							<li class="oceania"><a href="form.php" rel="shadowbox" title="Austrália"><img src="images/pass11.jpg" alt="Pic"></a></li>
							<li class="oceania"><a href="form.php" rel="shadowbox" title="China"><img src="images/pass12.jpg" alt="Pic"></a></li>
                            <li class="oceania"><a href="form.php" rel="shadowbox" title="Japão"><img src="images/pass13.jpg" alt="Pic"></a></li>
						</ul>
            	</div>
            
            <br /><br />
            
            <span style="font-size:12px;">*Com saída de São Paulo. Não inclui taxas de embarque. As tarifas podem sofrer alteração e embarques são em baixa temporada para tarifa de 6 meses. Valores em reais calculados sobre o câmbio turismo do dia 18/09/2013, quando USD = 2,35.</span>
            
        </div>
        
        <div id="base">
        	<img src="images/base.jpg" usemap="#Map" border="0" />
            <map name="Map" id="Map"><area shape="rect" coords="661,8,771,114" href="http://www.ieintercambio.com.br" target="_blank" /><area shape="rect" coords="12,86,227,114" href="http://www.ieintercambio.com.br" target="_blank" /><area shape="rect" coords="302,15,331,46" href="https://market.android.com/details?id=com.magtab.IEIntrcambio" target="_blank" /><area shape="rect" coords="263,16,293,46" href="https://itunes.apple.com/br/app/revista-ie-intercambio/id662152267?mt=8" target="_blank" /><area shape="rect" coords="225,15,254,46" href="https://www.facebook.com/IEintercambio" target="_blank" /><area shape="rect" coords="189,16,219,46" href="https://plus.google.com/116944547482760833313/posts" target="_blank" /><area shape="rect" coords="153,16,184,48" href="http://www.youtube.com/ieintercambio" target="_blank" /><area shape="rect" coords="119,16,150,48" href="https://www.facebook.com/IEintercambio/app_168188869963563" target="_blank" /><area shape="rect" coords="83,16,114,46" href="http://pinterest.com/ieintercambio/" target="_blank" /><area shape="rect" coords="49,15,78,46" href="https://twitter.com/#!/ieintercambio" target="_blank" />
              <area shape="rect" coords="13,16,43,46" href="http://blog.ieintercambio.com.br" target="_blank" />
            </map>
        </div>
              
    </div>
    
    </body>

</html>